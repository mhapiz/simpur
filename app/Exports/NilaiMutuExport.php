<?php

namespace App\Exports;

use App\Models\NilaiMutu;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class NilaiMutuExport implements FromView, ShouldAutoSize
{
    /**
     * @return \Illuminate\Support\Collection
     */

    protected $filter;

    public function __construct($filter)
    {
        $this->filter = $filter;
    }


    public function view(): View
    {
        if ($this->filter != null) {
            $nm = NilaiMutu::with('mahasiswa')->whereYear('updated_at', '=', date('Y', strtotime($this->filter)))
                ->whereMonth('updated_at', '=', date('m', strtotime($this->filter)))
                ->get();
            return view('export.nilaimutu', [
                'nm' => $nm,
            ]);
        } else {
            $nm = NilaiMutu::with('mahasiswa')->get();
            return view('export.nilaimutu', [
                'nm' => $nm,
            ]);
        }
    }
}
