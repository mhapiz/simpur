<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables as DataTables;
use App\Models\Dosen;
use Maatwebsite\Excel\Facades\Excel;
use RealRashid\SweetAlert\Facades\Alert as Alert;
use App\Imports\DosenImport;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class DosenManageController extends Controller
{
    public function index()
    {
        return view('pages.admin.user-manage.dosen.index');
    }

    public function ajaxGetDosen()
    {
        $dosens = Dosen::select(['id_dosen', 'nip', 'nama_dosen', 'jabatan']);

        return DataTables::eloquent($dosens)
            ->addIndexColumn()
            ->addColumn('aksi', function ($row) {
                $editUrl = route('admin.user-manage.dosen.edit', $row->id_dosen);
                $deleteUrl = route('admin.user-manage.dosen.destroy', $row->id_dosen);
                return view('modules.backend._formActions', compact('editUrl', 'deleteUrl'));
            })
            ->rawColumns(['aksi'])
            ->make(true);
    }

    public function createDosen()
    {
        return view('pages.admin.user-manage.dosen.create');
    }

    public function storeDosen(Request $request)
    {
        $data = $request->validate(
            [
                'nip' => 'required|unique:dosens,nip',
                'nama_dosen' => 'required',
                'jabatan' => 'required',
            ],
            [
                'nip.required' => 'Kolom NIP Harus Diisi',
                'nip.unique' => 'NIP Sudah Terdaftar',
                'nama_dosen.required' => 'Kolom Nama Harus Diisi',
                'jabatan.required' => 'Kolom Jabatan Harus Diisi',
            ]
        );

        $data['updated_by'] = Auth::user()->identity;
        $dosenAdded = Dosen::create($data);

        User::create([
            'identity' => $dosenAdded->nip,
            'password' => Hash::make($dosenAdded->nip),
            'role' => 'DOSEN',
            'dosen_id' => $dosenAdded->id_dosen,
            'updated_by' => Auth::user()->identity,
        ]);

        Alert::success('Data Berhasil Ditambahkan', 'Dosen ' . $dosenAdded->nama_dosen . ' Bisa Login Menggunakan NIP');
        return redirect()->route('admin.user-manage.dosen.index');
    }


    public function addManyDosen()
    {
        return view('pages.admin.user-manage.dosen.addMany');
    }

    public function storeManyDosen(Request $request)
    {
        $request->validate(
            [
                'file_input' => 'required|mimes:xl,xls,xlsx|max:2048',
            ],
            [
                'file_input.required' => 'Harap Pilih File Excel Untuk Di upload',
                'file_input.mimes' => 'File Yang Diupload Harus berekstensi .xl, .xls, .xlsx',
                'file_input.max' => 'Ukuran File Yang Di Upload Harus Kurang Dari 2MB',
            ]
        );

        Excel::import(new DosenImport, $request->file('file_input'));

        Alert::success('Berhasil!', 'Data-Data Dosen Berhasil Ditambahkan');
        return redirect()->route('admin.user-manage.dosen.index');
    }

    public function editDosen($id)
    {
        $dosen = Dosen::find($id);
        return view('pages.admin.user-manage.dosen.edit', [
            'dosen' => $dosen
        ]);
    }

    public function updateDosen(Request $request, $id)
    {
        $theDosen = Dosen::find($id);
        $theUser = User::where('dosen_id', '=', $theDosen->id_dosen)->first();
        $data = $request->validate([
            'nama_dosen' => 'required',
            'jabatan' => 'required',

        ]);

        $theDosen['updated_by'] = Auth::user()->identity;

        $theDosen->update($data);

        if ($request->password != null) {
            $theUser->update([
                'password' => Hash::make($request->password),
                'updated_by' => Auth::user()->identity,
            ]);
        }

        Alert::success('Sukses', 'Data Dosen Berhasil Diperbarui');
        return redirect()->route('admin.user-manage.dosen.index');
    }

    public function destroyDosen($id)
    {
        die;
        $theDosen = Dosen::find($id);
        $theUser = User::where('dosen_id', '=', $theDosen->id_dosen)->first();

        $theDosen->delete();
        $theUser->delete();

        Alert::warning('Dihapus', 'Data Dosen Berhasil Dihapus');
        return redirect()->route('admin.user-manage.dosen.index');
    }
}
