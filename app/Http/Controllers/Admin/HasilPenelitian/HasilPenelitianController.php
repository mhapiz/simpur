<?php

namespace App\Http\Controllers\Admin\HasilPenelitian;

use App\Http\Controllers\Controller;
use App\Models\SeminarHasilPenelitian;
use App\Models\Mahasiswa;
use App\Models\Dosen;
use App\Models\NilaiMutu;
use App\Models\PenilaianPenyanggahSeminarHasilPenelitian;
use App\Models\PenilaianSeminarHasilPenelitian;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert as Alert;

class HasilPenelitianController extends Controller
{
    public function index()
    {
        if (request()->q !== null) {
            $q = request()->q;

            $dataSHP = SeminarHasilPenelitian::join('mahasiswas', 'seminar_hasil_penelitians.nim_seminaris', '=', 'mahasiswas.nim')
                ->join('nilai_mutus', 'seminar_hasil_penelitians.nim_seminaris', '=', 'nilai_mutus.nim')
                ->where([['seminar_hasil_penelitians.nim_seminaris', 'LIKE', '%' . $q . '%'], ['nilai_mutus.rerata_nilai_usulan_penelitian', '!=', null]])
                ->orWhere([['mahasiswas.nama_mahasiswa', 'LIKE', '%' . $q . '%'], ['nilai_mutus.rerata_nilai_usulan_penelitian', '!=', null]])
                ->select('*')
                ->orderBy('seminar_hasil_penelitians.created_at', 'DESC')
                ->paginate(6);

            return view('pages.admin.seminar-hasil.index', [
                'dataSHP' => $dataSHP
            ]);
        } else {

            $dataSHP = SeminarHasilPenelitian::join('nilai_mutus', 'seminar_hasil_penelitians.nim_seminaris', '=', 'nilai_mutus.nim')
                ->where('nilai_mutus.rerata_nilai_usulan_penelitian', '!=', null)
                ->select('*')
                ->orderBy('seminar_hasil_penelitians.created_at', 'DESC')
                ->paginate(6);

            return view('pages.admin.seminar-hasil.index', [
                'dataSHP' => $dataSHP
            ]);
        }
    }

    public function indexSudahDinilai()
    {
        if (request()->q !== null) {
            $q = request()->q;

            $dataSHP = SeminarHasilPenelitian::join('mahasiswas', 'seminar_hasil_penelitians.nim_seminaris', '=', 'mahasiswas.nim')
                ->join('nilai_mutus', 'seminar_hasil_penelitians.nim_seminaris', '=', 'nilai_mutus.nim')
                ->where([['seminar_hasil_penelitians.nim_seminaris', 'LIKE', '%' . $q . '%'], ['nilai_mutus.rerata_nilai_usulan_penelitian', '!=', null],  ['nilai_mutus.rerata_nilai_hasil_penelitian', '!=', null]])
                ->orWhere([['mahasiswas.nama_mahasiswa', 'LIKE', '%' . $q . '%'], ['nilai_mutus.rerata_nilai_usulan_penelitian', '!=', null],  ['nilai_mutus.rerata_nilai_hasil_penelitian', '!=', null]])
                ->select('*')
                ->orderBy('seminar_hasil_penelitians.created_at', 'DESC')
                ->paginate(6);

            return view('pages.admin.seminar-hasil.index', [
                'dataSHP' => $dataSHP
            ]);
        } else {

            $dataSHP = SeminarHasilPenelitian::join('nilai_mutus', 'seminar_hasil_penelitians.nim_seminaris', '=', 'nilai_mutus.nim')
                ->where([['nilai_mutus.rerata_nilai_usulan_penelitian', '!=', null], ['nilai_mutus.rerata_nilai_hasil_penelitian', '!=', null]])
                ->orderBy('seminar_hasil_penelitians.created_at', 'DESC')
                ->select('*')
                ->paginate(6);

            return view('pages.admin.seminar-hasil.index', [
                'dataSHP' => $dataSHP
            ]);
        }
    }

    public function show($id)
    {

        $shp = SeminarHasilPenelitian::find($id);

        if ($shp->penyanggah1 != null && $shp->penyanggah2 != null) {
            $pshp = PenilaianSeminarHasilPenelitian::where([['seminar_hasil_penelitian_id', '=', $id]])->with(['mahasiswa', 'shp', 'dosen'])->get();

            $ppshp = PenilaianPenyanggahSeminarHasilPenelitian::where([['seminar_hasil_penelitian_id', '=', $id]])->with(['mahasiswa', 'shp', 'dosen'])->get();

            return view('pages.admin.seminar-hasil.show', [
                'pshp' => $pshp,
                'ppshp' => $ppshp,
            ]);
        } else {
            Alert::warning('Gagal!', 'Penyanggah Belum Ditentukan');
            return redirect()->route('admin.hasil.index');
        }
    }

    public function print($id)
    {
        $shp = SeminarHasilPenelitian::find($id);
        $nilaishp = PenilaianSeminarHasilPenelitian::where([['seminar_hasil_penelitian_id', '=', $shp->id_seminar_hasil_penelitian]])->with(['mahasiswa', 'shp'])->get();
        $nilaiPenyanggahshp = PenilaianPenyanggahSeminarHasilPenelitian::where([['seminar_hasil_penelitian_id', '=', $shp->id_seminar_hasil_penelitian]])->with(['mahasiswa', 'shp'])->get();
        $namaMahasiswa = Mahasiswa::where('nim', '=', $shp->nim_seminaris)->first()->nama_mahasiswa;

        if ($shp->nilai_pembimbing1 != null && $shp->nilai_pembimbing2 != null && $shp->nilai_penyanggah1 != null && $shp->nilai_penyanggah2 != null) {
            if ($shp->tgl_seminar != null) {
                $pdf = PDF::loadview('print.shp.all', [
                    'nilaishp' => $nilaishp,
                    'nilaiPenyanggahshp' => $nilaiPenyanggahshp,
                ])->setPaper(array(0, 0, 609.4488, 935.433), 'portrait');

                return $pdf->download('Seminar Hasil Penelitian ' . $namaMahasiswa . ' .pdf');
                // return $pdf->stream();
            } else {
                Alert::error('Gagal!', 'Tanggal Belum Ditentukan');
                return redirect()->route('admin.hasil.index');
            }
        } else {
            Alert::error('Gagal!', 'Data Belum Lengkap');
            return redirect()->route('admin.hasil.index');
        }
    }

    public function printHasil($id)
    {
        $shp = SeminarHasilPenelitian::find($id);
        $nilaishp = PenilaianSeminarHasilPenelitian::where([['seminar_hasil_penelitian_id', '=', $shp->id_seminar_hasil_penelitian]])->with(['mahasiswa', 'shp'])->get();
        $namaMahasiswa = Mahasiswa::where('nim', '=', $shp->nim_seminaris)->first()->nama_mahasiswa;

        if ($shp->nilai_pembimbing1 != null && $shp->nilai_pembimbing2 != null && $shp->nilai_penyanggah1 != null && $shp->nilai_penyanggah2 != null) {
            if ($shp->tgl_seminar != null) {
                $pdf = PDF::loadview('print.shp.hasil', [
                    'nilaishp' => $nilaishp,
                ])->setPaper(array(0, 0, 609.4488, 935.433), 'portrait');

                return $pdf->download('Nilai Seminar Hasil Penelitian ' . $namaMahasiswa . ' .pdf');
                // return $pdf->stream();
            } else {
                Alert::error('Gagal!', 'Tanggal Belum Ditentukan');
                return redirect()->route('admin.hasil.index');
            }
        } else {
            Alert::error('Gagal!', 'Data Belum Lengkap');
            return redirect()->route('admin.hasil.index');
        }
    }

    public function printPenyanggahHasil($id)
    {
        $shp = SeminarHasilPenelitian::find($id);
        $nilaiPenyanggahshp = PenilaianPenyanggahSeminarHasilPenelitian::where([['seminar_hasil_penelitian_id', '=', $shp->id_seminar_hasil_penelitian]])->with(['mahasiswa', 'shp'])->get();
        $namaMahasiswa = Mahasiswa::where('nim', '=', $shp->nim_seminaris)->first()->nama_mahasiswa;

        if ($shp->nilai_pembimbing1 != null && $shp->nilai_pembimbing2 != null && $shp->nilai_penyanggah1 != null && $shp->nilai_penyanggah2 != null) {
            if ($shp->tgl_seminar != null) {
                $pdf = PDF::loadview('print.shp.penyanggah', [
                    'nilaiPenyanggahshp' => $nilaiPenyanggahshp,
                ])->setPaper(array(0, 0, 609.4488, 935.433), 'portrait');

                return $pdf->download('Nilai Penyanggah Seminar Hasil Penelitian ' . $namaMahasiswa . ' .pdf');
                // return $pdf->stream();
            } else {
                Alert::error('Gagal!', 'Tanggal Belum Ditentukan');
                return redirect()->route('admin.hasil.index');
            }
        } else {
            Alert::error('Gagal!', 'Data Belum Lengkap');
            return redirect()->route('admin.hasil.index');
        }
    }

    public function addTanggal(Request $request, $id)
    {
        $request->validate([
            'tanggal' => 'required',
        ]);

        $shp = SeminarHasilPenelitian::find($id);

        $shp->update([
            'tgl_seminar' => $request->tanggal,
            'updated_by' => Auth::user()->identity,
        ]);

        Alert::success('Berhasil!', 'Berhasil Menambahkan Tanggal');
        return redirect()->route('admin.hasil.index');
    }

    public function tambahPenyanggah($id)
    {
        $shp = SeminarHasilPenelitian::find($id);
        $seminaris = Mahasiswa::where('nim', '=', $shp->nim_seminaris)->first();
        $mhs = Mahasiswa::join('nilai_mutus', 'mahasiswas.nim', '=', 'nilai_mutus.nim')
            ->where([['nilai_mutus.rerata_nilai_usulan_penelitian', '!=', null], ['mahasiswas.nim', '!=', $shp->nim_seminaris], ['nilai_mutus.rerata_nilai_shp', '=', null]])
            ->get();
        $dosen = Dosen::all();
        return view('pages.admin.seminar-hasil.addPenyanggah', [
            'mhs' => $mhs,
            'dosen' => $dosen,
            'shp' => $shp,
            'seminaris' => $seminaris,
        ]);
    }

    public function storePenyanggah(Request $request, $id)
    {
        $request->validate([
            'penyanggah1' => 'required|exists:mahasiswas,nim',
            'penyanggah2' => 'required|exists:mahasiswas,nim|different:penyanggah1'
        ]);

        $shp = SeminarHasilPenelitian::find($id);

        // Mahasiswa::where('nim', '=', $request->penyanggah1)->first()->update([
        //     'penyanggah_shp' => true
        // ]);

        // Mahasiswa::where('nim', '=', $request->penyanggah2)->first()->update([
        //     'penyanggah_shp' => true
        // ]);

        $penyanggahPem1 = PenilaianPenyanggahSeminarHasilPenelitian::where([
            ['seminar_hasil_penelitian_id', '=', $shp->id_seminar_hasil_penelitian],
            ['penilai', '=', $shp->pembimbing1],
        ])->get();


        // $penyanggahPem1[0]->update([
        //     'nim_penyanggah' => $request->penyanggah1
        // ]);

        // $penyanggahPem1[1]->update([
        //     'nim_penyanggah' => $request->penyanggah2
        // ]);

        $penyanggahPem2 = PenilaianPenyanggahSeminarHasilPenelitian::where([
            ['seminar_hasil_penelitian_id', '=', $shp->id_seminar_hasil_penelitian],
            ['penilai', '=', $shp->pembimbing2],
        ])->get();

        // $penyanggahPem2[0]->update([
        //     'nim_penyanggah' => $request->penyanggah1
        // ]);

        // $penyanggahPem2[1]->update([
        //     'nim_penyanggah' => $request->penyanggah2
        // ]);

        if ($request->penyanggah1 != $shp->penyanggah1) {
            $penyanggahPem1[0]->update([
                'nim_penyanggah' => $request->penyanggah1,
                'nilai1' => null, 'hasil1' => null,
                'nilai2' => null, 'hasil2' => null,
                'nilai3' => null, 'hasil3' => null,
                'nilai4' => null, 'hasil4' => null,
                'nilai5' => null, 'hasil5' => null,
                'total_nilai' => null,
            ]);
            $penyanggahPem2[0]->update([
                'nim_penyanggah' => $request->penyanggah1,
                'nilai1' => null, 'hasil1' => null,
                'nilai2' => null, 'hasil2' => null,
                'nilai3' => null, 'hasil3' => null,
                'nilai4' => null, 'hasil4' => null,
                'nilai5' => null, 'hasil5' => null,
                'total_nilai' => null,
            ]);

            $shp->update([
                'nilai_penyanggah1' => null,
            ]);
        }

        if ($request->penyanggah2 != $shp->penyanggah2) {
            $penyanggahPem1[1]->update([
                'nim_penyanggah' => $request->penyanggah2,
                'nilai1' => null, 'hasil1' => null,
                'nilai2' => null, 'hasil2' => null,
                'nilai3' => null, 'hasil3' => null,
                'nilai4' => null, 'hasil4' => null,
                'nilai5' => null, 'hasil5' => null,
                'total_nilai' => null,
            ]);
            $penyanggahPem2[1]->update([
                'nim_penyanggah' => $request->penyanggah2,
                'nilai1' => null, 'hasil1' => null,
                'nilai2' => null, 'hasil2' => null,
                'nilai3' => null, 'hasil3' => null,
                'nilai4' => null, 'hasil4' => null,
                'nilai5' => null, 'hasil5' => null,
                'total_nilai' => null,
            ]);

            $shp->update([
                'nilai_penyanggah2' => null,
            ]);
        }

        $shp->update([
            'penyanggah1' => $request->penyanggah1,
            'penyanggah2' => $request->penyanggah2,
            'updated_by' => Auth::user()->identity,
        ]);

        if ($request->tgl_seminar != null) {
            $shp->update([
                'tgl_seminar' => $request->tgl_seminar,
                'updated_by' => Auth::user()->identity,
            ]);
        }

        Alert::success('Berhasil!', 'Berhasil Menambahkan Penyanggah');
        return redirect()->route('admin.hasil.index');
    }

    public function edit($id)
    {
        $mhs = Mahasiswa::all();
        $dosen = Dosen::all();
        $shp = SeminarHasilPenelitian::find($id);
        $nm = NilaiMutu::where('nim', '=', $shp->nim_seminaris)->first();
        return view('pages.admin.seminar q-hasil.edit', [
            'mhs' => $mhs,
            'dosen' => $dosen,
            'shp' => $shp,
            'nm' => $nm,
        ]);
    }

    public function update(Request $request, $id)
    {
        $shp = SeminarHasilPenelitian::find($id);

        if ($request->tgl_seminar != null) {
            $shp->update([
                'tgl_seminar' => $request->tgl_seminar,
            ]);
        }

        if ($request->nilai_menyanggah != null) {
            $nm = NilaiMutu::where('nim', '=', $shp->nim_seminaris)->first();
            $nm->update([
                'rerata_nilai_penyanggah_shp' =>  number_format($request->nilai_menyanggah, 2, ',', ''),
            ]);

            $seminaris = Mahasiswa::where('nim', '=', $shp->nim_seminaris)->first();
            $seminaris->update([
                'penyanggah_shp' => true,
            ]);
        }

        Alert::success('Berhasil!', 'Berhasil Memperbarui Seminar Hasil Penelitian');
        return redirect()->route('admin.hasil.index');
    }
}
