<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Imports\MahasiswaImport;
use App\Models\Mahasiswa;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;
use RealRashid\SweetAlert\Facades\Alert as Alert;

class MahasiswaManageController extends Controller
{
    public function index()
    {
        return view('pages.admin.user-manage.mahasiswa.index');
    }

    public function ajaxGetMahasiswa()
    {
        $mhs = Mahasiswa::select(['id_mahasiswa', 'nim', 'nama_mahasiswa', 'minat', 'tahun_angkatan'])->latest();

        return DataTables::eloquent($mhs)
            ->addIndexColumn()
            ->addColumn('aksi', function ($row) {
                $editUrl = route('admin.user-manage.mahasiswa.edit', $row->id_mahasiswa);
                $deleteUrl = route('admin.user-manage.mahasiswa.destroy', $row->id_mahasiswa);
                return view('modules.backend._formActions', compact('editUrl', 'deleteUrl'));
            })
            ->rawColumns(['aksi'])
            ->make(true);
    }

    public function createMahasiswa()
    {
        return view('pages.admin.user-manage.mahasiswa.create');
    }

    public function storeMahasiswa(Request $request)
    {
        $data = $request->validate(
            [
                'nim' => 'required|unique:mahasiswas,nim',
                'nama_mahasiswa' => 'required',
                'minat' => 'required',
                'tahun_angkatan' => 'required',
            ],
            [
                'nim.required' => 'Kolom NIM Harus Diisi',
                'nim.unique' => 'NIM Sudah Terdaftar',
                'nama_mahasiswa.required' => 'Kolom Nama Harus Diisi',
                'minat.required' => 'Harap Pilih Minat Mahasiswa',
                'tahun_angkatan.required' => 'Kolom Tahun Angkatan Harus Diisi',
            ]
        );

        Mahasiswa::create($data);

        if ($request->quickAdd == 'yes') {
            Alert::success('Berhasil!', 'Data Mahasiswa Berhasil Ditambahkan');
            return redirect()->back();
        }

        Alert::success('Berhasil!', 'Data Mahasiswa Berhasil Ditambahkan');
        return redirect()->route('admin.user-manage.mahasiswa.index');
    }

    public function addManyMahasiswa()
    {
        return view('pages.admin.user-manage.mahasiswa.addMany');
    }

    public function storeManyMahasiswa(Request $request)
    {

        $request->validate(
            [
                'file_input' => 'required|mimes:xl,xls,xlsx|max:2048',
            ],
            [
                'file_input.required' => 'Harap Pilih File Excel Untuk Di upload',
                'file_input.mimes' => 'File Yang Diupload Harus berekstensi .xl, .xls, .xlsx',
                'file_input.max' => 'Ukuran File Yang Di Upload Tidak Bisa Lebih Dari 2MB',
            ]
        );

        Excel::import(new MahasiswaImport, $request->file('file_input'));

        Alert::success('Berhasil!', 'Data-Data Mahasiswa Berhasil Ditambahkan');
        return redirect()->route('admin.user-manage.mahasiswa.index');
    }

    public function editMahasiswa($id)
    {
        $mhs = Mahasiswa::find($id);
        return view('pages.admin.user-manage.mahasiswa.edit', [
            'mhs' => $mhs,
        ]);
    }

    public function updateMahasiswa(Request $request, $id)
    {
        $theMhs = Mahasiswa::find($id);
        $data = $request->validate([
            'nama_mahasiswa' => 'required',
            'minat' => 'required',
            'tahun_angkatan' => 'required',
        ]);

        $theMhs['updated_by'] = Auth::user()->identity;
        $theMhs->update($data);

        Alert::success('Sukses', 'Data Mahasiswa Berhasil Diperbarui');
        return redirect()->route('admin.user-manage.mahasiswa.index');
    }

    public function destroyMahasiswa($id)
    {
        die;
        $mhs = Mahasiswa::find($id);

        $mhs->delete();

        Alert::warning('Dihapus', 'Data Mahasiswa Berhasil Dihapus');
        return redirect()->route('admin.user-manage.mahasiswa.index');
    }
}
