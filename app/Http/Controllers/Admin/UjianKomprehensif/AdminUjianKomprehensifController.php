<?php

namespace App\Http\Controllers\Admin\UjianKomprehensif;

use App\Http\Controllers\Controller;
use App\Models\Dosen;
use App\Models\Mahasiswa;
use App\Models\PenilaianUjianKomprehensif;
use App\Models\UjianKomprehensif;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use RealRashid\SweetAlert\Facades\Alert as Alert;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Riskihajar\Terbilang\Facades\Terbilang as Terbilang;

class AdminUjianKomprehensifController extends Controller
{
    public function index()
    {
        if (request()->q !== null) {
            $q = request()->q;

            $dataUjianKompre = UjianKomprehensif::join('mahasiswas', 'ujian_komprehensifs.nim_skripsi', '=', 'mahasiswas.nim')
                ->join('nilai_mutus', 'ujian_komprehensifs.nim_skripsi', '=', 'nilai_mutus.nim')
                ->where([['ujian_komprehensifs.nim_skripsi', 'LIKE', '%' . $q . '%'], ['nilai_mutus.rerata_nilai_hasil_penelitian', '!=', null], ['nilai_mutus.rerata_nilai_ujian_skripsi', '!=', null]])
                ->orWhere([['mahasiswas.nama_mahasiswa', 'LIKE', '%' . $q . '%'], ['nilai_mutus.rerata_nilai_hasil_penelitian', '!=', null], ['nilai_mutus.rerata_nilai_ujian_skripsi', '!=', null]])
                ->orderBy('ujian_komprehensifs.created_at', 'DESC')
                ->select('*')
                ->paginate(6);

            return view('pages.admin.ujian-kompre.index', [
                'dataUjianKompre' => $dataUjianKompre
            ]);
        } else {

            $dataUjianKompre = UjianKomprehensif::join('nilai_mutus', 'ujian_komprehensifs.nim_skripsi', '=', 'nilai_mutus.nim')
                ->where([['nilai_mutus.rerata_nilai_hasil_penelitian', '!=', null], ['nilai_mutus.rerata_nilai_ujian_skripsi', '!=', null]])
                ->orderBy('ujian_komprehensifs.created_at', 'DESC')
                ->select('*')
                ->paginate(6);

            return view('pages.admin.ujian-kompre.index', [
                'dataUjianKompre' => $dataUjianKompre
            ]);
        }
    }

    public function indexSudahDinilai()
    {
        if (request()->q !== null) {
            $q = request()->q;

            $dataUjianKompre = UjianKomprehensif::join('mahasiswas', 'ujian_komprehensifs.nim_skripsi', '=', 'mahasiswas.nim')
                ->join('nilai_mutus', 'ujian_komprehensifs.nim_skripsi', '=', 'nilai_mutus.nim')
                ->where([['ujian_komprehensifs.nim_skripsi', 'LIKE', '%' . $q . '%'], ['nilai_mutus.rerata_nilai_hasil_penelitian', '!=', null], ['nilai_mutus.rerata_nilai_ujian_skripsi', '!=', null], ['nilai_mutus.nilai_komprehensif', '!=', null]])
                ->orWhere([['mahasiswas.nama_mahasiswa', 'LIKE', '%' . $q . '%'], ['nilai_mutus.rerata_nilai_hasil_penelitian', '!=', null], ['nilai_mutus.rerata_nilai_ujian_skripsi', '!=', null], ['nilai_mutus.nilai_komprehensif', '!=', null]])
                ->orderBy('ujian_komprehensifs.created_at', 'DESC')
                ->select('*')
                ->paginate(6);

            return view('pages.admin.ujian-kompre.index', [
                'dataUjianKompre' => $dataUjianKompre
            ]);
        } else {

            $dataUjianKompre = UjianKomprehensif::join('nilai_mutus', 'ujian_komprehensifs.nim_skripsi', '=', 'nilai_mutus.nim')
                ->where([['nilai_mutus.rerata_nilai_hasil_penelitian', '!=', null], ['nilai_mutus.rerata_nilai_ujian_skripsi', '!=', null], ['nilai_mutus.nilai_komprehensif', '!=', null]])
                ->orderBy('ujian_komprehensifs.created_at', 'DESC')
                ->select('*')
                ->paginate(6);

            return view('pages.admin.ujian-kompre.index', [
                'dataUjianKompre' => $dataUjianKompre
            ]);
        }
    }

    public function printBeritaAcara(Request $request, $id)
    {
        $request->validate([
            'semester' => 'required'
        ]);
        $uk = UjianKomprehensif::with(['mahasiswa', 'nilaimutu'])->find($id);

        if ($uk->nilai_penguji1 != null && $uk->nilai_penguji2 != null && $uk->nilai_penguji3 != null && $uk->nilai_penguji4 != null) {

            if ($uk->tgl_skripsi != null) {

                $nilaiKomprehensif = str_replace(',', '.', $uk->nilaimutu->nilai_komprehensif);
                $hariSkripsi = Carbon::parse($uk->tgl_skripsi)->isoFormat('dddd');
                $tglSkripsi = Terbilang::make(date('j', strtotime($uk->tgl_skripsi)));
                $bulanSkripsi = Carbon::parse($uk->tgl_skripsi)->isoFormat('MMMM');
                $tahunSkripsi = Terbilang::make(date('Y', strtotime($uk->tgl_skripsi)));

                $uk['nilai_terbilang'] = Terbilang::make($nilaiKomprehensif);
                $uk['hariSkripsi'] = $hariSkripsi;
                $uk['tglSkripsi'] = $tglSkripsi;
                $uk['bulanSkripsi'] = $bulanSkripsi;
                $uk['tahunSkripsi'] = $tahunSkripsi;
                $uk['jam'] = date('h:i', strtotime($uk->tgl_skripsi));
                $uk['semester'] = $request->semester;

                $pdf = PDF::loadview('print.ujian-kompre', [
                    'uk' => $uk
                ])->setPaper(array(0, 0, 609.4488, 935.433), 'portrait');

                return $pdf->stream("dompdf_out.pdf", array("Attachment" => false));
                // return $pdf->download('berita acara komprehensif - ' . $uk->mahasiswa->nama_mahasiswa . '.pdf');
            } else {
                Alert::error('Gagal!', 'Tanggal Skripsi Belum Ditentukan');
                return redirect()->route('admin.ujian-kompre.index');
            }
        } else {
            Alert::error('Gagal!', 'Nilai Penguji Belum Lengkap');
            return redirect()->route('admin.ujian-kompre.index');
        }
    }

    public function printNilai($id)
    {
        $uk = UjianKomprehensif::find($id);
        $nuk = PenilaianUjianKomprehensif::where('ujian_komprehensif_id', '=', $uk->id_ujian_komprehensif)->with(['mahasiswa', 'uk', 'dosen'])->get();
        $namaMahasiswa = Mahasiswa::where('nim', '=', $uk->nim_skripsi)->first()->nama_mahasiswa;
        $nuk[0]['nilainilai'] = 'c. Nilai A : >= 80; 77 - < 80; B+ : 75 - < 77; B : 70 - < 75.';
        $nuk[1]['nilainilai'] = 'c. Nilai A : >= 80; 77 - < 80; B+ : 75 - < 77; B : 70 - < 75.';
        $nuk[2]['nilainilai'] = 'c. Nilai A : >= 80; 77 - < 80; B+ : 75 - < 77; B : 70 - < 75.';
        $nuk[3]['nilainilai'] = 'c. Nilai A : >= 80; 77 - < 80; B+ : 75 - < 77; B : 70 - < 75.';


        if ($uk->nilai_penguji1 != null && $uk->nilai_penguji2 != null && $uk->nilai_penguji3 != null && $uk->nilai_penguji4 != null) {
            if ($uk->tgl_skripsi != null) {
                $pdf = PDF::loadview('print.uk.all', [
                    'nuk' => $nuk,
                ])->setPaper(array(0, 0, 609.4488, 935.433), 'portrait');

                return $pdf->stream("dompdf_out.pdf", array("Attachment" => false));

                // return $pdf->download('Nilai Ujian Komprehensif ' . $namaMahasiswa . ' .pdf');
                // return $pdf->stream();
            } else {
                Alert::error('Gagal!', 'Tanggal Belum Ditentukan');
                return redirect()->route('admin.ujian-kompre.index');
            }
        } else {
            Alert::error('Gagal!', 'Data Belum Lengkap');
            return redirect()->route('admin.ujian-kompre.index');
        }
    }

    public function addTanggal(Request $request, $id)
    {
        $request->validate([
            'tanggal' => 'required',
        ]);

        $uk = UjianKomprehensif::find($id);

        $uk->update([
            'tgl_skripsi' => $request->tanggal,
            'updated_by' => Auth::user()->identity,
        ]);

        Alert::success('Berhasil!', 'Berhasil Menambahkan Tanggal');
        return redirect()->route('admin.ujian-kompre.index');
    }

    public function addPenguji($id)
    {
        $uk = UjianKomprehensif::find($id);
        $mhs = Mahasiswa::all();
        $dosen = Dosen::all();
        return view('pages.admin.ujian-kompre.addPenguji', [
            'uk' => $uk,
            'mhs' => $mhs,
            'dosen' => $dosen,
        ]);
    }

    public function storePenguji(Request $request, $id)
    {
        $request->validate(
            [
                'penguji3' => 'required',
                'penguji4' => 'required|different:penguji3',
            ],
            [
                'penguji4.different' => 'Penguji Tidak Boleh Sama'
            ]
        );
        $uk = UjianKomprehensif::find($id);

        // PENGUJI 3
        if ($uk->penguji3 == null) {
            PenilaianUjianKomprehensif::create([
                'ujian_komprehensif_id' => $id,
                'nim_skripsi' => $uk->nim_skripsi,
                'penilai' => $request->penguji3,
            ]);
        } else {
            if ($uk->penguji3 != $request->penguji3) {
                $existPenguji3 = PenilaianUjianKomprehensif::where([['ujian_komprehensif_id', '=', $id], ['penilai', '=', $uk->penguji3]])->first();
                $existPenguji3->update([
                    'penilai' => $request->penguji3,
                    'nilai1a' => null,
                    'hasil1a' => null,

                    'nilai1b' => null,
                    'hasil1b' => null,

                    'nilai2a' => null,
                    'hasil2a' => null,

                    'nilai2b' => null,
                    'hasil2b' => null,

                    'nilai3a' => null,
                    'hasil3a' => null,

                    'nilai3b' => null,
                    'hasil3b' => null,

                    'nilai4' => null,
                    'hasil4' => null,

                    'nilai5a' => null,
                    'hasil5a' => null,

                    'total_nilai' => null,
                ]);
            }
        }

        // PENGUJI 4
        if ($uk->penguji4 == null) {
            PenilaianUjianKomprehensif::create([
                'ujian_komprehensif_id' => $id,
                'nim_skripsi' => $uk->nim_skripsi,
                'penilai' => $request->penguji4,
            ]);
        } else {
            if ($uk->penguji4 != $request->penguji4) {
                $existPenguji4 = PenilaianUjianKomprehensif::where([['ujian_komprehensif_id', '=', $id], ['penilai', '=', $uk->penguji4]])->first();
                $existPenguji4->update([
                    'penilai' => $request->penguji4,
                    'nilai1a' => null,
                    'hasil1a' => null,

                    'nilai1b' => null,
                    'hasil1b' => null,

                    'nilai2a' => null,
                    'hasil2a' => null,

                    'nilai2b' => null,
                    'hasil2b' => null,

                    'nilai3a' => null,
                    'hasil3a' => null,

                    'nilai3b' => null,
                    'hasil3b' => null,

                    'nilai4' => null,
                    'hasil4' => null,

                    'nilai5a' => null,
                    'hasil5a' => null,

                    'total_nilai' => null,
                ]);
            }
        }

        $uk->update([
            'penguji3' => $request->penguji3,
            'penguji4' => $request->penguji4,
            'updated_by' => Auth::user()->identity,
        ]);

        if ($request->tgl_skripsi != null) {
            $uk->update([
                'tgl_skripsi' => $request->tgl_skripsi
            ]);
        }

        Alert::success('Berhasil!', 'Berhasil Menambahkan Penguji');
        return redirect()->route('admin.ujian-kompre.index');
    }

    public function show($id)
    {
        $penilaians = PenilaianUjianKomprehensif::with(['mahasiswa', 'uk'])->where('ujian_komprehensif_id', '=', $id)->get();

        // return $penilaians;
        // die;
        return view('pages.admin.ujian-kompre.show', [
            'penilaians' => $penilaians
        ]);
    }
}
