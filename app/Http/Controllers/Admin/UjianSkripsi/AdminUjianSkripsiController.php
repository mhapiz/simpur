<?php

namespace App\Http\Controllers\Admin\UjianSkripsi;

use App\Http\Controllers\Controller;
use App\Models\Mahasiswa;
use App\Models\PenilaianUjianSkripsi;
use App\Models\UjianSkripsi;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert as Alert;

class AdminUjianSkripsiController extends Controller
{
    public function index()
    {
        if (request()->q !== null) {
            $q = request()->q;

            $dataUjianSkripsi = UjianSkripsi::join('mahasiswas', 'ujian_skripsis.nim_skripsi', '=', 'mahasiswas.nim')
                ->join('nilai_mutus', 'ujian_skripsis.nim_skripsi', '=', 'nilai_mutus.nim')
                ->where([['ujian_skripsis.nim_skripsi', 'LIKE', '%' . $q . '%'], ['nilai_mutus.rerata_nilai_hasil_penelitian', '!=', null]])
                ->orWhere([['mahasiswas.nama_mahasiswa', 'LIKE', '%' . $q . '%'], ['nilai_mutus.rerata_nilai_hasil_penelitian', '!=', null]])
                // ->orderBy('ujian_skripsis.created_at', 'DESC')
                ->orderBy('nilai_mutus.rerata_nilai_ujian_skripsi', 'ASC')
                ->select('*')
                ->paginate(6);

            return view('pages.admin.ujian-skripsi.index', [
                'dataUjianSkripsi' => $dataUjianSkripsi
            ]);
        } else {
            $dataUjianSkripsi = UjianSkripsi::join('nilai_mutus', 'ujian_skripsis.nim_skripsi', '=', 'nilai_mutus.nim')
                ->where('nilai_mutus.rerata_nilai_hasil_penelitian', '!=', null)
                ->with('mahasiswa')
                // ->orderBy('ujian_skripsis.created_at', 'DESC')
                ->orderBy('nilai_mutus.rerata_nilai_ujian_skripsi', 'ASC')
                ->select('*')
                ->paginate(6);

            return view('pages.admin.ujian-skripsi.index', [
                'dataUjianSkripsi' => $dataUjianSkripsi
            ]);
        }
    }

    public function indexSudahDinilai()
    {
        if (request()->q !== null) {
            $q = request()->q;

            $dataUjianSkripsi = UjianSkripsi::join('mahasiswas', 'ujian_skripsis.nim_skripsi', '=', 'mahasiswas.nim')
                ->join('nilai_mutus', 'ujian_skripsis.nim_skripsi', '=', 'nilai_mutus.nim')
                ->where([['ujian_skripsis.nim_skripsi', 'LIKE', '%' . $q . '%'], ['nilai_mutus.rerata_nilai_hasil_penelitian', '!=', null], ['nilai_mutus.rerata_nilai_ujian_skripsi', '!=', null]])
                ->orWhere([['mahasiswas.nama_mahasiswa', 'LIKE', '%' . $q . '%'], ['nilai_mutus.rerata_nilai_hasil_penelitian', '!=', null], ['nilai_mutus.rerata_nilai_ujian_skripsi', '!=', null]])
                ->orderBy('ujian_skripsis.created_at', 'DESC')
                ->select('*')
                ->paginate(6);

            return view('pages.admin.ujian-skripsi.index', [
                'dataUjianSkripsi' => $dataUjianSkripsi
            ]);
        } else {
            $dataUjianSkripsi = UjianSkripsi::join('nilai_mutus', 'ujian_skripsis.nim_skripsi', '=', 'nilai_mutus.nim')
                ->where([['nilai_mutus.rerata_nilai_hasil_penelitian', '!=', null], ['nilai_mutus.rerata_nilai_ujian_skripsi', '!=', null]])
                ->with('mahasiswa')
                ->orderBy('ujian_skripsis.created_at', 'DESC')
                ->select('*')
                ->paginate(6);

            return view('pages.admin.ujian-skripsi.index', [
                'dataUjianSkripsi' => $dataUjianSkripsi
            ]);
        }
    }

    public function addTanggal(Request $request, $id)
    {
        $request->validate([
            'tanggal' => 'required',
        ]);

        $shp = UjianSkripsi::find($id);

        $shp->update([
            'tgl_skripsi' => $request->tanggal,
            'updated_by' => Auth::user()->identity,
        ]);

        Alert::success('Berhasil!', 'Berhasil Menambahkan Tanggal');
        return redirect()->route('admin.ujian-skripsi.index');
    }

    public function show($id)
    {
        $penilaians = PenilaianUjianSkripsi::with(['mahasiswa', 'us', 'dosen'])->where('ujian_skripsi_id', '=', $id)->get();

        return view('pages.admin.ujian-skripsi.show', [
            'penilaians' => $penilaians
        ]);
    }

    public function editTanggal($id)
    {
        $us = UjianSkripsi::with('mahasiswa')->find($id);

        return view('pages.admin.ujian-skripsi.edit', [
            'us' => $us,
        ]);
    }

    public function updateTanggal(Request $request, $id)
    {
        $data = $request->validate([
            'tgl_skripsi' => 'required',
        ]);

        $us = UjianSkripsi::with('mahasiswa')->find($id);

        $us->update($data);

        Alert::success('Berhasil!', 'Berhasil Mengubah Tanggal');
        return redirect()->route('admin.ujian-skripsi.index');
    }

    public function print($id)
    {
        $us = UjianSkripsi::find($id);
        $nus = PenilaianUjianSkripsi::where('ujian_skripsi_id', '=', $us->id_ujian_skripsi)->with(['mahasiswa', 'us'])->get();
        $namaMahasiswa = Mahasiswa::where('nim', '=', $us->nim_skripsi)->first()->nama_mahasiswa;


        if ($us->nilai_pembimbing1 != null && $us->nilai_pembimbing2 != null) {
            if ($us->tgl_skripsi != null) {
                $pdf = PDF::loadview('print.us.all', [
                    'nus' => $nus,
                ])->setPaper(array(0, 0, 609.4488, 935.433), 'portrait');

                return $pdf->download('Ujian Skripsi ' . $namaMahasiswa . ' .pdf');
                // return $pdf->stream();
            } else {
                Alert::error('Gagal!', 'Tanggal Belum Ditentukan');
                return redirect()->route('admin.ujian-skripsi.index');
            }
        } else {
            Alert::error('Gagal!', 'Data Belum Lengkap');
            return redirect()->route('admin.ujian-skripsi.index');
        }
    }
}
