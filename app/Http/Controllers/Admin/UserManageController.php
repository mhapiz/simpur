<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Imports\DosenImport;
use App\Models\User;
use App\Models\Dosen;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use RealRashid\SweetAlert\Facades\Alert as Alert;
use Yajra\DataTables\Facades\DataTables as DataTables;
use Maatwebsite\Excel\Facades\Excel;

class UserManageController extends Controller
{
    public function index()
    {
        return view('pages.admin.user-manage.index');
    }


    public function addSingleUser()
    {
        return view('pages.admin.user-manage.addSingleUser');
    }

    public function storeSingleUser(Request $request)
    {
        $data = $request->validate([
            'nip' => 'required|unique:users,identity',
            'password' => 'required|min:8',
        ]);

        User::create([
            'identity' => $data['nip'],
            'password' => Hash::make($data['password']),
            'role' => 'DOSEN',
        ]);

        Alert::success('Berhasil!', 'User Dosen Berhasil Ditambahkan');
        return redirect()->route('admin.user-manage.index');
    }

    public function addMany()
    {
        return view('pages.admin.user-manage.addMany');
    }

    public function storeMany(Request $request)
    {
        Excel::import(new DosenImport, $request->file('excel_input'));

        Alert::success('Berhasil!', 'User Dosen Berhasil Ditambahkan');
        return redirect()->route('admin.user-manage.index');
    }
}
