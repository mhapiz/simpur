<?php

namespace App\Http\Controllers\Admin\Usulan;

use App\Http\Controllers\Controller;
use App\Http\Requests\PenilaianPenyanggahSeminarUsulanPenelitianRequest;
use App\Models\Dosen;
use App\Models\Mahasiswa;
use App\Models\NilaiMutu;
use App\Models\PenilaianPenyanggahSeminarHasilPenelitian;
use App\Models\PenilaianPenyanggahSeminarUsulanPenelitian;
use App\Models\PenilaianSeminarHasilPenelitian;
use App\Models\PenilaianSeminarUsulanPenelitian;
use App\Models\PenilaianUjianKomprehensif;
use App\Models\PenilaianUjianSkripsi;
use App\Models\SeminarHasilPenelitian;
use App\Models\SeminarUsulanPenelitian;
use App\Models\UjianKomprehensif;
use App\Models\UjianSkripsi;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert as Alert;

class UsulanController extends Controller
{
    public function index()
    {

        if (request()->q !== null) {
            $q = request()->q;

            $dataSUP = SeminarUsulanPenelitian::join('mahasiswas', 'seminar_usulan_penelitians.nim_seminaris', '=', 'mahasiswas.nim')
                ->where('seminar_usulan_penelitians.nim_seminaris', 'LIKE', '%' . $q . '%')
                ->orWhere('mahasiswas.nama_mahasiswa', 'LIKE', '%' . $q . '%')
                ->orderBy('seminar_usulan_penelitians.created_at', 'DESC')
                ->select('*')
                ->paginate(6);

            return view('pages.admin.seminar-usulan.index', [
                'dataSUP' => $dataSUP
            ]);
        } else {
            $dataSUP = SeminarUsulanPenelitian::with('mahasiswa')->latest()->paginate(6);
            return view('pages.admin.seminar-usulan.index', [
                'dataSUP' => $dataSUP
            ]);
        }
    }

    public function indexSudahDinilai()
    {
        if (request()->q !== null) {
            $q = request()->q;

            $dataSUP = SeminarUsulanPenelitian::join('mahasiswas', 'seminar_usulan_penelitians.nim_seminaris', '=', 'mahasiswas.nim')
                ->join('nilai_mutus', 'seminar_usulan_penelitians.nim_seminaris', '=', 'nilai_mutus.nim')
                ->where([['seminar_usulan_penelitians.nim_seminaris', 'LIKE', '%' . $q . '%'], ['nilai_mutus.rerata_nilai_usulan_penelitian', '!=', null]])
                ->orWhere([['mahasiswas.nama_mahasiswa', 'LIKE', '%' . $q . '%'], ['nilai_mutus.rerata_nilai_usulan_penelitian', '!=', null]])
                ->orderBy('seminar_usulan_penelitians.created_at', 'DESC')
                ->select('*')
                ->paginate(6);

            return view('pages.admin.seminar-usulan.index', [
                'dataSUP' => $dataSUP
            ]);
        } else {
            $dataSUP = SeminarUsulanPenelitian::with('mahasiswa')
                ->join('nilai_mutus', 'seminar_usulan_penelitians.nim_seminaris', '=', 'nilai_mutus.nim')
                ->where([['nilai_mutus.rerata_nilai_usulan_penelitian', '!=', null]])
                ->orderBy('seminar_usulan_penelitians.created_at', 'DESC')
                ->paginate(6);
            return view('pages.admin.seminar-usulan.index', [
                'dataSUP' => $dataSUP
            ]);
        }
    }

    public function show($id)
    {
        $sup = SeminarUsulanPenelitian::find($id);

        if ($sup->penyanggah1 != null && $sup->penyanggah2 != null) {
            $psup = PenilaianSeminarUsulanPenelitian::where([['seminar_usulan_penelitian_id', '=', $id]])->with(['mahasiswa', 'sup', 'dosen'])->get();

            $ppsup = PenilaianPenyanggahSeminarUsulanPenelitian::where([['seminar_usulan_penelitian_id', '=', $id]])->with(['mahasiswa', 'sup', 'dosen'])->get();

            return view('pages.admin.seminar-usulan.show', [
                'psup' => $psup,
                'ppsup' => $ppsup,
            ]);
        } else {
            Alert::warning('Gagal!', 'Penyanggah Belum Ditentukan');
            return redirect()->route('admin.usulan.index');
        }
    }

    public function print($id)
    {
        $sup = SeminarUsulanPenelitian::find($id);
        $nilaisup = PenilaianSeminarUsulanPenelitian::where([['seminar_usulan_penelitian_id', '=', $sup->id_seminar_usulan_penelitian]])->with(['mahasiswa', 'sup'])->get();
        $nilaiPenyanggahsup = PenilaianPenyanggahSeminarUsulanPenelitian::where([['seminar_usulan_penelitian_id', '=', $sup->id_seminar_usulan_penelitian]])->with(['mahasiswa', 'sup'])->get();
        $namaMahasiswa = Mahasiswa::where('nim', '=', $sup->nim_seminaris)->first()->nama_mahasiswa;

        if ($sup->nilai_pembimbing1 != null && $sup->nilai_pembimbing2 != null && $sup->nilai_penyanggah1 != null && $sup->nilai_penyanggah2 != null) {
            $pdf = PDF::loadview('print.sup.all', [
                'nilaisup' => $nilaisup,
                'nilaiPenyanggahsup' => $nilaiPenyanggahsup,
            ])->setPaper(array(0, 0, 609.4488, 935.433), 'portrait');

            return $pdf->download('Seminar Usulan Penelitian ' . $namaMahasiswa . ' .pdf');
            // return $pdf->stream();
        } else {
            Alert::error('Gagal!', 'Data Belum Lengkap');
            return redirect()->route('admin.usulan.index');
        }
    }

    public function printUsulan($id)
    {
        $sup = SeminarUsulanPenelitian::find($id);
        $nilaisup = PenilaianSeminarUsulanPenelitian::where([['seminar_usulan_penelitian_id', '=', $sup->id_seminar_usulan_penelitian]])->with(['mahasiswa', 'sup'])->get();
        $namaMahasiswa = Mahasiswa::where('nim', '=', $sup->nim_seminaris)->first()->nama_mahasiswa;

        if ($sup->nilai_pembimbing1 != null && $sup->nilai_pembimbing2 != null && $sup->nilai_penyanggah1 != null && $sup->nilai_penyanggah2 != null) {
            $pdf = PDF::loadview('print.sup.usulan', [
                'nilaisup' => $nilaisup,
            ])->setPaper(array(0, 0, 609.4488, 935.433), 'portrait');

            return $pdf->download('Nilai Seminar Usulan Penelitian ' . $namaMahasiswa . ' .pdf');
            // return $pdf->stream();
        } else {
            Alert::error('Gagal!', 'Data Belum Lengkap');
            return redirect()->route('admin.usulan.index');
        }
    }

    public function printPenyanggahUsulan($id)
    {
        $sup = SeminarUsulanPenelitian::find($id);
        $nilaiPenyanggahsup = PenilaianPenyanggahSeminarUsulanPenelitian::where([['seminar_usulan_penelitian_id', '=', $sup->id_seminar_usulan_penelitian]])->with(['mahasiswa', 'sup'])->get();
        $namaMahasiswa = Mahasiswa::where('nim', '=', $sup->nim_seminaris)->first()->nama_mahasiswa;

        if ($sup->nilai_pembimbing1 != null && $sup->nilai_pembimbing2 != null && $sup->nilai_penyanggah1 != null && $sup->nilai_penyanggah2 != null) {
            $pdf = PDF::loadview('print.sup.penyanggah', [
                'nilaiPenyanggahsup' => $nilaiPenyanggahsup,
            ])->setPaper(array(0, 0, 609.4488, 935.433), 'portrait');

            return $pdf->download('Nilai Penyanggah Seminar Usulan Penelitian ' . $namaMahasiswa . ' .pdf');
            // return $pdf->stream();
        } else {
            Alert::error('Gagal!', 'Data Belum Lengkap');
            return redirect()->route('admin.usulan.index');
        }
    }

    public function create()
    {
        $sudahsup = SeminarUsulanPenelitian::select('nim_seminaris')->get();
        $mhs = Mahasiswa::whereNotIn('nim', $sudahsup)->get();
        $dosen = Dosen::all();
        return view('pages.admin.seminar-usulan.create', [
            'mhs' => $mhs,
            'dosen' => $dosen
        ]);
    }

    public function fetchNilai(Request $request)
    {
        $nim = $request->get('nim');

        $nm = NilaiMutu::where('nim', '=', $nim)->first();

        if (!$nm) {
            echo null;
        } else {
            $rerata = $nm->rerata_nilai_penyanggah_sup;
            // $rerata = floor($nm->rerata_nilai_penyanggah_sup);
            echo $rerata;
        }
    }

    public function store(Request $request)
    {
        $data = $request->validate(
            [
                'nim_seminaris' => 'required|exists:mahasiswas,nim',
                'pembimbing1' => 'required|exists:dosens,nip',
                'pembimbing2' => 'required|exists:dosens,nip|different:pembimbing1',
                'tgl_seminar' => 'required',
                'judul' => 'required',
            ],
            [
                'pembimbing2.different' => 'Pembimbing Tidak Boleh Sama!'
            ]
        );

        if ($request->nilai_menyanggah == null) {
            Alert::error('Gagal!', 'Nilai Menyanggah Masih Kosong');
            return redirect()->back();
        }

        if (!SeminarUsulanPenelitian::where('nim_seminaris', '=', $request->nim_seminaris)->exists()) {

            $addedSeminarUP = SeminarUsulanPenelitian::create($data);
            $addedSeminarUP->update([
                'updated_by' => Auth::user()->identity,
            ]);

            // ---TAMBAH KE 2 TABEL UNTUK PENILAIAN
            PenilaianSeminarUsulanPenelitian::create([
                'nim_seminaris' => $request->nim_seminaris,
                'seminar_usulan_penelitian_id' => $addedSeminarUP->id_seminar_usulan_penelitian,
                'penilai' => $request->pembimbing1,
            ]);
            PenilaianSeminarUsulanPenelitian::create([
                'nim_seminaris' => $request->nim_seminaris,
                'seminar_usulan_penelitian_id' => $addedSeminarUP->id_seminar_usulan_penelitian,
                'penilai' => $request->pembimbing2,
            ]);

            // TAMBAH PENILAIAN PENYANGGAH PEMBIMBING 1
            PenilaianPenyanggahSeminarUsulanPenelitian::create([
                'nim_seminaris' => $request->nim_seminaris,
                'seminar_usulan_penelitian_id' => $addedSeminarUP->id_seminar_usulan_penelitian,
                'penilai' => $request->pembimbing1,
            ]);
            PenilaianPenyanggahSeminarUsulanPenelitian::create([
                'nim_seminaris' => $request->nim_seminaris,
                'seminar_usulan_penelitian_id' => $addedSeminarUP->id_seminar_usulan_penelitian,
                'penilai' => $request->pembimbing1,
            ]);

            // TAMBAH PENILAIAN PENYANGGAH PEMBIMBING 2
            PenilaianPenyanggahSeminarUsulanPenelitian::create([
                'nim_seminaris' => $request->nim_seminaris,
                'seminar_usulan_penelitian_id' => $addedSeminarUP->id_seminar_usulan_penelitian,
                'penilai' => $request->pembimbing2,
            ]);
            PenilaianPenyanggahSeminarUsulanPenelitian::create([
                'nim_seminaris' => $request->nim_seminaris,
                'seminar_usulan_penelitian_id' => $addedSeminarUP->id_seminar_usulan_penelitian,
                'penilai' => $request->pembimbing2,
            ]);

            //---------------------------------------------------------------------------------

            $addedSeminarHP = SeminarHasilPenelitian::create([
                'nim_seminaris' => $request->nim_seminaris,
                'judul'  => $request->judul,
                'pembimbing1' => $request->pembimbing1,
                'pembimbing2' => $request->pembimbing2,
                'updated_by' => Auth::user()->identity,
            ]);
            // ---TAMBAH KE 2 TABEL UNTUK PENILAIAN
            PenilaianSeminarHasilPenelitian::create([
                'nim_seminaris' => $request->nim_seminaris,
                'seminar_hasil_penelitian_id' => $addedSeminarHP->id_seminar_hasil_penelitian,
                'penilai' => $request->pembimbing1,
            ]);
            PenilaianSeminarHasilPenelitian::create([
                'nim_seminaris' => $request->nim_seminaris,
                'seminar_hasil_penelitian_id' => $addedSeminarHP->id_seminar_hasil_penelitian,
                'penilai' => $request->pembimbing2,
            ]);

            // TAMBAH PENILAIAN PENYANGGAH PEMBIMBING 1
            PenilaianPenyanggahSeminarHasilPenelitian::create([
                'nim_seminaris' => $request->nim_seminaris,
                'seminar_hasil_penelitian_id' => $addedSeminarHP->id_seminar_hasil_penelitian,
                'penilai' => $request->pembimbing1,
            ]);
            PenilaianPenyanggahSeminarHasilPenelitian::create([
                'nim_seminaris' => $request->nim_seminaris,
                'seminar_hasil_penelitian_id' => $addedSeminarHP->id_seminar_hasil_penelitian,
                'penilai' => $request->pembimbing1,
            ]);

            // TAMBAH PENILAIAN PENYANGGAH PEMBIMBING 2
            PenilaianPenyanggahSeminarHasilPenelitian::create([
                'nim_seminaris' => $request->nim_seminaris,
                'seminar_hasil_penelitian_id' => $addedSeminarHP->id_seminar_hasil_penelitian,
                'penilai' => $request->pembimbing2,
            ]);
            PenilaianPenyanggahSeminarHasilPenelitian::create([
                'nim_seminaris' => $request->nim_seminaris,
                'seminar_hasil_penelitian_id' => $addedSeminarHP->id_seminar_hasil_penelitian,
                'penilai' => $request->pembimbing2,
            ]);


            //---------------------------------------------------------------------------------

            $addedUjianSkripsi = UjianSkripsi::create([
                'nim_skripsi' => $request->nim_seminaris,
                'judul'  => $request->judul,
                'pembimbing1' => $request->pembimbing1,
                'pembimbing2' => $request->pembimbing2,
                'updated_by' => Auth::user()->identity,
            ]);
            // ---TAMBAH KE 2 TABEL UNTUK PENILAIAN
            PenilaianUjianSkripsi::create([
                'nim_skripsi' => $request->nim_seminaris,
                'ujian_skripsi_id' => $addedUjianSkripsi->id_ujian_skripsi,
                'penilai' => $request->pembimbing1,
            ]);
            PenilaianUjianSkripsi::create([
                'nim_skripsi' => $request->nim_seminaris,
                'ujian_skripsi_id' => $addedUjianSkripsi->id_ujian_skripsi,
                'penilai' => $request->pembimbing2,
            ]);

            //---------------------------------------------------------------------------------

            $uk = UjianKomprehensif::create([
                'nim_skripsi' => $request->nim_seminaris,
                'penguji1' => $request->pembimbing1,
                'penguji2' => $request->pembimbing2,
                'updated_by' => Auth::user()->identity,
            ]);

            PenilaianUjianKomprehensif::create([
                'nim_skripsi' => $request->nim_seminaris,
                'ujian_komprehensif_id' => $uk->id_ujian_komprehensif,
                'penilai' => $request->pembimbing1,
            ]);

            PenilaianUjianKomprehensif::create([
                'nim_skripsi' => $request->nim_seminaris,
                'ujian_komprehensif_id' => $uk->id_ujian_komprehensif,
                'penilai' => $request->pembimbing2,
            ]);

            //---------------------------------------------------------------------------------

            if (!NilaiMutu::where('nim', '=', $request->nim_seminaris)->first()) {
                $newNM = NilaiMutu::create([
                    'nim' => $request->nim_seminaris,
                    'updated_by' => Auth::user()->identity,
                ]);
            } else {
                $newNM = NilaiMutu::where('nim', '=', $request->nim_seminaris)->first();
                $newNM->update([
                    'updated_by' => Auth::user()->identity,
                ]);
            }

            if ($request->nilai_menyanggah != null) {
                $newNM->update([
                    // 'rerata_nilai_penyanggah_sup' => number_format($request->nilai_menyanggah, 2, ',', ''),
                    'rerata_nilai_penyanggah_sup' => $request->nilai_menyanggah,
                ]);

                $seminaris = Mahasiswa::where('nim', '=', $request->nim_seminaris)->first();
                $seminaris->update([
                    'penyanggah_sup' => true,
                ]);
            }


            Alert::success('Berhasil!', 'Berhasil Menambahkan Data Seminar Usulan Penelitian');
            return redirect()->route('admin.usulan.index');
        } else {
            $namamhs = Mahasiswa::where('nim', '=', $request->nim_seminaris)->first()->nama_mahasiswa;
            Alert::warning('Gagal!', 'Mahasiswa/i' . $namamhs . ' Sudah Mengajukan Seminar Usulan Penelitian');
            return redirect()->route('admin.usulan.index');
        };
    }

    public function edit($id)
    {
        $mhs = Mahasiswa::all();
        $dosen = Dosen::all();
        $sup = SeminarUsulanPenelitian::with('nilaiMutu')->find($id);
        // return $dosen;
        // die;
        return view('pages.admin.seminar-usulan.edit', [
            'mhs' => $mhs,
            'dosen' => $dosen,
            'sup' => $sup,
        ]);
    }

    public function update(Request $request, $id)
    {
        $data = $request->validate([
            'nim_seminaris' => 'required|exists:mahasiswas,nim',
            'pembimbing1' => 'required|exists:dosens,nip',
            'pembimbing2' => 'required|exists:dosens,nip|different:pembimbing1',
            'tgl_seminar' => 'required',
            'judul' => 'required',
        ], [
            'pembimbing2.different' => 'Pembimbing Tidak Boleh Sama!'
        ]);

        $sup = SeminarUsulanPenelitian::find($id);
        $shp = SeminarHasilPenelitian::where([
            ['nim_seminaris', '=', $sup->nim_seminaris],
        ])->first();
        $us = UjianSkripsi::where([
            ['nim_skripsi', '=', $sup->nim_seminaris]
        ])->first();
        $uk = UjianKomprehensif::where([
            ['nim_skripsi', '=', $sup->nim_seminaris]
        ])->first();

        if ($request->pembimbing1 != $sup->pembimbing1) {
            // // 'pembimbing 1 beda';
            $psup1 = PenilaianSeminarUsulanPenelitian::where([
                ['seminar_usulan_penelitian_id', '=', $id],
                ['penilai', '=', $sup->pembimbing1],
            ])->first();

            $nilaisupp1 = PenilaianSeminarUsulanPenelitian::find($psup1->id_penilaian_seminar_usulan_penelitian);
            $nilaisupp1->update([
                'penilai' => $request->pembimbing1,
                'nilai1a1' => null,
                'hasil1a1' => null,
                'nilai1b2' => null,
                'hasil1b2' => null,
                'nilai1b3' => null,
                'hasil1b3' => null,
                'nilai1b4' => null,
                'hasil1b4' => null,
                'nilai2c' => null,
                'hasil2c' => null,
                'nilai2d' => null,
                'hasil2d' => null,
                'nilai2e' => null,
                'hasil2e' => null,
                'nilai3' => null,
                'hasil3' => null,
                'nilai4' => null,
                'hasil4' => null,
                'total_nilai' => null,
            ]);

            $sup->update([
                'nilai_pembimbing1' => null,
            ]);

            // UPDATE NILAI SANGGAH MENGYANGGAH YANG DINILAI PEMBIMBING 1

            $ppsup1 = PenilaianPenyanggahSeminarUsulanPenelitian::where([
                ['seminar_usulan_penelitian_id', '=', $id],
                ['penilai', '=', $sup->pembimbing1],
            ])->take(2)->get();


            foreach ($ppsup1 as $p1sup) {
                $p1sup->update([
                    'penilai' => $request->pembimbing1,
                    'nilai1' => null,
                    'hasil1' => null,
                    'nilai2' => null,
                    'hasil2' => null,
                    'nilai3' => null,
                    'hasil3' => null,
                    'nilai4' => null,
                    'hasil4' => null,
                    'total_nilai' => null,
                ]);
            }

            // -------------------------------------------------------------------
            // SEMINAR HASIL PENELITIAN

            $nilaishpp1 = PenilaianSeminarHasilPenelitian::where([
                ['seminar_hasil_penelitian_id', '=', $shp->id_seminar_hasil_penelitian],
                ['penilai', '=', $shp->pembimbing1]
            ])->first();

            $nilaishpp1->update([
                'penilai' => $request->pembimbing1,
                'nilai1a1' => null,
                'hasil1a1' => null,
                'nilai1b2' => null,
                'hasil1b2' => null,
                'nilai1b3' => null,
                'hasil1b3' => null,
                'nilai1b4' => null,
                'hasil1b4' => null,
                'nilai2c' => null,
                'hasil2c' => null,
                'nilai2d' => null,
                'hasil2d' => null,
                'nilai2e' => null,
                'hasil2e' => null,
                'nilai3' => null,
                'hasil3' => null,
                'nilai4' => null,
                'hasil4' => null,
                'total_nilai' => null,
            ]);

            $shp->update([
                'pembimbing1' => $request->pembimbing1,
                'nilai_pembimbing1' => null,
            ]);

            // UPDATE NILAI SANGGAH MENGYANGGAH YANG DINILAI PEMBIMBING 1

            $ppshp1 = PenilaianPenyanggahSeminarHasilPenelitian::where([
                ['seminar_hasil_penelitian_id', '=', $shp->id_seminar_hasil_penelitian],
                ['penilai', '=', $sup->pembimbing1],
            ])->take(2)->get();
            foreach ($ppshp1 as $p1shp) {
                $p1shp->update([
                    'penilai' => $request->pembimbing1,
                    'nilai1' => null,
                    'hasil1' => null,
                    'nilai2' => null,
                    'hasil2' => null,
                    'nilai3' => null,
                    'hasil3' => null,
                    'nilai4' => null,
                    'hasil4' => null,
                    'total_nilai' => null,
                ]);
            }

            // -------------------------------------------------
            // UJIAN SKRIPSI

            $nilaiusp1 = PenilaianUjianSkripsi::where([
                ['ujian_skripsi_id', '=', $us->id_ujian_skripsi],
                ['penilai', '=', $us->pembimbing1],
            ])->first();

            $nilaiusp1->update([
                'penilai' => $request->pembimbing1,
                'nilai1' => null,
                'hasil1' => null,
                'nilai2a' => null,
                'hasil2a' => null,
                'nilai2b' => null,
                'hasil2b' => null,
                'nilai2c' => null,
                'hasil2c' => null,
                'nilai3' => null,
                'hasil3' => null,
                'total_nilai' => null,
            ]);

            $us->update([
                'pembimbing1' => $request->pembimbing1,
                'nilai_pembimbing1' => null,
            ]);


            // -------------------------------------------------
            // UJIAN KOMPREHENSIF

            $nilaiukp1 = PenilaianUjianKomprehensif::where([
                ['ujian_komprehensif_id', '=', $uk->id_ujian_komprehensif],
                ['penilai', '=', $uk->penguji1],
            ])->first();

            $nilaiukp1->update([
                'penilai' => $request->pembimbing1,
                'nilai1' => null,
                'hasil1' => null,
                'nilai2a' => null,
                'hasil2a' => null,
                'nilai2b' => null,
                'hasil2b' => null,
                'nilai2c' => null,
                'hasil2c' => null,
                'nilai3' => null,
                'hasil3' => null,
                'total_nilai' => null,
            ]);

            $uk->update([
                'penguji1' => $request->pembimbing1,
                'nilai_penguji1' => null,
            ]);
        }

        //------
        //------

        if ($request->pembimbing2 != $sup->pembimbing2) {
            // // 'pembimbing 2 beda';
            $psup2 = PenilaianSeminarUsulanPenelitian::where([
                ['seminar_usulan_penelitian_id', '=', $id],
                ['penilai', '=', $sup->pembimbing2],
            ])->first();

            $nilaisupp2 = PenilaianSeminarUsulanPenelitian::find($psup2->id_penilaian_seminar_usulan_penelitian);
            $nilaisupp2->update([
                'penilai' => $request->pembimbing2,
                'nilai1a1' => null,
                'hasil1a1' => null,
                'nilai1b2' => null,
                'hasil1b2' => null,
                'nilai1b3' => null,
                'hasil1b3' => null,
                'nilai1b4' => null,
                'hasil1b4' => null,
                'nilai2c' => null,
                'hasil2c' => null,
                'nilai2d' => null,
                'hasil2d' => null,
                'nilai2e' => null,
                'hasil2e' => null,
                'nilai3' => null,
                'hasil3' => null,
                'nilai4' => null,
                'hasil4' => null,
                'total_nilai' => null,
            ]);

            $sup->update([
                'nilai_pembimbing2' => null,
            ]);

            // UPDATE NILAI SANGGAH MENGYANGGAH YANG DINILAI PEMBIMBING 1
            $ppsup2 = PenilaianPenyanggahSeminarUsulanPenelitian::where([
                ['seminar_usulan_penelitian_id', '=', $id],
                ['penilai', '=', $sup->pembimbing2],
            ])->latest()->take(2)->get();
            foreach ($ppsup2 as $p2sup) {
                $p2sup->update([
                    'penilai' => $request->pembimbing2,
                    'nilai1' => null,
                    'hasil1' => null,
                    'nilai2' => null,
                    'hasil2' => null,
                    'nilai3' => null,
                    'hasil3' => null,
                    'nilai4' => null,
                    'hasil4' => null,
                    'total_nilai' => null,
                ]);
            }

            // -------------------------------------------------------------------
            // SEMINAR HASIL PENELITIAN
            $nilaishpp2 = PenilaianSeminarHasilPenelitian::where([
                ['seminar_hasil_penelitian_id', '=', $shp->id_seminar_hasil_penelitian],
                ['penilai', '=', $shp->pembimbing2]
            ])->first();

            $nilaishpp2->update([
                'penilai' => $request->pembimbing2,
                'nilai1a1' => null,
                'hasil1a1' => null,
                'nilai1b2' => null,
                'hasil1b2' => null,
                'nilai1b3' => null,
                'hasil1b3' => null,
                'nilai1b4' => null,
                'hasil1b4' => null,
                'nilai2c' => null,
                'hasil2c' => null,
                'nilai2d' => null,
                'hasil2d' => null,
                'nilai2e' => null,
                'hasil2e' => null,
                'nilai3' => null,
                'hasil3' => null,
                'nilai4' => null,
                'hasil4' => null,
                'total_nilai' => null,
            ]);

            $shp->update([
                'pembimbing2' => $request->pembimbing2,
                'nilai_pembimbing2' => null,
            ]);

            // UPDATE NILAI SANGGAH MENGYANGGAH YANG DINILAI PEMBIMBING 1

            $ppshp2 = PenilaianPenyanggahSeminarHasilPenelitian::where([
                ['seminar_hasil_penelitian_id', '=', $shp->id_seminar_hasil_penelitian],
                ['penilai', '=', $sup->pembimbing2],
            ])->latest()->take(2)->get();
            foreach ($ppshp2 as $p2shp) {
                $p2shp->update([
                    'penilai' => $request->pembimbing2,
                    'nilai1' => null,
                    'hasil1' => null,
                    'nilai2' => null,
                    'hasil2' => null,
                    'nilai3' => null,
                    'hasil3' => null,
                    'nilai4' => null,
                    'hasil4' => null,
                    'total_nilai' => null,
                ]);
            }

            // -------------------------------------------------
            // UJIAN SKRIPSI
            $nilaiusp2 = PenilaianUjianSkripsi::where([
                ['ujian_skripsi_id', '=', $us->id_ujian_skripsi],
                ['penilai', '=', $us->pembimbing2],
            ])->first();

            $nilaiusp2->update([
                'penilai' => $request->pembimbing2,
                'nilai1' => null,
                'hasil1' => null,
                'nilai2a' => null,
                'hasil2a' => null,
                'nilai2b' => null,
                'hasil2b' => null,
                'nilai2c' => null,
                'hasil2c' => null,
                'nilai3' => null,
                'hasil3' => null,
                'total_nilai' => null,
            ]);

            $us->update([
                'pembimbing2' => $request->pembimbing2,
                'nilai_pembimbing2' => null,
            ]);


            // -------------------------------------------------
            // UJIAN KOMPREHENSIF

            $nilaiukp2 = PenilaianUjianKomprehensif::where([
                ['ujian_komprehensif_id', '=', $uk->id_ujian_komprehensif],
                ['penilai', '=', $uk->penguji2],
            ])->first();

            $nilaiukp2->update([
                'penilai' => $request->pembimbing2,
                'nilai1' => null,
                'hasil1' => null,
                'nilai2a' => null,
                'hasil2a' => null,
                'nilai2b' => null,
                'hasil2b' => null,
                'nilai2c' => null,
                'hasil2c' => null,
                'nilai3' => null,
                'hasil3' => null,
                'total_nilai' => null,
            ]);

            $uk->update([
                'penguji2' => $request->pembimbing2,
                'nilai_penguji2' => null,
            ]);
        }


        $sup['updated_by'] = Auth::user()->identity;
        $sup->update($data);
        $shp->update([
            'nim_seminaris' => $request->nim_seminaris,
            'judul' => $request->judul,
            'updated_by' => Auth::user()->identity,
        ]);
        $us->update([
            'nim_skripsi' => $request->nim_seminaris,
            'judul' => $request->judul,
            'updated_by' => Auth::user()->identity,
        ]);
        $uk->update([
            'nim_skripsi' => $request->nim_seminaris,
            'updated_by' => Auth::user()->identity,
            // 'judul' => $request->judul,
        ]);

        $newNM = NilaiMutu::where('nim', '=', $request->nim_seminaris)->first();
        $newNM->update([
            'updated_by' => Auth::user()->identity,
        ]);

        if ($request->nilai_menyanggah != null) {
            $newNM->update([
                // 'rerata_nilai_penyanggah_sup' => number_format($request->nilai_menyanggah, 2, ',', ''),
                'rerata_nilai_penyanggah_sup' => $request->nilai_menyanggah,
            ]);

            $seminaris = Mahasiswa::where('nim', '=', $request->nim_seminaris)->first();
            $seminaris->update([
                'penyanggah_sup' => true,
            ]);
        }


        Alert::success('Berhasil!', 'Berhasil Memperbarui Data Penelitian');
        return redirect()->route('admin.usulan.index');
    }

    public function tambahPenyanggah($id)
    {
        $sup = SeminarUsulanPenelitian::find($id);
        $sudahsup = SeminarUsulanPenelitian::select('nim_seminaris')->get();
        $seminaris = Mahasiswa::where('nim', '=', $sup->nim_seminaris)->first();
        $mhs = Mahasiswa::whereNotIn('nim', $sudahsup)->where([['nim', '!=', $sup->nim_seminaris]])->get();
        $dosen = Dosen::all();
        return view('pages.admin.seminar-usulan.addPenyanggah', [
            'mhs' => $mhs,
            'dosen' => $dosen,
            'sup' => $sup,
            'seminaris' => $seminaris,
        ]);
    }

    public function storePenyanggah(Request $request, $id)
    {
        $request->validate([
            'penyanggah1' => 'required|exists:mahasiswas,nim',
            'penyanggah2' => 'required|exists:mahasiswas,nim|different:penyanggah1'
        ]);

        $sup = SeminarUsulanPenelitian::find($id);

        Mahasiswa::where('nim', '=', $request->penyanggah1)->first()->update([
            'penyanggah_sup' => true
        ]);

        Mahasiswa::where('nim', '=', $request->penyanggah2)->first()->update([
            'penyanggah_sup' => true
        ]);

        $penyanggahPem1 = PenilaianPenyanggahSeminarUsulanPenelitian::where([
            ['seminar_usulan_penelitian_id', '=', $sup->id_seminar_usulan_penelitian],
            ['penilai', '=', $sup->pembimbing1],
        ])->get();

        // $penyanggahPem1[0]->update([
        //     'nim_penyanggah' => $request->penyanggah1
        // ]);



        // $penyanggahPem1[1]->update([
        //     'nim_penyanggah' => $request->penyanggah2
        // ]);



        $penyanggahPem2 = PenilaianPenyanggahSeminarUsulanPenelitian::where([
            ['seminar_usulan_penelitian_id', '=', $sup->id_seminar_usulan_penelitian],
            ['penilai', '=', $sup->pembimbing2],
        ])->get();

        // $penyanggahPem2[0]->update([
        //     'nim_penyanggah' => $request->penyanggah1
        // ]);

        // $penyanggahPem2[1]->update([
        //     'nim_penyanggah' => $request->penyanggah2
        // ]);

        if ($request->penyanggah1 != $sup->penyanggah1) {
            $penyanggahPem1[0]->update([
                'nim_penyanggah' => $request->penyanggah1,
                'nilai1' => null, 'hasil1' => null,
                'nilai2' => null, 'hasil2' => null,
                'nilai3' => null, 'hasil3' => null,
                'nilai4' => null, 'hasil4' => null,
                'nilai5' => null, 'hasil5' => null,
                'total_nilai' => null,
            ]);
            $penyanggahPem2[0]->update([
                'nim_penyanggah' => $request->penyanggah1,
                'nilai1' => null, 'hasil1' => null,
                'nilai2' => null, 'hasil2' => null,
                'nilai3' => null, 'hasil3' => null,
                'nilai4' => null, 'hasil4' => null,
                'nilai5' => null, 'hasil5' => null,
                'total_nilai' => null,
            ]);

            $sup->update([
                'nilai_penyanggah1' => null,
            ]);
        }

        if ($request->penyanggah2 != $sup->penyanggah2) {
            $penyanggahPem1[1]->update([
                'nim_penyanggah' => $request->penyanggah2,
                'nilai1' => null, 'hasil1' => null,
                'nilai2' => null, 'hasil2' => null,
                'nilai3' => null, 'hasil3' => null,
                'nilai4' => null, 'hasil4' => null,
                'nilai5' => null, 'hasil5' => null,
                'total_nilai' => null,
            ]);
            $penyanggahPem2[1]->update([
                'nim_penyanggah' => $request->penyanggah2,
                'nilai1' => null, 'hasil1' => null,
                'nilai2' => null, 'hasil2' => null,
                'nilai3' => null, 'hasil3' => null,
                'nilai4' => null, 'hasil4' => null,
                'nilai5' => null, 'hasil5' => null,
                'total_nilai' => null,
            ]);

            $sup->update([
                'nilai_penyanggah2' => null,
            ]);
        }



        $sup->update([
            'penyanggah1' => $request->penyanggah1,
            'penyanggah2' => $request->penyanggah2,
            'updated_by' => Auth::user()->identity,
        ]);

        Alert::success('Berhasil!', 'Berhasil Menambahkan Penyanggah');
        return redirect()->route('admin.usulan.index');
    }
}
