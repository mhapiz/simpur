<?php

namespace App\Http\Controllers\Dosen;

use App\Http\Controllers\Controller;
use App\Models\Dosen;
use App\Models\SeminarHasilPenelitian;
use App\Models\SeminarUsulanPenelitian;
use App\Models\UjianKomprehensif;
use App\Models\UjianSkripsi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DosenController extends Controller
{
    public function index()
    {
        $dosenLoggedIn = Dosen::find(Auth::user()->dosen_id);

        $dataSUP = SeminarUsulanPenelitian::with('mahasiswa')->where([
            ['pembimbing1', '=', $dosenLoggedIn->nip],
            ['nilai_pembimbing1', '=', null],
            ['penyanggah1', '!=', null],
            ['penyanggah2', '!=', null],
        ])->orWhere([
            ['pembimbing2', '=', $dosenLoggedIn->nip],
            ['nilai_pembimbing2', '=', null],
            ['penyanggah1', '!=', null],
            ['penyanggah2', '!=', null],
        ])->latest()->limit(3)->get();

        $dataSHP = SeminarHasilPenelitian::join('nilai_mutus', 'seminar_hasil_penelitians.nim_seminaris', '=', 'nilai_mutus.nim')
            ->where([
                ['nilai_mutus.rerata_nilai_usulan_penelitian', '!=', null],
                ['pembimbing1', '=', $dosenLoggedIn->nip],
                ['nilai_pembimbing1', '=', null],
                ['penyanggah1', '!=', null],
                ['penyanggah2', '!=', null],

            ])->orWhere([
                ['nilai_mutus.rerata_nilai_usulan_penelitian', '!=', null],
                ['pembimbing2', '=', $dosenLoggedIn->nip],
                ['nilai_pembimbing2', '=', null],
                ['penyanggah1', '!=', null],
                ['penyanggah2', '!=', null],

            ])
            ->with('mahasiswa')
            ->orderBy('seminar_hasil_penelitians.created_at', 'DESC')
            ->limit(3)->get();

        $dataUjianSkripsi = UjianSkripsi::join('nilai_mutus', 'ujian_skripsis.nim_skripsi', '=', 'nilai_mutus.nim')
            ->with('mahasiswa')->where([
                ['nilai_mutus.rerata_nilai_hasil_penelitian', '!=', null],
                ['pembimbing1', '=', $dosenLoggedIn->nip],
                ['nilai_pembimbing1', '=', null],
            ])->orWhere([
                ['nilai_mutus.rerata_nilai_hasil_penelitian', '!=', null],
                ['pembimbing2', '=', $dosenLoggedIn->nip],
                ['nilai_pembimbing2', '=', null],
            ])
            ->orderBy('ujian_skripsis.created_at', 'DESC')
            ->limit(3)->get();

        $dataUjianKomprehensif = UjianKomprehensif::join('nilai_mutus', 'ujian_komprehensifs.nim_skripsi', '=', 'nilai_mutus.nim')
            ->where([
                ['penguji1', '=', $dosenLoggedIn->nip],
                ['nilai_penguji1', '=', null],
                ['nilai_mutus.rerata_nilai_ujian_skripsi', '!=', null]
            ])->orWhere([
                ['penguji2', '=', $dosenLoggedIn->nip],
                ['nilai_penguji2', '=', null],
                ['nilai_mutus.rerata_nilai_ujian_skripsi', '!=', null]
            ])->orWhere([
                ['penguji3', '=', $dosenLoggedIn->nip],
                ['nilai_penguji3', '=', null],
                ['nilai_mutus.rerata_nilai_ujian_skripsi', '!=', null]
            ])->orWhere([
                ['penguji4', '=', $dosenLoggedIn->nip],
                ['nilai_penguji4', '=', null],
                ['nilai_mutus.rerata_nilai_ujian_skripsi', '!=', null]
            ])
            ->orderBy('ujian_komprehensifs.created_at', 'DESC')
            ->limit(3)->get();


        return view('pages.dosen.dashboard', [
            'dataSUP' => $dataSUP,
            'dataSHP' => $dataSHP,
            'dataUjianSkripsi' => $dataUjianSkripsi,
            'dataUjianKomprehensif' => $dataUjianKomprehensif,
        ]);
    }
}
