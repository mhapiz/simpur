<?php

namespace App\Http\Controllers\Dosen\NilaiMutu;

use App\Http\Controllers\Controller;
use App\Models\NilaiMutu;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use RealRashid\SweetAlert\Facades\Alert as Alert;

class NilaiMutuController extends Controller
{
    public function index()
    {
        return view('pages.dosen.nilai-mutu.index');
    }

    public function ajaxGetNilaiMutu()
    {
        $nilai = NilaiMutu::select(['id_nilai_mutu', 'nim', 'rerata_nilai_sup', 'rerata_nilai_penyanggah_sup', 'rerata_nilai_usulan_penelitian', 'rerata_nilai_shp', 'rerata_nilai_penyanggah_shp', 'rerata_nilai_hasil_penelitian', 'rerata_nilai_seminar', 'nilai_p1_ujian_skripsi', 'nilai_p2_ujian_skripsi', 'rerata_nilai_ujian_skripsi', 'nilai_komprehensif', 'nilai_skripsi', 'nilai_mutu'])
            ->orWhere('rerata_nilai_penyanggah_sup', '!=', null)
            ->orWhere('rerata_nilai_usulan_penelitian', '!=', null)
            ->orWhere('rerata_nilai_shp', '!=', null)
            ->orWhere('rerata_nilai_penyanggah_shp', '!=', null)
            ->orWhere('rerata_nilai_hasil_penelitian', '!=', null)
            ->orWhere('rerata_nilai_seminar', '!=', null)
            ->orWhere('nilai_p1_ujian_skripsi', '!=', null)
            ->orWhere('nilai_p2_ujian_skripsi', '!=', null)
            ->orWhere('rerata_nilai_ujian_skripsi', '!=', null)
            ->orWhere('nilai_komprehensif', '!=', null)
            ->orWhere('nilai_skripsi', '!=', null)
            ->orWhere('nilai_mutu', '!=', null)
            ->with('mahasiswa')->latest();

        return DataTables::eloquent($nilai)
            ->editColumn('nama', function ($row) {
                return $row->mahasiswa->nama_mahasiswa;
            })
            ->make(true);
    }

    public function edit($id)
    {
        $nilaiMutu = NilaiMutu::with(['mahasiswa'])->find($id);
        return view('pages.dosen.nilai-mutu.edit', [
            'nilaiMutu' => $nilaiMutu,
        ]);
    }

    public function update(Request $request, $id)
    {
        $nm = NilaiMutu::find($id);
        $data = $request->validate([
            'rerata_nilai_sup' => '', 'rerata_nilai_penyanggah_sup' => '', 'rerata_nilai_usulan_penelitian' => '',
            'rerata_nilai_shp' => '', 'rerata_nilai_penyanggah_shp' => '', 'rerata_nilai_hasil_penelitian' => '',
            'rerata_nilai_seminar' => '',
            'nilai_p1_ujian_skripsi' => '', 'nilai_p2_ujian_skripsi' => '', 'rerata_nilai_ujian_skripsi' => '',
            'nilai_komprehensif' => '',
            'nilai_skripsi' => '',
        ]);

        $nm->update($data);

        //RERATA NILAI USULAN DAN NILAI PENYANGGAH DI TABEL NILAI MUTU
        if ($nm->rerata_nilai_sup != null && $nm->rerata_nilai_penyanggah_sup) {
            $rerataUsulanXPenyanggah = (str_replace(',', '.', $nm->rerata_nilai_sup) + str_replace(',', '.', $nm->rerata_nilai_penyanggah_sup)) / 2;
            $rerataUsulanXPenyanggah = number_format(floatval(str_replace(',', '.', $rerataUsulanXPenyanggah)), 2, ',', '');
            $nm->update([
                'rerata_nilai_usulan_penelitian' => $rerataUsulanXPenyanggah
            ]);
        }

        //RERATA NILAI HASIL DAN NILAI PENYANGGAH DI TABEL NILAI MUTU
        if ($nm->rerata_nilai_shp != null && $nm->rerata_nilai_penyanggah_shp) {
            $rerataHasilXPenyanggah = (str_replace(',', '.', $nm->rerata_nilai_shp) + str_replace(',', '.', $nm->rerata_nilai_penyanggah_shp)) / 2;
            $rerataHasilXPenyanggah = number_format(floatval(str_replace(',', '.', $rerataHasilXPenyanggah)), 2, ',', '');
            $nm->update([
                'rerata_nilai_hasil_penelitian' => $rerataHasilXPenyanggah
            ]);
        }

        // RERATA NILAI SEMINAR
        if ($nm->rerata_nilai_hasil_penelitian != null && $nm->rerata_nilai_usulan_penelitian != null) {
            $rerataNilaiSeminar = (str_replace(',', '.', $nm->rerata_nilai_hasil_penelitian) + str_replace(',', '.', $nm->rerata_nilai_usulan_penelitian)) / 2;
            $rerataNilaiSeminar = number_format(floatval(str_replace(',', '.', $rerataNilaiSeminar)), 2, ',', '');
            $nm->update([
                'rerata_nilai_seminar' => $rerataNilaiSeminar
            ]);
        }

        if ($nm->nilai_p1_ujian_skripsi != null && $nm->nilai_p2_ujian_skripsi != null) {
            $rerataNilaiUs = (str_replace(',', '.', $nm->nilai_p1_ujian_skripsi) + str_replace(',', '.', $nm->nilai_p2_ujian_skripsi)) / 2;
            $rerataNilaiUs = number_format(floatval(str_replace(',', '.', $rerataNilaiUs)), 2, ',', '');
            $nm->update([
                'rerata_nilai_ujian_skripsi' => $rerataNilaiUs
            ]);
        }

        // NILAI SKRIPSI DAN NILAI MUTU
        if ($nm->nilai_komprehensif != null) {
            $nilaiSkripsi =  ((str_replace(',', '.', $nm->rerata_nilai_ujian_skripsi) * 4) + str_replace(',', '.', $nm->nilai_komprehensif) + str_replace(',', '.', $nm->rerata_nilai_seminar)) / 6;
            $nilaiSkripsi = number_format(floatval(str_replace(',', '.', $nilaiSkripsi)), 2, ',', '');

            if ($nilaiSkripsi >= 80) {
                $mutu = 'A';
            } elseif ($nilaiSkripsi >= 77 && $nilaiSkripsi < 80) {
                $mutu = 'A-';
            } elseif ($nilaiSkripsi >= 75 && $nilaiSkripsi < 77) {
                $mutu = 'B+';
            } elseif ($nilaiSkripsi >= 70 && $nilaiSkripsi < 75) {
                $mutu = 'B';
            }

            $nm->update([
                'nilai_skripsi' => $nilaiSkripsi,
                'nilai_mutu' => $mutu

            ]);
        }

        Alert::success('Berhasil!', 'Berhasil Merubah Nilai Mutu');
        return redirect()->route('dosen.nilai-mutu.index');
    }
}
