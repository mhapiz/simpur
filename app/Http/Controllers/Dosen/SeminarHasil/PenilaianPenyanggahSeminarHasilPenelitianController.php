<?php

namespace App\Http\Controllers\Dosen\SeminarHasil;

use App\Http\Controllers\Controller;
use App\Models\Dosen;
use App\Models\Mahasiswa;
use App\Http\Requests\PenilaianPenyanggahSeminarUsulanPenelitianRequest;
use App\Models\NilaiMutu;
use App\Models\PenilaianPenyanggahSeminarHasilPenelitian;
use App\Models\PenilaianUjianSkripsi;
use App\Models\SeminarHasilPenelitian;
use App\Models\UjianSkripsi;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert as Alert;

class PenilaianPenyanggahSeminarHasilPenelitianController extends Controller
{
    public function penilaianPenyanggah1($id)
    {
        $penilai = Dosen::where('id_dosen', '=', Auth::user()->dosen_id)->first();

        $mahasiswa = Mahasiswa::all();

        $shp = SeminarHasilPenelitian::find($id);


        // CEK
        if ($shp->penyanggah1 != null) {
            $namaPenyanggah = Mahasiswa::where('nim', '=', $shp->penyanggah1)->first()->nama_mahasiswa;

            $nmp1 = NilaiMutu::where('nim', '=', $shp->penyanggah1)->first();

            if ($nmp1 != null) {
                if ($nmp1->rerata_nilai_sup != null) {
                    if (PenilaianPenyanggahSeminarHasilPenelitian::where([
                        ['seminar_hasil_penelitian_id', '=', $id],
                        ['penilai', '=', $penilai->nip]
                    ])->first()) {

                        $penilaian = PenilaianPenyanggahSeminarHasilPenelitian::with(['mahasiswa', 'shp', 'dosen'])->where([
                            ['seminar_hasil_penelitian_id', '=', $id],
                            ['penilai', '=', $penilai->nip],
                            ['nim_penyanggah', '=', $shp->penyanggah1]
                        ])->first();

                        return view('pages.dosen.penilaian-penyanggah-seminar-hasil-penelitian.penilaian', [
                            'penilaian' => $penilaian,
                            'mahasiswa' => $mahasiswa,
                        ]);
                    } else {
                        return redirect()->route('forbidden');
                    }
                } else {
                    Alert::info('ups!', 'Seminar Usulan ' . $namaPenyanggah . ' Belum Dinilai');
                    return redirect()->route('dosen.seminar-hasil.index');
                }
            } else {
                Alert::info('ups!',  $namaPenyanggah . ' Belum Pernah');
                return redirect()->route('dosen.seminar-hasil.index');
            }
        } else {
            Alert::info('ups!', 'Penyanggah Belum Ditentukan');
            return redirect()->route('dosen.seminar-hasil.index');
        }
    }

    public function penilaianPenyanggah2($id)
    {
        $penilai = Dosen::where('id_dosen', '=', Auth::user()->dosen_id)->first();

        $mahasiswa = Mahasiswa::all();

        $shp = SeminarHasilPenelitian::find($id);

        // CEK
        if ($shp->penyanggah2 != null) {
            $namaPenyanggah = Mahasiswa::where('nim', '=', $shp->penyanggah2)->first()->nama_mahasiswa;

            $nmp2 = NilaiMutu::where('nim', '=', $shp->penyanggah2)->first();

            if ($nmp2->rerata_nilai_sup != null) {
                if (PenilaianPenyanggahSeminarHasilPenelitian::where([
                    ['seminar_hasil_penelitian_id', '=', $id],
                    ['penilai', '=', $penilai->nip]
                ])->first()) {

                    $penilaian = PenilaianPenyanggahSeminarHasilPenelitian::with(['mahasiswa', 'shp', 'dosen'])->where([
                        ['seminar_hasil_penelitian_id', '=', $id],
                        ['penilai', '=', $penilai->nip],
                        ['nim_penyanggah', '=', $shp->penyanggah2]
                    ])->first();

                    return view('pages.dosen.penilaian-penyanggah-seminar-hasil-penelitian.penilaian', [
                        'penilaian' => $penilaian,
                        'mahasiswa' => $mahasiswa,
                    ]);
                } else {
                    return redirect()->route('forbidden');
                }
            } else {
                Alert::info('ups!', 'Seminar Usulan ' . $namaPenyanggah . ' Belum Dinilai');
                return redirect()->route('dosen.seminar-hasil.index');
            }
        } else {
            Alert::info('ups!', 'Penyanggah Belum Ditentukan');
            return redirect()->route('dosen.seminar-hasil.index');
        }
    }

    public function storeNilai(PenilaianPenyanggahSeminarUsulanPenelitianRequest $request, $id)
    {

        $data = $request->all();

        $penilai = Dosen::where('id_dosen', '=', Auth::user()->dosen_id)->first();

        $penilaian = PenilaianPenyanggahSeminarHasilPenelitian::find($id);

        $shp = SeminarHasilPenelitian::find($penilaian->seminar_hasil_penelitian_id);

        $nilaiMutu = NilaiMutu::where('nim', '=', $shp->nim_seminaris)->first();

        $nilaiPenyanggahSingle = PenilaianPenyanggahSeminarHasilPenelitian::where([
            ['nim_penyanggah', '=', $request->nim_penyanggah], ['penilai', '=', $penilai->nip], ['seminar_hasil_penelitian_id', '=', $shp->id_seminar_hasil_penelitian]
        ])->first();

        $nilaiPenyanggahSingle->update($data);

        $nilaiPenyanggah = PenilaianPenyanggahSeminarHasilPenelitian::where([
            ['nim_penyanggah', '=', $request->nim_penyanggah], ['seminar_hasil_penelitian_id', '=', $shp->id_seminar_hasil_penelitian]
        ])->get();

        //RERATA NILAI PENYANGGAH DI TABEL NILAI MUTU
        if ($nilaiPenyanggah[0]->total_nilai != null && $nilaiPenyanggah[1]->total_nilai != null) {
            $rerataNP = (str_replace(',', '.', $nilaiPenyanggah[0]->total_nilai) + str_replace(',', '.', $nilaiPenyanggah[1]->total_nilai)) / 2;
            $rerataNP = number_format(floatval(str_replace(',', '.', $rerataNP)), 2, ',', '');
            if (NilaiMutu::where('nim', '=', $penilaian->nim_penyanggah)->first()) {
                $nm = NilaiMutu::where('nim', '=', $penilaian->nim_penyanggah)->first();
                $nm->update([
                    'rerata_nilai_penyanggah_shp' => $rerataNP,
                ]);
            } else {
                NilaiMutu::create([
                    'nim' => $penilaian->nim_penyanggah,
                    'rerata_nilai_penyanggah_shp' => $rerataNP,
                ]);
            }

            if ($shp->penyanggah1 == $request->nim_penyanggah) {
                $shp->update([
                    'nilai_penyanggah1' => $rerataNP,
                ]);
            } elseif ($shp->penyanggah2 == $request->nim_penyanggah) {
                $shp->update([
                    'nilai_penyanggah2' => $rerataNP,
                ]);
            }
        }


        $penilaian->update($data);

        Alert::success('Berhasil!', 'Berhasil Menambahkan Nilai Penyanggah');
        return redirect()->route('dosen.seminar-hasil.index');
    }
}
