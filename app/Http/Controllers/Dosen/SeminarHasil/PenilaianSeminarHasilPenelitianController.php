<?php

namespace App\Http\Controllers\Dosen\SeminarHasil;

use App\Http\Controllers\Controller;
use App\Http\Requests\PenilaianSeminarUsulanPenelitianRequest;
use App\Models\Dosen;
use App\Models\Mahasiswa;
use App\Models\NilaiMutu;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert as Alert;
//
use App\Models\PenilaianSeminarHasilPenelitian;
use App\Models\PenilaianPenyanggahSeminarHasilPenelitian;
use App\Models\SeminarHasilPenelitian;
//

class PenilaianSeminarHasilPenelitianController extends Controller
{
    public function show($id)
    {
        $dosen = Dosen::find(Auth::user()->dosen_id);

        $shp = SeminarHasilPenelitian::find($id);

        if ($shp->penyanggah1 != null && $shp->penyanggah2 != null) {

            $pshp = PenilaianSeminarHasilPenelitian::where([['seminar_hasil_penelitian_id', '=', $id], ['penilai', '=', $dosen->nip]])->with(['mahasiswa', 'shp', 'dosen'])->first();

            $ppshp = PenilaianPenyanggahSeminarHasilPenelitian::where([['seminar_hasil_penelitian_id', '=', $id], ['penilai', '=', $dosen->nip]])->with(['mahasiswa', 'shp', 'dosen'])->get();

            return view('pages.dosen.penilaian-seminar-hasil-penelitian.show', [
                'pshp' => $pshp,
                'ppshp' => $ppshp,
            ]);
        } else {
            Alert::warning('Gagal!', 'Penyanggah Belum Ditentukan');
            return redirect()->route('dosen.seminar-hasil.index');
        }
    }

    public function penilaian($id)
    {
        $penilai = Dosen::where('id_dosen', '=', Auth::user()->dosen_id)->first();

        $shp = SeminarHasilPenelitian::find($id);

        $nmSeminaris = NilaiMutu::where('nim', '=', $shp->nim_seminaris)->first();

        $namaSeminaris =  Mahasiswa::where('nim', '=', $shp->nim_seminaris)->first()->nama_mahasiswa;

        // CEK
        if (PenilaianSeminarHasilPenelitian::where('seminar_hasil_penelitian_id', '=', $id)->where('penilai', '=', $penilai->nip)->first()) {

            if ($nmSeminaris->rerata_nilai_penyanggah_shp != null) {
                $penilaian = PenilaianSeminarHasilPenelitian::with(['mahasiswa', 'shp', 'dosen'])->where('seminar_hasil_penelitian_id', '=', $id)->where('penilai', '=', $penilai->nip)->first();

                return view('pages.dosen.penilaian-seminar-hasil-penelitian.penilaian', [
                    'penilaian' => $penilaian
                ]);
            } else {
                Alert::warning('Belum Pernah Menyanggah', 'Mahasiswa/i ' . $namaSeminaris . ' Belum Pernah Menyanggah Seminar Hasil Penelitian');
                return redirect()->route('dosen.seminar-hasil.index');
            }
        } else {
            return redirect()->route('forbidden');
        }
    }



    public function storeNilai(PenilaianSeminarUsulanPenelitianRequest $request, $id)
    {
        $data = $request->all();

        $penilai = Dosen::where('id_dosen', '=', Auth::user()->dosen_id)->first();

        $penilaian = PenilaianSeminarHasilPenelitian::find($id);

        $shp = SeminarHasilPenelitian::findOrFail($penilaian->seminar_hasil_penelitian_id);

        $nilaiMutu = NilaiMutu::where('nim', '=', $shp->nim_seminaris)->first();


        if ($penilai->nip == $shp->pembimbing1) {
            $shp->update([
                'nilai_pembimbing1' => $request->total_nilai,
            ]);
        } elseif ($penilai->nip == $shp->pembimbing2) {
            $shp->update([
                'nilai_pembimbing2' => $request->total_nilai,
            ]);
        }

        //RERATA NILAI HASIL DARI DOSEN DI TABEL NILAI MUTU
        if ($shp->nilai_pembimbing1 != null && $shp->nilai_pembimbing2 != null) {
            $rerataNilaiHasil = (str_replace(',', '.', $shp->nilai_pembimbing1) + str_replace(',', '.', $shp->nilai_pembimbing2)) / 2;
            $rerataNilaiHasil = number_format(floatval(str_replace(',', '.', $rerataNilaiHasil)), 2, ',', '');
            $nilaiMutu->update([
                'rerata_nilai_shp' => $rerataNilaiHasil
            ]);
        }

        //RERATA NILAI HASIL DAN NILAI PENYANGGAH DI TABEL NILAI MUTU
        if ($nilaiMutu->rerata_nilai_shp != null && $nilaiMutu->rerata_nilai_penyanggah_shp) {
            $rerataHasilXPenyanggah = (str_replace(',', '.', $nilaiMutu->rerata_nilai_shp) + str_replace(',', '.', $nilaiMutu->rerata_nilai_penyanggah_shp)) / 2;
            $rerataHasilXPenyanggah = number_format(floatval(str_replace(',', '.', $rerataHasilXPenyanggah)), 2, ',', '');
            $nilaiMutu->update([
                'rerata_nilai_hasil_penelitian' => $rerataHasilXPenyanggah
            ]);
        }

        // RERATA NILAI SEMINAR
        if ($nilaiMutu->rerata_nilai_hasil_penelitian != null && $nilaiMutu->rerata_nilai_usulan_penelitian != null) {
            $rerataNilaiSeminar = (str_replace(',', '.', $nilaiMutu->rerata_nilai_hasil_penelitian) + str_replace(',', '.', $nilaiMutu->rerata_nilai_usulan_penelitian)) / 2;
            $rerataNilaiSeminar = number_format(floatval(str_replace(',', '.', $rerataNilaiSeminar)), 2, ',', '');
            $nilaiMutu->update([
                'rerata_nilai_seminar' => $rerataNilaiSeminar
            ]);
        }

        $penilaian->update($data);

        Alert::success('Berhasil!', 'Berhasil Menambahkan Nilai');
        return redirect()->route('dosen.seminar-hasil.index');
    }
}
