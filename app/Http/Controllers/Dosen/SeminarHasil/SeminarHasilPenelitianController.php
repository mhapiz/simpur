<?php

namespace App\Http\Controllers\Dosen\SeminarHasil;

use App\Http\Controllers\Controller;
use App\Models\Dosen;
use App\Models\SeminarHasilPenelitian;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert as Alert;

class SeminarHasilPenelitianController extends Controller
{
    public function index()
    {
        $dosen = Dosen::find(Auth::user()->dosen_id);

        if (request()->q !== null) {
            $q = request()->q;

            $dataSHP = SeminarHasilPenelitian::join('mahasiswas', 'seminar_hasil_penelitians.nim_seminaris', '=', 'mahasiswas.nim')
                ->join('nilai_mutus', 'seminar_hasil_penelitians.nim_seminaris', '=', 'nilai_mutus.nim')
                ->where([['seminar_hasil_penelitians.nim_seminaris', 'LIKE', '%' . $q . '%'], ['pembimbing1', '=', $dosen->nip], ['nilai_mutus.rerata_nilai_usulan_penelitian', '!=', null]])
                ->orWhere([['mahasiswas.nama_mahasiswa', 'LIKE', '%' . $q . '%'], ['pembimbing1', '=', $dosen->nip], ['nilai_mutus.rerata_nilai_usulan_penelitian', '!=', null]])
                ->orWhere([['seminar_hasil_penelitians.nim_seminaris', 'LIKE', '%' . $q . '%'], ['pembimbing2', '=', $dosen->nip], ['nilai_mutus.rerata_nilai_usulan_penelitian', '!=', null]])
                ->orWhere([['mahasiswas.nama_mahasiswa', 'LIKE', '%' . $q . '%'], ['pembimbing2', '=', $dosen->nip], ['nilai_mutus.rerata_nilai_usulan_penelitian', '!=', null]])
                ->orderBy('nilai_mutus.rerata_nilai_hasil_penelitian', 'ASC')
                ->select('*')
                ->paginate(6);

            return view('pages.dosen.seminar-hasil-skripsi.index', [
                'dataSHP' => $dataSHP
            ]);
        } else {
            $dataSHP = SeminarHasilPenelitian::join('nilai_mutus', 'seminar_hasil_penelitians.nim_seminaris', '=', 'nilai_mutus.nim')
                ->where([['nilai_mutus.rerata_nilai_usulan_penelitian', '!=', null], ['pembimbing1', '=', $dosen->nip]])
                ->orWhere([['nilai_mutus.rerata_nilai_usulan_penelitian', '!=', null], ['pembimbing2', '=', $dosen->nip]])
                ->orderBy('nilai_mutus.rerata_nilai_hasil_penelitian', 'ASC')
                ->select('*')
                ->paginate(6);

            return view('pages.dosen.seminar-hasil-skripsi.index', [
                'dataSHP' => $dataSHP
            ]);
        }
    }

    public function indexBelumDinilai()
    {
        $dosen = Dosen::find(Auth::user()->dosen_id);

        if (request()->q !== null) {
            $q = request()->q;

            $dataSHP = SeminarHasilPenelitian::join('mahasiswas', 'seminar_hasil_penelitians.nim_seminaris', '=', 'mahasiswas.nim')
                ->join('nilai_mutus', 'seminar_hasil_penelitians.nim_seminaris', '=', 'nilai_mutus.nim')
                ->where([['seminar_hasil_penelitians.nim_seminaris', 'LIKE', '%' . $q . '%'], ['pembimbing1', '=', $dosen->nip], ['seminar_hasil_penelitians.nilai_pembimbing1', '=', null]])
                ->orWhere([['seminar_hasil_penelitians.nim_seminaris', 'LIKE', '%' . $q . '%'], ['pembimbing1', '=', $dosen->nip], ['seminar_hasil_penelitians.nilai_penyanggah1', '=', null]])
                ->orWhere([['seminar_hasil_penelitians.nim_seminaris', 'LIKE', '%' . $q . '%'], ['pembimbing1', '=', $dosen->nip], ['seminar_hasil_penelitians.nilai_penyanggah2', '=', null]])
                //
                ->orWhere([['mahasiswas.nama_mahasiswa', 'LIKE', '%' . $q . '%'], ['pembimbing1', '=', $dosen->nip], ['seminar_hasil_penelitians.nilai_pembimbing1', '=', null]])
                ->orWhere([['mahasiswas.nama_mahasiswa', 'LIKE', '%' . $q . '%'], ['pembimbing1', '=', $dosen->nip], ['seminar_hasil_penelitians.nilai_penyanggah1', '=', null]])
                ->orWhere([['mahasiswas.nama_mahasiswa', 'LIKE', '%' . $q . '%'], ['pembimbing1', '=', $dosen->nip],  ['seminar_hasil_penelitians.nilai_penyanggah2', '=', null]])
                //
                ->orWhere([['seminar_hasil_penelitians.nim_seminaris', 'LIKE', '%' . $q . '%'], ['pembimbing2', '=', $dosen->nip], ['seminar_hasil_penelitians.nilai_pembimbing2', '=', null]])
                ->orWhere([['seminar_hasil_penelitians.nim_seminaris', 'LIKE', '%' . $q . '%'], ['pembimbing2', '=', $dosen->nip], ['seminar_hasil_penelitians.nilai_penyanggah1', '=', null]])
                ->orWhere([['seminar_hasil_penelitians.nim_seminaris', 'LIKE', '%' . $q . '%'], ['pembimbing2', '=', $dosen->nip], ['seminar_hasil_penelitians.nilai_penyanggah2', '=', null]])
                //
                ->orWhere([['mahasiswas.nama_mahasiswa', 'LIKE', '%' . $q . '%'], ['pembimbing2', '=', $dosen->nip], ['seminar_hasil_penelitians.nilai_pembimbing2', '=', null]])
                ->orWhere([['mahasiswas.nama_mahasiswa', 'LIKE', '%' . $q . '%'], ['pembimbing2', '=', $dosen->nip], ['seminar_hasil_penelitians.nilai_penyanggah1', '=', null]])
                ->orWhere([['mahasiswas.nama_mahasiswa', 'LIKE', '%' . $q . '%'], ['pembimbing2', '=', $dosen->nip], ['seminar_hasil_penelitians.nilai_penyanggah2', '=', null]])
                //
                ->select('*')
                ->orderBy('seminar_hasil_penelitians.created_at', 'DESC')
                ->paginate(6);

            return view('pages.dosen.seminar-hasil-skripsi.index', [
                'dataSHP' => $dataSHP
            ]);
        } else {
            $dataSHP = SeminarHasilPenelitian::join('nilai_mutus', 'seminar_hasil_penelitians.nim_seminaris', '=', 'nilai_mutus.nim')
                ->where([['nilai_mutus.rerata_nilai_usulan_penelitian', '!=', null], ['pembimbing1', '=', $dosen->nip], ['seminar_hasil_penelitians.nilai_pembimbing1', '=', null]])
                ->where([['nilai_mutus.rerata_nilai_usulan_penelitian', '!=', null], ['pembimbing1', '=', $dosen->nip], ['seminar_hasil_penelitians.nilai_penyanggah1', '=', null]])
                ->where([['nilai_mutus.rerata_nilai_usulan_penelitian', '!=', null], ['pembimbing1', '=', $dosen->nip], ['seminar_hasil_penelitians.nilai_penyanggah2', '=', null]])
                //
                ->orWhere([['nilai_mutus.rerata_nilai_usulan_penelitian', '!=', null], ['pembimbing2', '=', $dosen->nip], ['seminar_hasil_penelitians.nilai_pembimbing2', '=', null]])
                ->orWhere([['nilai_mutus.rerata_nilai_usulan_penelitian', '!=', null], ['pembimbing2', '=', $dosen->nip], ['seminar_hasil_penelitians.nilai_penyanggah1', '=', null]])
                ->orWhere([['nilai_mutus.rerata_nilai_usulan_penelitian', '!=', null], ['pembimbing2', '=', $dosen->nip], ['seminar_hasil_penelitians.nilai_penyanggah2', '=', null]])
                //
                ->select('*')
                ->orderBy('seminar_hasil_penelitians.created_at', 'DESC')
                ->paginate(6);

            return view('pages.dosen.seminar-hasil-skripsi.index', [
                'dataSHP' => $dataSHP
            ]);
        }
    }
}
