<?php

namespace App\Http\Controllers\Dosen\SeminarUsulan;

use App\Http\Controllers\Controller;
use App\Models\Dosen;
use App\Models\Mahasiswa;
use App\Http\Requests\PenilaianPenyanggahSeminarUsulanPenelitianRequest;
use App\Models\NilaiMutu;
use App\Models\PenilaianPenyanggahSeminarUsulanPenelitian;
use App\Models\SeminarUsulanPenelitian;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert as Alert;

class PenilaianPenyanggahSeminarUsulanPenelitianController extends Controller
{
    public function penilaianPenyanggah1($id)
    {
        $penilai = Dosen::where('id_dosen', '=', Auth::user()->dosen_id)->first();

        $mahasiswa = Mahasiswa::all();

        $sup = SeminarUsulanPenelitian::find($id);



        // CEK
        if ($sup->penyanggah1 != null) {
            if (PenilaianPenyanggahSeminarUsulanPenelitian::where([['seminar_usulan_penelitian_id', '=', $id], ['penilai', '=', $penilai->nip]])->first()) {;

                $penilaian = PenilaianPenyanggahSeminarUsulanPenelitian::with(['mahasiswa', 'sup', 'dosen'])->where([['seminar_usulan_penelitian_id', '=', $id], ['penilai', '=', $penilai->nip], ['nim_penyanggah', '=', $sup->penyanggah1]])->first();

                return view('pages.dosen.penilaian-penyanggah-seminar-usulan-penelitian.penilaian', [
                    'penilaian' => $penilaian,
                    'mahasiswa' => $mahasiswa,
                ]);
            } else {
                return redirect()->route('forbidden');
            }
        } else {
            Alert::info('ups!', 'Penyanggah Belum Ditentukan');
            return redirect()->route('dosen.seminar-usulan.index');
        }
    }

    public function penilaianPenyanggah2($id)
    {
        $penilai = Dosen::where('id_dosen', '=', Auth::user()->dosen_id)->first();

        $mahasiswa = Mahasiswa::all();

        $sup = SeminarUsulanPenelitian::find($id);

        // CEK
        if ($sup->penyanggah2) {
            if (PenilaianPenyanggahSeminarUsulanPenelitian::where([['seminar_usulan_penelitian_id', '=', $id], ['penilai', '=', $penilai->nip]])->first()) {

                $penilaian = PenilaianPenyanggahSeminarUsulanPenelitian::with(['mahasiswa', 'sup', 'dosen'])->where([['seminar_usulan_penelitian_id', '=', $id], ['penilai', '=', $penilai->nip], ['nim_penyanggah', '=', $sup->penyanggah2]])->first();

                return view('pages.dosen.penilaian-penyanggah-seminar-usulan-penelitian.penilaian', [
                    'penilaian' => $penilaian,
                    'mahasiswa' => $mahasiswa,
                ]);
            } else {
                return redirect()->route('forbidden');
            }
        } else {
            Alert::info('ups!', 'Penyanggah Belum Ditentukan');
            return redirect()->route('dosen.seminar-usulan.index');
        }
    }

    public function storeNilai(PenilaianPenyanggahSeminarUsulanPenelitianRequest $request, $id)
    {
        $data = $request->all();

        $penilai = Dosen::where('id_dosen', '=', Auth::user()->dosen_id)->first();

        $penilaian = PenilaianPenyanggahSeminarUsulanPenelitian::find($id);

        $sup = SeminarUsulanPenelitian::find($penilaian->seminar_usulan_penelitian_id);

        $nilaiMutu = NilaiMutu::where('nim', '=', $sup->nim_seminaris)->first();

        $nilaiPenyanggahSingle = PenilaianPenyanggahSeminarUsulanPenelitian::where([
            ['nim_penyanggah', '=', $request->nim_penyanggah], ['penilai', '=', $penilai->nip], ['seminar_usulan_penelitian_id', '=', $sup->id_seminar_usulan_penelitian]
        ])->first();

        $nilaiPenyanggahSingle->update($data);

        $nilaiPenyanggah = PenilaianPenyanggahSeminarUsulanPenelitian::where([
            ['nim_penyanggah', '=', $request->nim_penyanggah], ['seminar_usulan_penelitian_id', '=', $sup->id_seminar_usulan_penelitian]
        ])->get();

        //RERATA NILAI PENYANGGAH DI TABEL NILAI MUTU
        if ($nilaiPenyanggah[0]->total_nilai != null && $nilaiPenyanggah[1]->total_nilai != null) {
            $rerataNP = (str_replace(',', '.', $nilaiPenyanggah[0]->total_nilai) + str_replace(',', '.', $nilaiPenyanggah[1]->total_nilai)) / 2;
            $rerataNP = number_format(floatval(str_replace(',', '.', $rerataNP)), 2, ',', '');
            if (NilaiMutu::where('nim', '=', $penilaian->nim_penyanggah)->first()) {
                $nm = NilaiMutu::where('nim', '=', $penilaian->nim_penyanggah)->first();
                $nm->update([
                    'rerata_nilai_penyanggah_sup' => $rerataNP,
                ]);
            } else {
                NilaiMutu::create([
                    'nim' => $penilaian->nim_penyanggah,
                    'rerata_nilai_penyanggah_sup' => $rerataNP,
                ]);
            }

            if ($sup->penyanggah1 == $request->nim_penyanggah) {
                $sup->update([
                    'nilai_penyanggah1' => $rerataNP,
                ]);
            } elseif ($sup->penyanggah2 == $request->nim_penyanggah) {
                $sup->update([
                    'nilai_penyanggah2' => $rerataNP,
                ]);
            }
        }

        Alert::success('Berhasil!', 'Berhasil Menambahkan Nilai Penyanggah');
        return redirect()->route('dosen.seminar-usulan.index');
    }
}
