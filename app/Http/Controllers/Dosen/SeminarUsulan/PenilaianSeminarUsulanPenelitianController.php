<?php

namespace App\Http\Controllers\Dosen\SeminarUsulan;

use App\Http\Controllers\Controller;
use App\Http\Requests\PenilaianSeminarUsulanPenelitianRequest;
use App\Models\Dosen;
use App\Models\Mahasiswa;
use App\Models\NilaiMutu;
use App\Models\PenilaianPenyanggahSeminarUsulanPenelitian;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert as Alert;
use App\Models\PenilaianSeminarUsulanPenelitian;
use App\Models\SeminarUsulanPenelitian;
//

class PenilaianSeminarUsulanPenelitianController extends Controller
{

    public function show($id)
    {
        $dosen = Dosen::find(Auth::user()->dosen_id);

        $sup = SeminarUsulanPenelitian::find($id);

        if ($sup->penyanggah1 != null && $sup->penyanggah2 != null) {
            $psup = PenilaianSeminarUsulanPenelitian::where([['seminar_usulan_penelitian_id', '=', $id], ['penilai', '=', $dosen->nip]])->with(['mahasiswa', 'sup', 'dosen'])->first();

            $ppsup = PenilaianPenyanggahSeminarUsulanPenelitian::where([['seminar_usulan_penelitian_id', '=', $id], ['penilai', '=', $dosen->nip]])->with(['mahasiswa', 'sup', 'dosen'])->get();

            return view('pages.dosen.penilaian-seminar-usulan-penelitian.show', [
                'psup' => $psup,
                'ppsup' => $ppsup,
            ]);  # code...
        } else {
            Alert::warning('Gagal!', 'Penyanggah Belum Ditentukan');
            return redirect()->route('dosen.seminar-usulan.index');
        }
    }

    public function penilaian($id)
    {
        $penilai = Dosen::where('id_dosen', '=', Auth::user()->dosen_id)->first();

        $sup = SeminarUsulanPenelitian::find($id);

        $nmSeminaris = NilaiMutu::where('nim', '=', $sup->nim_seminaris)->first();

        $namaSeminaris =  Mahasiswa::where('nim', '=', $sup->nim_seminaris)->first()->nama_mahasiswa;

        // CEK
        if (PenilaianSeminarUsulanPenelitian::where('seminar_usulan_penelitian_id', '=', $id)->where('penilai', '=', $penilai->nip)->first()) {
            if ($nmSeminaris->rerata_nilai_penyanggah_sup != null) {
                $penilaian = PenilaianSeminarUsulanPenelitian::with(['mahasiswa', 'sup', 'dosen'])->where('seminar_usulan_penelitian_id', '=', $id)->where('penilai', '=', $penilai->nip)->first();
                return view('pages.dosen.penilaian-seminar-usulan-penelitian.penilaian', [
                    'penilaian' => $penilaian
                ]);
            } else {
                Alert::warning('Belum Pernah Menyanggah', 'Mahasiswa/i ' . $namaSeminaris . ' Belum Pernah Menyanggah');
                return redirect()->route('dosen.seminar-usulan.index');
            }
        } else {
            return redirect()->route('forbidden');
        }
    }

    public function storeNilai(PenilaianSeminarUsulanPenelitianRequest $request, $id)
    {
        $data = $request->all();

        $penilai = Dosen::where('id_dosen', '=', Auth::user()->dosen_id)->first();

        $penilaian = PenilaianSeminarUsulanPenelitian::find($id);

        $sup = SeminarUsulanPenelitian::find($penilaian->seminar_usulan_penelitian_id);

        $nilaiMutu = NilaiMutu::where('nim', '=', $sup->nim_seminaris)->first();


        if ($penilai->nip == $sup->pembimbing1) {
            $sup->update([
                'nilai_pembimbing1' => $request->total_nilai,
            ]);
        } elseif ($penilai->nip == $sup->pembimbing2) {
            $sup->update([
                'nilai_pembimbing2' => $request->total_nilai,
            ]);
        }

        //RERATA NILAI USULAN DARI DOSEN DI TABEL NILAI MUTU
        if ($sup->nilai_pembimbing1 != null && $sup->nilai_pembimbing2 != null) {
            $rerataNilaiUsulan = (str_replace(',', '.', $sup->nilai_pembimbing1) + str_replace(',', '.', $sup->nilai_pembimbing2)) / 2;
            $rerataNilaiUsulan = number_format(floatval(str_replace(',', '.', $rerataNilaiUsulan)), 2, ',', '');
            $nilaiMutu->update([
                'rerata_nilai_sup' => $rerataNilaiUsulan
            ]);
        }

        //RERATA NILAI USULAN DAN NILAI PENYANGGAH DI TABEL NILAI MUTU
        if ($nilaiMutu->rerata_nilai_sup != null && $nilaiMutu->rerata_nilai_penyanggah_sup) {
            $rerataUsulanXPenyanggah = (str_replace(',', '.', $nilaiMutu->rerata_nilai_sup) + str_replace(',', '.', $nilaiMutu->rerata_nilai_penyanggah_sup)) / 2;
            $rerataUsulanXPenyanggah = number_format(floatval(str_replace(',', '.', $rerataUsulanXPenyanggah)), 2, ',', '');
            $nilaiMutu->update([
                'rerata_nilai_usulan_penelitian' => $rerataUsulanXPenyanggah
            ]);
        }


        $penilaian->update($data);

        Alert::success('Berhasil!', 'Berhasil Menambahkan Nilai');
        return redirect()->route('dosen.seminar-usulan.index');
    }
}
