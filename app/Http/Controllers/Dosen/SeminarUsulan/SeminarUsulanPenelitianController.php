<?php

namespace App\Http\Controllers\Dosen\SeminarUsulan;

use App\Http\Controllers\Controller;
use App\Models\Dosen;
use App\Models\Mahasiswa;
use App\Models\SeminarUsulanPenelitian;
use Illuminate\Support\Facades\Auth;

class SeminarUsulanPenelitianController extends Controller
{
    public function index()
    {
        $dosen = Dosen::find(Auth::user()->dosen_id);

        if (request()->q !== null) {
            $q = request()->q;

            $dataSUP = SeminarUsulanPenelitian::join('mahasiswas', 'seminar_usulan_penelitians.nim_seminaris', '=', 'mahasiswas.nim')
                ->join('nilai_mutus', 'seminar_usulan_penelitians.nim_seminaris', '=', 'nilai_mutus.nim')
                ->where([['seminar_usulan_penelitians.nim_seminaris', 'LIKE', '%' . $q . '%'], ['pembimbing1', '=', $dosen->nip]])
                ->orWhere([['mahasiswas.nama_mahasiswa', 'LIKE', '%' . $q . '%'], ['pembimbing1', '=', $dosen->nip]])
                ->orWhere([['seminar_usulan_penelitians.nim_seminaris', 'LIKE', '%' . $q . '%'], ['pembimbing2', '=', $dosen->nip]])
                ->orWhere([['mahasiswas.nama_mahasiswa', 'LIKE', '%' . $q . '%'], ['pembimbing2', '=', $dosen->nip]])
                ->orderBy('nilai_mutus.rerata_nilai_usulan_penelitian', 'ASC')
                ->select('*')
                ->paginate(6);

            return view('pages.dosen.seminar-usulan-penelitian.index', [
                'dataSUP' => $dataSUP
            ]);
        } else {
            $dataSUP = SeminarUsulanPenelitian::join('nilai_mutus', 'seminar_usulan_penelitians.nim_seminaris', '=', 'nilai_mutus.nim')
                ->where('pembimbing1', '=', $dosen->nip)
                ->orWhere('pembimbing2', '=', $dosen->nip)
                ->with('mahasiswa')
                ->orderBy('nilai_mutus.rerata_nilai_usulan_penelitian', 'ASC')
                ->select('*')
                ->paginate(6);

            return view('pages.dosen.seminar-usulan-penelitian.index', [
                'dataSUP' => $dataSUP
            ]);
        }
    }
    public function indexBelumDinilai()
    {
        $dosen = Dosen::find(Auth::user()->dosen_id);

        if (request()->q !== null) {
            $q = request()->q;

            $dataSUP = SeminarUsulanPenelitian::join('mahasiswas', 'seminar_usulan_penelitians.nim_seminaris', '=', 'mahasiswas.nim')
                ->where([['seminar_usulan_penelitians.nim_seminaris', 'LIKE', '%' . $q . '%'], ['pembimbing1', '=', $dosen->nip], ['seminar_usulan_penelitians.nilai_pembimbing1', '=', null]])
                ->orWhere([['seminar_usulan_penelitians.nim_seminaris', 'LIKE', '%' . $q . '%'], ['pembimbing1', '=', $dosen->nip], ['seminar_usulan_penelitians.nilai_penyanggah1', '=', null]])
                ->orWhere([['seminar_usulan_penelitians.nim_seminaris', 'LIKE', '%' . $q . '%'], ['pembimbing1', '=', $dosen->nip], ['seminar_usulan_penelitians.nilai_penyanggah2', '=', null]])
                //
                ->orWhere([['mahasiswas.nama_mahasiswa', 'LIKE', '%' . $q . '%'], ['pembimbing1', '=', $dosen->nip], ['seminar_usulan_penelitians.nilai_pembimbing1', '=', null]])
                ->orWhere([['mahasiswas.nama_mahasiswa', 'LIKE', '%' . $q . '%'], ['pembimbing1', '=', $dosen->nip], ['seminar_usulan_penelitians.nilai_penyanggah1', '=', null]])
                ->orWhere([['mahasiswas.nama_mahasiswa', 'LIKE', '%' . $q . '%'], ['pembimbing1', '=', $dosen->nip],  ['seminar_usulan_penelitians.nilai_penyanggah2', '=', null]])
                //
                ->orWhere([['seminar_usulan_penelitians.nim_seminaris', 'LIKE', '%' . $q . '%'], ['pembimbing2', '=', $dosen->nip], ['seminar_usulan_penelitians.nilai_pembimbing2', '=', null]])
                ->orWhere([['seminar_usulan_penelitians.nim_seminaris', 'LIKE', '%' . $q . '%'], ['pembimbing2', '=', $dosen->nip], ['seminar_usulan_penelitians.nilai_penyanggah1', '=', null]])
                ->orWhere([['seminar_usulan_penelitians.nim_seminaris', 'LIKE', '%' . $q . '%'], ['pembimbing2', '=', $dosen->nip], ['seminar_usulan_penelitians.nilai_penyanggah2', '=', null]])
                //
                ->orWhere([['mahasiswas.nama_mahasiswa', 'LIKE', '%' . $q . '%'], ['pembimbing2', '=', $dosen->nip], ['seminar_usulan_penelitians.nilai_pembimbing2', '=', null]])
                ->orWhere([['mahasiswas.nama_mahasiswa', 'LIKE', '%' . $q . '%'], ['pembimbing2', '=', $dosen->nip], ['seminar_usulan_penelitians.nilai_penyanggah1', '=', null]])
                ->orWhere([['mahasiswas.nama_mahasiswa', 'LIKE', '%' . $q . '%'], ['pembimbing2', '=', $dosen->nip], ['seminar_usulan_penelitians.nilai_penyanggah2', '=', null]])
                //
                ->orderBy('seminar_usulan_penelitians.created_at', 'DESC')
                ->select('*')
                ->paginate(6);

            return view('pages.dosen.seminar-usulan-penelitian.index', [
                'dataSUP' => $dataSUP
            ]);
        } else {
            $dataSUP = SeminarUsulanPenelitian::where([['pembimbing1', '=', $dosen->nip], ['seminar_usulan_penelitians.nilai_pembimbing1', '=', null]])
                ->orWhere([['pembimbing1', '=', $dosen->nip], ['seminar_usulan_penelitians.nilai_penyanggah1', '=', null]])
                ->orWhere([['pembimbing1', '=', $dosen->nip], ['seminar_usulan_penelitians.nilai_penyanggah2', '=', null]])
                ->orWhere([['pembimbing2', '=', $dosen->nip], ['seminar_usulan_penelitians.nilai_pembimbing2', '=', null]])
                ->orWhere([['pembimbing2', '=', $dosen->nip], ['seminar_usulan_penelitians.nilai_penyanggah1', '=', null]])
                ->orWhere([['pembimbing2', '=', $dosen->nip], ['seminar_usulan_penelitians.nilai_penyanggah2', '=', null]])
                ->with('mahasiswa')
                ->orderBy('seminar_usulan_penelitians.created_at', 'DESC')
                ->select('*')
                ->paginate(6);

            return view('pages.dosen.seminar-usulan-penelitian.index', [
                'dataSUP' => $dataSUP
            ]);
        }
    }
}
