<?php

namespace App\Http\Controllers\Dosen\Ujian;

use App\Http\Controllers\Controller;
use App\Http\Requests\PenilaianUjianRequest;
use App\Models\Dosen;
use App\Models\NilaiMutu;
use App\Models\PenilaianUjianKomprehensif;
use App\Models\PenilaianUjianSkripsi;
use App\Models\UjianKomprehensif;
use App\Models\UjianSkripsi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert as Alert;

class PenilaianUjianSkripsiController extends Controller
{
    public function show($id)
    {
        $dosen = Dosen::find(Auth::user()->dosen_id);

        $us = PenilaianUjianSkripsi::where([['ujian_skripsi_id', '=', $id], ['penilai', '=', $dosen->nip]])->with(['mahasiswa', 'us', 'dosen'])->first();

        return view('pages.dosen.penilaian-ujian-skripsi.show', [
            'us' => $us
        ]);
    }

    public function penilaian($id)
    {
        $penilai = Dosen::where('id_dosen', '=', Auth::user()->dosen_id)->first();
        // CEK
        if (PenilaianUjianSkripsi::where('ujian_skripsi_id', '=', $id)->where('penilai', '=', $penilai->nip)->first()) {

            $penilaian = PenilaianUjianSkripsi::with(['mahasiswa', 'us', 'dosen'])->where('ujian_skripsi_id', '=', $id)->where('penilai', '=', $penilai->nip)->first();
            return view('pages.dosen.penilaian-ujian-skripsi.penilaian', [
                'penilaian' => $penilaian
            ]);
        } else {
            return redirect()->route('forbidden');
        }
    }

    public function storeNilai(PenilaianUjianRequest $request, $id)
    {
        $data = $request->all();

        $penilai = Dosen::where('id_dosen', '=', Auth::user()->dosen_id)->first();

        $penilaian = PenilaianUjianSkripsi::find($id);

        $us = UjianSkripsi::find($penilaian->ujian_skripsi_id);

        $nilaiMutu = NilaiMutu::where('nim', '=', $us->nim_skripsi)->first();

        if ($penilai->nip == $us->pembimbing1) {
            $us->update([
                'nilai_pembimbing1' => $request->total_nilai,
            ]);
            $nilaiMutu->update([
                'nilai_p1_ujian_skripsi' => $us->nilai_pembimbing1,
            ]);
        } elseif ($penilai->nip == $us->pembimbing2) {
            $us->update([
                'nilai_pembimbing2' => $request->total_nilai,
            ]);
            $nilaiMutu->update([
                'nilai_p2_ujian_skripsi' => $us->nilai_pembimbing2,
            ]);
        }

        if ($nilaiMutu->nilai_p1_ujian_skripsi != null && $nilaiMutu->nilai_p2_ujian_skripsi != null) {
            $rerataNilaiUs = (str_replace(',', '.', $us->nilai_pembimbing1) + str_replace(',', '.', $us->nilai_pembimbing2)) / 2;
            $rerataNilaiUs = number_format(floatval(str_replace(',', '.', $rerataNilaiUs)), 2, ',', '');
            $nilaiMutu->update([
                'rerata_nilai_ujian_skripsi' => $rerataNilaiUs
            ]);
        }

        if ($request->tgl_skripsi != null) {
            $us->update([
                'tgl_skripsi' => $request->tgl_skripsi,
            ]);
        }

        $penilaian->update($data);

        Alert::success('Berhasil!', 'Berhasil Menambahkan Nilai');
        return redirect()->route('dosen.ujian-skripsi.index');
    }
}
