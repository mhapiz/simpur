<?php

namespace App\Http\Controllers\Dosen\Ujian;

use App\Http\Controllers\Controller;
use App\Models\Dosen;
use App\Models\UjianSkripsi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert as Alert;

class UjianSkripsiController extends Controller
{
    public function index()
    {
        $dosen = Dosen::find(Auth::user()->dosen_id);

        if (request()->q !== null) {
            $q = request()->q;

            $dataUjianSkripsi = UjianSkripsi::join('mahasiswas', 'ujian_skripsis.nim_skripsi', '=', 'mahasiswas.nim')
                ->join('nilai_mutus', 'ujian_skripsis.nim_skripsi', '=', 'nilai_mutus.nim')
                ->where([['ujian_skripsis.nim_skripsi', 'LIKE', '%' . $q . '%'], ['pembimbing1', '=', $dosen->nip], ['nilai_mutus.rerata_nilai_hasil_penelitian', '!=', null]])
                ->orWhere([['mahasiswas.nama_mahasiswa', 'LIKE', '%' . $q . '%'], ['pembimbing1', '=', $dosen->nip], ['nilai_mutus.rerata_nilai_hasil_penelitian', '!=', null]])
                ->orWhere([['ujian_skripsis.nim_skripsi', 'LIKE', '%' . $q . '%'], ['pembimbing2', '=', $dosen->nip], ['nilai_mutus.rerata_nilai_hasil_penelitian', '!=', null]])
                ->orWhere([['mahasiswas.nama_mahasiswa', 'LIKE', '%' . $q . '%'], ['pembimbing2', '=', $dosen->nip], ['nilai_mutus.rerata_nilai_hasil_penelitian', '!=', null]])
                ->orderBy('nilai_mutus.rerata_nilai_ujian_skripsi', 'ASC')
                ->select('*')
                ->paginate(6);

            return view('pages.dosen.ujian-skripsi.index', [
                'dataUjianSkripsi' => $dataUjianSkripsi
            ]);
        } else {
            $dataUjianSkripsi = UjianSkripsi::join('nilai_mutus', 'ujian_skripsis.nim_skripsi', '=', 'nilai_mutus.nim')
                ->where([['pembimbing1', '=', $dosen->nip], ['nilai_mutus.rerata_nilai_hasil_penelitian', '!=', null]])
                ->orWhere([['pembimbing2', '=', $dosen->nip], ['nilai_mutus.rerata_nilai_hasil_penelitian', '!=', null]])
                ->with('mahasiswa')
                ->orderBy('nilai_mutus.rerata_nilai_ujian_skripsi', 'ASC')
                ->select('*')
                ->paginate(6);

            return view('pages.dosen.ujian-skripsi.index', [
                'dataUjianSkripsi' => $dataUjianSkripsi
            ]);
        }
    }

    public function indexBelumDinilai()
    {
        $dosen = Dosen::find(Auth::user()->dosen_id);

        if (request()->q !== null) {
            $q = request()->q;

            $dataUjianSkripsi = UjianSkripsi::join('mahasiswas', 'ujian_skripsis.nim_skripsi', '=', 'mahasiswas.nim')
                ->join('nilai_mutus', 'ujian_skripsis.nim_skripsi', '=', 'nilai_mutus.nim')
                ->where([['ujian_skripsis.nim_skripsi', 'LIKE', '%' . $q . '%'], ['pembimbing1', '=', $dosen->nip], ['nilai_mutus.rerata_nilai_hasil_penelitian', '!=', null], ['ujian_skripsis.nilai_pembimbing1', '=', null]])
                ->orWhere([['mahasiswas.nama_mahasiswa', 'LIKE', '%' . $q . '%'], ['pembimbing1', '=', $dosen->nip], ['nilai_mutus.rerata_nilai_hasil_penelitian', '!=', null], ['ujian_skripsis.nilai_pembimbing1', '=', null]])
                ->orWhere([['ujian_skripsis.nim_skripsi', 'LIKE', '%' . $q . '%'], ['pembimbing2', '=', $dosen->nip], ['nilai_mutus.rerata_nilai_hasil_penelitian', '!=', null], ['ujian_skripsis.nilai_pembimbing2', '=', null]])
                ->orWhere([['mahasiswas.nama_mahasiswa', 'LIKE', '%' . $q . '%'], ['pembimbing2', '=', $dosen->nip], ['nilai_mutus.rerata_nilai_hasil_penelitian', '!=', null], ['ujian_skripsis.nilai_pembimbing2', '=', null]])
                ->orderBy('ujian_skripsis.created_at', 'DESC')
                ->select('*')
                ->paginate(6);

            return view('pages.dosen.ujian-skripsi.index', [
                'dataUjianSkripsi' => $dataUjianSkripsi
            ]);
        } else {
            $dataUjianSkripsi = UjianSkripsi::join('nilai_mutus', 'ujian_skripsis.nim_skripsi', '=', 'nilai_mutus.nim')
                ->where([['pembimbing1', '=', $dosen->nip], ['nilai_mutus.rerata_nilai_hasil_penelitian', '!=', null], ['ujian_skripsis.nilai_pembimbing1', '=', null]])
                ->orWhere([['pembimbing2', '=', $dosen->nip], ['nilai_mutus.rerata_nilai_hasil_penelitian', '!=', null], ['ujian_skripsis.nilai_pembimbing2', '=', null]])
                ->with('mahasiswa')
                ->orderBy('ujian_skripsis.created_at', 'DESC')
                ->select('*')
                ->paginate(6);

            return view('pages.dosen.ujian-skripsi.index', [
                'dataUjianSkripsi' => $dataUjianSkripsi
            ]);
        }
    }
}
