<?php

namespace App\Http\Controllers\Dosen\UjianKomprehensif;

use App\Http\Controllers\Controller;
use App\Models\Dosen;
use App\Models\NilaiMutu;
use App\Models\PenilaianUjianKomprehensif;
use App\Models\UjianKomprehensif;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert as Alert;

class PenilaianUjianKomprehensifController extends Controller
{
    public function show($id)
    {
        $dosen = Dosen::find(Auth::user()->dosen_id);

        $uk = PenilaianUjianKomprehensif::where([['ujian_komprehensif_id', '=', $id], ['penilai', '=', $dosen->nip]])->with(['mahasiswa', 'uk'])->first();

        return view('pages.dosen.penilaian-ujian-komprehensif.show', [
            'uk' => $uk
        ]);
    }

    public function penilaian($id)
    {
        $penilai = Dosen::where('id_dosen', '=', Auth::user()->dosen_id)->first();
        $ujianKomprehensif = UjianKomprehensif::find($id);

        $penilaianUjianKompre = PenilaianUjianKomprehensif::where('ujian_komprehensif_id', '=', $id)->get();


        if (PenilaianUjianKomprehensif::where('ujian_komprehensif_id', '=', $id)->where('penilai', '=', $penilai->nip)->first()) {

            $penilaian = PenilaianUjianKomprehensif::with(['mahasiswa', 'uk', 'dosen'])->where('ujian_komprehensif_id', '=', $id)->where('penilai', '=', $penilai->nip)->first();
            return view('pages.dosen.penilaian-ujian-komprehensif.penilaian', [
                'penilaian' => $penilaian
            ]);
        } else {
            if ($penilaianUjianKompre->count() == 4) {
                Alert::error('Gagal', 'Kuota Dosen Penguji Sudah Terpenuhi');
                return redirect()->route('dosen.ujian-kompre.index');
            } else {
                PenilaianUjianKomprehensif::create([
                    'nim_skripsi' => $ujianKomprehensif->nim_skripsi,
                    'ujian_komprehensif_id' => $ujianKomprehensif->id_ujian_komprehensif,
                    'penilai' => $penilai->nip,
                ]);

                if ($ujianKomprehensif->penguji3 == null) {
                    $ujianKomprehensif->update([
                        'penguji3' => $penilai->nip
                    ]);
                } elseif ($ujianKomprehensif->penguji4 == null) {
                    $ujianKomprehensif->update([
                        'penguji4' => $penilai->nip
                    ]);
                }

                $penilaian = PenilaianUjianKomprehensif::with(['mahasiswa', 'uk', 'dosen'])->where('ujian_komprehensif_id', '=', $id)->where('penilai', '=', $penilai->nip)->first();

                return view('pages.dosen.penilaian-ujian-komprehensif.penilaian', [
                    'penilaian' => $penilaian
                ]);
            }
        }
    }

    public function storeNilai(Request $request, $id)
    {
        $data = $request->all();

        $penilai = Dosen::where('id_dosen', '=', Auth::user()->dosen_id)->first();

        $penilaian = PenilaianUjianKomprehensif::find($id);

        $uk = UjianKomprehensif::find($penilaian->ujian_komprehensif_id);

        $nilaiMutu = NilaiMutu::where('nim', '=', $uk->nim_skripsi)->first();

        if ($penilai->nip == $uk->penguji1) {
            $uk->update([
                'nilai_penguji1' => $request->total_nilai,
            ]);
        } elseif ($penilai->nip == $uk->penguji2) {
            $uk->update([
                'nilai_penguji2' => $request->total_nilai,
            ]);
        } elseif ($penilai->nip == $uk->penguji3) {
            $uk->update([
                'nilai_penguji3' => $request->total_nilai,
            ]);
        } elseif ($penilai->nip == $uk->penguji4) {
            $uk->update([
                'nilai_penguji4' => $request->total_nilai,
            ]);
        }

        if ($uk->nilai_penguji1 != null && $uk->nilai_penguji2 != null && $uk->nilai_penguji3 != null && $uk->nilai_penguji4 != null) {
            $rerataNilaiKompre = (str_replace(',', '.', $uk->nilai_penguji1) + str_replace(',', '.', $uk->nilai_penguji2) + str_replace(',', '.', $uk->nilai_penguji3) + str_replace(',', '.', $uk->nilai_penguji4)) / 4;
            $rerataNilaiKompre = number_format(floatval(str_replace(',', '.', $rerataNilaiKompre)), 2, ',', '');
            $nilaiMutu->update([
                'nilai_komprehensif' => $rerataNilaiKompre
            ]);
        }

        if ($nilaiMutu->nilai_komprehensif != null) {
            $nilaiSkripsi =  ((str_replace(',', '.', $nilaiMutu->rerata_nilai_ujian_skripsi) * 4) + str_replace(',', '.', $nilaiMutu->nilai_komprehensif) + str_replace(',', '.', $nilaiMutu->rerata_nilai_seminar)) / 6;
            $nilaiSkripsi = number_format(floatval(str_replace(',', '.', $nilaiSkripsi)), 2, ',', '');

            $nilaiSkripsi = round(str_replace(',', '.', $nilaiSkripsi));

            if ($nilaiSkripsi >= 80) {
                $mutu = 'A';
            } elseif ($nilaiSkripsi >= 77 && $nilaiSkripsi < 80) {
                $mutu = 'A-';
            } elseif ($nilaiSkripsi >= 75 && $nilaiSkripsi < 77) {
                $mutu = 'B+';
            } elseif ($nilaiSkripsi >= 70 && $nilaiSkripsi < 75) {
                $mutu = 'B';
            } else {
                $mutu = 'C';
            }

            $nilaiMutu->update([
                'nilai_skripsi' => $nilaiSkripsi,
                'nilai_mutu' => $mutu

            ]);
        }


        if ($request->tgl_skripsi != null) {
            $uk->update([
                'tgl_skripsi' => $request->tgl_skripsi,
            ]);
        }

        $penilaian->update($data);

        Alert::success('Berhasil!', 'Berhasil Menambahkan Nilai');
        return redirect()->route('dosen.ujian-kompre.index');
    }
}
