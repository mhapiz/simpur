<?php

namespace App\Http\Controllers\Dosen\UjianKomprehensif;

use App\Http\Controllers\Controller;
use App\Models\Dosen;
use App\Models\UjianKomprehensif;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Riskihajar\Terbilang\Facades\Terbilang as Terbilang;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class UjianKomprehensifController extends Controller
{
    public function index()
    {
        $dosen = Dosen::find(Auth::user()->dosen_id);

        if (request()->q !== null) {
            $q = request()->q;

            $dataUjianKompre = UjianKomprehensif::join('mahasiswas', 'ujian_komprehensifs.nim_skripsi', '=', 'mahasiswas.nim')
                ->join('nilai_mutus', 'ujian_komprehensifs.nim_skripsi', '=', 'nilai_mutus.nim')
                ->where([['ujian_komprehensifs.nim_skripsi', 'LIKE', '%' . $q . '%'], ['nilai_mutus.rerata_nilai_ujian_skripsi', '!=', null], ['nilai_mutus.rerata_nilai_hasil_penelitian', '!=', null], ['penguji1', '=', $dosen->nip]])
                ->orWhere([['mahasiswas.nama_mahasiswa', 'LIKE', '%' . $q . '%'], ['nilai_mutus.rerata_nilai_ujian_skripsi', '!=', null], ['nilai_mutus.rerata_nilai_hasil_penelitian', '!=', null], ['penguji1', '=', $dosen->nip]])
                ->orWhere([['ujian_komprehensifs.nim_skripsi', 'LIKE', '%' . $q . '%'], ['nilai_mutus.rerata_nilai_ujian_skripsi', '!=', null], ['nilai_mutus.rerata_nilai_hasil_penelitian', '!=', null], ['penguji2', '=', $dosen->nip]])
                ->orWhere([['mahasiswas.nama_mahasiswa', 'LIKE', '%' . $q . '%'], ['nilai_mutus.rerata_nilai_ujian_skripsi', '!=', null], ['nilai_mutus.rerata_nilai_hasil_penelitian', '!=', null], ['penguji2', '=', $dosen->nip]])
                ->orWhere([['ujian_komprehensifs.nim_skripsi', 'LIKE', '%' . $q . '%'], ['nilai_mutus.rerata_nilai_ujian_skripsi', '!=', null], ['nilai_mutus.rerata_nilai_hasil_penelitian', '!=', null], ['penguji3', '=', $dosen->nip]])
                ->orWhere([['mahasiswas.nama_mahasiswa', 'LIKE', '%' . $q . '%'], ['nilai_mutus.rerata_nilai_ujian_skripsi', '!=', null], ['nilai_mutus.rerata_nilai_hasil_penelitian', '!=', null], ['penguji3', '=', $dosen->nip]])
                ->orWhere([['ujian_komprehensifs.nim_skripsi', 'LIKE', '%' . $q . '%'], ['nilai_mutus.rerata_nilai_ujian_skripsi', '!=', null], ['nilai_mutus.rerata_nilai_hasil_penelitian', '!=', null], ['penguji4', '=', $dosen->nip]])
                ->orWhere([['mahasiswas.nama_mahasiswa', 'LIKE', '%' . $q . '%'], ['nilai_mutus.rerata_nilai_ujian_skripsi', '!=', null], ['nilai_mutus.rerata_nilai_hasil_penelitian', '!=', null], ['penguji4', '=', $dosen->nip]])
                ->orderBy('nilai_mutus.nilai_komprehensif', 'ASC')
                ->select('*')
                ->paginate(6);

            return view('pages.dosen.ujian-komprehensif.index', [
                'dataUjianKompre' => $dataUjianKompre
            ]);
        } else {

            $dataUjianKompre = UjianKomprehensif::join('nilai_mutus', 'ujian_komprehensifs.nim_skripsi', '=', 'nilai_mutus.nim')
                ->where([['nilai_mutus.rerata_nilai_hasil_penelitian', '!=', null], ['nilai_mutus.rerata_nilai_ujian_skripsi', '!=', null], ['penguji1', '=', $dosen->nip]])
                ->orWhere([['nilai_mutus.rerata_nilai_hasil_penelitian', '!=', null], ['nilai_mutus.rerata_nilai_ujian_skripsi', '!=', null], ['penguji2', '=', $dosen->nip]])
                ->orWhere([['nilai_mutus.rerata_nilai_hasil_penelitian', '!=', null], ['nilai_mutus.rerata_nilai_ujian_skripsi', '!=', null], ['penguji3', '=', $dosen->nip]])
                ->orWhere([['nilai_mutus.rerata_nilai_hasil_penelitian', '!=', null], ['nilai_mutus.rerata_nilai_ujian_skripsi', '!=', null], ['penguji4', '=', $dosen->nip]])
                ->orderBy('nilai_mutus.nilai_komprehensif', 'ASC')
                ->select('*')
                ->paginate(6);

            return view('pages.dosen.ujian-komprehensif.index', [
                'dataUjianKompre' => $dataUjianKompre
            ]);
        }
    }

    public function indexBelumDinilai()
    {
        $dosen = Dosen::find(Auth::user()->dosen_id);

        if (request()->q !== null) {
            $q = request()->q;

            $dataUjianKompre = UjianKomprehensif::join('mahasiswas', 'ujian_komprehensifs.nim_skripsi', '=', 'mahasiswas.nim')
                ->join('nilai_mutus', 'ujian_komprehensifs.nim_skripsi', '=', 'nilai_mutus.nim')
                //
                ->where([['ujian_komprehensifs.nim_skripsi', 'LIKE', '%' . $q . '%'], ['nilai_mutus.rerata_nilai_ujian_skripsi', '!=', null], ['nilai_mutus.rerata_nilai_hasil_penelitian', '!=', null], ['penguji1', '=', $dosen->nip], ['nilai_penguji1', '=', null]])
                ->orWhere([['mahasiswas.nama_mahasiswa', 'LIKE', '%' . $q . '%'], ['nilai_mutus.rerata_nilai_ujian_skripsi', '!=', null], ['nilai_mutus.rerata_nilai_hasil_penelitian', '!=', null], ['penguji1', '=', $dosen->nip], ['nilai_penguji1', '=', null]])
                //
                ->orWhere([['ujian_komprehensifs.nim_skripsi', 'LIKE', '%' . $q . '%'], ['nilai_mutus.rerata_nilai_ujian_skripsi', '!=', null], ['nilai_mutus.rerata_nilai_hasil_penelitian', '!=', null], ['penguji2', '=', $dosen->nip], ['nilai_penguji2', '=', null]])
                ->orWhere([['mahasiswas.nama_mahasiswa', 'LIKE', '%' . $q . '%'], ['nilai_mutus.rerata_nilai_ujian_skripsi', '!=', null], ['nilai_mutus.rerata_nilai_hasil_penelitian', '!=', null], ['penguji2', '=', $dosen->nip], ['nilai_penguji2', '=', null]])
                //
                ->orWhere([['ujian_komprehensifs.nim_skripsi', 'LIKE', '%' . $q . '%'], ['nilai_mutus.rerata_nilai_ujian_skripsi', '!=', null], ['nilai_mutus.rerata_nilai_hasil_penelitian', '!=', null], ['penguji3', '=', $dosen->nip], ['nilai_penguji3', '=', null]])
                ->orWhere([['mahasiswas.nama_mahasiswa', 'LIKE', '%' . $q . '%'], ['nilai_mutus.rerata_nilai_ujian_skripsi', '!=', null], ['nilai_mutus.rerata_nilai_hasil_penelitian', '!=', null], ['penguji3', '=', $dosen->nip], ['nilai_penguji3', '=', null]])
                //
                ->orWhere([['ujian_komprehensifs.nim_skripsi', 'LIKE', '%' . $q . '%'], ['nilai_mutus.rerata_nilai_ujian_skripsi', '!=', null], ['nilai_mutus.rerata_nilai_hasil_penelitian', '!=', null], ['penguji4', '=', $dosen->nip], ['nilai_penguji4', '=', null]])
                ->orWhere([['mahasiswas.nama_mahasiswa', 'LIKE', '%' . $q . '%'], ['nilai_mutus.rerata_nilai_ujian_skripsi', '!=', null], ['nilai_mutus.rerata_nilai_hasil_penelitian', '!=', null], ['penguji4', '=', $dosen->nip], ['nilai_penguji4', '=', null]])
                //
                ->orderBy('ujian_komprehensifs.created_at', 'DESC')
                ->select('*')
                ->paginate(6);

            return view('pages.dosen.ujian-komprehensif.index', [
                'dataUjianKompre' => $dataUjianKompre
            ]);
        } else {

            $dataUjianKompre = UjianKomprehensif::join('nilai_mutus', 'ujian_komprehensifs.nim_skripsi', '=', 'nilai_mutus.nim')
                ->where([['nilai_mutus.rerata_nilai_hasil_penelitian', '!=', null], ['nilai_mutus.rerata_nilai_ujian_skripsi', '!=', null], ['penguji1', '=', $dosen->nip], ['nilai_penguji1', '=', null]])
                ->orWhere([['nilai_mutus.rerata_nilai_hasil_penelitian', '!=', null], ['nilai_mutus.rerata_nilai_ujian_skripsi', '!=', null], ['penguji2', '=', $dosen->nip], ['nilai_penguji2', '=', null]])
                ->orWhere([['nilai_mutus.rerata_nilai_hasil_penelitian', '!=', null], ['nilai_mutus.rerata_nilai_ujian_skripsi', '!=', null], ['penguji3', '=', $dosen->nip], ['nilai_penguji3', '=', null]])
                ->orWhere([['nilai_mutus.rerata_nilai_hasil_penelitian', '!=', null], ['nilai_mutus.rerata_nilai_ujian_skripsi', '!=', null], ['penguji4', '=', $dosen->nip], ['nilai_penguji4', '=', null]])
                ->select('*')
                ->orderBy('ujian_komprehensifs.created_at', 'DESC')
                ->paginate(6);

            return view('pages.dosen.ujian-komprehensif.index', [
                'dataUjianKompre' => $dataUjianKompre
            ]);
        }
    }

    public function printBeritaAcara(Request $request, $id)
    {
        $request->validate([
            'semester' => 'required'
        ]);
        $uk = UjianKomprehensif::with(['mahasiswa', 'nilaimutu'])->find($id);

        if ($uk->nilai_penguji1 != null && $uk->nilai_penguji2 != null && $uk->nilai_penguji3 != null && $uk->nilai_penguji4 != null) {

            if ($uk->tgl_skripsi != null) {

                $nilaiSkripsi = str_replace(',', '.', $uk->nilaimutu->nilai_skripsi);
                $hariSkripsi = Carbon::parse($uk->tgl_skripsi)->isoFormat('dddd');
                $tglSkripsi = Terbilang::make(date('j', strtotime($uk->tgl_skripsi)));
                $bulanSkripsi = Carbon::parse($uk->tgl_skripsi)->isoFormat('MMMM');
                $tahunSkripsi = Terbilang::make(date('Y', strtotime($uk->tgl_skripsi)));

                $uk['nilai_terbilang'] = Terbilang::make($nilaiSkripsi);
                $uk['hariSkripsi'] = $hariSkripsi;
                $uk['tglSkripsi'] = $tglSkripsi;
                $uk['bulanSkripsi'] = $bulanSkripsi;
                $uk['tahunSkripsi'] = $tahunSkripsi;
                $uk['jam'] = date('h:i', strtotime($uk->tgl_skripsi));
                $uk['semester'] = $request->semester;

                $pdf = PDF::loadview('print.ujian-kompre', [
                    'uk' => $uk
                ])->setPaper(array(0, 0, 609.4488, 935.433), 'portrait');

                return $pdf->download('berita acara komprehensif - ' . $uk->mahasiswa->nama_mahasiswa . '.pdf');
            } else {
                Alert::error('Gagal!', 'Tanggal Skripsi Belum Ditentukan');
                return redirect()->route('dosen.ujian-kompre.index');
            }
        } else {
            Alert::error('Gagal!', 'Nilai Penguji Belum Lengkap');
            return redirect()->route('dosen.ujian-kompre.index');
        }
    }
}
