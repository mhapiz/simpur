<?php

namespace App\Http\Controllers\Profil;

use App\Http\Controllers\Controller;
use App\Models\Dosen;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use RealRashid\SweetAlert\Facades\Alert as Alert;


class ProfilController extends Controller
{
    public function index()
    {
        $loggedInUser = Auth::user();
        if ($loggedInUser->role == 'SUPERADMIN') {
            $user = User::find($loggedInUser->id_user);
        } elseif ($loggedInUser->role == 'ADMIN') {
            $user = User::find($loggedInUser->id_user);
        } elseif ($loggedInUser->role == 'DOSEN') {
            $user = User::with('Dosen')->find($loggedInUser->id_user);
        }

        // return $user;
        // die;
        return view('pages.profil.index', [
            'user' => $user
        ]);
    }

    public function editProfile()
    {
        $loggedInUser = Auth::user();
        if ($loggedInUser->role == 'SUPERADMIN') {
            $user = User::find($loggedInUser->id_user);
        } elseif ($loggedInUser->role == 'ADMIN') {
            $user = User::find($loggedInUser->id_user);
        } elseif ($loggedInUser->role == 'DOSEN') {
            $user = User::with('Dosen')->find($loggedInUser->id_user);
        }

        return view('pages.profil.edit', [
            'user' => $user
        ]);
    }

    public function updateProfilAdmin(Request $request, $id)
    {
        $user = User::find($id);
        $data = $request->validate([
            'username' => 'required',
        ]);

        if (!$request->password) {
            $user->update([
                'identity' => $data['username']
            ]);
        } else {
            $user->update([
                'identity' => $data['username'],
                'password' => Hash::make($request->password),
            ]);
        }

        Auth::login($user);

        Alert::success('Berhasil !', 'Data Anda Berhasil Diperbarui');
        return redirect()->route('myProfile.index');
    }

    public function updateProfileDosen(Request $request, $id)
    {
        $user = User::find($id);
        $dataDosen = Dosen::find($user->dosen_id);
        $data = $request->validate([
            'nama' => 'required',
            'jabatan' => 'required',
        ]);

        if (!$request->password) {
            $dataDosen->update([
                'nama_dosen' => $data['nama'],
                'jabatan' => $data['jabatan'],
            ]);
        } else {
            $dataDosen->update([
                'nama_dosen' => $data['nama'],
                'jabatan' => $data['jabatan'],
            ]);
            $user->update([
                'password' => Hash::make($request->password),
            ]);
        }

        Auth::login($user);

        Alert::success('Berhasil !', 'Data Anda Berhasil Diperbarui!');
        return redirect()->route('myProfile.index');
    }
}
