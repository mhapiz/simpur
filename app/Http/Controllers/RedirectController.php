<?php
    namespace App\Http\Controllers;

    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Auth;

    class RedirectController extends Controller
    {
        public function redirect()
        {
            if (Auth::user()->role == 'SUPERADMIN') {
                return redirect()->route('super-admin.index');
            } elseif (Auth::user()->role == 'ADMIN') {
                return redirect()->route('admin.index');
            } elseif (Auth::user()->role == 'DOSEN') {
                return redirect()->route('dosen.index');
            }
        }

        public function forbidden()
        {
            return view('pages.special-page.403');
        }
    }
