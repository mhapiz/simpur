<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use App\Models\Dosen;
use App\Models\SeminarHasilPenelitian;
use App\Models\SeminarUsulanPenelitian;
use App\Models\UjianKomprehensif;
use App\Models\UjianSkripsi;
use App\Models\User;
use Illuminate\Http\Request;

class LogAktivitasController extends Controller
{
    public function index()
    {
        $logsup = SeminarUsulanPenelitian::with('mahasiswa')->latest('updated_at')->limit(5)->get();
        $logshp = SeminarHasilPenelitian::with('mahasiswa')->latest('updated_at')->limit(5)->get();
        $logus = UjianSkripsi::with('mahasiswa')->latest('updated_at')->limit(5)->get();
        $loguk = UjianKomprehensif::with('mahasiswa')->latest('updated_at')->limit(5)->get();
        $logUser = User::latest('updated_at')->limit(5)->get();
        $logDosen = Dosen::latest('updated_at')->limit(5)->get();
        return view('pages.superadmin.log.index', [
            'logsup' => $logsup,
            'logshp' => $logshp,
            'logus' => $logus,
            'loguk' => $loguk,
            'logUser' => $logUser,
            'logDosen' => $logDosen,
        ]);
    }
}
