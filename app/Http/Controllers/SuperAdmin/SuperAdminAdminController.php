<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use App\Imports\DosenImport;
use App\Models\Dosen;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables as DataTables;
use RealRashid\SweetAlert\Facades\Alert as Alert;

class SuperAdminAdminController extends Controller
{
    public function index()
    {
        return view('pages.superadmin.user-manage.admin.index');
    }

    public function ajaxGetAdmin()
    {
        $admins = User::select(['id_user', 'identity', 'role'])->where('role', '=', 'ADMIN')->latest();
        return DataTables::eloquent($admins)
            ->addIndexColumn()
            ->addColumn('aksi', function ($row) {
                $editUrl = route('super-admin.user-manage.admin.edit', $row->id_user);
                $deleteUrl = route('super-admin.user-manage.admin.destroy', $row->id_user);
                return view('modules.backend._formActions', compact('editUrl', 'deleteUrl'));
            })
            ->editColumn('role', function ($row) {
                return 'ADMIN';
            })
            ->rawColumns(['aksi'])
            ->make(true);
    }

    public function createAdmin()
    {
        return view('pages.superadmin.user-manage.admin.create');
    }

    public function storeAdmin(Request $request)
    {
        $request->validate(
            [
                'username' => 'required|unique:users,identity',
                'password' => 'required|min:6',
                'password2' => 'same:password',
            ],
            [
                'password2.same' => 'Kata Sandi tidak sesuai',
            ]
        );

        User::create([
            'identity' => $request->username,
            'password' => Hash::make($request->password),
            'role' => 'ADMIN',
        ]);

        Alert::success('Sukses', 'Data Admin Berhasil Dtambahkan');
        return redirect()->route('super-admin.user-manage.admin.index');
    }

    public function editAdmin($id)
    {
        $user = User::find($id);
        return view('pages.superadmin.user-manage.admin.edit', [
            'user' => $user
        ]);
    }

    public function updateAdmin(Request $request, $id)
    {
        $theUser = User::find($id);
        $data = $request->validate([
            'identity' => 'required',
        ], [
            'identity.required' => 'Harap Isi Kolom Username',
        ]);

        $theUser->update($data);

        if ($request->password != null) {
            $theUser->update([
                'password' => Hash::make($request->password)
            ]);
        }

        Alert::success('Sukses', 'Data Admin Berhasil Diperbarui');
        return redirect()->route('super-admin.user-manage.admin.index');
    }

    public function destroyAdmin($id)
    {
        $theUser = User::find($id);

        $theUser->delete();

        Alert::warning('Dihapus', 'Data Admin Berhasil Dihapus');
        return redirect()->route('super-admin.user-manage.admin.index');
    }
}
