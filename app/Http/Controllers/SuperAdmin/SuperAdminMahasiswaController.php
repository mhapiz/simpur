<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use App\Models\Mahasiswa;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;
use RealRashid\SweetAlert\Facades\Alert as Alert;

class SuperAdminMahasiswaController extends Controller
{
    public function index()
    {
        return view('pages.superadmin.user-manage.mahasiswa.index');
    }

    public function ajaxGetMahasiswa()
    {
        $mhs = Mahasiswa::select(['id_mahasiswa', 'nim', 'nama_mahasiswa', 'minat', 'tahun_angkatan'])->latest();

        return DataTables::eloquent($mhs)
            ->addIndexColumn()
            ->addColumn('aksi', function ($row) {
                $editUrl = route('admin.user-manage.mahasiswa.edit', $row->id_mahasiswa);
                $deleteUrl = route('admin.user-manage.mahasiswa.destroy', $row->id_mahasiswa);
                return view('modules.backend._formActions', compact('editUrl', 'deleteUrl'));
            })
            ->rawColumns(['aksi'])
            ->make(true);
    }

    public function createMahasiswa()
    {
        return view('pages.superadmin.user-manage.mahasiswa.create');
    }

    public function storeMahasiswa(Request $request)
    {
        $data = $request->validate(
            [
                'nim' => 'required|unique:mahasiswas,nim',
                'nama_mahasiswa' => 'required',
                'minat' => 'required',
                'tahun_angkatan' => 'required',
            ],
            [
                'nim.required' => 'Kolom NIM Harus Diisi',
                'nim.unique' => 'NIM Sudah Terdaftar',
                'nama_mahasiswa.required' => 'Kolom Nama Harus Diisi',
                'minat.required' => 'Harap Pilih Minat Mahasiswa',
                'tahun_angkatan.required' => 'Kolom Tahun Angkatan Harus Diisi',
            ]
        );

        $mhsAdded = Mahasiswa::create($data);

        User::create([
            'identity' => $mhsAdded->nim,
            'password' => Hash::make($mhsAdded->nim),
            'role' => 'MAHASISWA',
            'mahasiswa_id' => $mhsAdded->id_mahasiswa,
        ]);

        Alert::success('Berhasil!', 'Data Mahasiswa Berhasil Ditambahkan');
        return redirect()->route('super-admin.user-manage.mahasiswa.index');
    }

    public function editMahasiswa($id)
    {
        return 'ini edit';
    }

    public function destroyMahasiswa($id)
    {
        return 'ini edit';
    }
}
