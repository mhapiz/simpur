<?php

namespace App\Http\Livewire\SeminarUsulanPenelitian;

use App\Models\SeminarUsulanPenelitian;
use Livewire\Component;

class Show extends Component
{
    public function render()
    {
        // $dataSUP = SeminarUsulanPenelitian::with('mahasiswa')->latest()->get();
        // return view('livewire.seminar-usulan-penelitian.show', [
        //     'dataSUP' => $dataSUP
        // ]);
        return view('livewire.seminar-usulan-penelitian.show');
    }
}
