<?php

namespace App\Http\Middleware;

use App\Models\Dosen;
use App\Models\PenilaianSeminarUsulanPenelitian;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Pembimbing
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $penilai = Dosen::where('id_dosen', '=', Auth::user()->dosen_id)->first();
        if (PenilaianSeminarUsulanPenelitian::where('penilai', '=', $penilai->nip)->first()) {
            return $next($request);
        }

        return 'KADA TAMBUS NAH';
        die;
    }
}
