<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PenilaianPenyanggahSeminarUsulanPenelitianRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nilai1' => 'required',
            'hasil1' => 'required',
            'nilai2' => 'required',
            'hasil2' => 'required',
            'nilai3' => 'required',
            'hasil3' => 'required',
            'nilai4' => 'required',
            'hasil4' => 'required',
            'total_nilai' => 'required',
        ];
    }
}
