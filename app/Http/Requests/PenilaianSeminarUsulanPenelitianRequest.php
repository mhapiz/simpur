<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PenilaianSeminarUsulanPenelitianRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nilai1a1' => 'required',
            'hasil1a1' => 'required',
            'nilai1b2' => 'required',
            'hasil1b2' => 'required',
            'nilai1b3' => 'required',
            'hasil1b3' => 'required',
            'nilai1b4' => 'required',
            'hasil1b4' => 'required',
            'nilai2c' => 'required',
            'hasil2c' => 'required',
            'nilai2d' => 'required',
            'hasil2d' => 'required',
            'nilai2e' => 'required',
            'hasil2e' => 'required',
            'nilai3' => 'required',
            'hasil3' => 'required',
            'nilai4' => 'required',
            'hasil4' => 'required',
            'total_nilai' => 'required',
        ];
    }
}
