<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PenilaianUjianRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nilai1' => 'required',
            'hasil1' => 'required',
            'nilai2a' => 'required',
            'hasil2a' => 'required',
            'nilai2b' => 'required',
            'hasil2b' => 'required',
            'nilai2c' => 'required',
            'hasil2c' => 'required',
            'nilai3' => 'required',
            'hasil3' => 'required',
            'total_nilai' => 'required',
        ];
    }
}
