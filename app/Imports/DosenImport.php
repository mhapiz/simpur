<?php

namespace App\Imports;

use App\Models\Dosen;
use App\Models\User;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class DosenImport implements ToCollection, WithHeadingRow
{
    public function collection(Collection $rows)
    {
        foreach ($rows as $row) {

            if ($row['nip'] != null) {
                if (!Dosen::where('nip', '=', $row['nip'])->first()) {
                    $dosen = Dosen::create([
                        'nama_dosen' => $row['nama'],
                        'nip' => trim($row['nip']),
                        'jabatan' => 'Dosen',
                        'updated_by' => Auth::user()->identity,
                    ]);

                    User::create([
                        'identity' => trim($row['nip']),
                        'password' => Hash::make(trim($row['nip'])),
                        'role' => 'DOSEN',
                        'dosen_id' => $dosen->id_dosen,
                        'updated_by' => Auth::user()->identity,
                    ]);
                }
            } else {
                return;
            }
        }
    }
}
