<?php

namespace App\Imports;

use App\Models\Mahasiswa;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class MahasiswaImport implements ToCollection, WithHeadingRow
{

    public function collection(Collection $rows)
    {
        foreach ($rows as $row) {

            if ($row['nim'] != null) {
                if (!Mahasiswa::where('nim', '=', $row['nim'])->first()) {
                    Mahasiswa::create([
                        'nim' => trim($row['nim']),
                        'nama_mahasiswa' => $row['nama'],
                        'minat' => $row['minat'],
                        'tahun_angkatan' => $row['tahunangkatan'],
                        'updated_by' => Auth::user()->identity,
                    ]);
                }
            } else {
                return;
            }
        }
    }
}
