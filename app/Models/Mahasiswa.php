<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mahasiswa extends Model
{
    use HasFactory;
    protected $table = 'mahasiswas';
    protected $primaryKey = 'id_mahasiswa';
    protected $fillable = [
        'nim', 'nama_mahasiswa', 'minat', 'tahun_angkatan', 'penyanggah_sup', 'penyanggah_shp'
    ];
}
