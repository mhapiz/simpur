<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NilaiMutu extends Model
{
    use HasFactory;

    protected $table = 'nilai_mutus';
    protected $primaryKey = 'id_nilai_mutu';

    protected $fillable = [
        'nim',

        'rerata_nilai_sup', 'rerata_nilai_penyanggah_sup', 'rerata_nilai_usulan_penelitian',

        'rerata_nilai_shp', 'rerata_nilai_penyanggah_shp', 'rerata_nilai_hasil_penelitian',

        'rerata_nilai_seminar',

        'nilai_p1_ujian_skripsi', 'nilai_p2_ujian_skripsi', 'rerata_nilai_ujian_skripsi',

        'nilai_komprehensif',

        'nilai_skripsi', 'nilai_mutu',

        'updated_by',
    ];
    public function mahasiswa()
    {
        return $this->belongsTo(Mahasiswa::class, 'nim', 'nim');
    }
}
