<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PenilaianPenyanggahSeminarUsulanPenelitian extends Model
{
    use HasFactory;
    protected $table = 'penilaian_penyanggah_seminar_usulan_penelitians';
    protected $primaryKey = 'id_penilaian_penyanggah_seminar_usulan_penelitian';
    protected $fillable = [
        'nim_seminaris', 'nim_penyanggah', 'seminar_usulan_penelitian_id', 'penilai',
        'nilai1', 'hasil1',
        'nilai2', 'hasil2',
        'nilai3', 'hasil3',
        'nilai4', 'hasil4',
        'nilai5', 'hasil5',
        'total_nilai'
    ];

    public function mahasiswa()
    {
        return $this->belongsTo(Mahasiswa::class, 'nim_seminaris', 'nim');
    }

    public function sup()
    {
        return $this->belongsTo(SeminarUsulanPenelitian::class,  'seminar_usulan_penelitian_id', 'id_seminar_usulan_penelitian');
    }

    public function dosen()
    {
        return $this->belongsTo(Dosen::class,  'penilai', 'nip');
    }
}
