<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PenilaianSeminarUsulanPenelitian extends Model
{
    use HasFactory;
    protected $table = 'penilaian_seminar_usulan_penelitians';
    protected $primaryKey = 'id_penilaian_seminar_usulan_penelitian';
    protected $fillable = [
        'nim_seminaris', 'seminar_usulan_penelitian_id', 'penilai',
        'nilai1a1', 'hasil1a1',
        'nilai1b2', 'hasil1b2',
        'nilai1b3', 'hasil1b3',
        'nilai1b4', 'hasil1b4',
        'nilai2c', 'hasil2c',
        'nilai2d', 'hasil2d',
        'nilai2e', 'hasil2e',
        'nilai3', 'hasil3',
        'nilai4', 'hasil4',
        'total_nilai'
    ];

    public function mahasiswa()
    {
        return $this->belongsTo(Mahasiswa::class, 'nim_seminaris', 'nim');
    }

    public function sup()
    {
        return $this->belongsTo(SeminarUsulanPenelitian::class,  'seminar_usulan_penelitian_id', 'id_seminar_usulan_penelitian');
    }

    public function dosen()
    {
        return $this->belongsTo(Dosen::class,  'penilai', 'nip');
    }
}
