<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PenilaianUjianKomprehensif extends Model
{
    use HasFactory;
    protected $table = 'penilaian_ujian_komprehensifs';
    protected $primaryKey = 'id_penilaian_ujian_komprehensif';
    protected $fillable = [
        'nim_skripsi', 'ujian_komprehensif_id', 'penilai',
        'nilai1a', 'hasil1a',
        'nilai1b', 'hasil1b',

        'nilai2a', 'hasil2a',
        'nilai2b', 'hasil2b',

        'nilai3a', 'hasil3a',
        'nilai3b', 'hasil3b',

        'nilai4', 'hasil4',

        'nilai5a', 'hasil5a',

        'total_nilai'
    ];

    public function mahasiswa()
    {
        return $this->belongsTo(Mahasiswa::class, 'nim_skripsi', 'nim');
    }

    public function uk()
    {
        return $this->belongsTo(UjianKomprehensif::class, 'ujian_komprehensif_id', 'id_ujian_komprehensif');
    }

    public function dosen()
    {
        return $this->belongsTo(Dosen::class, 'penilai', 'nip');
    }
}
