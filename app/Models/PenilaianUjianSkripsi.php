<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PenilaianUjianSkripsi extends Model
{
    use HasFactory;
    protected $table = 'penilaian_ujian_skripsis';
    protected $primaryKey = 'id_penilaian_ujian_skripsi';
    protected $fillable = [
        'nim_skripsi', 'ujian_skripsi_id', 'penilai',
        'nilai1', 'hasil1',
        'nilai2a', 'hasil2a',
        'nilai2b', 'hasil2b',
        'nilai2c', 'hasil2c',
        'nilai3', 'hasil3',
        'total_nilai'
    ];

    public function mahasiswa()
    {
        return $this->belongsTo(Mahasiswa::class, 'nim_skripsi', 'nim');
    }

    public function us()
    {
        return $this->belongsTo(UjianSkripsi::class, 'ujian_skripsi_id', 'id_ujian_skripsi');
    }

    public function dosen()
    {
        return $this->belongsTo(Dosen::class, 'penilai', 'nip');
    }
}
