<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SeminarHasilPenelitian extends Model
{
    use HasFactory;
    protected $table = 'seminar_hasil_penelitians';
    protected $primaryKey = 'id_seminar_hasil_penelitian';
    protected $fillable = [
        'nim_seminaris', 'tgl_seminar', 'judul',
        'pembimbing1', 'pembimbing2', 'nilai_pembimbing1', 'nilai_pembimbing2',
        'penyanggah1', 'penyanggah2', 'nilai_penyanggah1', 'nilai_penyanggah2',
        'updated_by'
    ];

    public function mahasiswa()
    {
        return $this->belongsTo(Mahasiswa::class, 'nim_seminaris', 'nim');
    }
}
