<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SeminarUsulanPenelitian extends Model
{
    use HasFactory;
    protected $table = 'seminar_usulan_penelitians';
    protected $primaryKey = 'id_seminar_usulan_penelitian';
    protected $fillable = [
        'nim_seminaris', 'tgl_seminar', 'judul', 'pembimbing1', 'pembimbing2', 'nilai_pembimbing1', 'nilai_pembimbing2', 'penyanggah1', 'penyanggah2', 'nilai_penyanggah1', 'nilai_penyanggah2',
        'updated_by'
    ];

    public function mahasiswa()
    {
        return $this->belongsTo(Mahasiswa::class, 'nim_seminaris', 'nim');
    }

    public function nilaiMutu()
    {
        return $this->hasOne(NilaiMutu::class, 'nim', 'nim_seminaris');
    }
}
