<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UjianKomprehensif extends Model
{
    use HasFactory;
    protected $table = 'ujian_komprehensifs';
    protected $primaryKey = 'id_ujian_komprehensif';
    protected $fillable = [
        'nim_skripsi', 'tgl_skripsi',
        'penguji1', 'nilai_penguji1',
        'penguji2', 'nilai_penguji2',
        'penguji3', 'nilai_penguji3',
        'penguji4', 'nilai_penguji4',
        'updated_by',
    ];

    public function mahasiswa()
    {
        return $this->belongsTo(Mahasiswa::class, 'nim_skripsi', 'nim');
    }

    public function penilaian()
    {
        return $this->hasMany(PenilaianUjianKomprehensif::class, 'ujian_komprehensif_id', 'id_ujian_komprehensif');
    }

    public function nilaimutu()
    {
        return $this->belongsTo(NilaiMutu::class, 'nim_skripsi', 'nim');
    }
}
