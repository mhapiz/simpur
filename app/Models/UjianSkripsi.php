<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UjianSkripsi extends Model
{
    use HasFactory;
    protected $table = 'ujian_skripsis';
    protected $primaryKey = 'id_ujian_skripsi';
    protected $fillable = [
        'nim_skripsi', 'tgl_skripsi', 'judul',
        'pembimbing1', 'pembimbing2', 'nilai_pembimbing1', 'nilai_pembimbing2', 'updated_by'
    ];
    public function mahasiswa()
    {
        return $this->belongsTo(Mahasiswa::class, 'nim_skripsi', 'nim');
    }
}
