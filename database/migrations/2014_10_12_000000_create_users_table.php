<?php

use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id('id_user');
            $table->string('identity')->unique();
            $table->string('password');
            $table->string('role')->default('MAHASISWA');
            $table->unsignedBigInteger('dosen_id')->unique()->nullable();
            $table->unsignedBigInteger('mahasiswa_id')->unique()->nullable();
            $table->rememberToken();
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });

        DB::table('users')->insert([
            'identity' => 'superadmin',
            'password' => '$2y$10$e52DLwNDQh40ai1W5hSd/uIgwHr/dQWklKYG0X053MZp79FJ6.hxK',
            'role' => 'SUPERADMIN',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        DB::table('users')->insert([
            'identity' => 'admin1',
            'password' => '$2y$10$ZrF1euj5QnWuwA93sE5OsujwX4Q1mmph15ImVnZWcP1juOu20rfLG',
            'role' => 'ADMIN',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        DB::table('users')->insert([
            'identity' => 'admin2',
            'password' => '$2y$10$ZrF1euj5QnWuwA93sE5OsujwX4Q1mmph15ImVnZWcP1juOu20rfLG',
            'role' => 'ADMIN',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
