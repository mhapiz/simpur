<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePenilaianPenyanggahSeminarUsulanPenelitiansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penilaian_penyanggah_seminar_usulan_penelitians', function (Blueprint $table) {
            $table->id('id_penilaian_penyanggah_seminar_usulan_penelitian');
            $table->string('nim_seminaris')->nullable();
            $table->string('nim_penyanggah')->nullable();
            $table->unsignedBigInteger('seminar_usulan_penelitian_id')->nullable();
            $table->string('penilai')->nullable();

            $table->string('nilai1')->nullable();
            $table->string('hasil1')->nullable();

            $table->string('nilai2')->nullable();
            $table->string('hasil2')->nullable();

            $table->string('nilai3')->nullable();
            $table->string('hasil3')->nullable();

            $table->string('nilai4')->nullable();
            $table->string('hasil4')->nullable();

            $table->string('nilai5')->nullable();
            $table->string('hasil5')->nullable();

            $table->string('total_nilai')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penilaian_penyanggah_seminar_usulan_penelitians');
    }
}
