<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSeminarHasilPenelitiansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seminar_hasil_penelitians', function (Blueprint $table) {
            $table->id('id_seminar_hasil_penelitian');
            $table->string('nim_seminaris')->nullable();
            $table->date('tgl_seminar')->nullable();
            $table->text('judul')->nullable();
            $table->string('pembimbing1')->nullable();
            $table->string('pembimbing2')->nullable();
            $table->string('nilai_pembimbing1')->nullable();
            $table->string('nilai_pembimbing2')->nullable();
            $table->string('penyanggah1')->nullable();
            $table->string('penyanggah2')->nullable();
            $table->string('nilai_penyanggah1')->nullable();
            $table->string('nilai_penyanggah2')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seminar_hasil_penelitians');
    }
}
