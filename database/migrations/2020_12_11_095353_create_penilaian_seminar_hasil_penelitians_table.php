<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePenilaianSeminarHasilPenelitiansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penilaian_seminar_hasil_penelitians', function (Blueprint $table) {
            $table->id('id_penilaian_seminar_hasil_penelitian');
            $table->string('nim_seminaris')->nullable();
            $table->unsignedBigInteger('seminar_hasil_penelitian_id')->nullable();
            $table->string('penilai')->nullable();
            $table->string('nilai1a1')->nullable();
            $table->string('hasil1a1')->nullable();

            $table->string('nilai1b2')->nullable();
            $table->string('hasil1b2')->nullable();

            $table->string('nilai1b3')->nullable();
            $table->string('hasil1b3')->nullable();

            $table->string('nilai1b4')->nullable();
            $table->string('hasil1b4')->nullable();

            $table->string('nilai2c')->nullable();
            $table->string('hasil2c')->nullable();

            $table->string('nilai2d')->nullable();
            $table->string('hasil2d')->nullable();

            $table->string('nilai2e')->nullable();
            $table->string('hasil2e')->nullable();

            $table->string('nilai3')->nullable();
            $table->string('hasil3')->nullable();

            $table->string('nilai4')->nullable();
            $table->string('hasil4')->nullable();

            $table->string('total_nilai')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penilaian_seminar_hasil_penelitians');
    }
}
