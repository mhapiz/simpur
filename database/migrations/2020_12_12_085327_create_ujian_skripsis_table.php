<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUjianSkripsisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ujian_skripsis', function (Blueprint $table) {
            $table->id('id_ujian_skripsi');
            $table->string('nim_skripsi')->nullable();
            $table->date('tgl_skripsi')->nullable();
            $table->text('judul')->nullable();
            $table->string('pembimbing1')->nullable();
            $table->string('pembimbing2')->nullable();
            $table->string('nilai_pembimbing1')->nullable();
            $table->string('nilai_pembimbing2')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ujian_skripsis');
    }
}
