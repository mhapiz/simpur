<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePenilaianUjianSkripsisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penilaian_ujian_skripsis', function (Blueprint $table) {
            $table->id('id_penilaian_ujian_skripsi');
            $table->string('nim_skripsi')->nullable();
            $table->unsignedBigInteger('ujian_skripsi_id')->nullable();
            $table->string('penilai')->nullable();

            $table->string('nilai1')->nullable();
            $table->string('hasil1')->nullable();

            $table->string('nilai2a')->nullable();
            $table->string('hasil2a')->nullable();

            $table->string('nilai2b')->nullable();
            $table->string('hasil2b')->nullable();

            $table->string('nilai2c')->nullable();
            $table->string('hasil2c')->nullable();

            $table->string('nilai3')->nullable();
            $table->string('hasil3')->nullable();

            $table->string('total_nilai')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penilaian_ujian_skripsis');
    }
}
