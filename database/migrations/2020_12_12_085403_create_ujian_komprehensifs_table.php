<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUjianKomprehensifsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ujian_komprehensifs', function (Blueprint $table) {
            $table->id('id_ujian_komprehensif');
            $table->string('nim_skripsi')->nullable();
            $table->dateTime('tgl_skripsi')->nullable();

            $table->string('penguji1')->nullable();
            $table->string('nilai_penguji1')->nullable();

            $table->string('penguji2')->nullable();
            $table->string('nilai_penguji2')->nullable();

            $table->string('penguji3')->nullable();
            $table->string('nilai_penguji3')->nullable();

            $table->string('penguji4')->nullable();
            $table->string('nilai_penguji4')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ujian_komprehensifs');
    }
}
