<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePenilaianUjianKomprehensifsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penilaian_ujian_komprehensifs', function (Blueprint $table) {
            $table->id('id_penilaian_ujian_komprehensif');
            $table->string('nim_skripsi')->nullable();
            $table->unsignedBigInteger('ujian_komprehensif_id')->nullable();
            $table->string('penilai')->nullable();

            $table->string('nilai1a')->nullable();
            $table->string('hasil1a')->nullable();

            $table->string('nilai1b')->nullable();
            $table->string('hasil1b')->nullable();

            $table->string('nilai2a')->nullable();
            $table->string('hasil2a')->nullable();

            $table->string('nilai2b')->nullable();
            $table->string('hasil2b')->nullable();

            $table->string('nilai3a')->nullable();
            $table->string('hasil3a')->nullable();

            $table->string('nilai3b')->nullable();
            $table->string('hasil3b')->nullable();

            $table->string('nilai4')->nullable();
            $table->string('hasil4')->nullable();

            $table->string('nilai5a')->nullable();
            $table->string('hasil5a')->nullable();

            $table->string('total_nilai')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penilaian_ujian_komprehensifs');
    }
}
