<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNilaiMutusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nilai_mutus', function (Blueprint $table) {
            $table->id('id_nilai_mutu');
            $table->string('nim');

            $table->string('rerata_nilai_sup')->nullable();
            $table->string('rerata_nilai_penyanggah_sup')->nullable();
            $table->string('rerata_nilai_usulan_penelitian')->nullable();

            $table->string('rerata_nilai_shp')->nullable();
            $table->string('rerata_nilai_penyanggah_shp')->nullable();
            $table->string('rerata_nilai_hasil_penelitian')->nullable();

            $table->string('rerata_nilai_seminar')->nullable();

            $table->string('nilai_p1_ujian_skripsi')->nullable();
            $table->string('nilai_p2_ujian_skripsi')->nullable();
            $table->string('rerata_nilai_ujian_skripsi')->nullable();

            $table->string('nilai_komprehensif')->nullable();

            $table->string('nilai_skripsi')->nullable();

            $table->string('nilai_mutu')->nullable();

            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nilai_mutus');
    }
}
