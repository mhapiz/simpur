<?php

namespace Database\Seeders;

use App\Models\Mahasiswa;
use App\Models\NilaiMutu;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class MahasiswaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');

        for ($i = 1; $i <= 50; $i++) {

            // insert data ke table pegawai menggunakan Faker
            $mhs = Mahasiswa::create([
                'nim' => $faker->unique()->randomNumber,
                'nama_mahasiswa' => $faker->name,
                'minat' => $faker->randomElement(['Manajemen Hutan', 'Silvikultur', 'Teknologi Hasil Hutan']),
                'tahun_angkatan' => $faker->randomElement(['2010', '2011', '2012', '2013', '2014', '2015', '2016', '2017', '2018']),
                'created_at' => Carbon::now(),
            ]);

            NilaiMutu::create([
                'nim' => $mhs['nim'],
                'created_at' => Carbon::now(),
            ]);
        }
    }
}
