gj.dialog.messages['id-id'] = {
    Close: 'Tutup',
    DefaultTitle: 'Dialog'
};
gj.grid.messages['id-id'] = {
    First: 'Pertama',
    Previous: 'Sebelumnya',
    Next: 'Selanjutnya',
    Last: 'Terakhir',
    Page: 'Halaman',
    FirstPageTooltip: 'Halaman Pertama',
    PreviousPageTooltip: 'Halaman Sebelumnya',
    NextPageTooltip: 'Halaman Selanjutnya',
    LastPageTooltip: 'Halaman Terakhir',
    Refresh: 'Refresh',
    Of: 'of',
    DisplayingRecords: 'Mnampilkan Catatan',
    RowsPerPage: 'Baris Per Halaman:',
    Edit: 'Edit',
    Delete: 'Hapus',
    Update: 'Update',
    Cancel: 'Batal',
    NoRecordsFound: 'No records found.',
    Loading: 'Loading...'
};
gj.editor.messages['id-id'] = {
    bold: 'Bold',
    italic: 'Italic',
    strikethrough: 'Strikethrough',
    underline: 'Underline',
    listBulleted: 'List Bulleted',
    listNumbered: 'List Numbered',
    indentDecrease: 'Indent Decrease',
    indentIncrease: 'Indent Increase',
    alignLeft: 'Align Left',
    alignCenter: 'Align Center',
    alignRight: 'Align Right',
    alignJustify: 'Align Justify',
    undo: 'Undo',
    redo: 'Redo'
};
gj.core.messages['id-id'] = {
    monthNames: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
    monthShortNames: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agu', 'Sep', 'Okt', 'Nov', 'Des'],
    weekDaysMin: ['M', 'S', 'S', 'R', 'K', 'J', 'S'],
    weekDaysShort: ["Min", "Sen", "Sel", "Rab", "Kam", "Jum", "Sab"],
    weekDays: ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu"],
    am: 'AM',
    pm: 'PM',
    ok: 'Ok',
    cancel: 'Cancel',
    titleFormat: 'mmmm yyyy'
};
