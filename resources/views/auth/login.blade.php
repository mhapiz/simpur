<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>SIMPUR | Login</title>
    <link rel="stylesheet" href="{{ url('css/app.css') }}">
    <style>
        .hijaunya {
            background-color: #13b94a;
        }

        .mywhite {
            background: #f7f7f7;
        }

    </style>
</head>

<body class="relative">
    <div class="h-screen w-full absolute ">
        <div class="absolute top-0 left-0 h-full w-full bg-black bg-opacity-50" style="z-index: 2;"></div>
        <img src="{{ url('adminarea/assets/img/ulm.jpg') }}" alt="ulm"
            class="top-0 left-0 h-full w-full object-cover object-center" style="z-index: 1;">
    </div>
    <div class="grid grid-cols-12 h-screen w-full  relative" style="z-index: 10;">
        <div
            class="col-span-12 md:col-span-8 md:col-start-3 lg:col-span-5 lg:col-start-8 xl:col-span-4 xl:col-start-9 flex justify-center items-center ">
            <div
                class="mywhite shadow-xl rounded w-full mx-10 py-4  lg:h-screen lg:flex lg:flex-col lg:justify-center lg:mx-0 ">
                <div class="pb-8">
                    <h1 class="text-center text-4xl text-gray-500">Simpur Login</h1>
                </div>

                <div class="px-5 py-4 mb-4">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="input mb-8">
                            <label for="identity" class="block text-gray-700 text-sm font-bold mb-2">
                                <span class="text-red-500">&nbsp;*</span>
                                NIP
                            </label>
                            <div class="mt-1 relative rounded-md shadow-sm">
                                <div class="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none">
                                    <svg class="h-5 w-5 text-gray-400" fill="none" stroke-linecap="round"
                                        stroke-linejoin="round" stroke-width="2" stroke="currentColor"
                                        viewBox="0 0 24 24">
                                        <path d="M16 7a4 4 0 11-8 0 4 4 0 018 0zM12 14a7 7 0 00-7 7h14a7 7 0 00-7-7z">
                                        </path>
                                    </svg>
                                </div>
                                <input name="identity" id="identity" type="text" value="{{ old('identity') }}"
                                    class="w-full appearance-none text-gray-900 rounded px-4 py-2  @error('identity')border-2 border-red-400 @enderror focus:outline-none focus:ring-2 focus:ring-green-600 focus:border-transparent transition duration-300 ease-in-out"
                                    placeholder="NIP" />
                            </div>
                            @error('identity')
                                <div class="flex flex-col gap-1 px-4 mt-2">
                                    <strong class="text-red-500 text-xs italic capitalize">*{{ $message }}</strong>
                                </div>
                            @enderror
                        </div>

                        <div class="input mb-8">
                            <label for="password" class="block text-gray-700 text-sm font-bold mb-2">
                                <span class="text-red-500">&nbsp;*</span>
                                Kata Sandi
                            </label>
                            <div class="mt-1 relative rounded-md shadow-sm">
                                <div class="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none">
                                    <svg class="h-5 w-5 text-gray-400" fill="none" stroke-linecap="round"
                                        stroke-linejoin="round" stroke-width="2" stroke="currentColor"
                                        viewBox="0 0 24 24">
                                        <path
                                            d="M12 15v2m-6 4h12a2 2 0 002-2v-6a2 2 0 00-2-2H6a2 2 0 00-2 2v6a2 2 0 002 2zm10-10V7a4 4 0 00-8 0v4h8z">
                                        </path>
                                    </svg>
                                </div>
                                <input name="password" id="password" type="password"
                                    class="w-full appearance-none text-gray-900 @error('password')border-2 border-red-400 @enderror rounded px-4 py-2 focus:outline-none focus:ring-2 focus:ring-green-600 focus:border-transparent transition duration-300 ease-in-out"
                                    placeholder="Kata Sandi" autocomplete="off" />
                            </div>
                            @error('password')
                                <div class="flex flex-col gap-1 px-4 mt-2">
                                    <strong class="text-red-500 text-xs italic capitalize">*{{ $message }}</strong>
                                </div>
                            @enderror

                            <div class="px-2 mt-2">
                                <input id="checkBox" type="checkbox" onclick="lihatPassword()">
                                <label onclick="lihatPassword()" style="cursor: pointer">
                                    Lihat Password
                                </label>
                            </div>
                        </div>


                        <div class="mb-2 text-center">
                            <button class="w-full bg-green-500 py-2 rounded uppercase" type="submit"
                                style="color: #f7f7f7">
                                Login
                            </button>
                        </div>
                        <!-- <hr>
                <div class="mt-8 flex justify-between">
                  <a href="#">Daftar Dosen</a>
                  <a href="#">Daftar Mahasiswa</a>
                </div> -->
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        function lihatPassword() {
            var x = document.getElementById("password");
            var c = document.getElementById("checkBox");
            if (x.type === "password") {
                c.checked = true;
                x.type = "text";
            } else {
                x.type = "password";
                c.checked = false;
            }
        }

    </script>

</body>

</html>
