<x-guest-layout>
    <x-jet-authentication-card>
        <x-slot name="logo">
            <x-jet-authentication-card-logo />
        </x-slot>

        <x-jet-validation-errors class="mb-4" />

        <form method="POST" action="{{ route('register') }}">
            @csrf

            <div>
                <x-jet-label for="name" value="{{ __('Name') }}" />
                <x-jet-input id="name" class="block mt-1 w-full" type="text" name="name" :value="old('name')" required
                    autofocus autocomplete="name" />
            </div>

            <div class="mt-4">
                <x-jet-label for="email" value="{{ __('Email') }}" />
                <x-jet-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')"
                    required />
            </div>

            <div class="mt-4">
                <x-jet-label for="password" value="{{ __('Password') }}" />
                <x-jet-input id="password" class="block mt-1 w-full" type="password" name="password" required
                    autocomplete="new-password" />
            </div>

            <div class="mt-4">
                <x-jet-label for="password_confirmation" value="{{ __('Confirm Password') }}" />
                <x-jet-input id="password_confirmation" class="block mt-1 w-full" type="password"
                    name="password_confirmation" required autocomplete="new-password" />
            </div>

            <div class="flex items-center justify-end mt-4">
                <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('login') }}">
                    {{ __('Already registered?') }}
                </a>

                <x-jet-button class="ml-4">
                    {{ __('Register') }}
                </x-jet-button>
            </div>
        </form>
    </x-jet-authentication-card>
</x-guest-layout>

{{-- <!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>SIMPUR | Login</title>
    <link rel="stylesheet" href="{{ url('css/app.css') }}">
<style>
    .hijaunya {
        background-color: #13b94a;
    }
</style>
</head>

<body class="align-middle">
    <div class="grid grid-cols-12 gap-2 mt-40 md:mt-24 lg:mt-22">
        <div class="col-span-12 px-14 md:col-span-10 md:col-start-2 lg:col-span-4 lg:col-start-5">
            <div class="">
                <div class="pb-8">
                    <h1 class="text-center text-4xl">Login</h1>
                </div>

                <div class="bg-white shadow-xl rounded px-5 py-4 mb-4">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="input mb-8">
                            <label for="identity" class="block text-gray-700 text-sm font-bold mb-2">
                                <span class="text-red-500">&nbsp;*</span>
                                NIDN/NIM
                            </label>
                            <div class="mt-1 relative rounded-md shadow-sm">
                                <div class="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none">
                                    <svg class="h-5 w-5 text-gray-400" fill="none" stroke-linecap="round"
                                        stroke-linejoin="round" stroke-width="2" stroke="currentColor"
                                        viewBox="0 0 24 24">
                                        <path d="M16 7a4 4 0 11-8 0 4 4 0 018 0zM12 14a7 7 0 00-7 7h14a7 7 0 00-7-7z">
                                        </path>
                                    </svg>
                                </div>
                                <input name="identity" id="identity" type="text" value="{{ old('identity') }}"
                                    class="w-full appearance-none text-gray-900 rounded px-4 py-2  @error('identity')border-2 border-red-400 @enderror focus:outline-none focus:ring-2 focus:ring-green-600 focus:border-transparent transition duration-300 ease-in-out"
                                    placeholder="NIDN/NIM" />
                            </div>
                            @error('identity')
                            <div class="flex flex-col gap-1 px-4 mt-2">
                                <strong class="text-red-500 text-xs italic capitalize">*{{ $message }}</strong>
                            </div>
                            @enderror
                        </div>

                        <div class="input mb-8">
                            <label for="password" class="block text-gray-700 text-sm font-bold mb-2">
                                <span class="text-red-500">&nbsp;*</span>
                                Password
                            </label>
                            <div class="mt-1 relative rounded-md shadow-sm">
                                <div class="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none">
                                    <svg class="h-5 w-5 text-gray-400" fill="none" stroke-linecap="round"
                                        stroke-linejoin="round" stroke-width="2" stroke="currentColor"
                                        viewBox="0 0 24 24">
                                        <path
                                            d="M12 15v2m-6 4h12a2 2 0 002-2v-6a2 2 0 00-2-2H6a2 2 0 00-2 2v6a2 2 0 002 2zm10-10V7a4 4 0 00-8 0v4h8z">
                                        </path>
                                    </svg>
                                </div>
                                <input name="password" id="password" type="password"
                                    class="w-full appearance-none text-gray-900 @error('password')border-2 border-red-400 @enderror rounded px-4 py-2 focus:outline-none focus:ring-2 focus:ring-green-600 focus:border-transparent transition duration-300 ease-in-out"
                                    placeholder="Kata Sandi" />
                            </div>
                            @error('password')
                            <div class="flex flex-col gap-1 px-4 mt-2">
                                <strong class="text-red-500 text-xs italic capitalize">*{{ $message }}</strong>
                            </div>
                            @enderror
                        </div>

                        <div class="mb-2 text-center">
                            <button class="w-full bg-green-500 py-2 rounded" type="submit">
                                Login
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>

</html> --}}
