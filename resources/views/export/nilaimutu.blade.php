<table>
    <tr>
        <td colspan="8" style="text-align: center; font-weight: 700;">Data Nilai Mutu</td>
    </tr>
</table>
<table>
    <thead>
        <tr>
            <td rowspan="2" style="text-align: center">Nama Mahasiswa</td>
            <td colspan="3" style="text-align: center">Nilai Proposal</td>
            <td colspan="3" style="text-align: center">Nilai Skripsi</td>
            <td rowspan="2" style="text-align: center">Nilai Seminar Rerata</td>
            <td colspan="3" style="text-align: center">Nilai Ujian Skripsi</td>
            <td rowspan="2" style="text-align: center">Nilai Komprehensif</td>
            <td rowspan="2" style="text-align: center">Nilai Skripsi</td>
            <td rowspan="2" style="text-align: center">Nilai Mutu</td>
        </tr>
        <tr>
            <td style="text-align: center">Penyanggah Proposal</td>
            <td style="text-align: center">Seminar Proposal</td>
            <td style="text-align: center">Rerata Nilai Proposal</td>

            <td style="text-align: center">Penyanggah Skripsi</td>
            <td style="text-align: center">Seminar Skripsi</td>
            <td style="text-align: center">Rerata Nilai Skripsi</td>

            <td style="text-align: center">Pemb. I</td>
            <td style="text-align: center">Pemb. II</td>
            <td style="text-align: center">Rerata</td>
        </tr>
    </thead>
    <tbody>
        @foreach ($nm as $n)
        <tr>
            <td style="text-align: center">{{ $n->mahasiswa->nama_mahasiswa }}</td>
            {{--  --}}
            <td style="text-align: center">{{ $n->rerata_nilai_penyanggah_sup }}</td>
            <td style="text-align: center">{{ $n->rerata_nilai_sup }}</td>
            <td style="text-align: center">{{ $n->rerata_nilai_usulan_penelitian }}</td>
            {{--  --}}
            <td style="text-align: center">{{ $n->rerata_nilai_penyanggah_shp }}</td>
            <td style="text-align: center">{{ $n->rerata_nilai_shp }}</td>
            <td style="text-align: center">{{ $n->rerata_nilai_hasil_penelitian }}</td>
            {{--  --}}
            <td style="text-align: center">{{ $n->rerata_nilai_seminar }}</td>
            {{--  --}}
            <td style="text-align: center">{{ $n->nilai_p1_ujian_skripsi }}</td>
            <td style="text-align: center">{{ $n->nilai_p2_ujian_skripsi }}</td>
            <td style="text-align: center">{{ $n->rerata_nilai_ujian_skripsi }}</td>
            {{--  --}}
            <td style="text-align: center">{{ $n->nilai_komprehensif }}</td>
            <td style="text-align: center">{{ $n->nilai_skripsi }}</td>
            <td style="text-align: center">{{ $n->nilai_mutu }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
