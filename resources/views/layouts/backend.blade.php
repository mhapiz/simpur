<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    {{-- <meta name="description" content="Sistem Penilaian">
    <meta name="keywords" content="HTML, CSS, JavaScript"> --}}
    <meta name="author" content="mhapiz">
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
        name='viewport' />

    @include('modules.backend.style')
    @stack('tambahStyle')
    <style>
        .table thead th {
            font-size: 1rem !important;
            font-weight: 700 !important;
        }

        .preloader {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            z-index: 9999;
            display: flex;
            justify-content: center;
            align-items: center;
            background-color: #fff;
        }

        .preloader .loader {
            border: 16px solid #f3f3f3;
            border-radius: 50%;
            border-top: 16px solid #13b94a;
            width: 120px;
            height: 120px;
            -webkit-animation: spin 1.4s linear infinite;
            /* Safari */
            animation: spin 1.4s linear infinite;
        }

        /* Safari */
        @-webkit-keyframes spin {
            0% {
                -webkit-transform: rotate(0deg);
            }

            100% {
                -webkit-transform: rotate(360deg);
            }
        }

        @keyframes spin {
            0% {
                transform: rotate(0deg);
            }

            100% {
                transform: rotate(360deg);
            }
        }
    </style>

    <title>
        @yield('title')
    </title>
</head>

<body class="">
    <div class="preloader">
        <div class="loader">
        </div>
    </div>
    <div class="wrapper ">
        @include('modules.backend.sidebar')
        <div class="main-panel" id="main-panel">
            <!-- Navbar -->
            @include('modules.backend.navbar')
            <!-- End Navbar -->
            <div class="panel-header">
                @yield('header')
            </div>
            <div class="content">
                @include('sweetalert::alert')

                @yield('content')
            </div>
            <footer class="footer px-5">
                <div class="d-flex justify-content-between">
                    <div></div>
                    <div>
                        SIMPUR V.1.0
                    </div>
                </div>
            </footer>
        </div>
    </div>

    @include('modules.backend.script')
    @stack('tambahScript')
    <script>
        $(document).ready(function(){
            $(".preloader").fadeOut();
        })
    </script>

</body>

</html>
