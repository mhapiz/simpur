<div class="btn-group" role="group" aria-label="Basic example">

    <a class="btn btn-warning text-white" href="{!! $editUrl !!}" id="btnEdit">
        <i class="fas fa-edit"></i>
    </a>

    <form action=" {!! $deleteUrl !!} " method="POST">
        @csrf
        @method('delete')
        <button class="btn btn-danger rounded-right"
            style="border-bottom-left-radius: 0px; border-top-left-radius: 0px; "
            onclick="return confirm('Apakah Anda Yakin Ingin Menghapus Data Ini?')" id="btnHapus">
            <i class="fa fa-trash"></i>
        </button>
    </form>

</div>
