<nav class="navbar navbar-expand-lg bg-primary  navbar-absolute">
    <div class="container-fluid">
        <div class="navbar-wrapper">
            <div class="navbar-toggle">
                <button type="button" class="navbar-toggler">
                    <span class="navbar-toggler-bar bar1"></span>
                    <span class="navbar-toggler-bar bar2"></span>
                    <span class="navbar-toggler-bar bar3"></span>
                </button>
            </div>
            @if (Auth::user()->role == 'ADMIN')
            <a class="navbar-brand" href="{{ route('admin.index') }}"> Go To Home</a>
            @elseif( Auth::user()->role == 'DOSEN')
            <a class="navbar-brand" href="{{ route('dosen.index') }}"> Go To Home</a>

            @endif
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation"
            aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
        </button>
        <div class="collapse navbar-collapse justify-content-end" id="navigation">
            <ul class="navbar-nav">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle d-flex align-items-center" id="navbarDropdownMenuLink"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="cursor: pointer">
                        <p class="d-none d-lg-inline">
                            @if (Auth::user()->role == 'ADMIN' || Auth::user()->role == 'SUPERADMIN')
                            {{ Auth::user()->identity }}
                            @elseif( Auth::user()->role == 'DOSEN')
                            {{ \App\Models\Dosen::find(Auth::user()->dosen_id)->nama_dosen }}
                            @endif
                        </p>
                        <span class="material-icons mx-2">
                            person_outline
                        </span>
                        <p>
                            <span class="d-lg-none d-md-block">
                                @if (Auth::user()->role == 'ADMIN' || Auth::user()->role == 'SUPERADMIN')
                                {{ Auth::user()->identity }}
                                @elseif( Auth::user()->role == 'DOSEN')
                                {{ \App\Models\Dosen::find(Auth::user()->dosen_id)->nama_dosen }}
                                @endif
                            </span>
                        </p>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item d-flex align-items-center" href="{{ route('myProfile.index') }}">
                            <span class="material-icons mr-3">
                                person_outline
                            </span>
                            <span class="font-weight-bold">Profile</span>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item text-danger d-flex align-items-center" href="#" data-toggle="modal"
                            data-target="#log-outModal">
                            <span class="material-icons mr-3">
                                login
                            </span>
                            <span class="font-weight-bold">Log out</span>
                        </a>

                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>

{{-- LOGOUT MODAL --}}
<div class="modal fade" id="log-outModal" tabindex="-1" aria-labelledby="log-outModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="log-outModalLabel">Log Out?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Apakah Anda Yakin Ingin Keluar ?
                <div class="d-flex my-3 justify-content-end">
                    <button type="button" class="btn btn-secondary mr-2" data-dismiss="modal">Tutup</button>
                    <form action="{{route('logout')}}" method="POST">
                        <button class=" btn btn-danger px-2 " type="submit">
                            @csrf
                            Keluar
                        </button>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
