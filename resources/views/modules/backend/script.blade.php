<!--   Core JS Files   -->
<script src="{{ url('adminarea/assets/js/core/jquery.min.js') }}"></script>
<script src="{{ url('adminarea/assets/js/core/popper.min.js') }}"></script>
<script src="{{ url('adminarea/assets/js/core/bootstrap.min.js') }}"></script>
<script src="{{ url('adminarea/assets/js/plugins/perfect-scrollbar.jquery.min.js') }}"></script>
<!--  Notifications Plugin    -->
<script src="{{ url('adminarea/assets/js/plugins/bootstrap-notify.js') }}"></script>
<!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
<script src="{{ url('adminarea/assets/js/now-ui-dashboard.min.js?v=1.5.0') }}" type="text/javascript"></script>
<!-- Now Ui Dashboard DEMO methods, don't include it in your project! -->
<script src="{{ url('adminarea/assets/demo/demo.js') }}"></script>
<script>

</script>
