<div class="sidebar" data-color="orange">

    <div class="logo">
        <a href="{{ url('/') }}">
            <img src="{{ url('adminarea/assets/img/logo-ulm.png') }}" alt="Logo" class="mx-auto d-block"
                style="height: 6rem">
        </a>
        <a href="{{ url('/') }}" class="simple-text logo-normal text-center">
            Sistem Informasi <br>
            Penilaian Skripsi <br>
            Program Studi Kehutanan
        </a>
    </div>
    <div class="sidebar-wrapper" id="sidebar-wrapper">
        <ul class="nav">
            @if (Auth::user()->role === 'ADMIN')
            <li class="{{ (request()->is('admin')) ? 'active' : '' }}">
                <a href="{{ route('admin.index') }}" class=" d-flex align-items-center">
                    <i class="fas fa-home"></i>
                    <p class="font-weight-bold">Home</p>
                </a>
            </li>

            <li>
                <a data-toggle="collapse" href="#seminar" class="collapsed " aria-expanded="false">
                    <i class="fas fa-signature"></i>
                    <p class="font-weight-bold">
                        Seminar <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse {{ (request()->is('admin/*-penelitian*')) ? 'show' : '' }}" id="seminar" style="">
                    <ul class="nav">

                        <li class="{{ (request()->is('admin/usulan-penelitian*')) ? 'active' : '' }}">
                            <a href="{{ route('admin.usulan.index') }}" class=" d-flex align-items-center">
                                <i class="fas fa-file-alt"></i>
                                <p class="font-weight-bold small">Seminar Usulan Penelitian</p>
                            </a>
                        </li>

                        <li class="{{ (request()->is('admin/hasil-penelitian*')) ? 'active' : '' }}">
                            <a href="{{ route('admin.hasil.index') }}" class=" d-flex align-items-center">
                                <i class="fas fa-file-alt"></i>
                                <p class="font-weight-bold small">Seminar Hasil Penelitian</p>
                            </a>
                        </li>

                    </ul>
                </div>
            </li>


            <li>
                <a data-toggle="collapse" href="#ujian" class="collapsed " aria-expanded="false">

                    <i class="fas fa-book-reader"></i>

                    <p class="font-weight-bold">
                        Ujian <b class="caret"></b>
                    </p>

                </a>

                <div class="collapse {{ (request()->is('admin/ujian*')) ? 'show' : '' }}" id="ujian" style="">
                    <ul class="nav">

                        <li class="{{ (request()->is('admin/ujian-skripsi*')) ? 'active' : '' }}">
                            <a href="{{ route('admin.ujian-skripsi.index') }}" class=" d-flex align-items-center">
                                <i class="fas fa-file-alt"></i>
                                <p class="font-weight-bold small">Ujian Skripsi</p>
                            </a>
                        </li>

                        <li class="{{ (request()->is('admin/ujian-komprehensif*')) ? 'active' : '' }}">
                            <a href="{{ route('admin.ujian-kompre.index') }}" class=" d-flex align-items-center">
                                <i class="fas fa-file-alt"></i>
                                <p class="font-weight-bold small">Ujian Komprehensif</p>
                            </a>
                        </li>

                    </ul>
                </div>


            </li>


            <li class="{{ (request()->is('admin/nilai-mutu')) ? 'active' : '' }}">
                <a href="{{ route('admin.nilai-mutu.index') }}" class=" d-flex align-items-center">
                    <i class="fas fa-graduation-cap"></i>

                    <p class="font-weight-bold">Nilai Mutu</p>
                </a>
            </li>


            <li>
                <a data-toggle="collapse" href="#user-manage" class="collapsed " aria-expanded="false">

                    <i class="fas fa-user-cog"></i>
                    <p class="font-weight-bold">
                        User Manage <b class="caret"></b>
                    </p>

                </a>

                <div class="collapse {{ (request()->is('admin/user-manage*')) ? 'show' : '' }}" id="user-manage"
                    style="">
                    <ul class="nav">

                        <li class="{{ (request()->is('admin/user-manage/dosen*')) ? 'active' : '' }}">
                            <a href="{{ route('admin.user-manage.dosen.index') }}"
                                class="px-3 d-flex align-items-center">
                                <i class="fas fa-user-tie"></i>
                                <p class="font-weight-bold">Dosen</p>
                            </a>
                        </li>

                        <li class="{{ (request()->is('admin/user-manage/mahasiswa*')) ? 'active' : '' }}">
                            <a href="{{ route('admin.user-manage.mahasiswa.index') }}"
                                class="px-3 d-flex align-items-center">
                                <i class="fas fa-user-alt"></i>
                                <p class="font-weight-bold">Mahasiswa</p>
                            </a>
                        </li>

                    </ul>
                </div>
            </li>



            @elseif(Auth::user()->role === 'DOSEN')
            <li class="{{ (request()->is('dosen')) ? 'active' : '' }}">
                <a href="{{ route('dosen.index') }}" class=" d-flex align-items-center">
                    <i class="fas fa-home"></i>
                    <p class="font-weight-bold">Home</p>
                </a>
            </li>

            <li>
                <a data-toggle="collapse" href="#seminar" class="collapsed " aria-expanded="false">
                    <i class="fas fa-signature"></i>
                    <p class="font-weight-bold">
                        Seminar <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse {{ (request()->is('dosen/seminar*')) ? 'show' : '' }}" id="seminar" style="">
                    <ul class="nav">

                        <li class="{{ (request()->is('dosen/seminar/*usulan-penelitian*')) ? 'active' : '' }}">
                            <a href="{{ route('dosen.seminar-usulan.index') }}" class=" d-flex align-items-center">
                                <i class="fas fa-file-alt"></i>
                                <p class="font-weight-bold small">Seminar Usulan Penelitian</p>
                            </a>
                        </li>

                        <li class="{{ (request()->is('dosen/seminar/*hasil-penelitian*')) ? 'active' : '' }}">
                            <a href="{{ route('dosen.seminar-hasil.index') }}" class=" d-flex align-items-center">
                                <i class="fas fa-file-alt"></i>
                                <p class="font-weight-bold small">Seminar Hasil Penelitian</p>
                            </a>
                        </li>

                    </ul>
                </div>
            </li>

            <li>

                <a data-toggle="collapse" href="#ujian" class="collapsed " aria-expanded="false">

                    <i class="fas fa-book-reader"></i>

                    <p class="font-weight-bold">
                        Ujian <b class="caret"></b>
                    </p>

                </a>

                <div class="collapse {{ (request()->is('dosen/ujian*')) ? 'show' : '' }}" id="ujian" style="">
                    <ul class="nav">

                        <li class="{{ (request()->is('dosen/ujian/ujian-skripsi*')) ? 'active' : '' }}">
                            <a href="{{ route('dosen.ujian-skripsi.index') }}" class=" d-flex align-items-center">
                                <i class="fas fa-file-alt"></i>
                                <p class="font-weight-bold small">Ujian Skripsi</p>
                            </a>
                        </li>

                        <li class="{{ (request()->is('dosen/ujian/ujian-komprehensif*')) ? 'active' : '' }}">
                            <a href="{{ route('dosen.ujian-kompre.index') }}" class=" d-flex align-items-center">
                                <i class="fas fa-file-alt"></i>
                                <p class="font-weight-bold small">Ujian Komprehensif</p>
                            </a>
                        </li>

                    </ul>
                </div>


            </li>

            {{-- <li class="{{ (request()->is('dosen/nilai-mutu*')) ? 'active' : '' }}">
            <a href="{{ route('dosen.nilai-mutu.index') }}" class=" d-flex align-items-center">
                <i class="fas fa-graduation-cap"></i>

                <p class="font-weight-bold">Nilai Mutu</p>
            </a>
            </li> --}}

            @elseif(Auth::user()->role === 'SUPERADMIN')
            <li class="{{ (request()->is('super-admin')) ? 'active' : '' }}">
                <a href="{{ route('super-admin.index') }}" class=" d-flex align-items-center">
                    <i class="fas fa-home"></i>
                    <p class="font-weight-bold">Home</p>
                </a>
            </li>

            <li class="{{ (request()->is('super-admin/log-aktivitas')) ? 'active' : '' }}">
                <a href="{{ route('super-admin.log.index') }}" class=" d-flex align-items-center">
                    <i class="fas fa-clipboard-list nav-icon"></i>
                    <p class="font-weight-bold">Log Aktivitas</p>
                </a>
            </li>

            <li>
                <a data-toggle="collapse" href="#user-manage" class="collapsed " aria-expanded="false">

                    <i class="fas fa-user-cog"></i>
                    <p class="font-weight-bold">
                        User Manage <b class="caret"></b>
                    </p>

                </a>

                <div class="collapse {{ (request()->is('super-admin/user-manage*')) ? 'show' : '' }}" id="user-manage"
                    style="">
                    <ul class="nav">

                        <li class="{{ (request()->is('super-admin/user-manage/admin*')) ? 'active' : '' }}">
                            <a href="{{ route('super-admin.user-manage.admin.index') }}"
                                class="px-3 d-flex align-items-center">
                                <i class="fas fa-users-cog"></i>
                                <p class="font-weight-bold">Admin</p>
                            </a>
                        </li>



                    </ul>
                </div>
            </li>
            @endif


        </ul>
    </div>
</div>


{{-- <li class="{{ (request()->is('super-admin/user-manage/dosen*')) ? 'active' : '' }}">
<a href="{{ route('super-admin.user-manage.dosen.index') }}" class="px-3 d-flex align-items-center">
    <i class="fas fa-user-tie"></i>
    <p class="font-weight-bold">Dosen</p>
</a>
</li>

<li class="{{ (request()->is('super-admin/user-manage/mahasiswa*')) ? 'active' : '' }}">
    <a href="{{ route('super-admin.user-manage.mahasiswa.index') }}" class="px-3 d-flex align-items-center">
        <i class="fas fa-user-alt"></i>
        <p class="font-weight-bold">Mahasiswa</p>
    </a>
</li> --}}
