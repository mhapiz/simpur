@extends('layouts.backend')

@section('title')
SIMPUR | Dashboard
@endsection

@section('header')
<div class="header text-center">
    <h2 class="title">Home</h2>
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12 col-md-6">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb bg-white rounded-pill shadow">
                        <li class="breadcrumb-item active" aria-current="page">Home</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <h4>Pintasan</h4>
    </div>
    <div class="col-12 col-md-6 col-lg-4">
        <a class="default" href="{{ route('admin.usulan.create') }}">
            <div class="card  card-tasks">
                <div class="card-header d-flex justify-content-between" style="background: ">
                    <div>
                        <h5 class="card-category">Seminar Usulan Penelitian</h5>
                        <h5 class="card-title">Tambah Seminar Usulan</h5>
                    </div>

                    <div>
                        <i class="icon far fa-plus-square fa-2x opacity-50"></i>
                    </div>
                </div>
            </div>
        </a>
    </div>

    <div class="col-12 col-md-6 col-lg-4">
        <a class="default" href="{{ route('admin.user-manage.dosen.create') }}">
            <div class="card card-tasks">
                <div class="card-header d-flex justify-content-between" style="background: ">
                    <div>
                        <h5 class="card-category">Dosen</h5>
                        <h5 class="card-title">Tambah Dosen</h5>
                    </div>

                    <div>
                        <i class="fas fa-user-tie icon fa-2x opacity-50"></i>
                    </div>
                </div>
            </div>
        </a>
    </div>

    <div class="col-12 col-md-6 col-lg-4">
        <a class="default" href="{{ route('admin.user-manage.mahasiswa.create') }}">
            <div class="card  card-tasks">
                <div class="card-header d-flex justify-content-between">
                    <div>
                        <h5 class="card-category">Mahasiswa</h5>
                        <h5 class="card-title">Tambah Mahasiswa</h5>
                    </div>

                    <div>
                        <i class="fas fa-user-alt icon fa-2x opacity-50"></i>
                    </div>
                </div>
            </div>
        </a>
    </div>
</div>
@endsection


@push('tambahStyle')
<style>
    a.default {
        text-decoration: none;
        color: black;
    }

    .icon {
        transition: all .2s ease-in-out;
    }

    a.default:hover .icon {
        -ms-transform: scale(1.5);
        -webkit-transform: scale(1.5);
        transform: scale(1.5);
    }

    .opacity-50 {
        opacity: .5;
    }
</style>
@endpush
