@extends('layouts.backend')

@section('title')
SIMPUR | Admin - Edit Nilai Mutu
@endsection

@section('header')
<div class="header text-center d-none">
    <h2 class=" title">Edit Nilai Mutu</h2>
    <p class="category">Handcrafted by our friend <a target="_blank" href="https://github.com/mouse0270">Robert
            McIntosh</a>. Please checkout the <a href="http://bootstrap-notify.remabledesigns.com/" target="_blank">full
            documentation.</a></p>
</div>
@endsection

@section('content')
<div class="row" style="margin-top: -10rem">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h5 class="card-title">Edit Nilai Mutu <span
                        class="font-weight-bold">{{ $nilaiMutu->mahasiswa->nama_mahasiswa }}</span></h5>
            </div>
            <div class="card-body">
                <form class="form-horizontal" action="{{ route('admin.nilai-mutu.update', $nilaiMutu->id_nilai_mutu) }}"
                    method="POST">
                    @method('PUT')
                    @csrf
                    <div class="row">
                        <div class="col-12 col-md-4">
                            <div class="row border-right">
                                <div class="col-12 text-center">
                                    <h4>Nilai Proposal</h4>
                                </div>
                                <div class="col-12">
                                    <label class="">Nilai Menyanggah</label>

                                    <div class="form-group">
                                        <input type="text" name="rerata_nilai_penyanggah_sup" class="form-control"
                                            value="{{ $nilaiMutu->rerata_nilai_penyanggah_sup }}">
                                        @error('rerata_nilai_penyanggah_sup')
                                        <span class="invalid-feedback ml-1" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-12">
                                    <label class="">Nilai Seminar</label>

                                    <div class="form-group">
                                        <input type="text" name="rerata_nilai_sup" class="form-control"
                                            value="{{ $nilaiMutu->rerata_nilai_sup }}">
                                        @error('rerata_nilai_sup')
                                        <span class="invalid-feedback ml-1" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-12">
                                    <label class="">Rerata Nilai</label>

                                    <div class="form-group">
                                        <input type="text" name="rerata_nilai_usulan_penelitian"
                                            class="form-control noEdit"
                                            value="{{ $nilaiMutu->rerata_nilai_usulan_penelitian }}">
                                        @error('rerata_nilai_usulan_penelitian')
                                        <span class="invalid-feedback ml-1" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <hr>
                            </div>
                        </div>

                        <div class="col-12 col-md-4">
                            <div class="row border-right">
                                <div class="col-12 text-center">
                                    <h4>Nilai Skripsi</h4>
                                </div>
                                <div class="col-12">
                                    <label class="">Nilai Menyanggah</label>

                                    <div class="form-group">
                                        <input type="text" name="rerata_nilai_penyanggah_shp" class="form-control"
                                            value="{{ $nilaiMutu->rerata_nilai_penyanggah_shp }}">
                                        @error('rerata_nilai_penyanggah_shp')
                                        <span class="invalid-feedback ml-1" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-12">
                                    <label class="">Nilai Seminar</label>

                                    <div class="form-group">
                                        <input type="text" name="rerata_nilai_shp" class="form-control"
                                            value="{{ $nilaiMutu->rerata_nilai_shp }}">
                                        @error('rerata_nilai_shp')
                                        <span class="invalid-feedback ml-1" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-12">
                                    <label class="">Rerata Nilai</label>

                                    <div class="form-group">
                                        <input type="text" name="rerata_nilai_hasil_penelitian" class="form-control"
                                            value="{{ $nilaiMutu->rerata_nilai_hasil_penelitian }}">
                                        @error('rerata_nilai_hasil_penelitian')
                                        <span class="invalid-feedback ml-1" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-md-4">
                            <div class="row border-right">
                                <div class="col-12 text-center">
                                    <h4>Rerata Nilai Seminar</h4>
                                </div>
                                <div class="col-12">
                                    <label>Rerata Nilai Seminar</label>
                                    <div class="form-group">
                                        <input type="text" name="rerata_nilai_seminar" class="form-control"
                                            value="{{ $nilaiMutu->rerata_nilai_seminar }}">
                                        @error('rerata_nilai_seminar')
                                        <span class="invalid-feedback ml-1" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row border-bottom">
                        <div class="col-12 col-md-4">
                            <div class="row border-right">
                                <div class="col-12 text-center">
                                    <h4>Nilai Ujian Skripsi</h4>
                                </div>
                                <div class="col-12">
                                    <label class="">Pembimbing 1</label>

                                    <div class="form-group">
                                        <input type="text" name="nilai_p1_ujian_skripsi" class="form-control"
                                            value="{{ $nilaiMutu->nilai_p1_ujian_skripsi }}">
                                        @error('nilai_p1_ujian_skripsi')
                                        <span class="invalid-feedback ml-1" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-12">
                                    <label class="">Pembimbing 2</label>

                                    <div class="form-group">
                                        <input type="text" name="nilai_p2_ujian_skripsi" class="form-control"
                                            value="{{ $nilaiMutu->nilai_p2_ujian_skripsi }}">
                                        @error('nilai_p2_ujian_skripsi')
                                        <span class="invalid-feedback ml-1" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-12">
                                    <label class="">Rerata Nilai Ujian Skripsi</label>

                                    <div class="form-group">
                                        <input type="text" name="rerata_nilai_ujian_skripsi" class="form-control noEdit"
                                            value="{{ $nilaiMutu->rerata_nilai_ujian_skripsi }}">
                                        @error('rerata_nilai_ujian_skripsi')
                                        <span class="invalid-feedback ml-1" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="col-12 col-md-4">
                            <div class="row border-right">
                                <div class="col-12 text-center">
                                    <h4>Nilai Ujian Komprehensif</h4>
                                </div>
                                <div class="col-12">
                                    <label class="">Rerata Nilai Ujian Komprehensif</label>

                                    <div class="form-group">
                                        <input type="text" name="nilai_komprehensif" class="form-control noEdit"
                                            value="{{ $nilaiMutu->nilai_komprehensif }}">
                                        @error('nilai_komprehensif')
                                        <span class="invalid-feedback ml-1" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="col-12 col-md-4">
                            <div class="row border-right">
                                <div class="col-12 text-center">
                                    <h4>Nilai Skripsi</h4>
                                </div>
                                <div class="col-12">
                                    <label class="">Nilai Skripsi</label>

                                    <div class="form-group">
                                        <input type="text" name="nilai_skripsi" class="form-control noEdit"
                                            value="{{ $nilaiMutu->nilai_skripsi }}">
                                        @error('nilai_skripsi')
                                        <span class="invalid-feedback ml-1" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>





                    <div class="row">
                        <div class="col-12 text-right">
                            <button type="submit" class="btn btn-success">Edit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
@endsection

@push('tambahStyle')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css"
    integrity="sha512-+4zCK9k+qNFUR5X+cKL9EIR+ZOhtIloNl9GIKS57V1MyNsYpYcUrUeQc9vNfzsWfV28IaLL3i96P9sdNyeRssA=="
    crossorigin="anonymous" />
<link rel="stylesheet" href="{{ url('adminarea/plugins/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ url('adminarea/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ url('adminarea/plugins/gijgo/css/gijgo.min.css') }}">

<style>
    .noEdit {
        pointer-events: none;
    }
</style>

@endpush

@push('tambahScript')
<script src="{{ url('adminarea/plugins/select2/js/select2.full.min.js') }}"></script>
<script src="{{ url('adminarea/plugins/gijgo/js/gijgo.min.js') }}"></script>
<script src="{{ url('adminarea/plugins/gijgo/js/messages/messages.id-id.js') }}"></script>
<script>
    $('#selectNIM, #selectPembimbing1, #selectPembimbing2').select2({
        placeholder: "",
        theme: 'bootstrap4',
  });

    $('#tgl_seminar').datepicker({
      uiLibrary: 'materialdesign',
      format: "yyyy-mm-dd",
      locale: 'id-id',
    });
</script>
@endpush
