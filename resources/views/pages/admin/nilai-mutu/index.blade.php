@extends('layouts.backend')

@section('title')
SIMPUR | Nilai Mutu
@endsection

@section('header')
<div class="header text-center">
    <h2 class=" title">Data Nilai Mutu</h2>
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12 col-md-6">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb bg-white rounded-pill shadow">
                        <li class="breadcrumb-item"><a href="{{ route('admin.index') }}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Nilai Mutu</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <div class="row">
                    <div class="col-12 d-flex justify-content-between">
                        <div></div>
                        <div class="d-flex ">

                            <button type="button" class="btn btn-success btn-sm d-flex align-items-center rounded"
                                data-toggle="modal" data-target="#exampleModal">
                                <span class="material-icons  mr-3">
                                    cloud_download
                                </span>
                                <span class="font-weight-bold">Export Excel</span>
                            </button>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="table-responsive">
                            <table style="width: 100%" class="table table-hover table-bordered text-center"
                                id="table-nilai">
                                <thead class="text-Success thead-dark">
                                    <tr>
                                        <td rowspan="2" width="10px">No.</td>
                                        <td rowspan="2" style="5%">Nama Mahasiswa</td>
                                        <td colspan="3">Nilai Proposal</td>
                                        <td colspan="3">Nilai Skripsi</td>
                                        <td rowspan="2">Nilai Seminar Rerata</td>
                                        <td colspan="3">Nilai Ujian Skripsi</td>
                                        <td rowspan="2">Nilai Komprehensif</td>
                                        <td rowspan="2">Nilai Skripsi</td>
                                        <td rowspan="2">Nilai Mutu</td>
                                        <td rowspan="2">Aksi</td>
                                    </tr>
                                    <tr>
                                        <td>Penyanggah Proposal</td>
                                        <td>Seminar Proposal</td>
                                        <td>Rerata Nilai Proposal</td>

                                        <td>Penyanggah Skripsi</td>
                                        <td>Seminar Skripsi</td>
                                        <td>Rerata Nilai Skripsi</td>

                                        <td>Pemb. I</td>
                                        <td>Pemb. II</td>
                                        <td>Rerata</td>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>



<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Export Nilai Mutu</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="filter" action="{{ route('admin.nilai-mutu.export') }}" method="POST">
                    @csrf

                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label> Filter </label>
                                <br>
                                <input type="text" name="filter"
                                    class="form-control filter @error('filter') is-invalid @enderror" id="filter"
                                    autocomplete="off" placeholder="Pilih Bulan dan Tahun">
                                @error('filter')
                                <span class="invalid-feedback ml-1" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                                <small class="form-text text-muted">Biarkan kosong untuk export semua data</small>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary" form="filter">Export</button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('tambahStyle')
<link rel="stylesheet" href="{{ url('adminarea/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ url('adminarea/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr@latest/dist/plugins/monthSelect/style.css">
<style>
    #btnHapus {
        pointer-events: none;
        display: none;
    }
</style>
@endpush

@push('tambahScript')
<script src="{{ url('adminarea/plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ url('adminarea/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ url('adminarea/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ url('adminarea/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script src="https://npmcdn.com/flatpickr/dist/l10n/id.js"></script>
<script src="https://cdn.jsdelivr.net/npm/flatpickr@latest/dist/plugins/monthSelect/index.js"></script>
<script>
    function htmlDecode(data){
        var txt=document.createElement('textarea');
        txt.innerHTML=data;
        return txt.value
    }

$(document).ready(function() {
    $('#table-nilai').DataTable({
      language:{
            "url":"//cdn.datatables.net/plug-ins/1.10.21/i18n/Indonesian.json",
            "sEmptyTable":"Tidak Ada Data"
      },
      processing:true,
      serverside:true,
      ajax:"{{ route('ajax.adminGetNilaiMutu') }}",
      columns:[
        {data:'DT_RowIndex', name:'DT_RowIndex',orderable: false, searchable: false},
        {data:'nama', name:'nama'},
        {data:'rerata_nilai_penyanggah_sup', name:'rerata_nilai_penyanggah_sup', orderable: false, searchable: false},
        {data:'rerata_nilai_sup', name:'rerata_nilai_sup', orderable: false, searchable: false},
        {data:'rerata_nilai_usulan_penelitian', name:'rerata_nilai_usulan_penelitian', orderable: false, searchable: false},
        //
        {data:'rerata_nilai_penyanggah_shp', name:'rerata_nilai_penyanggah_shp', orderable: false, searchable: false},
        {data:'rerata_nilai_shp', name:'rerata_nilai_shp', orderable: false, searchable: false},
        {data:'rerata_nilai_hasil_penelitian', name:'rerata_nilai_hasil_penelitian', orderable: false, searchable: false},
        //
        {data:'rerata_nilai_seminar', name:'rerata_nilai_seminar', orderable: false, searchable: false},
        {data:'nilai_p1_ujian_skripsi', name:'nilai_p1_ujian_skripsi', orderable: false, searchable: false},
        {data:'nilai_p2_ujian_skripsi', name:'nilai_p2_ujian_skripsi', orderable: false, searchable: false},
        {data:'rerata_nilai_ujian_skripsi', name:'rerata_nilai_ujian_skripsi', orderable: false, searchable: false},
        {data:'nilai_komprehensif', name:'nilai_komprehensif', orderable: false, searchable: false},
        {data:'nilai_skripsi', name:'nilai_skripsi', orderable: false, searchable: false},
        {data:'nilai_mutu', name:'nilai_mutu', orderable: true, searchable: true},
        {data: "aksi",
                render: function(data){
                    return htmlDecode(data);
                }, orderable: false, searchable: false
        }

      ]
    });

});

flatpickr.localize(flatpickr.l10ns.id);
$(".filter").attr('readonly', false);
$(".filter").flatpickr({
    allowInput:true,
    altInput: true,
    static: true,
    plugins: [new monthSelectPlugin({shorthand: false, dateFormat: "Y-m", altFormat: "M Y"})]
});
</script>
@endpush
