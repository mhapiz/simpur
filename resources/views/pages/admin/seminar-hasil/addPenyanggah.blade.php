@extends('layouts.backend')

@section('title')
SIMPUR | Seminar Hasil Skripsi
@endsection

@section('header')
<div class="header text-center">
    <h2 class=" title">Tambah Penyanggah Seminar Hasil Skripsi</h2>
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12 col-md-6">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb bg-white rounded-pill shadow">
                        <li class="breadcrumb-item"><a href="{{ route('admin.index') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.hasil.index') }}">Seminar Hasil
                                Penelitian</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Tambah Penyanggah
                        </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row mt-5 mt-md-3 mt-lg-0">
    <div class="col-12">
        <div class="card card-body">
            <label class="">Mahasiswa</label>

            <div class="">
                <div class="form-group">
                    <select id="selectNIM" class="@error('nim_seminaris') is-invalid @enderror" style="width: 100%;"
                        disabled>
                        <option> {{ $seminaris->nim }} -- {{ $seminaris->nama_mahasiswa }}</option>
                    </select>
                </div>
            </div>

            @if($errors->count() > 0)
            <p>The following errors have occurred:</p>

            <ul>
                @foreach($errors->all() as $message)
                <li>{{$message}}</li>
                @endforeach
            </ul>
            @endif

            <div class="row">
                <div class="col-12 col-md-6">
                    <label class="">Pembimbing 1</label>

                    <div class="form-group">
                        <select id="selectPembimbing1" name="pembimbing1"
                            class="@error('pembimbing1') is-invalid @enderror" style="width: 100%;" disabled>
                            <option></option>
                            @foreach ($dosen as $d)
                            <option value="{{ $d->nip }}" {{ $d->nip == $shp->pembimbing1 ? 'selected' : '' }}>
                                {{ $d->nip }} -- {{ $d->nama_dosen }}</option>
                            @endforeach
                        </select>
                        @error('nim_seminaris')
                        <span class="invalid-feedback ml-1" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <label class="">Pembimbing 2</label>

                    <div class="form-group">
                        <select id="selectPembimbing2" name="pembimbing2"
                            class="@error('pembimbing2') is-invalid @enderror" style="width: 100%;" disabled>
                            <option></option>
                            @foreach ($dosen as $d)
                            <option value="{{ $d->nip }}" {{ $d->nip == $shp->pembimbing2 ? 'selected' : '' }}>
                                {{ $d->nip }} -- {{ $d->nama_dosen }}</option>
                            @endforeach
                        </select>
                        @error('nim_seminaris')
                        <span class="invalid-feedback ml-1" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
            </div>

            <label class="">Tanggal Seminar</label>

            <div class="">
                <div class="form-group">

                    <form id="addPenyanggah"
                        action="{{ route('admin.hasil.storePenyanggah', $shp->id_seminar_hasil_penelitian) }}"
                        method="POST">
                        @csrf

                        @if ($shp->tgl_seminar == null)
                        <input type="text" name="tgl_seminar" class="@error('tgl_seminar') is-invalid @enderror"
                            id="tgl_seminar" autocomplete="off" value="{{ $shp->tgl_seminar }}">
                        @else
                        <input type="text" name="tgl_seminar" class="@error('tgl_seminar') is-invalid @enderror"
                            id="tgl_seminar" autocomplete="off" value="{{ $shp->tgl_seminar }}" disabled>
                        @endif

                        @error('tgl_seminar')
                        <span class="invalid-feedback ml-1" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                </div>
            </div>

            <label class="">Judul</label>

            <div class="">
                <div class="form-group">
                    <textarea name="judul" style="width: 100%" rows="5" disabled>{{ $shp->judul }}</textarea>
                    @error('judul')
                    <span class="invalid-feedback ml-1" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>


            <div class="row">
                <div class="col-12 col-md-6">
                    <label class="">Penyanggah 1</label>

                    <div class="form-group">
                        <select id="selectPenyanggah1" name="penyanggah1"
                            class="@error('penyanggah1') is-invalid @enderror" style="width: 100%;">
                            <option></option>

                            @if ($shp->penyanggah1 != null)
                            <option value="{{ $shp->penyanggah1 }}" selected>{{ $shp->penyanggah1 }} --
                                {{ \App\Models\Mahasiswa::where('nim','=',$shp->penyanggah1)->first()->nama_mahasiswa }}
                            </option>
                            @endif

                            @foreach ($mhs as $m)
                            <option value="{{ $m->nim }}" {{ $m->nim == $shp->penyanggah1 ? 'selected' : '' }}>
                                {{ $m->nim }} -- {{ $m->nama_mahasiswa }}</option>
                            @endforeach
                        </select>
                        @error('penyanggah1')
                        <span class="invalid-feedback ml-1" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <label class="">Penyanggah 2</label>

                    <div class="form-group">
                        <select id="selectPenyanggah2" name="penyanggah2"
                            class="@error('penyanggah2') is-invalid @enderror" style="width: 100%;">
                            <option></option>

                            @if ($shp->penyanggah2 != null)
                            <option value="{{ $shp->penyanggah2 }}" selected>{{ $shp->penyanggah2 }} --
                                {{ \App\Models\Mahasiswa::where('nim','=',$shp->penyanggah2)->first()->nama_mahasiswa }}
                            </option>
                            @endif

                            @foreach ($mhs as $m)
                            <option value="{{ $m->nim }}" {{ $m->nim == $shp->penyanggah2 ? 'selected' : '' }}>
                                {{ $m->nim }} -- {{ $m->nama_mahasiswa }}</option>
                            @endforeach
                        </select>
                        @error('penyanggah2')
                        <span class="invalid-feedback ml-1" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
            </div>
            </form>

            <div class="row">
                <div class="col-12 text-right">
                    <button type="submit" form="addPenyanggah" class="btn btn-success">Tambah Penyanggah</button>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection

@push('tambahStyle')
<link rel="stylesheet" href="{{ url('adminarea/plugins/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ url('adminarea/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">

@endpush

@push('tambahScript')
<script src="{{ url('adminarea/plugins/select2/js/select2.full.min.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script src="https://npmcdn.com/flatpickr/dist/l10n/id.js"></script>
<script>
    $('#selectNIM, #selectPembimbing1, #selectPembimbing2,#selectPenyanggah1, #selectPenyanggah2').select2({
        placeholder: "Pilih ...",
        theme: 'bootstrap4',
    });

    flatpickr.localize(flatpickr.l10ns.id);
    $('#tgl_seminar').attr('readonly', false);
    $('#tgl_seminar').flatpickr({
        allowInput:true,
        altInput: true,
        altFormat: "j F Y",
        dateFormat: "Y-m-d H:i",
    });


</script>
@endpush
