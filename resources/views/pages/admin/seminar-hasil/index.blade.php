@extends('layouts.backend')

@section('title')
SIMPUR | Seminar Hasil Penelitian
@endsection

@section('header')
<div class="header text-center">
    <h2 class="title">Seminar Hasil Penelitian</h2>
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12 col-md-6">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb bg-white rounded-pill shadow">
                        <li class="breadcrumb-item"><a href="{{ route('admin.index') }}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Seminar Hasil Penelitian</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row mt-5 mt-md-0">
    <div class="col-12">
        <div class="card rounded-pill">
            <div
                class="card-body px-0 pl-md-4 pr-md-4 pb-3 d-flex flex-column flex-lg-row align-items-center justify-content-between">

                <div class="d-flex">
                    <a href="{{ route('admin.hasil.index') }}"
                        class="btn {{ (request()->is('admin/hasil-penelitian')) ? 'btn-success' : 'btn-outline-success' }} btn-sm rounded-pill d-flex align-items-center">
                        Semua
                    </a>

                    <a href="{{ route('admin.hasil.indexSudahDinilai') }}"
                        class="btn {{ (request()->is('admin/hasil-penelitian/sudah-dinilai')) ? 'btn-success' : 'btn-outline-success' }} btn-sm rounded-pill d-flex align-items-center">
                        Sudah Dinilai
                    </a>

                </div>

                <div class="d-flex">
                    <form class="d-flex align-items-center" action="" method="GET">
                        <input class="form-control" type="search" placeholder="Cari Nama " aria-label="Search" name="q"
                            style="height: 2rem">
                        <button
                            class="btn btn-primary rounded-pill my-2 my-sm-0 d-flex justify-content-between align-items-center"
                            type="submit" style="height: 2rem">
                            <i class="fas fa-search"></i>
                            <span class="ml-2 d-none d-md-block">Cari</span>
                        </button>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
<div class="row">

    @foreach ($dataSHP as $shp)
    <div class="col-12 col-lg-6">
        <div class="card">
            <div class="card-header d-flex justify-content-between align-items-center">
                <h5 class="card-category">Seminar Hasil Penelitian</h5>

                <div class="d-flex">
                    <a href="{{ route('admin.hasil.show', $shp->id_seminar_hasil_penelitian) }}"
                        class="btn btn-round btn-outline-default btn-simple btn-icon no-caret d-flex justify-content-center align-items-center"
                        data-toggle="tooltip" data-placement="right" title="Lihat Nilai">
                        <span class="material-icons">
                            visibility
                        </span>
                    </a>

                    <div class="dropdown  mx-2">
                        <button type="button"
                            class="btn btn-round btn-outline-default dropdown-toggle btn-simple btn-icon no-caret d-flex justify-content-center align-items-center"
                            data-toggle="dropdown">
                            <span class="material-icons">
                                list
                            </span>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item"
                                href="{{ route('admin.hasil.edit', $shp->id_seminar_hasil_penelitian) }}">
                                Edit </a>
                            <a class="dropdown-item"
                                href="{{ route('admin.hasil.tambahPenyanggah', $shp->id_seminar_hasil_penelitian) }}">
                                Tambah Penyanggah</a>
                        </div>
                    </div>

                    <div class="dropdown">
                        <button type="button"
                            class="btn btn-round btn-outline-default dropdown-toggle btn-simple btn-icon no-caret d-flex justify-content-center align-items-center"
                            data-toggle="dropdown">
                            <span class="material-icons">
                                print
                            </span>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item"
                                href="{{ route('admin.hasil.cetakNilaiHasil', $shp->id_seminar_hasil_penelitian) }}"
                                target="_blank" data-toggle=" tooltip" data-placement="right"
                                title="Cetak Nilai Seminar Hasil Penelitian">
                                Cetak Nilai Seminar Hasil Penelitian
                            </a>
                            <a class="dropdown-item"
                                href="{{ route('admin.hasil.cetakNilaiPenyanggahHasil', $shp->id_seminar_hasil_penelitian) }}"
                                target="_blank" data-toggle=" tooltip" data-placement="right"
                                title="Cetak Nilai Seminar Hasil Penelitian">
                                Cetak Nilai Penyanggah Seminar Hasil Penelitian
                            </a>
                            <a class="dropdown-item"
                                href="{{ route('admin.hasil.cetak', $shp->id_seminar_hasil_penelitian) }}"
                                target="_blank" data-toggle=" tooltip" data-placement="right"
                                title="Cetak Nilai Seminar Hasil Penelitian">
                                Cetak Nilai Seminar Hasil Penelitian dan Penyanggah
                            </a>
                        </div>
                    </div>

                </div>

            </div>
            <div class="card-body">

                <dl class="row">
                    <dt class="mt-3 mt-md-2 mt-lg-0 col-4 col-md-5 d-flex justify-content-between">
                        Nama / NIM Seminaris<span class="ml-1">:</span>
                    </dt>
                    <dd class="mt-3 mt-md-2 mt-lg-0 col-8 col-md-7">
                        {{ $shp->mahasiswa->nama_mahasiswa }} / {{ $shp->nim_seminaris }}
                    </dd>

                    <dt class="mt-3 mt-md-2 mt-lg-0 col-4 col-md-5 d-flex justify-content-between">
                        Program Studi / Minat<span class="ml-1">:</span>
                    </dt>
                    <dd class="mt-3 mt-md-2 mt-lg-0 col-8 col-md-7">
                        Kehutanan / {{ $shp->mahasiswa->minat }}
                    </dd>

                    <dt
                        class="mt-3 mt-md-2 mt-lg-0 col-4 col-md-5 d-flex justify-content-between {{ $shp->tgl_seminar == null ? 'pt-1 pt-md-3' : '' }}">
                        Hari / Tgl Seminar<span class="ml-1">:</span>
                    </dt>
                    <dd class="mt-3 mt-md-2 mt-lg-0 col-8 col-md-7 d-flex">
                        @if ($shp->tgl_seminar == null)
                        <form class="row d-flex align-items-center"
                            action="{{ route('admin.hasil.addTanggal', $shp->id_seminar_hasil_penelitian) }}"
                            method="POST">
                            @method('PUT')
                            @csrf
                            <div class="col-12 col-md-6">
                                <input type="text" name="tanggal" class="tanggal @error('tanggal') is-invalid @enderror"
                                    id="tanggal" autocomplete="off">
                                @error('tanggal')
                                <span class="invalid-feedback ml-1" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="col-12 col-md-6">
                                <button type="submit" class="btn btn-primary btn-sm">Tentukan</button>
                            </div>
                        </form>
                        @else
                        {{ \Carbon\Carbon::parse($shp->tgl_seminar)->isoFormat('dddd / D MMMM Y') }}
                        @endif
                    </dd>

                    <dt class="mt-3 mt-md-2 mt-lg-0 col-4 col-md-5 d-flex justify-content-between">
                        Judul Seminar<span class="ml-1">:</span>
                    </dt>
                    <dd class="mt-3 mt-md-2 mt-lg-0 col-8 col-md-7">
                        {{ \Illuminate\Support\Str::limit($shp->judul, 350, $end='...') }}
                    </dd>
                </dl>

            </div>
            <hr>
            <div class="card-footer">
                <div class="row">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-12">
                                <h6 class="text-center">Pembimbing</h6>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="d-flex justify-content-between align-items-center" style="">
                                    <div class="text-truncate" style="">
                                        <small
                                            class="text-muted text-sm">{{ App\Models\Dosen::where('nip','=' ,$shp->pembimbing1)->first()->nama_dosen }}</small>
                                    </div>
                                    <div class="d-flex align-items-center">
                                        Nilai :
                                        @if ($shp->nilai_pembimbing1 == null)
                                        <span class="ml-2 material-icons text-danger">
                                            highlight_off
                                        </span>
                                        @else
                                        <span class="ml-2 material-icons text-success">
                                            check_circle_outline
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="d-flex justify-content-between align-items-center">
                                    <div class="text-truncate" style="">
                                        <small
                                            class="text-muted text-sm">{{ App\Models\Dosen::where('nip','=' ,$shp->pembimbing2)->first()->nama_dosen }}</small>
                                    </div>
                                    <div class="d-flex align-items-center">
                                        Nilai :
                                        @if ($shp->nilai_pembimbing2 == null)
                                        <span class="ml-2 material-icons text-danger">
                                            highlight_off
                                        </span>
                                        @else
                                        <span class="ml-2 material-icons text-success">
                                            check_circle_outline
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="row">
                            <div class="col-12">
                                <h6 class="text-center">Penyanggah</h6>
                            </div>
                            <div class="col-12">
                                <div class="d-flex justify-content-between align-items-center" style="width: 100%">
                                    <div>
                                        @if ($shp->penyanggah1 == null)
                                        <small class="text-muted text-sm text-danger">
                                            Belum Ditentukan
                                        </small>
                                        @else
                                        <small class="text-muted text-sm">
                                            {{ App\Models\Mahasiswa::where('nim','=' ,$shp->penyanggah1)->first()->nama_mahasiswa }}
                                        </small>
                                        @endif
                                    </div>
                                    <div class="d-flex align-items-center">
                                        Nilai :
                                        @if ($shp->nilai_penyanggah1 == null)
                                        <span class="ml-2 material-icons text-danger">
                                            highlight_off
                                        </span>
                                        @else
                                        <span class="ml-2 material-icons text-success">
                                            check_circle_outline
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="d-flex justify-content-between align-items-center">
                                    <div>
                                        @if ($shp->penyanggah2 == null)
                                        <small class="text-muted text-sm text-danger">
                                            Belum Ditentukan
                                        </small>
                                        @else
                                        <small class="text-muted text-sm">
                                            {{ App\Models\Mahasiswa::where('nim','=' ,$shp->penyanggah2)->first()->nama_mahasiswa }}
                                        </small>
                                        @endif

                                    </div>
                                    <div class="d-flex align-items-center">
                                        Nilai :
                                        @if ($shp->nilai_penyanggah2 == null)
                                        <span class="ml-2 material-icons text-danger">
                                            highlight_off
                                        </span>
                                        @else
                                        <span class="ml-2 material-icons text-success">
                                            check_circle_outline
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    @endforeach

</div>

<div class="row">
    <div class="col-12">
        {{ $dataSHP->links('modules.backend.pagination') }}
    </div>
</div>
@endsection

@push('tambahStyle')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
@endpush
@push('tambahScript')
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script src="https://npmcdn.com/flatpickr/dist/l10n/id.js"></script>

<script>
    flatpickr.localize(flatpickr.l10ns.id);
    $('.tanggal').each(function() {
        $(this).attr('readonly', false);
        $(this).flatpickr({
            allowInput:true,
            altInput: true,
            altFormat: "j F Y",
            dateFormat: "Y-m-d H:i",
        });
    });
</script>
@endpush
