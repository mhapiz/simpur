@extends('layouts.backend')

@section('title')
SIMPUR | Seminar Usulan Skripsi
@endsection

@section('header')
<div class="header text-center">
    <h2 class=" title">Tambah Penyanggah Seminar Usulan Skripsi</h2>
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12 col-md-6">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb bg-white rounded-pill shadow">
                        <li class="breadcrumb-item"><a href="{{ route('admin.index') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.usulan.index') }}">Seminar
                                Usulan Skripsi</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Tambah Penyanggah
                        </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card card-body">
            <label class="">Mahasiswa</label>

            <div class="">
                <div class="form-group">
                    <select id="selectNIM" name="nim_seminaris" class="@error('nim_seminaris') is-invalid @enderror"
                        style="width: 100%;" disabled>
                        <option>{{ $seminaris->nim }} -- {{ $seminaris->nama_mahasiswa }}</option>
                    </select>

                </div>
            </div>

            <div class="row">
                <div class="col-12 col-md-6">
                    <label class="">Pembimbing 1</label>

                    <div class="form-group">
                        <select id="selectPembimbing1" name="pembimbing1"
                            class="@error('pembimbing1') is-invalid @enderror" style="width: 100%;" disabled>
                            <option></option>
                            @foreach ($dosen as $d)
                            <option value="{{ $d->nip }}" {{ $d->nip == $sup->pembimbing1 ? 'selected' : '' }}>
                                {{ $d->nip }} -- {{ $d->nama_dosen }}</option>
                            @endforeach
                        </select>
                        @error('nim_seminaris')
                        <span class="invalid-feedback ml-1" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <label class="">Pembimbing 2</label>

                    <div class="form-group">
                        <select id="selectPembimbing2" name="pembimbing2"
                            class="@error('pembimbing2') is-invalid @enderror" style="width: 100%;" disabled>
                            <option></option>
                            @foreach ($dosen as $d)
                            <option value="{{ $d->nip }}" {{ $d->nip == $sup->pembimbing2 ? 'selected' : '' }}>
                                {{ $d->nip }} -- {{ $d->nama_dosen }}</option>
                            @endforeach
                        </select>
                        @error('nim_seminaris')
                        <span class="invalid-feedback ml-1" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
            </div>

            <label class="">Tanggal Seminar</label>

            <div class="">
                <div class="form-group">
                    <input type="text" name="tgl_seminar" class="@error('tgl_seminar') is-invalid @enderror"
                        id="tgl_seminar" autocomplete="off" value="{{ $sup->tgl_seminar }}" disabled>
                    @error('tgl_seminar')
                    <span class="invalid-feedback ml-1" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>

            <label class="">Judul</label>

            <div class="">
                <div class="form-group">
                    <textarea name="judul" style="width: 100%" rows="5" disabled>{{ $sup->judul }}</textarea>
                    @error('judul')
                    <span class="invalid-feedback ml-1" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>

            <form id="addPenyanggah"
                action="{{ route('admin.usulan.storePenyanggah', $sup->id_seminar_usulan_penelitian) }}" method="POST">
                @csrf
                <div class="row">
                    <div class="col-12 col-md-6">
                        <label class="">Penyanggah 1</label>

                        <div class="form-group">
                            <select id="selectPenyanggah1" name="penyanggah1"
                                class="@error('penyanggah1') is-invalid @enderror" style="width: 100%;">
                                <option></option>
                                @if ($sup->penyanggah1 != null)
                                <option value="{{ $sup->penyanggah1 }}" selected>{{ $sup->penyanggah1 }} --
                                    {{ \App\Models\Mahasiswa::where('nim','=',$sup->penyanggah1)->first()->nama_mahasiswa }}
                                </option>
                                @endif

                                @foreach ($mhs as $m)
                                <option value="{{ $m->nim }}">
                                    {{ $m->nim }} -- {{ $m->nama_mahasiswa }}</option>
                                @endforeach

                            </select>
                            @error('penyanggah1')
                            <span class="invalid-feedback ml-1" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <label class="">Penyanggah 2</label>

                        <div class="form-group">
                            <select id="selectPenyanggah2" name="penyanggah2"
                                class="@error('penyanggah2') is-invalid @enderror" style="width: 100%;">
                                <option></option>

                                @if ($sup->penyanggah2 != null)
                                <option value="{{ $sup->penyanggah2 }}" selected>{{ $sup->penyanggah2 }} --
                                    {{ \App\Models\Mahasiswa::where('nim','=',$sup->penyanggah2)->first()->nama_mahasiswa }}
                                </option>
                                @endif

                                @foreach ($mhs as $m)
                                <option value="{{ $m->nim }}">
                                    {{ $m->nim }} -- {{ $m->nama_mahasiswa }}</option>
                                @endforeach

                            </select>
                            @error('penyanggah2')
                            <span class="invalid-feedback ml-1" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                </div>
            </form>

            <div class="row">
                <div class="col-12 text-right">
                    <button type="submit" form="addPenyanggah" class="btn btn-success">Tambah Penyanggah</button>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection

@push('tambahStyle')
<link rel="stylesheet" href="{{ url('adminarea/plugins/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ url('adminarea/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
@endpush

@push('tambahScript')
<script src="{{ url('adminarea/plugins/select2/js/select2.full.min.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script src="https://npmcdn.com/flatpickr/dist/l10n/id.js"></script>
<script>
    $('#selectNIM, #selectPembimbing1, #selectPembimbing2,#selectPenyanggah1, #selectPenyanggah2').select2({
        placeholder: "Pilih ...",
        theme: 'bootstrap4',
    });

  flatpickr.localize(flatpickr.l10ns.id);
    $('#tgl_seminar').flatpickr({
        allowInput:true,
        altInput: true,
        altFormat: "j F Y",
        dateFormat: "Y-m-d H:i",
    });
</script>
@endpush