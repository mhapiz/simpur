@extends('layouts.backend')

@section('title')
SIMPUR | Seminar Usulan Skripsi
@endsection

@section('header')
<div class="header text-center">
    <h2 class=" title">Tambah Seminar Usulan Skripsi</h2>
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12 col-md-6">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb bg-white rounded-pill shadow">
                        <li class="breadcrumb-item"><a href="{{ route('admin.index') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.usulan.index') }}">Seminar
                                Usulan Skripsi</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Tambah Seminar Usulan Skripsi</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row mt-5 mt-md-3 mt-lg-0">
    <div class="col-12">
        <div class="card card-body">
            <form action="{{ route('admin.usulan.store') }}" method="POST">
                @csrf

                <div class="row">
                    <div class="col-12 col-md-10">
                        <label class="">Mahasiswa</label>

                        <div class="">
                            <div class="form-group">
                                <select id="selectNIM" name="nim_seminaris"
                                    class="@error('nim_seminaris') is-invalid @enderror" style="width: 100%;">
                                    <option></option>
                                    @foreach ($mhs as $m)
                                    @if (old('nim_seminaris') == $m->nim)
                                    <option value="{{ $m->nim }}" selected>
                                        {{ $m->nim }} -- {{ $m->nama_mahasiswa }}
                                    </option>
                                    @else
                                    <option value="{{ $m->nim }}">
                                        {{ $m->nim }} -- {{ $m->nama_mahasiswa }}
                                    </option>
                                    @endif
                                    @endforeach
                                </select>
                                @error('nim_seminaris')
                                <span class="invalid-feedback ml-1" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-md-2 d-flex align-items-end">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#mhsModal">
                            Tambah Mahasiswa
                        </button>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-md-6">
                        <label class="">Pembimbing 1</label>

                        <div class="form-group">
                            <select id="selectPembimbing1" name="pembimbing1"
                                class="@error('pembimbing1') is-invalid @enderror" style="width: 100%;">
                                <option></option>
                                @foreach ($dosen as $d)
                                <option value="{{ $d->nip }}" {{ old('pembimbing1' == $d->nip ? 'selected' : '') }}>
                                    {{ $d->nip }}
                                    -- {{ $d->nama_dosen }}</option>
                                @endforeach
                            </select>
                            @error('pembimbing1')
                            <span class="invalid-feedback ml-1" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <label class="">Pembimbing 2</label>

                        <div class="form-group">
                            <select id="selectPembimbing2" name="pembimbing2"
                                class="@error('pembimbing2') is-invalid @enderror" style="width: 100%;">
                                <option></option>
                                @foreach ($dosen as $d)
                                <option value="{{ $d->nip }}" {{ old('pembimbing2' == $d->nip ? 'selected' : '') }}>
                                    {{ $d->nip }} -- {{ $d->nama_dosen }}</option>
                                @endforeach
                            </select>
                            @error('pembimbing2')
                            <span class="invalid-feedback ml-1" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                </div>

                <label class="">Tanggal Seminar</label>

                <div class="">
                    <div class="form-group">
                        <input type="text" name="tgl_seminar" class="@error('tgl_seminar') is-invalid @enderror"
                            id="tgl_seminar" autocomplete="off">
                        @error('tgl_seminar')
                        <span class="invalid-feedback ml-1" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

                <label class="">Judul</label>

                <div class="">
                    <div class="form-group">
                        <textarea name="judul" style="width: 100%" rows="5">{{ old('judul') }}</textarea>
                        @error('judul')
                        <span class="invalid-feedback ml-1" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

                <label class="">Nilai Menyanggah</label>

                <div class="row">
                    <div class="col-12 col-md-2">
                        <div class="form-group">
                            <input type="text" id="nilai_menyanggah" class="form-control" name="nilai_menyanggah">
                            @error('nilai_menyanggah')
                            <span class="invalid-feedback ml-1" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                            {{-- <small class="form-text text-warning ml-3">*Optional</small> --}}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 text-right">
                        <button type="submit" class="btn btn-success">Tambah</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>



<!-- Modal -->
<div class="modal fade" id="mhsModal" tabindex="-1" aria-labelledby="mhsModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="mhsModalLabel">Tambah Cepat Data Mahasiswa</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="addMhs" action="{{ route('admin.user-manage.mahasiswa.store') }}" method="POST">
                    @csrf
                    <input type="hidden" name="quickAdd" value="yes">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>NIM</label>
                                <input type="text" name="nim" class="form-control @error('nim') is-invalid @enderror"
                                    placeholder="Masukkan NIM" value="{{ old('nim') }}">
                                @error('nim')
                                <div class="invalid-feedback ml-3">
                                    *{{ $message }}
                                </div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Nama Mahasiswa</label>
                                <input type="text" name="nama_mahasiswa"
                                    class="form-control @error('nama_mahasiswa') is-invalid @enderror"
                                    placeholder="Nama Mahasiswa" value="{{ old('nama_mahasiswa') }}">
                                @error('nama_mahasiswa')
                                <div class="invalid-feedback ml-3">
                                    *{{ $message }}
                                </div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Minat</label>
                                <select name="minat"
                                    class="custom-select form-control @error('minat') is-invalid @enderror">
                                    <option></option>
                                    <option value="Manajemen Hutan">Manajemen Hutan</option>
                                    <option value="Silvikultur">Silvikultur</option>
                                    <option value="Teknologi Hasil Hutan">Teknologi Hasil Hutan</option>
                                </select>
                                @error('minat')
                                <div class="invalid-feedback ml-3">
                                    *{{ $message }}
                                </div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Tahun Angkatan</label>
                                <input type="text" name="tahun_angkatan"
                                    class="form-control @error('tahun_angkatan') is-invalid @enderror"
                                    placeholder="tahun Angkatan" value="{{ old('tahun_angkatan') }}">
                                @error('tahun_angkatan')
                                <div class="invalid-feedback ml-3">
                                    *{{ $message }}
                                </div>
                                @enderror
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" form="addMhs" class="btn btn-primary">Tambah</button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('tambahStyle')
<link rel="stylesheet" href="{{ url('adminarea/plugins/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ url('adminarea/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">

@endpush

@push('tambahScript')
<script src="{{ url('adminarea/plugins/select2/js/select2.full.min.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script src="https://npmcdn.com/flatpickr/dist/l10n/id.js"></script>
<script>
    $('#selectNIM, #selectPembimbing1, #selectPembimbing2').select2({
            placeholder: "Pilih ...",
            theme: 'bootstrap4',
        });
        flatpickr.localize(flatpickr.l10ns.id);
        $('#tgl_seminar').flatpickr({
            allowInput: true,
            altInput: true,
            altFormat: "j F Y",
            dateFormat: "Y-m-d H:i",
        });

        $('#selectNIM').change(function() {
            var nim = $(this).val();
            var _token = $('input[name="_token"]').val();
            // console.log(id);
            $.ajax({
                url: "{{ route('admin.usulan.fetchNilai') }}",
                method: "POST",
                data: {
                    nim: nim,
                    _token: _token
                },
                success: function(res) {
                    if (res) {
                        $('#nilai_menyanggah').val(res);
                        $('#nilai_menyanggah').prop("readonly", true);
                        // console.log(res);

                    } else {
                        $('#nilai_menyanggah').val(null);
                        $('#nilai_menyanggah').prop("readonly", false);
                        // console.log(res);
                    }
                }
            });
        });

</script>
@endpush
