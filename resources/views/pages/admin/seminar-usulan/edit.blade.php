@extends('layouts.backend')

@section('title')
    SIMPUR | Seminar Usulan Skripsi
@endsection

@section('header')
    <div class="header text-center d-none">
        {{-- <h2 class=" title">Edit Seminar Usulan Skripsi</h2>
    <p class="category">Handcrafted by our friend <a target="_blank" href="https://github.com/mouse0270">Robert
            McIntosh</a>. Please checkout the <a href="http://bootstrap-notify.remabledesigns.com/" target="_blank">full
            documentation.</a></p> --}}
    </div>
@endsection

@section('content')
    <div class="row" style="margin-top: -10rem">
        <div class="col-12">
            <div class="card card-body">
                <form class="form-horizontal"
                    action="{{ route('admin.usulan.update', $sup->id_seminar_usulan_penelitian) }}" method="POST">
                    @method('PUT')
                    @csrf
                    <label class="">Mahasiswa</label>

                    <div class="">
                        <div class="form-group">
                            <select id="selectNIM" name="nim_seminaris" class="@error('nim_seminaris') is-invalid @enderror"
                                style="width: 100%;">
                                <option>Cari Mahasiswa</option>
                                @foreach ($mhs as $m)
                                    <option value="{{ $m->nim }}"
                                        {{ $m->nim == $sup->nim_seminaris ? 'selected' : '' }}>
                                        {{ $m->nim }} --
                                        {{ $m->nama_mahasiswa }}</option>
                                @endforeach
                            </select>
                            @error('nim_seminaris')
                                <span class="invalid-feedback ml-1" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-md-6">
                            <label class="">Pembimbing 1</label>

                            <div class="form-group">
                                <select id="selectPembimbing1" name="pembimbing1"
                                    class="@error('pembimbing1') is-invalid @enderror" style="width: 100%;">
                                    <option>Pilih Pembimbing</option>
                                    @foreach ($dosen as $d)
                                        <option value="{{ $d->nip }}"
                                            {{ $d->nip == $sup->pembimbing1 ? 'selected' : '' }}>
                                            {{ $d->nip }}
                                            -- {{ $d->nama_dosen }}</option>
                                    @endforeach
                                </select>
                                @error('nim_seminaris')
                                    <span class="invalid-feedback ml-1" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <label class="">Pembimbing 2</label>

                            <div class="form-group">
                                <select id="selectPembimbing2" name="pembimbing2"
                                    class="@error('pembimbing2') is-invalid @enderror" style="width: 100%;">
                                    <option>Pilih Pembimbing</option>
                                    @foreach ($dosen as $d)
                                        <option value="{{ $d->nip }}"
                                            {{ $d->nip == $sup->pembimbing2 ? 'selected' : '' }}>
                                            {{ $d->nip }} -- {{ $d->nama_dosen }}
                                        </option>
                                    @endforeach

                                </select>
                                @error('nim_seminaris')
                                    <span class="invalid-feedback ml-1" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <label class="">Tanggal Seminar</label>

                    <div class="">
                        <div class="form-group">
                            <input type="text" name="tgl_seminar" class="@error('tgl_seminar') is-invalid @enderror"
                                id="tgl_seminar" autocomplete="off" value="{{ $sup->tgl_seminar }}">
                            @error('tgl_seminar')
                                <span class="invalid-feedback ml-1" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <label class="">Judul</label>

                    <div class="">
                        <div class="form-group">
                            {{-- <input type="text" name="judul"
                            class="form-control @error('judul') is-invalid @enderror rounded-lg"> --}}
                            <textarea name="judul" style="width: 100%" rows="5">{{ $sup->judul }}</textarea>
                            @error('judul')
                                <span class="invalid-feedback ml-1" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <label class="">Nilai Menyanggah</label>

                    <div class="row">
                        <div class="col-12 col-md-2">
                            <div class="form-group">
                                <input type="text" id="nilai_menyanggah" class="form-control" name="nilai_menyanggah"
                                    value="{{ $sup->nilaiMutu->rerata_nilai_penyanggah_sup }}">
                                @error('nilai_menyanggah')
                                    <span class="invalid-feedback ml-1" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                {{-- <small class="form-text text-warning ml-3">*Optional</small> --}}
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 text-right">
                            <button type="submit" class="btn btn-success">Perbarui</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    </div>
@endsection

@push('tambahStyle')
    <link rel="stylesheet" href="{{ url('adminarea/plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ url('adminarea/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">

@endpush

@push('tambahScript')
    <script src="{{ url('adminarea/plugins/select2/js/select2.full.min.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script src="https://npmcdn.com/flatpickr/dist/l10n/id.js"></script>
    <script>
        $('#selectNIM, #selectPembimbing1, #selectPembimbing2').select2({
            placeholder: "Select a state",
            theme: 'bootstrap4',
            // tags: true,
            // tokenSeparators: [',', ' ']
        });

        flatpickr.localize(flatpickr.l10ns.id);
        $('#tgl_seminar').flatpickr({
            allowInput: true,
            altInput: true,
            altFormat: "j F Y",
            dateFormat: "Y-m-d H:i",
        });

    </script>
@endpush
