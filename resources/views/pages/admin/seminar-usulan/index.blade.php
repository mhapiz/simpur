@extends('layouts.backend')

@section('title')
SIMPUR | Seminar Usulan Penelitian
@endsection

@section('header')
<div class="header text-center">
    <h2 class="title">Seminar Usulan Penelitian</h2>
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12 col-md-6">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb bg-white rounded-pill shadow">
                        <li class="breadcrumb-item"><a href="{{ route('admin.index') }}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Seminar Usulan Penelitian</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row mt-5 mt-md-0">
    <div class="col-12">
        <div class="card rounded-pill">
            <div
                class="card-body px-0 pl-md-4 pr-md-4 pb-3 d-flex flex-column flex-lg-row align-items-center justify-content-between">

                <div class="d-flex">
                    <a href="{{ route('admin.usulan.create') }}"
                        class="btn btn-info btn-sm rounded-pill d-flex align-items-center">
                        <span class="material-icons">
                            add
                        </span>
                        <span class="d-none d-md-block"> Tambah Seminar Usulan Penelitian </span>
                    </a>

                    <a href="{{ route('admin.usulan.index') }}"
                        class="btn {{ (request()->is('admin/usulan-penelitian')) ? 'btn-success' : 'btn-outline-success' }} btn-sm rounded-pill d-flex align-items-center">
                        Semua
                    </a>

                    <a href="{{ route('admin.usulan.indexSudahDinilai') }}"
                        class="btn {{ (request()->is('admin/usulan-penelitian/sudah-dinilai')) ? 'btn-success' : 'btn-outline-success' }} btn-sm rounded-pill d-flex align-items-center">
                        Sudah Dinilai
                    </a>

                </div>

                <div class="d-flex">
                    <form class="d-flex align-items-center" action="" method="GET">
                        <input class="form-control" type="search" placeholder="Cari Nama " aria-label="Search" name="q"
                            style="height: 2rem">
                        <button
                            class="btn btn-primary rounded-pill my-2 my-sm-0 d-flex justify-content-between align-items-center"
                            type="submit" style="height: 2rem">
                            <i class="fas fa-search"></i>
                            <span class="ml-2 d-none d-md-block">Cari</span>
                        </button>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
<div class="row">

    @foreach ($dataSUP as $sup)
    <div class="col-12 col-md-12 col-lg-6">
        <div class="card">
            <div class="card-header d-flex justify-content-between align-items-center">
                <h5 class="card-category">Seminar Usulan Skripsi</h5>

                <div class="d-flex">
                    <a href="{{ route('admin.usulan.show', $sup->id_seminar_usulan_penelitian) }}"
                        class="btn btn-round btn-outline-default btn-simple btn-icon no-caret d-flex justify-content-center align-items-center "
                        data-toggle="tooltip" data-placement="right" title="Lihat Nilai">
                        <span class="material-icons">
                            visibility
                        </span>
                    </a>

                    <div class="dropdown mx-2">
                        <button type="button"
                            class="btn btn-round btn-outline-default dropdown-toggle btn-simple btn-icon no-caret d-flex justify-content-center align-items-center"
                            data-toggle="dropdown">
                            <span class="material-icons">
                                list
                            </span>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item"
                                href="{{ route('admin.usulan.edit', $sup->id_seminar_usulan_penelitian) }}">Edit</a>
                            <a class="dropdown-item"
                                href="{{ route('admin.usulan.tambahPenyanggah', $sup->id_seminar_usulan_penelitian) }}">
                                Tambah Penyanggah</a>
                        </div>
                    </div>

                    <div class="dropdown">
                        <button type="button"
                            class="btn btn-round btn-outline-default dropdown-toggle btn-simple btn-icon no-caret d-flex justify-content-center align-items-center"
                            data-toggle="dropdown">
                            <span class="material-icons">
                                print
                            </span>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item"
                                href="{{ route('admin.usulan.cetakNilaiUsulan', $sup->id_seminar_usulan_penelitian) }}"
                                target="_blank" data-toggle=" tooltip" data-placement="right"
                                title="Cetak Nilai Seminar Usulan Penelitian">
                                Cetak Nilai Seminar Usulan Penelitian
                            </a>
                            <a class="dropdown-item"
                                href="{{ route('admin.usulan.cetakNilaiPenyanggahUsulan', $sup->id_seminar_usulan_penelitian) }}"
                                target="_blank" data-toggle=" tooltip" data-placement="right"
                                title="Cetak Nilai Penyanggah Seminar Usulan Penelitian">
                                Cetak Nilai Penyanggah Seminar Usulan Penelitian
                            </a>
                            <a class="dropdown-item"
                                href="{{ route('admin.usulan.cetak', $sup->id_seminar_usulan_penelitian) }}"
                                target="_blank" data-toggle=" tooltip" data-placement="right"
                                title="Cetak Nilai Seminar Usulan Penelitian">
                                Cetak Nilai Seminar Usulan Penelitian dan Penyanggah
                            </a>

                        </div>
                    </div>


                </div>

            </div>
            <div class="card-body">

                <dl class="row">
                    <dt class="mt-3 mt-md-2 mt-lg-0 col-4 col-md-5 d-flex justify-content-between">
                        Nama / NIM Seminaris<span class="ml-1">:</span>
                    </dt>
                    <dd class="mt-3 mt-md-2 mt-lg-0 col-8 col-md-7">
                        {{ $sup->mahasiswa->nama_mahasiswa }} / {{ $sup->nim_seminaris }}
                    </dd>

                    <dt class="mt-3 mt-md-2 mt-lg-0 col-4 col-md-5 d-flex justify-content-between">
                        Program Studi / Minat<span class="ml-1">:</span>
                    </dt>
                    <dd class="mt-3 mt-md-2 mt-lg-0 col-8 col-md-7">
                        Kehutanan / {{ $sup->mahasiswa->minat }}
                    </dd>

                    <dt class="mt-3 mt-md-2 mt-lg-0 col-4 col-md-5 d-flex justify-content-between">
                        Hari / Tgl Seminar<span class="ml-1">:</span>
                    </dt>
                    <dd class="mt-3 mt-md-2 mt-lg-0 col-8 col-md-7">
                        {{ \Carbon\Carbon::parse($sup->tgl_seminar)->isoFormat('dddd / D MMMM Y') }}
                    </dd>

                    <dt class="mt-3 mt-md-2 mt-lg-0 col-4 col-md-5 d-flex justify-content-between">
                        Judul Seminar<span class="ml-1">:</span>
                    </dt>
                    <dd class="mt-3 mt-md-2 mt-lg-0 col-8 col-md-7">
                        {{ \Illuminate\Support\Str::limit($sup->judul, 350, $end='...') }}
                    </dd>
                </dl>

            </div>
            <hr>
            <div class="card-footer">
                <div class="row">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-12">
                                <h6 class="text-center">Pembimbing</h6>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="d-flex justify-content-between align-items-center" style="">
                                    <div class="text-truncate" style="">
                                        <small
                                            class="text-muted text-sm">{{ App\Models\Dosen::where('nip','=' ,$sup->pembimbing1)->first()->nama_dosen }}</small>
                                    </div>

                                    <div class="d-flex align-items-center justify-content-center">
                                        Nilai :
                                        @if ($sup->nilai_pembimbing1 == null)
                                        <span class="ml-2 material-icons text-danger">
                                            highlight_off
                                        </span>
                                        @else
                                        <span class="ml-2 material-icons text-success">
                                            check_circle_outline
                                        </span>
                                        @endif
                                    </div>

                                </div>

                                <div class="d-flex justify-content-between align-items-center">
                                    <div class="text-truncate" style="">
                                        <small
                                            class="text-muted text-sm">{{ App\Models\Dosen::where('nip','=' ,$sup->pembimbing2)->first()->nama_dosen }}</small>
                                    </div>
                                    <div class="d-flex align-items-center justify-content-center">
                                        Nilai :
                                        @if ($sup->nilai_pembimbing2 == null)
                                        <span class="ml-2 material-icons text-danger">
                                            highlight_off
                                        </span>
                                        @else
                                        <span class="ml-2 material-icons text-success">
                                            check_circle_outline
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="row">
                            <div class="col-12">
                                <h6 class="text-center">Penyanggah</h6>
                            </div>
                            <div class="col-12">
                                <div class="d-flex justify-content-between align-items-center" style="width: 100%">
                                    <div>
                                        @if ($sup->penyanggah1 == null)
                                        <small class="text-muted text-sm text-danger">
                                            Belum Ditentukan
                                        </small>
                                        @else
                                        <small class="text-muted text-sm">
                                            {{ App\Models\Mahasiswa::where('nim','=' ,$sup->penyanggah1)->first()->nama_mahasiswa }}
                                        </small>
                                        @endif
                                    </div>
                                    <div class="d-flex align-items-center">
                                        Nilai :
                                        @if ($sup->nilai_penyanggah1 == null)
                                        <span class="ml-2 material-icons text-danger">
                                            highlight_off
                                        </span>
                                        @else
                                        <span class="ml-2 material-icons text-success">
                                            check_circle_outline
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="d-flex justify-content-between align-items-center">
                                    <div>
                                        @if ($sup->penyanggah2 == null)
                                        <small class="text-muted text-sm text-danger">
                                            Belum Ditentukan
                                        </small>
                                        @else
                                        <small class="text-muted text-sm">
                                            {{ App\Models\Mahasiswa::where('nim','=' ,$sup->penyanggah2)->first()->nama_mahasiswa }}
                                        </small>
                                        @endif

                                    </div>
                                    <div class="d-flex align-items-center">
                                        Nilai :
                                        @if ($sup->nilai_penyanggah2 == null)
                                        <span class="ml-2 material-icons text-danger">
                                            highlight_off
                                        </span>
                                        @else
                                        <span class="ml-2 material-icons text-success">
                                            check_circle_outline
                                        </span>
                                        @endif
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    @endforeach

</div>

<div class="row">
    <div class="col-12">
        {{ $dataSUP->links('modules.backend.pagination') }}
    </div>
</div>
@endsection

@push('tambahStyle')
{{-- @livewireStyles --}}
@endpush
@push('tambahScript')
{{-- @livewireScripts --}}
@endpush
