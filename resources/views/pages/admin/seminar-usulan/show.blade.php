@extends('layouts.backend')

@section('title')
SIMPUR | Seminar Usulan Skripsi
@endsection

@section('header')
{{--  --}}
@endsection

@section('content')
<div class="row" style="margin-top: -10rem">
    <div class="col-12 col-lg-12">

        @foreach ($psup as $p)
        <div class="card">
            <div class="card-body">
                <table style="width: 100%;">
                    <tr>
                        <td class="font-weight-bold text-center text-uppercase h4 underline pb-5" colspan="3">
                            <u>Nilai Seminar Usulan Penelitian</u></td>
                    </tr>
                </table>

                <dl class="row">
                    <dt class="mt-3 mt-md-2 mt-lg-0 col-4 col-md-2 col-lg-3 d-flex justify-content-between">
                        Nama / NIM Seminaris<span class="ml-1">:</span>
                    </dt>
                    <dd class="mt-3 mt-md-2 mt-lg-0 col-8 col-md-10 col-lg-9">
                        {{ $p->mahasiswa->nama_mahasiswa }} /
                        {{ $p->nim_seminaris }}
                    </dd>

                    <dt class="mt-3 mt-md-2 mt-lg-0 col-4 col-md-2 col-lg-3 d-flex justify-content-between">
                        Program Studi / Minat<span class="ml-1">:</span>
                    </dt>
                    <dd class="mt-3 mt-md-2 mt-lg-0 col-8 col-md-10 col-lg-9">
                        Kehutanan / {{ $p->mahasiswa->minat }}
                    </dd>

                    <dt class="mt-3 mt-md-2 mt-lg-0 col-4 col-md-2 col-lg-3 d-flex justify-content-between">
                        Hari / Tgl Seminar<span class="ml-1">:</span>
                    </dt>
                    <dd class="mt-3 mt-md-2 mt-lg-0 col-8 col-md-10 col-lg-9">
                        {{ \Carbon\Carbon::parse($p->sup->tgl_seminar)->isoFormat('dddd / D MMMM Y') }}
                    </dd>

                    <dt class="mt-3 mt-md-2 mt-lg-0 col-4 col-md-2 col-lg-3 d-flex justify-content-between">
                        Judul Seminar<span class="ml-1">:</span>
                    </dt>
                    <dd class="mt-3 mt-md-2 mt-lg-0 col-8 col-md-10 col-lg-9">
                        {{ $p->sup->judul }}
                    </dd>
                </dl>

                <table class="mt-4" style="width: 100%;" id="tabelBeborder">
                    <thead style="text-align: center;">
                        <tr style="width: 100%;">
                            <td style="width: auto;" class="font-weight-bold">NO</td>
                            <td style="width: auto;" class="font-weight-bold">ASPEK YANG DINILAI</td>
                            <td style="width: auto;" class="font-weight-bold">NILAI <br> (>70-100)</td>
                            <td style="width: auto;" class="font-weight-bold">BOBOT</td>
                            <td style="width: auto;" class="font-weight-bold">BOBOT X <br> NILAI</td>
                        </tr>
                    </thead>

                    <tbody>
                        <tr>
                            <td rowspan="6" class="text-center">1</td>
                            <td colspan="4">a. Penulisan</td>
                        <tr>
                            <td class="pl-3">1) Kesesuaian nashkah usulan penelitian dengan pedoman penulisan
                                ilmian
                                yang berlaku</td>
                            <td>
                                <input type="text" placeholder="..." id="nilai1a1" name="nilai1a1"
                                    onkeyup="hitung(this.value.replace(',', '.'), 0.10, 'hasil1a1')"
                                    value="{{ $p->nilai1a1 }}" disabled>
                            </td>
                            <td class="text-center">0,10</td>
                            <td>
                                <input type="text" placeholder="..." id="hasil1a1" name="hasil1a1"
                                    value="{{ $p->hasil1a1 }}" disabled>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">b. Substansi isi naskah usulan penelitian</td>
                        </tr>
                        <tr>
                            <td class="pl-3">2) Judul ringkas, selaras dengan batang tubuh naskah usulan
                                penelitian
                            </td>
                            <td>
                                <input type="text" placeholder="..." id="nilai1b2" name="nilai1b2"
                                    onkeyup="hitung(this.value.replace(',', '.'), 0.10, 'hasil1b2')"
                                    value="{{ $p->nilai1b2 }}" disabled>
                            </td>
                            <td class="text-center">0,10</td>
                            <td>
                                <input type="text" placeholder="..." id="hasil1b2" name="hasil1b2"
                                    value="{{ $p->hasil1b2 }}" disabled>
                            </td>
                        </tr>
                        <tr>
                            <td class="pl-3">3) Metode penelitian sesuai dengan tujuan penelitian</td>
                            <td>
                                <input type="text" placeholder="..." id="nilai1b3" name="nilai1b3"
                                    onkeyup="hitung(this.value.replace(',', '.'), 0.10, 'hasil1b3')"
                                    value="{{ $p->nilai1b3 }}" disabled>
                            </td>
                            <td class="text-center">0,10</td>
                            <td>
                                <input type="text" placeholder="..." id="hasil1b3" name="hasil1b3"
                                    value="{{ $p->hasil1b3 }}" disabled>
                            </td>
                        </tr>
                        <tr>
                            <td class="pl-3">4) Pustaka yang diacu minimal 15 sumber pustaka dan 80% berasal
                                dari
                                pustaka primer</td>
                            <td>
                                <input type="text" placeholder="..." id="nilai1b4" name="nilai1b4"
                                    onkeyup="hitung(this.value.replace(',', '.'), 0.10, 'hasil1b4')"
                                    value="{{ $p->nilai1b4 }}" disabled>
                            </td>
                            <td class="text-center">0,10</td>
                            <td>
                                <input type="text" placeholder="..." id="hasil1b4" name="hasil1b4"
                                    value="{{ $p->hasil1b4 }}" disabled>
                            </td>
                        </tr>
                        </tr>
                        <!-- -- -->
                        <tr>
                            <td rowspan="4" class="text-center">2</td>
                            <td colspan="4">Penugasan materi presentasi</td>
                        <tr>
                            <td class="pl-3">c. Kemampuan pemaparan</td>
                            <td>
                                <input type="text" placeholder="..." id="nilai2c" name="nilai2c"
                                    onkeyup="hitung(this.value.replace(',', '.'), 0.10, 'hasil2c')"
                                    value="{{ $p->nilai2c }}" disabled>
                            </td>
                            <td class="text-center">0,15</td>
                            <td>
                                <input type="text" placeholder="..." id="hasil2c" name="hasil2c"
                                    value="{{ $p->hasil2c }}" disabled>
                            </td>
                        </tr>
                        <tr>
                            <td class="pl-3">d. Kualitas penyajian</td>
                            <td>
                                <input type="text" placeholder="..." id="nilai2d" name="nilai2d"
                                    onkeyup="hitung(this.value.replace(',', '.'), 0.10, 'hasil2d')"
                                    value="{{ $p->nilai2d }}" disabled>
                            </td>
                            <td class="text-center">0,10</td>
                            <td>
                                <input type="text" placeholder="..." id="hasil2d" name="hasil2d"
                                    value="{{ $p->hasil2d }}" disabled>
                            </td>
                        </tr>
                        <tr>
                            <td class="pl-3">e. Ketepatan waktu</td>
                            <td>
                                <input type="text" placeholder="..." id="nilai2e" name="nilai2e"
                                    onkeyup="hitung(this.value.replace(',', '.'), 0.10, 'hasil2e')"
                                    value="{{ $p->nilai2e }}" disabled>
                            </td>
                            <td class="text-center">0,10</td>
                            <td>
                                <input type="text" placeholder="..." id="hasil2e" name="hasil2e"
                                    value="{{ $p->hasil2e }}" disabled>
                            </td>
                        </tr>
                        </tr>

                        <!-- -- -->
                        <tr>
                            <td class="text-center">3</td>
                            <td>Kemampuan penalaran dan pemikiran ilmiah (logis, kritis, sistematis dan kreatif)
                            </td>
                            <td>
                                <input type="text" placeholder="..." id="nilai3" name="nilai3"
                                    onkeyup="hitung(this.value.replace(',', '.'), 0.10, 'hasil3')"
                                    value="{{ $p->nilai3 }}" disabled>
                            </td>
                            <td class="text-center">0,15</td>
                            <td>
                                <input type="text" placeholder="..." id="hasil3" name="hasil3" value="{{ $p->hasil3 }}"
                                    disabled>
                            </td>
                        </tr>
                        <!-- -- -->
                        <tr>
                            <td class="text-center">4</td>
                            <td>Sikap dan etika seminaris</td>
                            <td>
                                <input type="text" placeholder="..." id="nilai4" name="nilai4"
                                    onkeyup="hitung(this.value.replace(',', '.'), 0.10, 'hasil4')"
                                    value="{{ $p->nilai4 }}" disabled>
                            </td>
                            <td class="text-center">0,10</td>
                            <td>
                                <input type="text" placeholder="..." id="hasil4" name="hasil4" value="{{ $p->hasil4 }}"
                                    disabled>
                            </td>
                        </tr>
                        <!-- -- -->
                        <tr class="">
                            <td colspan="4" class="text-center font-weight-bold py-4">Total Nilai</td>
                            <td class="text-center">
                                <input type="text" placeholder="..." id="total_nilai" name="total_nilai"
                                    value="{{ $p->total_nilai }}" disabled>
                            </td>
                        </tr>
                    </tbody>
                </table>

                <table style="width: 100%;">
                    <tr>
                        <td style="width: 65%;"></td>
                        <td>Banjarbaru,
                            {{ \Carbon\Carbon::parse($p->sup->tgl_seminar)->isoFormat('D MMMM Y') }}
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 65%;"></td>
                        <td>
                            @if ($p->sup->pembimbing1 == $p->dosen->nip)
                            Dosen Pembimbing I,
                            @elseif($p->sup->pembimbing2 == $p->dosen->nip)
                            Dosen Pembimbing II,
                            @endif

                        </td>
                    </tr>
                    <tr height="5rem">
                        <td style="width: 65%;"></td>
                        <td> <br> <br> <br> <br> </td>
                    </tr>
                    <tr>
                        <td style="width: 65%;"></td>
                        <td>{{ $p->dosen->nama_dosen }}</td>
                    </tr>
                    <tr>
                        <td style="width: 60%;"></td>
                        <td>NIP. {{ $p->dosen->nip }}</td>
                    </tr>
                </table>
            </div>
        </div>
        @endforeach


        @foreach ($ppsup as $pp)
        <div class="card">
            <div class="card-body">
                <table style="width: 100%;">
                    <tr>
                        <td class="font-weight-bold text-center text-uppercase h4 underline pb-5" colspan="3">
                            <u>Lembar Penilaian Penyanggah Seminar Usulan Penelitian</u></td>
                    </tr>
                </table>

                <dl class="row">
                    <dt class="mt-3 mt-md-2 mt-lg-0 col-4 d-flex justify-content-between">
                        Nama / NIM Seminaris / Tanggal<span class="ml-1">:</span>
                    </dt>
                    <dd class="mt-3 mt-md-2 mt-lg-0 col-8 ">
                        {{ $pp->mahasiswa->nama_mahasiswa }} /
                        {{ $pp->nim_seminaris }} /
                        {{ \Carbon\Carbon::parse($pp->sup->tgl_seminar)->isoFormat('D MMMM Y') }}
                    </dd>

                    <dt class="mt-3 mt-md-2 mt-lg-0 col-4 d-flex justify-content-between">
                        Program Studi / Minat<span class="ml-1">:</span>
                    </dt>
                    <dd class="mt-3 mt-md-2 mt-lg-0 col-8 ">
                        Kehutanan / {{ $pp->mahasiswa->minat }}
                    </dd>

                    <dt class="mt-3 mt-md-2 mt-lg-0 col-4 d-flex justify-content-between">
                        Nama Penyanggah / NIM<span class="ml-1">:</span>
                    </dt>
                    <dd class="mt-3 mt-md-2 mt-lg-0 col-8 ">
                        {{ App\Models\Mahasiswa::where('nim','=',$pp->nim_penyanggah)->first()->nama_mahasiswa }}
                        /
                        {{ $pp->nim_penyanggah }}
                    </dd>

                    <dt class="mt-3 mt-md-2 mt-lg-0 col-4 d-flex justify-content-between">
                        Judul Seminar<span class="ml-1">:</span>
                    </dt>
                    <dd class="mt-3 mt-md-2 mt-lg-0 col-8 ">
                        {{ $pp->sup->judul }}
                    </dd>
                </dl>

                <table class="mt-5" style="width: 100%;" id="tabelBeborder">
                    <thead style="text-align: center;">
                        <tr style="width: 100%;">
                            <td style="width: 5%;" class="font-weight-bold">NO</td>
                            <td style="width: auto;" class="font-weight-bold">ASPEK YANG DINILAI</td>
                            <td style="width: 8%;" class="font-weight-bold">NILAI <br> (>70-100)</td>
                            <td style="width: 7%;" class="font-weight-bold">BOBOT</td>
                            <td style="width: 8%;" class="font-weight-bold">BOBOT X <br> NILAI</td>
                        </tr>
                    </thead>
                    <tbody>

                        <input type="hidden" name="nim_penyanggah" value="{{ $pp->nim_penyanggah }}">
                        <tr>
                            <td class="text-center">1.</td>
                            <td>Kemampuan melakukan kritikan/saran/pendapat dalam bentuk komunikasi secara lisan
                            </td>
                            <td>
                                <input disabled type="text" autocomplete="off" placeholder="..." id="nilai1" autofocus
                                    name="nilai1"
                                    onkeyup="hitung(this.value.replace(',', '.').replace(',', '.'), 0.2, 'hasil1')"
                                    value="{{ $pp->nilai1 }}">

                            </td>
                            <td class="text-center">0,2</td>
                            <td>
                                <input disabled type="text" autocomplete="off" placeholder="..." id="hasil1"
                                    name="hasil1" value="{{ $pp->hasil1 }}" readonly>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center">2.</td>
                            <td>Saran, pendapat dan kritikan yang bersifat perbaikan terhadap naskah Seminar
                                Usulan
                            </td>
                            <td>
                                <input disabled type="text" autocomplete="off" placeholder="..." id="nilai2" autofocus
                                    name="nilai2" onkeyup="hitung(this.value.replace(',', '.'), 0.2, 'hasil2')"
                                    value="{{ $pp->nilai2 }}">
                            </td>
                            <td class="text-center">0,2</td>
                            <td>
                                <input disabled type="text" autocomplete="off" placeholder="..." id="hasil2"
                                    name="hasil2" value="{{ $pp->hasil2 }}" readonly>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center">3.</td>
                            <td>Kedalaman saran, pendapat dan kritikan yang disampaikan kepada seminaris</td>
                            <td>
                                <input disabled type="text" autocomplete="off" placeholder="..." id="nilai3" autofocus
                                    name="nilai3" onkeyup="hitung(this.value.replace(',', '.'), 0.2, 'hasil3')"
                                    value="{{ $pp->nilai3 }}">
                            </td>
                            <td class="text-center">0,2</td>
                            <td>
                                <input disabled type="text" autocomplete="off" placeholder="..." id="hasil3"
                                    name="hasil3" value="{{ $pp->hasil3 }}" readonly>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center">4.</td>
                            <td>Saran, perbaikan dan pendapat tidak keluar dari tema usulan penelitian</td>
                            <td>
                                <input disabled type="text" autocomplete="off" placeholder="..." id="nilai4" autofocus
                                    name="nilai4" onkeyup="hitung(this.value.replace(',', '.'), 0.2, 'hasil4')"
                                    value="{{ $pp->nilai4 }}">
                            </td>
                            <td class="text-center">0,2</td>
                            <td>
                                <input disabled type="text" autocomplete="off" placeholder="..." id="hasil4"
                                    name="hasil4" value="{{ $pp->hasil4 }}" readonly>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center">5.</td>
                            <td>Kemampuan menyampaikan gagasan, ide yang bersifat kreatif dan inovatif terhadap
                                naskah seminar</td>
                            <td>
                                <input disabled type="text" autocomplete="off" placeholder="..." id="nilai5" autofocus
                                    name="nilai5" onkeyup="hitung(this.value.replace(',', '.'), 0.2, 'hasil5')"
                                    value="{{ $pp->nilai5 }}">
                            </td>
                            <td class="text-center">0,2</td>
                            <td>
                                <input disabled type="text" autocomplete="off" placeholder="..." id="hasil5"
                                    name="hasil5" value="{{ $pp->hasil5 }}" readonly>
                            </td>
                        </tr>
                        <!-- -- -->
                        <tr class="">
                            <td colspan="4" class="text-center font-weight-bold py-4">Total Nilai</td>
                            <td class="text-center">
                                <input disabled type="text" autocomplete="off" placeholder="..." id="total_nilai"
                                    name="total_nilai" value="{{ $pp->total_nilai }}" readonly>
                            </td>
                        </tr>
                    </tbody>
                </table>

                <table style="width: 100%;">
                    <td>
                        <tr>
                            <td style="width: 60%;"></td>
                            <td>Banjarbaru,
                                @if ($pp->sup->tgl_seminar == null)
                                <span class="text-danger">-</span>
                                @else
                                {{ \Carbon\Carbon::parse($pp->sup->tgl_seminar)->isoFormat(' D MMMM Y') }}
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 60%;"></td>
                            <td>
                                @if ($pp->sup->pembimbing1 == $pp->dosen->nip)
                                Dosen Pembimbing I,
                                @elseif($pp->sup->pembimbing2 == $pp->dosen->nip)
                                Dosen Pembimbing II,
                                @endif
                            </td>
                        </tr>
                        <tr height="5rem">
                            <td style="width: 60%;"></td>
                            <td> <br> <br> <br> <br> </td>
                        </tr>
                        <tr>
                            <td style="width: 60%;"></td>
                            <td>{{ $pp->dosen->nama_dosen }}</td>
                        </tr>
                        <tr>
                            <td style="width: 60%;"></td>
                            <td>NIP. {{ $pp->dosen->nip }}</td>
                        </tr>
                    </td>
                </table>

            </div>
        </div>
        @endforeach

    </div>
</div>
@endsection

@push('tambahStyle')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css"
    integrity="sha512-+4zCK9k+qNFUR5X+cKL9EIR+ZOhtIloNl9GIKS57V1MyNsYpYcUrUeQc9vNfzsWfV28IaLL3i96P9sdNyeRssA=="
    crossorigin="anonymous" />
<link rel="stylesheet" href="{{ url('adminarea/plugins/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ url('adminarea/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
<style>
    table#tabelBeborder td {
        border: 1px solid black;
    }

    table#tabelBeborder input {
        border-width: 0px;
        text-align: center;
        width: 100%;
    }

    td {
        padding-left: .3rem;
    }
</style>
@endpush

@push('tambahScript')
<script src="{{ url('adminarea/plugins/select2/js/select2.full.min.js') }}"></script>
<script>
    $('#selectNIM, #selectPembimbing1, #selectPembimbing2').select2({
        placeholder: "Select a state",
        theme: 'bootstrap4',
  });

    function hitung(a, b, c){
      a = parseFloat(a);
      var inputHasil = document.getElementById(c);
      var bobotxnilai = a * b;
      inputHasil.value = bobotxnilai.toFixed(2).replace('.', ',').replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
      // --
      var totalNilaiInput = document.getElementById('total_nilai');
      var hasil1a1 = parseFloat( document.getElementById('hasil1a1').value.replace(',', '.'));
      var hasil1b2 = parseFloat( document.getElementById('hasil1b2').value.replace(',', '.'));
      var hasil1b3 = parseFloat( document.getElementById('hasil1b3').value.replace(',', '.'));
      var hasil1b4 = parseFloat( document.getElementById('hasil1b4').value.replace(',', '.'));
      var hasil2c = parseFloat( document.getElementById('hasil2c').value.replace(',', '.'));
      var hasil2d = parseFloat( document.getElementById('hasil2d').value.replace(',', '.'));
      var hasil2e = parseFloat( document.getElementById('hasil2e').value.replace(',', '.'));
      var hasil3 = parseFloat( document.getElementById('hasil3').value.replace(',', '.'));
      var hasil4 = parseFloat( document.getElementById('hasil4').value.replace(',', '.'));

      totalNilaix = hasil1a1 + hasil1b2 + hasil1b3 + hasil1b4 + hasil2c + hasil2d + hasil2e + hasil3 + hasil4;
      if (!isNaN(totalNilaix)) {
        totalNilaiInput.value = totalNilaix.toFixed(2).replace('.', ',').replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
      }
    }
</script>
@endpush
