@extends('layouts.backend')

@section('title')
SIMPUR | Ujian Komprehensif
@endsection

@section('header')
<div class="header text-center">
    <h2 class=" title">Tambah Penguji Ujian Komprehensif</h2>
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12 col-md-6">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb bg-white rounded-pill shadow">
                        <li class="breadcrumb-item"><a href="{{ route('admin.index') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.ujian-kompre.index') }}">Ujian
                                Komprehensif</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Tambah Penguji</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row mt-5 mt-md-0">
    <div class="col-12">
        <div class="card card-body">
            <label class="">Mahasiswa</label>

            <div class="">
                <div class="form-group">
                    <select id="selectNIM" name="nim_seminaris" class="@error('nim_seminaris') is-invalid @enderror"
                        style="width: 100%;" disabled>
                        <option></option>
                        @foreach ($mhs as $m)
                        <option value="{{ $m->nim }}" {{ $m->nim == $uk->nim_skripsi ? 'selected' : '' }}>
                            {{ $m->nim }} --
                            {{ $m->nama_mahasiswa }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="row">
                <div class="col-12 col-md-6">
                    <label class="">Penguji 1</label>

                    <div class="form-group">
                        <select id="selectPenguji1" name="penguji1" class="@error('penguji1') is-invalid @enderror"
                            style="width: 100%;" disabled>
                            <option></option>
                            @foreach ($dosen as $d)
                            <option value="{{ $d->nip }}" {{ $d->nip == $uk->penguji1 ? 'selected' : '' }}>
                                {{ $d->nip }} -- {{ $d->nama_dosen }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <label class="">Penguji 2</label>

                    <div class="form-group">
                        <select id="selectPenguji2" name="penguji2" class="@error('penguji2') is-invalid @enderror"
                            style="width: 100%;" disabled>
                            <option></option>
                            @foreach ($dosen as $d)
                            <option value="{{ $d->nip }}" {{ $d->nip == $uk->penguji2 ? 'selected' : '' }}>
                                {{ $d->nip }} -- {{ $d->nama_dosen }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>


            <form id="addPenguji" action="{{ route('admin.ujian-kompre.storePenguji', $uk->id_ujian_komprehensif) }}"
                method="POST">
                @method('PUT')
                @csrf
                <div class="row">
                    <div class="col-12 col-md-6">
                        <label class="">Penguji 3</label>

                        <div class="form-group">
                            <select id="selectPenguji3" name="penguji3" class="@error('penguji3') is-invalid @enderror"
                                style="width: 100%;" autofocus>
                                <option></option>
                                @foreach ($dosen as $d)
                                <option value="{{ $d->nip }}" {{ $d->nip == $uk->penguji3 ? 'selected' : '' }}>
                                    {{ $d->nip }} -- {{ $d->nama_dosen }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <label class="">Penguji 4</label>

                        <div class="form-group">
                            <select id="selectPenguji4" name="penguji4" class="@error('penguji4') is-invalid @enderror"
                                style="width: 100%;" autofocus>
                                <option></option>
                                @foreach ($dosen as $d)
                                <option value="{{ $d->nip }}" {{ $d->nip == $uk->penguji4 ? 'selected' : '' }}>
                                    {{ $d->nip }} -- {{ $d->nama_dosen }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>


                <label class="">Tanggal Skripsi</label>

                <div class="row">
                    <div class="form-group col-12 col-md-4">
                        <input type="text" name="tgl_skripsi" class="@error('tgl_skripsi') is-invalid @enderror"
                            id="tgl_skripsi" autocomplete="off" value="{{ $uk->tgl_skripsi }}">
                        @error('tgl_skripsi')
                        <span class="invalid-feedback ml-1" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
            </form>

            <div class="row">
                <div class="col-12 text-right">
                    <button type="submit" form="addPenguji" class="btn btn-success">Tambah Penguji</button>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection

@push('tambahStyle')
<link rel="stylesheet" href="{{ url('adminarea/plugins/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ url('adminarea/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">

@endpush

@push('tambahScript')
<script src="{{ url('adminarea/plugins/select2/js/select2.full.min.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script src="https://npmcdn.com/flatpickr/dist/l10n/id.js"></script>
<script>
    $('#selectNIM, #selectPenguji1, #selectPenguji2, #selectPenguji3, #selectPenguji4').select2({
        placeholder: "Pilih ...",
        theme: 'bootstrap4',
    });

    flatpickr.localize(flatpickr.l10ns.id);
    $('#tgl_skripsi').attr('readonly', false);
    $('#tgl_skripsi').flatpickr({
        allowInput:true,
        altInput: true,
        enableTime: true,
        time_24hr: true,
        defaultHour: 8,
        altFormat: "j F Y H:i",
        dateFormat: "Y-m-d H:i",
    });

</script>
@endpush
