@extends('layouts.backend')

@section('title')
SIMPUR | Ujian Skripsi
@endsection

@section('header')
{{--  --}}
@endsection

@section('content')
<div class="row justify-content-center" style="margin-top: -10rem">
    <div class="col-12 col-md-8">
        <div class="card">
            <div class="card-body">
                <table style="width: 100%;">
                    <tr>
                        <td class="font-weight-bold text-center text-uppercase h4 underline pb-5" colspan="3">
                            Edit Tanggal Ujian Skripsi
                        </td>
                    </tr>
                </table>
                <div class="row">
                    <div class="col-sm-4 d-flex justify-content-start justify-content-md-between">
                        Nama / NIM Seminaris <span class="ml-2">:</span>
                    </div>
                    <div class="col">
                        {{ $us->mahasiswa->nama_mahasiswa }} /
                        {{ $us->mahasiswa->nim }}

                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4 d-flex justify-content-start justify-content-md-between">
                        Program Studi / Minat<span class="ml-2">:</span>
                    </div>
                    <div class="col">Kehutanan / {{ $us->mahasiswa->minat }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-4 d-flex justify-content-start justify-content-md-between">
                        Tgl Skripsi<span class="ml-2">:</span>
                    </div>
                    <div class="col">
                        <form id="editTglSkripsi"
                            action="{{ route('admin.ujian-skripsi.updateTanggal', $us->id_ujian_skripsi) }}"
                            method="POST">
                            @csrf
                            @method('PUT')
                            <input type="text" name="tgl_skripsi"
                                class="col-12 col-md-4 @error('tgl_skripsi') is-invalid @enderror" id="tgl_skripsi"
                                autocomplete="off" value="{{ $us->tgl_skripsi }}">
                        </form>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4 d-flex justify-content-start justify-content-md-between">
                        Judul<span class="ml-2">:</span>
                    </div>
                    <div class="col">
                        {{ $us->judul }}
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 text-right">
                        <button type="submit" form="editTglSkripsi" class="btn btn-primary">Perbarui</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('tambahStyle')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
<style>
</style>
@endpush

@push('tambahScript')
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script src="https://npmcdn.com/flatpickr/dist/l10n/id.js"></script>
<script>
    flatpickr.localize(flatpickr.l10ns.id);
    $('#tgl_skripsi').attr('readonly', false);
    $('#tgl_skripsi').flatpickr({
        allowInput:true,
        altInput: true,
        altFormat: "j F Y",
        dateFormat: "Y-m-d H:i",
    });

</script>
@endpush
