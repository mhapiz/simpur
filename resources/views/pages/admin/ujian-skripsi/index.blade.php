@extends('layouts.backend')

@section('title')
SIMPUR | Ujian Skripsi
@endsection

@section('header')
<div class="header text-center">
    <h2 class="title">Ujian Skripsi</h2>
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12 col-md-6">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb bg-white rounded-pill shadow">
                        <li class="breadcrumb-item"><a href="{{ route('admin.index') }}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Ujian Skripsi</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card rounded-pill">
            <div
                class="card-body px-0 pl-md-4 pr-md-4 pb-3 d-flex flex-column flex-lg-row align-items-center justify-content-between">

                <div class="d-flex">
                    <a href="{{ route('admin.ujian-skripsi.index') }}"
                        class="btn {{ (request()->is('admin/ujian-skripsi')) ? 'btn-success' : 'btn-outline-success' }} btn-sm rounded-pill d-flex align-items-center">
                        Semua
                    </a>

                    <a href="{{ route('admin.ujian-skripsi.indexSudahDinilai') }}"
                        class="btn {{ (request()->is('admin/ujian-skripsi/sudah-dinilai')) ? 'btn-success' : 'btn-outline-success' }} btn-sm rounded-pill d-flex align-items-center">
                        Sudah Dinilai
                    </a>
                </div>

                <div class="d-flex">
                    <form class="d-flex align-items-center" action="" method="GET">
                        <input class="form-control" type="search" placeholder="Cari Nama " aria-label="Search" name="q"
                            style="height: 2rem">
                        <button
                            class="btn btn-primary rounded-pill my-2 my-sm-0 d-flex justify-content-between align-items-center"
                            type="submit" style="height: 2rem">
                            <i class="fas fa-search"></i>
                            <span class="ml-2 d-none d-md-block">Cari</span>
                        </button>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
<div class="row">

    @foreach ($dataUjianSkripsi as $us)

    <div class="col-12 col-lg-6">
        <div class="card">
            <div class="card-header d-flex justify-content-between align-items-center">
                <h5 class="card-category">Ujian Skripsi</h5>

                <div class="d-flex">
                    <a href="{{ route('admin.ujian-skripsi.show', $us->id_ujian_skripsi) }}"
                        class="btn btn-round btn-outline-default btn-simple btn-icon no-caret d-flex justify-content-center align-items-center mx-2"
                        data-toggle="tooltip" data-placement="right" title="Lihat Nilai">
                        <span class="material-icons">
                            visibility
                        </span>
                    </a>

                    <a href="{{ route('admin.ujian-skripsi.editTanggal', $us->id_ujian_skripsi) }}"
                        class="btn btn-round btn-outline-default btn-simple btn-icon no-caret d-flex justify-content-center align-items-center mx-2"
                        data-toggle="tooltip" data-placement="right" title="Lihat Nilai">
                        <span class="material-icons">
                            today
                        </span>
                    </a>

                    <a class="btn btn-round btn-outline-default btn-simple btn-icon no-caret d-flex justify-content-center align-items-center mx-2"
                        href="{{ route('admin.ujian-skripsi.cetak', $us->id_ujian_skripsi) }}" target="_blank"
                        data-toggle=" tooltip" data-placement="right" title="Print Nilai Ujian Skripsi">
                        <span class="material-icons">
                            print
                        </span>
                    </a>

                </div>

            </div>
            <div class="card-body">

                <dl class="row">
                    <dt class="mt-3 mt-md-2 mt-lg-0 col-4 col-md-5 d-flex justify-content-between">
                        Nama / NIM <span class="ml-1">:</span>
                    </dt>
                    <dd class="mt-3 mt-md-2 mt-lg-0 col-8 col-md-7">
                        {{ $us->mahasiswa->nama_mahasiswa }} / {{ $us->nim_skripsi }}
                    </dd>

                    <dt class="mt-3 mt-md-2 mt-lg-0 col-4 col-md-5 d-flex justify-content-between">
                        Program Studi / Minat<span class="ml-1">:</span>
                    </dt>
                    <dd class="mt-3 mt-md-2 mt-lg-0 col-8 col-md-7">
                        Kehutanan / {{ $us->mahasiswa->minat }}
                    </dd>

                    <dt
                        class="mt-3 mt-md-2 mt-lg-0 col-4 col-md-5 d-flex justify-content-between {{ $us->tgl_skripsi == null ? 'pt-1 pt-md-3' :'' }}">
                        Hari / Tgl Skripsi<span class="ml-1">:</span>
                    </dt>
                    <dd class="mt-3 mt-md-2 mt-lg-0 col-8 col-md-7 d-flex">
                        @if ($us->tgl_skripsi == null)
                        <form class="row d-flex align-items-center"
                            action="{{ route('admin.ujian-skripsi.addTanggal', $us->id_ujian_skripsi) }}" method="POST">
                            @method('PUT')
                            @csrf
                            <div class="col-12 col-md-6">
                                <input type="text" name="tanggal" class="tanggal @error('tanggal') is-invalid @enderror"
                                    id="tanggal" autocomplete="off">
                                @error('tanggal')
                                <span class="invalid-feedback ml-1" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="col-12 col-md-6">
                                <button type="submit" class="btn btn-primary btn-sm">Tentukan</button>
                            </div>
                        </form>
                        @else
                        {{ \Carbon\Carbon::parse($us->tgl_skripsi)->isoFormat('dddd / D MMMM Y') }}
                        @endif
                    </dd>

                    <dt class="mt-3 mt-md-2 mt-lg-0 col-4 col-md-5 d-flex justify-content-between">
                        Judul Seminar<span class="ml-1">:</span>
                    </dt>
                    <dd class="mt-3 mt-md-2 mt-lg-0 col-8 col-md-7">
                        {{ \Illuminate\Support\Str::limit($us->judul, 300, $end='...') }}
                    </dd>
                </dl>

            </div>
            <hr>
            <div class="card-footer">
                <div class="row justify-content-center">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-12 text-center">
                                <h6>Pembimbing</h6>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="d-flex justify-content-between align-items-center" style="">
                                    <div class="text-truncate" style="">
                                        <small
                                            class="text-muted text-sm">{{ App\Models\Dosen::where('nip','=' ,$us->pembimbing1)->first()->nama_dosen }}</small>
                                    </div>

                                    <div class="d-flex align-items-center">
                                        Nilai :
                                        @if ($us->nilai_pembimbing1 == null)
                                        <span class="ml-2 material-icons text-danger">
                                            highlight_off
                                        </span>
                                        @else
                                        <span class="ml-2 material-icons text-success">
                                            check_circle_outline
                                        </span>
                                        @endif
                                    </div>

                                </div>

                                <div class="d-flex justify-content-between align-items-center">
                                    <div class="text-truncate" style="">
                                        <small
                                            class="text-muted text-sm">{{ App\Models\Dosen::where('nip','=' ,$us->pembimbing2)->first()->nama_dosen }}</small>
                                    </div>

                                    <div class="d-flex align-items-center">
                                        Nilai :
                                        @if ($us->nilai_pembimbing2 == null)
                                        <span class="ml-2 material-icons text-danger">
                                            highlight_off
                                        </span>
                                        @else
                                        <span class="ml-2 material-icons text-success">
                                            check_circle_outline
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endforeach

</div>

<div class="row">
    <div class="col-12">
        {{ $dataUjianSkripsi->links('modules.backend.pagination') }}
    </div>
</div>
@endsection

@push('tambahStyle')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
@endpush
@push('tambahScript')
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script src="https://npmcdn.com/flatpickr/dist/l10n/id.js"></script>

<script>
    flatpickr.localize(flatpickr.l10ns.id);
    $('.tanggal').each(function() {
        $(this).attr('readonly', false);
        $(this).flatpickr({
            allowInput:true,
            altInput: true,
            altFormat: "j F Y",
            dateFormat: "Y-m-d H:i",
        });
    });
</script>
@endpush
