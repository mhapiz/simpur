@extends('layouts.backend')

@section('title')
SIMPUR | Tambah Data Dosen
@endsection

@section('header')
<div class="header text-center">
    <h2 class=" title">Tambah Data Dosen </h2>
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12 col-md-6">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb bg-white rounded-pill shadow">
                        <li class="breadcrumb-item"><a href="{{ route('admin.index') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.user-manage.dosen.index') }}">Data
                                Dosen</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Tambah Data Dosen</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row mt-5 mt-md-0 justify-content-center">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header d-flex justify-content-between align-items-start">
                <h5 class="title">Tambah Data Dosen</h5>

                <!-- Button trigger modal -->
                <a href="#" class="" data-toggle="modal" data-target="#exampleModal">
                    Cara Upload
                </a>


            </div>
            <div class="card-body">
                <form action="{{ route('admin.user-manage.dosen.storeMany') }}" method="POST"
                    enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Pilih File Excel</label>
                                <div class="custom-file">
                                    <input type="file"
                                        class="custom-file-input @error('file_input') is-invalid @enderror"
                                        id="file-input" name="file_input">
                                    <label class="custom-file-label" for="file-input">Pilih File</label>
                                </div>
                                @error('file_input')
                                <div class="text-danger text-sm">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-end">
                        <div class="col-6 text-right">
                            <button type="submit" class="btn btn-success">Tambah</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Cara Upload Data Excel Dosen Ke Web</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>1. Silahkan download file <a href="{{ url('excel/excel-dosen-siap-upload.xlsx') }}">excel-dosen siap
                        upload</a>
                </p>
                <p>2. Salin data dosen ke file <a href="#">excel-dosen siap upload</a> dan sesuaikan dengan kolomnya
                </p>
                <p>3. Upload file <a href="#">excel-dosen siap upload</a> ke web dengan data yang diisikan dengan data
                    dosen
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('tambahScript')
<script>
    $('#file-input').on('change',function(){
        var fileName = $(this).val();
        $(this).next('.custom-file-label').html(fileName);
    })
</script>
@endpush
