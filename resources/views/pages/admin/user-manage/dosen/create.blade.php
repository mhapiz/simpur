@extends('layouts.backend')

@section('title')
SIMPUR | Tambah Data Dosen
@endsection

@section('header')
<div class="header text-center">
    <h2 class=" title">Tambah Data Dosen</h2>
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12 col-md-6">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb bg-white rounded-pill shadow">
                        <li class="breadcrumb-item"><a href="{{ route('admin.index') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.user-manage.dosen.index') }}">Data
                                Dosen</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Tambah Data Dosen</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row mt-5 mt-md-0 justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">
                <h5 class="title">Tambah Akun Dosen</h5>
            </div>
            <div class="card-body">
                <form action="{{ route('admin.user-manage.dosen.store') }}" method="POST">
                    @csrf
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>NIP</label>
                                <input type="text" name="nip" class="form-control @error('nip') is-invalid @enderror"
                                    placeholder="NIP" value="{{ old('nip') }}">
                                @error('nip')
                                <div class="invalid-feedback ml-3">
                                    *{{ $message }}
                                </div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Nama Dosen</label>
                                <input type="text" name="nama_dosen"
                                    class="form-control @error('nama_dosen') is-invalid @enderror"
                                    placeholder="Nama Dosen" value="{{ old('nama_dosen') }}">
                                @error('nama_dosen')
                                <div class="invalid-feedback ml-3">
                                    *{{ $message }}
                                </div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Jabatan</label>
                                <input type="text" name="jabatan"
                                    class="form-control @error('jabatan') is-invalid @enderror" placeholder="Jabatan"
                                    value="{{ old('jabatan') }}">
                                @error('jabatan')
                                <div class="invalid-feedback ml-3">
                                    *{{ $message }}
                                </div>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-end">
                        <div class="col-6 text-right">
                            <button type="submit" class="btn btn-success">Tambah</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
