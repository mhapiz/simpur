@extends('layouts.backend')

@section('title')
SIMPUR | Perbarui Data Dosen
@endsection

@section('header')
<div class="header text-center">
    <h2 class=" title">Perbarui Data Dosen</h2>
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12 col-md-6">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb bg-white rounded-pill shadow">
                        <li class="breadcrumb-item">
                            <a href="{{ route('admin.index') }}">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="{{ route('admin.user-manage.dosen.index') }}">Data
                                Dosen</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Perbarui Data Dosen</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row mt-5 mt-md-0 justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">
                <h5 class="title">Perbarui Akun Dosen</h5>
            </div>
            <div class="card-body">
                <form action="{{ route('admin.user-manage.dosen.update', $dosen->id_dosen) }}" method="POST">
                    @method('PUT')
                    @csrf
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>NIP</label>
                                <input type="text" name="nip" class="form-control @error('nip') is-invalid @enderror"
                                    placeholder="NIP" value="{{ $dosen->nip }}" disabled>
                                @error('nip')
                                <div class="invalid-feedback ml-3">
                                    *{{ $message }}
                                </div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Nama Dosen</label>
                                <input type="text" name="nama_dosen"
                                    class="form-control @error('nama_dosen') is-invalid @enderror"
                                    placeholder="Nama Dosen" value="{{ $dosen->nama_dosen }}">
                                @error('nama_dosen')
                                <div class="invalid-feedback ml-3">
                                    *{{ $message }}
                                </div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Jabatan</label>
                                <input type="text" name="jabatan"
                                    class="form-control @error('jabatan') is-invalid @enderror" placeholder="Jabatan"
                                    value="{{ $dosen->jabatan }}">
                                @error('jabatan')
                                <div class="invalid-feedback ml-3">
                                    *{{ $message }}
                                </div>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <p>
                        <a class="btn btn-sm btn-warning" data-toggle="collapse" href="#gantiPass" role="button"
                            aria-expanded="false" aria-controls="gantiPass">
                            Ganti Password
                        </a>
                    </p>
                    <div class="collapse" id="gantiPass">
                        <div class="form-group" id="newPass">
                            <label for="password">Password Baru</label>
                            <input type="password" name="password"
                                class="form-control @error('password') is-invalid @enderror" id="password"
                                placeholder="Masukkan Password">
                            {{--  --}}
                            <div style="margin-left: 1rem; margin-top: 10px;">
                                <input style="cursor: pointer" id="checkBox" type="checkbox" onclick="lihatPassword()">
                                <label style="cursor: pointer" for="defaultCheck1" onclick="lihatPassword()">
                                    Lihat Password
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="row justify-content-end">
                        <div class="col-6 text-right">
                            <button type="submit" class="btn btn-success">Perbarui</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection


@push('tambahScript')
<script>
    function lihatPassword() {
        var x = document.getElementById("password");
        var c = document.getElementById("checkBox");
        if (x.type === "password") {
            c.checked = true;
            x.type = "text";
        } else {
            x.type = "password";
            c.checked = false;
        }
    }
</script>
@endpush
