@extends('layouts.backend')

@section('title')
SIMPUR | Perbarui Data Mahasiswa
@endsection

@section('header')
<div class="header text-center">
    <h2 class=" title">Perbarui Data Mahasiswa </h2>
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12 col-md-6">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb bg-white rounded-pill shadow">
                        <li class="breadcrumb-item">
                            <a href="{{ route('admin.index') }}">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="{{ route('admin.user-manage.dosen.index') }}">Data
                                Dosen</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Perbarui Data Dosen</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row mt-5 mt-md-0 justify-content-center">
    <div class="col-12 col-md-8">
        <div class="card">
            <div class="card-header">
                <h5 class="title">Perbarui Data Mahasiswa</h5>
            </div>
            <div class="card-body">
                <form action="{{ route('admin.user-manage.mahasiswa.update', $mhs->id_mahasiswa ) }}" method="POST">
                    @method('PUT')
                    @csrf
                    <div class="row">
                        <div class="col-12 col-md-12">
                            <div class="form-group">
                                <label>NIM</label>
                                <input type="text" name="nim" class="form-control @error('nim') is-invalid @enderror"
                                    placeholder="NIM" value="{{ $mhs->nim }}" disabled>
                                @error('nim')
                                <div class="invalid-feedback ml-3">
                                    *{{ $message }}
                                </div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-12 col-md-12">
                            <div class="form-group">
                                <label>Nama Dosen</label>
                                <input type="text" name="nama_mahasiswa"
                                    class="form-control @error('nama_mahasiswa') is-invalid @enderror"
                                    placeholder="Nama Dosen" value="{{ $mhs->nama_mahasiswa }}">
                                @error('nama_mahasiswa')
                                <div class="invalid-feedback ml-3">
                                    *{{ $message }}
                                </div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-12 col-md-6">
                            <div class="form-group">
                                <label>Minat</label>
                                <select name="minat"
                                    class="custom-select form-control @error('minat') is-invalid @enderror">
                                    <option value="Manajemen Hutan"
                                        {{ $mhs->minat == 'Manajemen Hutan' ? 'selected' : '' }}>Manajemen
                                        Hutan</option>
                                    <option value="Silvikultur" {{ $mhs->minat == 'Silvikultur' ? 'selected' : '' }}>
                                        Silvikultur
                                    </option>
                                    <option value="Teknologi Hasil Hutan"
                                        {{ $mhs->minat == 'Teknologi Hasil Hutan' ? 'selected' : '' }}>
                                        Teknologi Hasil Hutan</option>
                                </select>
                                @error('minat')
                                <div class="invalid-feedback ml-3">
                                    *{{ $message }}
                                </div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-12 col-md-6">
                            <div class="form-group">
                                <label>Tahun Angkatan</label>
                                <input type="text" name="tahun_angkatan"
                                    class="form-control @error('tahun_angkatan') is-invalid @enderror"
                                    placeholder="tahun Angkatan" value="{{ $mhs->tahun_angkatan }}">
                                @error('tahun_angkatan')
                                <div class="invalid-feedback ml-3">
                                    *{{ $message }}
                                </div>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="row justify-content-end">
                        <div class="col-6 text-right">
                            <button type="submit" class="btn btn-success">Perbarui</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection


@push('tambahScript')
<script>
    function lihatPassword() {
        var x = document.getElementById("password");
        var c = document.getElementById("checkBox");
        if (x.type === "password") {
            c.checked = true;
            x.type = "text";
        } else {
            x.type = "password";
            c.checked = false;
        }
    }
</script>
@endpush
