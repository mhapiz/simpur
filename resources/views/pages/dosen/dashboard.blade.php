@extends('layouts.backend')

@section('title')
SIMPUR | Dashboard
@endsection

@section('header')
<div class="header text-center">
    <h2 class="title">Home</h2>
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12 col-md-6">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb bg-white rounded-pill shadow">
                        <li class="breadcrumb-item active" aria-current="page">Home</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row">

    {{-- SEMINAR Usulan PENELITIAN --}}
    <div class="col-12">
        <div class="card">
            <div class="card-header text-center">
                <h5 class="card-title">Seminar Usulan Penelitian (Belum Dinilai)</h5>
            </div>
            <div class="card-body row justify-content-center">
                @forelse ($dataSUP as $sup)
                <div class="col-12 col-lg-6">
                    <div class="card rounded-lg">
                        <div class="card-header d-flex justify-content-between align-items-center">
                            <h5 class="card-category">Seminar Usulan Penelitian</h5>

                            <div class="d-flex">
                                <a href=""
                                    class="btn btn-round btn-outline-default btn-simple btn-icon no-caret d-flex justify-content-center align-items-center mr-2">
                                    <span class="material-icons">
                                        visibility
                                    </span>
                                </a>

                                <div class="dropdown">
                                    <button type="button"
                                        class="btn btn-round btn-outline-default dropdown-toggle btn-simple btn-icon no-caret d-flex justify-content-center align-items-center"
                                        data-toggle="dropdown">
                                        <span class="material-icons">
                                            list
                                        </span>
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item"
                                            href="{{ route('dosen.seminar-usulan.penilaian.create', $sup->id_seminar_usulan_penelitian) }}">Tambah
                                            Nilai</a>
                                        <a class="dropdown-item"
                                            href="{{ route('dosen.penyanggah-1-seminar-usulan.penilaian.create', $sup->id_seminar_usulan_penelitian) }}">Tambah
                                            Nilai Penyanggah 1</a>
                                        <a class="dropdown-item"
                                            href="{{ route('dosen.penyanggah-2-seminar-usulan.penilaian.create', $sup->id_seminar_usulan_penelitian) }}">Tambah
                                            Nilai Penyanggah 2</a>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="card-body">

                            <dl class="row">
                                <dt class="mt-3 mt-md-2 mt-lg-0 col-4 col-md-5 d-flex justify-content-between">
                                    Nama / NIM Seminaris<span class="ml-1">:</span>
                                </dt>
                                <dd class="mt-3 mt-md-2 mt-lg-0 col-8 col-md-7">
                                    {{ $sup->mahasiswa->nama_mahasiswa }} / {{ $sup->nim_seminaris }}
                                </dd>

                                <dt class="mt-3 mt-md-2 mt-lg-0 col-4 col-md-5 d-flex justify-content-between">
                                    Program Studi / Minat<span class="ml-1">:</span>
                                </dt>
                                <dd class="mt-3 mt-md-2 mt-lg-0 col-8 col-md-7">
                                    Kehutanan / {{ $sup->mahasiswa->minat }}
                                </dd>

                                <dt class="mt-3 mt-md-2 mt-lg-0 col-4 col-md-5 d-flex justify-content-between">
                                    Hari / Tgl Seminar<span class="ml-1">:</span>
                                </dt>
                                <dd class="mt-3 mt-md-2 mt-lg-0 col-8 col-md-7">
                                    @if ($sup->tgl_seminar == null)
                                    <span class="text-danger">Tanggal Belum Ditentukan</span>
                                    @else
                                    {{ \Carbon\Carbon::parse($sup->tgl_seminar)->isoFormat('dddd / D MMMM Y') }}
                                    @endif
                                </dd>

                                <dt class="mt-3 mt-md-2 mt-lg-0 col-4 col-md-5 d-flex justify-content-between">
                                    Judul Seminar<span class="ml-1">:</span>
                                </dt>
                                <dd class="mt-3 mt-md-2 mt-lg-0 col-8 col-md-7">
                                    {{ \Illuminate\Support\Str::limit($sup->judul, 350, $end='...') }}
                                </dd>
                            </dl>


                        </div>
                        <div class="card-footer">
                            <hr>
                            <div class="row">
                                <div class="col-12">
                                    <div class="row">
                                        <div class="col-12 text-center">
                                            <h6>Pembimbing</h6>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="d-flex justify-content-between align-items-center" style="">
                                                <div class="text-truncate" style="">
                                                    <small
                                                        class="text-muted text-sm">{{ App\Models\Dosen::where('nip','=' ,$sup->pembimbing1)->first()->nama_dosen }}</small>
                                                </div>

                                                <div class="d-flex align-items-center justify-content-center">
                                                    Nilai :
                                                    @if ($sup->nilai_pembimbing1 == null)
                                                    <span class="ml-2 material-icons text-danger">
                                                        highlight_off
                                                    </span>
                                                    @else
                                                    <span class="ml-2 material-icons text-success">
                                                        check_circle_outline
                                                    </span>
                                                    @endif
                                                </div>

                                            </div>

                                            <div class="d-flex justify-content-between align-items-center">
                                                <div class="text-truncate" style="">
                                                    <small
                                                        class="text-muted text-sm">{{ App\Models\Dosen::where('nip','=' ,$sup->pembimbing2)->first()->nama_dosen }}</small>
                                                </div>
                                                <div class="d-flex align-items-center justify-content-center">
                                                    Nilai :
                                                    @if ($sup->nilai_pembimbing2 == null)
                                                    <span class="ml-2 material-icons text-danger">
                                                        highlight_off
                                                    </span>
                                                    @else
                                                    <span class="ml-2 material-icons text-success">
                                                        check_circle_outline
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-12">
                                    <div class="row">
                                        <div class="col-12 text-center">
                                            <h6> Penyanggah</h6>
                                        </div>
                                        <div class="col-12">
                                            <div class="d-flex justify-content-between align-items-center"
                                                style="width: 100%">
                                                <div>
                                                    @if ($sup->penyanggah1 == null)
                                                    <small class="text-muted text-sm text-danger">
                                                        Belum Ditentukan
                                                    </small>
                                                    @else
                                                    <small class="text-muted text-sm">
                                                        {{ App\Models\Mahasiswa::where('nim','=' ,$sup->penyanggah1)->first()->nama_mahasiswa }}
                                                    </small>
                                                    @endif
                                                </div>
                                                <div class="d-flex align-items-center">
                                                    Nilai :
                                                    @if ($sup->nilai_penyanggah1 == null)
                                                    <span class="ml-2 material-icons text-danger">
                                                        highlight_off
                                                    </span>
                                                    @else
                                                    <span class="ml-2 material-icons text-success">
                                                        check_circle_outline
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="d-flex justify-content-between align-items-center">
                                                <div>
                                                    @if ($sup->penyanggah2 == null)
                                                    <small class="text-muted text-sm text-danger">
                                                        Belum Ditentukan
                                                    </small>
                                                    @else
                                                    <small class="text-muted text-sm">
                                                        {{ App\Models\Mahasiswa::where('nim','=' ,$sup->penyanggah2)->first()->nama_mahasiswa }}
                                                    </small>
                                                    @endif

                                                </div>
                                                <div class="d-flex align-items-center">
                                                    Nilai :
                                                    @if ($sup->nilai_penyanggah2 == null)
                                                    <span class="ml-2 material-icons text-danger">
                                                        highlight_off
                                                    </span>
                                                    @else
                                                    <span class="ml-2 material-icons text-success">
                                                        check_circle_outline
                                                    </span>
                                                    @endif
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                @empty
                <div class="col-12 col-lg-6">
                    <div class="card rounded-lg">
                        <div class="card-header d-flex justify-content-between align-items-center">
                            <h5 class="card-category">Seminar Usulan Penelitian</h5>
                        </div>
                        <div class="card-body text-center">
                            <h3>Belum Ada</h3>
                        </div>
                    </div>
                </div>
                @endforelse

            </div>
        </div>
    </div>

    {{-- SEMINAR HASIL PENELITIAN --}}
    <div class="col-12">
        <div class="card">
            <div class="card-header text-center">
                <h5 class="card-title">Seminar Hasil Penelitian (Belum Dinilai)</h5>
            </div>
            <div class="card-body row justify-content-center">
                @forelse ($dataSHP as $shp)
                <div class="col-12 col-lg-6">
                    <div class="card rounded-lg">
                        <div class="card-header d-flex justify-content-between align-items-center">
                            <h5 class="card-category">Seminar Hasil Penelitian</h5>

                            <div class="d-flex">
                                <a href=""
                                    class="btn btn-round btn-outline-default btn-simple btn-icon no-caret d-flex justify-content-center align-items-center mr-2">
                                    <span class="material-icons">
                                        visibility
                                    </span>
                                </a>

                                <div class="dropdown">
                                    <button type="button"
                                        class="btn btn-round btn-outline-default dropdown-toggle btn-simple btn-icon no-caret d-flex justify-content-center align-items-center"
                                        data-toggle="dropdown">
                                        <span class="material-icons">
                                            list
                                        </span>
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right">

                                        <a class="dropdown-item"
                                            href="{{ route('dosen.seminar-hasil.penilaian.create', $shp->id_seminar_hasil_penelitian) }}">
                                            Tambah
                                            Nilai</a>
                                        <a class="dropdown-item"
                                            href="{{ route('dosen.penyanggah-1-seminar-hasil.penilaian.create', $shp->id_seminar_hasil_penelitian) }}">Tambah
                                            Nilai Penyanggah 1</a>
                                        <a class="dropdown-item"
                                            href="{{ route('dosen.penyanggah-2-seminar-hasil.penilaian.create', $shp->id_seminar_hasil_penelitian) }}">Tambah
                                            Nilai Penyanggah 2</a>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="card-body">

                            <dl class="row">
                                <dt class="mt-3 mt-md-2 mt-lg-0 col-4 col-md-5 d-flex justify-content-between">
                                    Nama / NIM Seminaris<span class="ml-1">:</span>
                                </dt>
                                <dd class="mt-3 mt-md-2 mt-lg-0 col-8 col-md-7">
                                    {{ $shp->mahasiswa->nama_mahasiswa }} / {{ $shp->nim_seminaris }}
                                </dd>

                                <dt class="mt-3 mt-md-2 mt-lg-0 col-4 col-md-5 d-flex justify-content-between">
                                    Program Studi / Minat<span class="ml-1">:</span>
                                </dt>
                                <dd class="mt-3 mt-md-2 mt-lg-0 col-8 col-md-7">
                                    Kehutanan / {{ $shp->mahasiswa->minat }}
                                </dd>

                                <dt class="mt-3 mt-md-2 mt-lg-0 col-4 col-md-5 d-flex justify-content-between">
                                    Hari / Tgl Seminar<span class="ml-1">:</span>
                                </dt>
                                <dd class="mt-3 mt-md-2 mt-lg-0 col-8 col-md-7">
                                    @if ($shp->tgl_seminar == null)
                                    <span class="text-danger">Tanggal Belum Ditentukan</span>
                                    @else
                                    {{ \Carbon\Carbon::parse($shp->tgl_seminar)->isoFormat('dddd / D MMMM Y') }}
                                    @endif
                                </dd>

                                <dt class="mt-3 mt-md-2 mt-lg-0 col-4 col-md-5 d-flex justify-content-between">
                                    Judul Seminar<span class="ml-1">:</span>
                                </dt>
                                <dd class="mt-3 mt-md-2 mt-lg-0 col-8 col-md-7">
                                    {{ \Illuminate\Support\Str::limit($shp->judul, 350, $end='...') }}
                                </dd>
                            </dl>

                        </div>
                        <div class="card-footer">
                            <hr>
                            <div class="row">
                                <div class="col-12">
                                    <div class="row">
                                        <div class="col-12 text-center">
                                            <h6>Pembimbing</h6>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="d-flex justify-content-between align-items-center" style="">
                                                <div class="text-truncate" style="">
                                                    <small
                                                        class="text-muted text-sm">{{ App\Models\Dosen::where('nip','=' ,$shp->pembimbing1)->first()->nama_dosen }}</small>
                                                </div>

                                                <div class="d-flex align-items-center justify-content-center">
                                                    Nilai :
                                                    @if ($shp->nilai_pembimbing1 == null)
                                                    <span class="ml-2 material-icons text-danger">
                                                        highlight_off
                                                    </span>
                                                    @else
                                                    <span class="ml-2 material-icons text-success">
                                                        check_circle_outline
                                                    </span>
                                                    @endif
                                                </div>

                                            </div>

                                            <div class="d-flex justify-content-between align-items-center">
                                                <div class="text-truncate" style="">
                                                    <small
                                                        class="text-muted text-sm">{{ App\Models\Dosen::where('nip','=' ,$shp->pembimbing2)->first()->nama_dosen }}</small>
                                                </div>
                                                <div class="d-flex align-items-center justify-content-center">
                                                    Nilai :
                                                    @if ($shp->nilai_pembimbing2 == null)
                                                    <span class="ml-2 material-icons text-danger">
                                                        highlight_off
                                                    </span>
                                                    @else
                                                    <span class="ml-2 material-icons text-success">
                                                        check_circle_outline
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-12">
                                    <div class="row">
                                        <div class="col-12 text-center ">
                                            <h6>Penyanggah</h6>
                                        </div>
                                        <div class="col-12">
                                            <div class="d-flex justify-content-between align-items-center"
                                                style="width: 100%">
                                                <div>
                                                    @if ($shp->penyanggah1 == null)
                                                    <small class="text-muted text-sm text-danger">
                                                        Belum Ditentukan
                                                    </small>
                                                    @else
                                                    <small class="text-muted text-sm">
                                                        {{ App\Models\Mahasiswa::where('nim','=' ,$shp->penyanggah1)->first()->nama_mahasiswa }}
                                                    </small>
                                                    @endif
                                                </div>
                                                <div class="d-flex align-items-center">
                                                    Nilai :
                                                    @if ($shp->nilai_penyanggah1 == null)
                                                    <span class="ml-2 material-icons text-danger">
                                                        highlight_off
                                                    </span>
                                                    @else
                                                    <span class="ml-2 material-icons text-success">
                                                        check_circle_outline
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="d-flex justify-content-between align-items-center">
                                                <div>
                                                    @if ($shp->penyanggah2 == null)
                                                    <small class="text-muted text-sm text-danger">
                                                        Belum Ditentukan
                                                    </small>
                                                    @else
                                                    <small class="text-muted text-sm">
                                                        {{ App\Models\Mahasiswa::where('nim','=' ,$shp->penyanggah2)->first()->nama_mahasiswa }}
                                                    </small>
                                                    @endif

                                                </div>
                                                <div class="d-flex align-items-center">
                                                    Nilai :
                                                    @if ($shp->nilai_penyanggah2 == null)
                                                    <span class="ml-2 material-icons text-danger">
                                                        highlight_off
                                                    </span>
                                                    @else
                                                    <span class="ml-2 material-icons text-success">
                                                        check_circle_outline
                                                    </span>
                                                    @endif
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                @empty
                <div class="col-12 col-lg-6">
                    <div class="card rounded-lg">
                        <div class="card-header d-flex justify-content-between align-items-center">
                            <h5 class="card-category">Seminar Hasil Penelitian</h5>
                        </div>
                        <div class="card-body text-center">
                            <h3>Belum Ada</h3>
                        </div>
                    </div>
                </div>
                @endforelse
            </div>
        </div>
    </div>

    {{-- Ujian Skripsi --}}
    <div class="col-12">
        <div class="card">
            <div class="card-header text-center">
                <h5 class="card-title">Ujian Skripsi (Belum Dinilai)</h5>
            </div>
            <div class="card-body row justify-content-center">
                @forelse ($dataUjianSkripsi as $us)
                <div class="col-12 col-lg-6">
                    <div class="card rounded-lg">
                        <div class="card-header d-flex justify-content-between align-items-center">
                            <h5 class="card-category">Ujian Skripsi</h5>

                            <div class="d-flex">
                                <a href=""
                                    class="btn btn-round btn-outline-default btn-simple btn-icon no-caret d-flex justify-content-center align-items-center mr-2">
                                    <span class="material-icons">
                                        visibility
                                    </span>
                                </a>

                                <a href="{{ route('dosen.ujian-skripsi.penilaian.create', $us->id_ujian_skripsi) }}"
                                    class="btn btn-round btn-outline-default btn-simple btn-icon no-caret d-flex justify-content-center align-items-center">
                                    <span class="material-icons">
                                        rate_review
                                    </span>
                                </a>
                            </div>

                        </div>
                        <div class="card-body">

                            <dl class="row">
                                <dt class="mt-3 mt-md-2 mt-lg-0 col-4 col-md-5 d-flex justify-content-between">
                                    Nama / NIM <span class="ml-1">:</span>
                                </dt>
                                <dd class="mt-3 mt-md-2 mt-lg-0 col-8 col-md-7">
                                    {{ $us->mahasiswa->nama_mahasiswa }} / {{ $us->nim_skripsi }}
                                </dd>

                                <dt class="mt-3 mt-md-2 mt-lg-0 col-4 col-md-5 d-flex justify-content-between">
                                    Program Studi / Minat<span class="ml-1">:</span>
                                </dt>
                                <dd class="mt-3 mt-md-2 mt-lg-0 col-8 col-md-7">
                                    Kehutanan / {{ $us->mahasiswa->minat }}
                                </dd>

                                <dt class="mt-3 mt-md-2 mt-lg-0 col-4 col-md-5 d-flex justify-content-between">
                                    Hari / Tgl Skripsi<span class="ml-1">:</span>
                                </dt>
                                <dd class="mt-3 mt-md-2 mt-lg-0 col-8 col-md-7 d-flex">
                                    @if ($us->tgl_skripsi == null)
                                    <span class="text-danger">Tanggal Belum Ditentukan</span>
                                    @else
                                    {{ \Carbon\Carbon::parse($us->tgl_skripsi)->isoFormat('dddd / D MMMM Y') }}
                                    @endif
                                </dd>

                                <dt class="mt-3 mt-md-2 mt-lg-0 col-4 col-md-5 d-flex justify-content-between">
                                    Judul Seminar<span class="ml-1">:</span>
                                </dt>
                                <dd class="mt-3 mt-md-2 mt-lg-0 col-8 col-md-7">
                                    {{ \Illuminate\Support\Str::limit($us->judul, 350, $end='...') }}
                                </dd>
                            </dl>

                        </div>
                        <div class="card-footer">
                            <hr>
                            <div class="row">
                                <div class="col-12">
                                    <div class="row">
                                        <div class="col-12 text-center">
                                            <h6>Pembimbing</h6>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="d-flex justify-content-between align-items-center" style="">
                                                <div class="text-truncate" style="">
                                                    <small
                                                        class="text-muted text-sm">{{ App\Models\Dosen::where('nip','=' ,$us->pembimbing1)->first()->nama_dosen }}</small>
                                                </div>

                                                <div class="d-flex align-items-center justify-content-center">
                                                    Nilai :
                                                    @if ($us->nilai_pembimbing1 == null)
                                                    <span class="ml-2 material-icons text-danger">
                                                        highlight_off
                                                    </span>
                                                    @else
                                                    <span class="ml-2 material-icons text-success">
                                                        check_circle_outline
                                                    </span>
                                                    @endif
                                                </div>

                                            </div>

                                            <div class="d-flex justify-content-between align-items-center">
                                                <div class="text-truncate" style="">
                                                    <small
                                                        class="text-muted text-sm">{{ App\Models\Dosen::where('nip','=' ,$us->pembimbing2)->first()->nama_dosen }}</small>
                                                </div>
                                                <div class="d-flex align-items-center justify-content-center">
                                                    Nilai :
                                                    @if ($us->nilai_pembimbing2 == null)
                                                    <span class="ml-2 material-icons text-danger">
                                                        highlight_off
                                                    </span>
                                                    @else
                                                    <span class="ml-2 material-icons text-success">
                                                        check_circle_outline
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                @empty
                <div class="col-12 col-lg-6">
                    <div class="card rounded-lg">
                        <div class="card-header d-flex justify-content-between align-items-center">
                            <h5 class="card-category">Seminar Usulan Penelitian</h5>
                        </div>
                        <div class="card-body text-center">
                            <h3>Belum Ada</h3>
                        </div>
                    </div>
                </div>
                @endforelse
            </div>
        </div>
    </div>

    {{-- Ujian komprehensif --}}
    <div class="col-12">
        <div class="card">
            <div class="card-header text-center">
                <h5 class="card-title">Ujian Komprehensif (Belum Dinilai)</h5>
            </div>
            <div class="card-body row justify-content-center">
                @forelse ($dataUjianKomprehensif as $uk)
                <div class="col-12 col-lg-6">
                    <div class="card rounded-lg">
                        <div class="card-header d-flex justify-content-between align-items-center">
                            <h5 class="card-category">Ujian Komprehensif</h5>

                            <div class="d-flex">
                                <a href=""
                                    class="btn btn-round btn-outline-default btn-simple btn-icon no-caret d-flex justify-content-center align-items-center mr-2">
                                    <span class="material-icons">
                                        visibility
                                    </span>
                                </a>

                                <a href="{{ route('dosen.ujian-kompre.penilaian.create', $uk->id_ujian_komprehensif) }}"
                                    class="btn btn-round btn-outline-default btn-simple btn-icon no-caret d-flex justify-content-center align-items-center">
                                    <span class="material-icons">
                                        rate_review
                                    </span>
                                </a>
                            </div>

                        </div>
                        <div class="card-body">

                            <dl class="row">
                                <dt class="mt-3 mt-md-2 mt-lg-0 col-4 col-md-5 d-flex justify-content-between">
                                    Nama / NIM <span class="ml-1">:</span>
                                </dt>
                                <dd class="mt-3 mt-md-2 mt-lg-0 col-8 col-md-7">
                                    {{ $uk->mahasiswa->nama_mahasiswa }} / {{ $uk->nim_skripsi }}
                                </dd>

                                <dt class="mt-3 mt-md-2 mt-lg-0 col-4 col-md-5 d-flex justify-content-between">
                                    Program Studi / Minat<span class="ml-1">:</span>
                                </dt>
                                <dd class="mt-3 mt-md-2 mt-lg-0 col-8 col-md-7">
                                    Kehutanan / {{ $uk->mahasiswa->minat }}
                                </dd>

                                <dt class="mt-3 mt-md-2 mt-lg-0 col-4 col-md-5 d-flex justify-content-between">
                                    Hari / Tgl Skripsi<span class="ml-1">:</span>
                                </dt>
                                <dd class="mt-3 mt-md-2 mt-lg-0 col-8 col-md-7 d-flex">
                                    @if ($uk->tgl_skripsi == null)
                                    <span class="text-danger">Tanggal Belum Ditentukan</span>
                                    @else
                                    {{ \Carbon\Carbon::parse($uk->tgl_skripsi)->isoFormat('dddd / D MMMM Y') }}
                                    Jam
                                    {{ date('h:i', strtotime($uk->tgl_skripsi)) }}
                                    @endif
                                </dd>

                            </dl>

                        </div>
                        <div class="card-footer">
                            <hr>
                            <div class="row">
                                <div class="col-12 text-center mb-2">
                                    <h6>Penguji</h6>
                                </div>

                                <div class="col-12">
                                    <div class="d-flex justify-content-between align-items-center" style="">
                                        <div class="text-truncate" style="">
                                            <small class="text-muted text-sm">
                                                1.
                                                {{ App\Models\Dosen::where('nip','=' ,$uk->penguji1)->first()->nama_dosen }}
                                            </small>
                                        </div>
                                        <div class="d-flex align-items-center">
                                            Nilai :
                                            @if ($uk->nilai_penguji1 == null)
                                            <span class="ml-2 material-icons text-danger">
                                                highlight_off
                                            </span>
                                            @else
                                            <span class="ml-2 material-icons text-success">
                                                check_circle_outline
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="d-flex justify-content-between align-items-center">
                                        <div class="text-truncate" style="">
                                            <small class="text-muted text-sm">
                                                2.
                                                {{ App\Models\Dosen::where('nip','=' ,$uk->penguji2)->first()->nama_dosen }}
                                            </small>
                                        </div>
                                        <div class="d-flex align-items-center">
                                            Nilai :
                                            @if ($uk->nilai_penguji2 == null)
                                            <span class="ml-2 material-icons text-danger">
                                                highlight_off
                                            </span>
                                            @else
                                            <span class="ml-2 material-icons text-success">
                                                check_circle_outline
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-12">
                                    <div class="d-flex justify-content-between align-items-center" style="">
                                        <div class="text-truncate" style="">
                                            @if ($uk->penguji3 != null)
                                            <small class="text-muted text-sm">
                                                3.
                                                {{ App\Models\Dosen::where('nip','=' ,$uk->penguji3)->first()->nama_dosen }}
                                            </small>
                                            @else
                                            <small class="text-danger text-sm">
                                                3. Belum Ditentukan
                                            </small>
                                            @endif
                                        </div>
                                        <div class="d-flex align-items-center">
                                            Nilai :
                                            @if ($uk->nilai_penguji3 == null)
                                            <span class="ml-2 material-icons text-danger">
                                                highlight_off
                                            </span>
                                            @else
                                            <span class="ml-2 material-icons text-success">
                                                check_circle_outline
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="d-flex justify-content-between align-items-center">
                                        <div class="text-truncate" style="">
                                            @if ($uk->penguji3 != null)
                                            <small class="text-muted text-sm">
                                                4.
                                                {{ App\Models\Dosen::where('nip','=' ,$uk->penguji4)->first()->nama_dosen }}
                                            </small>
                                            @else
                                            <small class="text-danger text-sm">
                                                4. Belum Ditentukan
                                            </small>
                                            @endif
                                        </div>
                                        <div class="d-flex align-items-center">
                                            Nilai :
                                            @if ($uk->nilai_penguji4 == null)
                                            <span class="ml-2 material-icons text-danger">
                                                highlight_off
                                            </span>
                                            @else
                                            <span class="ml-2 material-icons text-success">
                                                check_circle_outline
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                @empty
                <div class="col-12 col-lg-6">
                    <div class="card rounded-lg">
                        <div class="card-header d-flex justify-content-between align-items-center">
                            <h5 class="card-category">Ujian Komprehensif</h5>
                        </div>
                        <div class="card-body text-center">
                            <h3>Belum Ada</h3>
                        </div>
                    </div>
                </div>
                @endforelse
            </div>
        </div>
    </div>

</div>
@endsection
