@extends('layouts.backend')

@section('title')
SIMPUR | Nilai Mutu
@endsection

@section('header')
<div class="header text-center">
    <h2 class=" title">Data Nilai Mutu</h2>
    <div class="row justify-content-center">
        <div class="col-12 col-md-6">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb bg-white rounded-pill shadow">
                    <li class="breadcrumb-item"><a href="{{ route('dosen.index') }}">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Nilai Mutu</li>
                </ol>
            </nav>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <div class="row">
                    <div class="col-12">
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered text-center" id="table-nilai">
                                <thead class="text-Success thead-dark">
                                    <tr>
                                        <td rowspan="2" style="5%">Nama Mahasiswa</td>
                                        <td colspan="3">Nilai Proposal</td>
                                        <td colspan="3">Nilai Skripsi</td>
                                        <td rowspan="2">Nilai Seminar Rerata</td>
                                        <td colspan="3">Nilai Ujian Skripsi</td>
                                        <td rowspan="2">Nilai Komprehensif</td>
                                        <td rowspan="2">Nilai Skripsi</td>
                                        <td rowspan="2">Nilai Mutu</td>
                                    </tr>
                                    <tr>
                                        <td>Penyanggah Proposal</td>
                                        <td>Seminar Proposal</td>
                                        <td>Rerata Nilai Proposal</td>

                                        <td>Penyanggah Skripsi</td>
                                        <td>Seminar Skripsi</td>
                                        <td>Rerata Nilai Skripsi</td>

                                        <td>Pemb. I</td>
                                        <td>Pemb. II</td>
                                        <td>Rerata</td>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection

@push('tambahStyle')
<link rel="stylesheet" href="{{ url('adminarea/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ url('adminarea/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">

<style>
    #btnHapus {
        pointer-events: none;
        display: none;
    }
</style>
@endpush

@push('tambahScript')
<script src="{{ url('adminarea/plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ url('adminarea/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ url('adminarea/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ url('adminarea/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>

<script>
    $(document).ready(function() {
    $('#table-nilai').DataTable({
      language:{
            "url":"//cdn.datatables.net/plug-ins/1.10.21/i18n/Indonesian.json",
            "sEmptyTable":"Tidak Ada Data"
      },
      processing:true,
      serverside:true,
      ajax:"{{ route('ajax.getNilaiMutu') }}",
      columns:[
        {data:'nama', name:'nama'},
        {data:'rerata_nilai_penyanggah_sup', name:'rerata_nilai_penyanggah_sup', orderable: false, searchable: false},
        {data:'rerata_nilai_sup', name:'rerata_nilai_sup', orderable: false, searchable: false},
        {data:'rerata_nilai_usulan_penelitian', name:'rerata_nilai_usulan_penelitian', orderable: false, searchable: false},
        //
        {data:'rerata_nilai_penyanggah_shp', name:'rerata_nilai_penyanggah_shp', orderable: false, searchable: false},
        {data:'rerata_nilai_shp', name:'rerata_nilai_shp', orderable: false, searchable: false},
        {data:'rerata_nilai_hasil_penelitian', name:'rerata_nilai_hasil_penelitian', orderable: false, searchable: false},
        //
        {data:'rerata_nilai_seminar', name:'rerata_nilai_seminar', orderable: false, searchable: false},
        {data:'nilai_p1_ujian_skripsi', name:'nilai_p1_ujian_skripsi', orderable: false, searchable: false},
        {data:'nilai_p2_ujian_skripsi', name:'nilai_p2_ujian_skripsi', orderable: false, searchable: false},
        {data:'rerata_nilai_ujian_skripsi', name:'rerata_nilai_ujian_skripsi', orderable: false, searchable: false},
        {data:'nilai_komprehensif', name:'nilai_komprehensif', orderable: false, searchable: false},
        {data:'nilai_skripsi', name:'nilai_skripsi'},
        {data:'nilai_mutu', name:'nilai_mutu'},



      ]
    });

  } );
</script>
@endpush
