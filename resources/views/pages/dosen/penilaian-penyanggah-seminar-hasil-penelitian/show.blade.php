@extends('layouts.backend')

@section('title')
SIMPUR | Seminar Usulan Skripsi
@endsection

@section('header')
{{--  --}}
@endsection

@section('content')
<div class="row" style="margin-top: -10rem">
    <div class="row">
        <div class="col-12">

            @foreach ($penilaians as $penilaian)
            <div class="card">
                <div class="card-body">
                    <table style="width: 100%;">
                        <tr>
                            <td class="font-weight-bold text-center text-uppercase h4 underline pb-5" colspan="3">
                                <u>Nilai Seminar Usulan Penelitian</u></td>
                        </tr>
                        <tr>
                            <td style="width: 19%; position: relative;" class=""><span
                                    style="position: absolute; top: 0;">Nama/NIM Seminaris</span></td>
                            <td style="width:  1%; position: relative;" class=""><span
                                    style="position: absolute; top: 0;">:</span></td>
                            <td style="width: 70%;" class="">{{ $penilaian->mahasiswa->nama_mahasiswa }} /
                                {{ $penilaian->nim_seminaris }}</td>
                        </tr>
                        <tr>
                            <td style="width: 19%; position: relative;" class=""><span
                                    style="position: absolute; top: 0;">Program Studi Minat</span></td>
                            <td style="width: 1%; position: relative;" class=""><span
                                    style="position: absolute; top: 0;">:</span></td>
                            <td style="width: 70%;" class="">Kehutanan / {{ $penilaian->mahasiswa->minat }}
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 19%; position: relative;" class=""><span
                                    style="position: absolute; top: 0;">Hari/Tgl Seminar</span></td>
                            <td style="width: 1%; position: relative;" class=" "><span
                                    style="position: absolute; top: 0;">:</span></td>
                            <td style="width: 70%;" class="">
                                {{ \Carbon\Carbon::parse($penilaian->sup->tgl_seminar)->isoFormat('dddd / D MMMM Y') }}
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 19%;position: relative;" class="mt-3"><span
                                    style="position: absolute; top: 0;">Judul Seminar</span></td>
                            <td style="width: 1%;position: relative;" class="mt-3 "><span
                                    style="position: absolute; top: 0;">:</span></td>
                            <td style="width: 70%;" style="word-wrap: break-word">{{ $penilaian->sup->judul }} </td>
                        </tr>
                    </table>

                    <table class="" style="width: 100%;" id="tabelBeborder">
                        <thead style="text-align: center;">
                            <tr style="width: 100%;">
                                <td style="width: 5%;" class="font-weight-bold">NO</td>
                                <td style="width: auto;" class="font-weight-bold">ASPEK YANG DINILAI</td>
                                <td style="width: 8%;" class="font-weight-bold">NILAI <br> (>70-100)</td>
                                <td style="width: 7%;" class="font-weight-bold">BOBOT</td>
                                <td style="width: 8%;" class="font-weight-bold">BOBOT X <br> NILAI</td>
                            </tr>
                        </thead>

                        <tbody>
                            <tr>
                                <td rowspan="6" class="text-center">1</td>
                                <td colspan="4">a. Penulisan</td>
                            <tr>
                                <td class="pl-3">1) Kesesuaian nashkah usulan penelitian dengan pedoman penulisan
                                    ilmian
                                    yang berlaku</td>
                                <td>
                                    <input type="text" placeholder="..." id="nilai1a1" name="nilai1a1"
                                        onkeyup="hitung(this.value.replace(',', '.'), 0.10, 'hasil1a1')"
                                        value="{{ $penilaian->nilai1a1 }}" disabled>
                                </td>
                                <td class="text-center">0,10</td>
                                <td>
                                    <input type="text" placeholder="..." id="hasil1a1" name="hasil1a1"
                                        value="{{ $penilaian->hasil1a1 }}" disabled>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">b. Substansi isi naskah usulan penelitian</td>
                            </tr>
                            <tr>
                                <td class="pl-3">2) Judul ringkas, selaras dengan batang tubuh naskah usulan
                                    penelitian
                                </td>
                                <td>
                                    <input type="text" placeholder="..." id="nilai1b2" name="nilai1b2"
                                        onkeyup="hitung(this.value.replace(',', '.'), 0.10, 'hasil1b2')"
                                        value="{{ $penilaian->nilai1b2 }}" disabled>
                                </td>
                                <td class="text-center">0,10</td>
                                <td>
                                    <input type="text" placeholder="..." id="hasil1b2" name="hasil1b2"
                                        value="{{ $penilaian->hasil1b2 }}" disabled>
                                </td>
                            </tr>
                            <tr>
                                <td class="pl-3">3) Metode penelitian sesuai dengan tujuan penelitian</td>
                                <td>
                                    <input type="text" placeholder="..." id="nilai1b3" name="nilai1b3"
                                        onkeyup="hitung(this.value.replace(',', '.'), 0.10, 'hasil1b3')"
                                        value="{{ $penilaian->nilai1b3 }}" disabled>
                                </td>
                                <td class="text-center">0,10</td>
                                <td>
                                    <input type="text" placeholder="..." id="hasil1b3" name="hasil1b3"
                                        value="{{ $penilaian->hasil1b3 }}" disabled>
                                </td>
                            </tr>
                            <tr>
                                <td class="pl-3">4) Pustaka yang diacu minimal 15 sumber pustaka dan 80% berasal
                                    dari
                                    pustaka primer</td>
                                <td>
                                    <input type="text" placeholder="..." id="nilai1b4" name="nilai1b4"
                                        onkeyup="hitung(this.value.replace(',', '.'), 0.10, 'hasil1b4')"
                                        value="{{ $penilaian->nilai1b4 }}" disabled>
                                </td>
                                <td class="text-center">0,10</td>
                                <td>
                                    <input type="text" placeholder="..." id="hasil1b4" name="hasil1b4"
                                        value="{{ $penilaian->hasil1b4 }}" disabled>
                                </td>
                            </tr>
                            </tr>
                            <!-- -- -->
                            <tr>
                                <td rowspan="4" class="text-center">2</td>
                                <td colspan="4">Penugasan materi presentasi</td>
                            <tr>
                                <td class="pl-3">c. Kemampuan pemaparan</td>
                                <td>
                                    <input type="text" placeholder="..." id="nilai2c" name="nilai2c"
                                        onkeyup="hitung(this.value.replace(',', '.'), 0.10, 'hasil2c')"
                                        value="{{ $penilaian->nilai2c }}" disabled>
                                </td>
                                <td class="text-center">0,15</td>
                                <td>
                                    <input type="text" placeholder="..." id="hasil2c" name="hasil2c"
                                        value="{{ $penilaian->hasil2c }}" disabled>
                                </td>
                            </tr>
                            <tr>
                                <td class="pl-3">d. Kualitas penyajian</td>
                                <td>
                                    <input type="text" placeholder="..." id="nilai2d" name="nilai2d"
                                        onkeyup="hitung(this.value.replace(',', '.'), 0.10, 'hasil2d')"
                                        value="{{ $penilaian->hasil2d }}" disabled>
                                </td>
                                <td class="text-center">0,10</td>
                                <td>
                                    <input type="text" placeholder="..." id="hasil2d" name="hasil2d"
                                        value="{{ $penilaian->hasil2d }}" disabled>
                                </td>
                            </tr>
                            <tr>
                                <td class="pl-3">e. Ketepatan waktu</td>
                                <td>
                                    <input type="text" placeholder="..." id="nilai2e" name="nilai2e"
                                        onkeyup="hitung(this.value.replace(',', '.'), 0.10, 'hasil2e')"
                                        value="{{ $penilaian->hasil2e }}" disabled>
                                </td>
                                <td class="text-center">0,10</td>
                                <td>
                                    <input type="text" placeholder="..." id="hasil2e" name="hasil2e"
                                        value="{{ $penilaian->hasil2e }}" disabled>
                                </td>
                            </tr>
                            </tr>

                            <!-- -- -->
                            <tr>
                                <td class="text-center">3</td>
                                <td>Kemampuan penalaran dan pemikiran ilmiah (logis, kritis, sistematis dan kreatif)
                                </td>
                                <td>
                                    <input type="text" placeholder="..." id="nilai3" name="nilai3"
                                        onkeyup="hitung(this.value.replace(',', '.'), 0.10, 'hasil3')"
                                        value="{{ $penilaian->nilai3 }}" disabled>
                                </td>
                                <td class="text-center">0,15</td>
                                <td>
                                    <input type="text" placeholder="..." id="hasil3" name="hasil3"
                                        value="{{ $penilaian->hasil3 }}" disabled>
                                </td>
                            </tr>
                            <!-- -- -->
                            <tr>
                                <td class="text-center">4</td>
                                <td>Sikap dan etika seminaris</td>
                                <td>
                                    <input type="text" placeholder="..." id="nilai4" name="nilai4"
                                        onkeyup="hitung(this.value.replace(',', '.'), 0.10, 'hasil4')"
                                        value="{{ $penilaian->nilai4 }}" disabled>
                                </td>
                                <td class="text-center">0,10</td>
                                <td>
                                    <input type="text" placeholder="..." id="hasil4" name="hasil4"
                                        value="{{ $penilaian->hasil4 }}" disabled>
                                </td>
                            </tr>
                            <!-- -- -->
                            <tr class="">
                                <td colspan="4" class="text-center font-weight-bold py-4">Total Nilai</td>
                                <td class="text-center">
                                    <input type="text" placeholder="..." id="total_nilai" name="total_nilai"
                                        value="{{ $penilaian->total_nilai }}" disabled>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <table style="width: 100%;">
                        <td>
                            <tr>
                                <td style="width: 70%;"></td>
                                <td>Banjarbaru, xx September xxxx</td>
                            </tr>
                            <tr>
                                <td style="width: 70%;"></td>
                                <td>
                                    @if ($penilaian->sup->pembimbing1 == $penilaian->dosen->nip)
                                    Dosen Pembimbing I,
                                    @elseif($penilaian->sup->pembimbing2 == $penilaian->dosen->nip)
                                    Dosen Pembimbing II,
                                    @endif

                                </td>
                            </tr>
                            <tr height="5rem">
                                <td style="width: 70%;"></td>
                                <td> <br> <br> <br> <br> </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;"></td>
                                <td>{{ $penilaian->dosen->nama_dosen }}</td>
                            </tr>
                            <tr>
                                <td style="width: 70%;"></td>
                                <td>NIP. {{ $penilaian->dosen->nip }}</td>
                            </tr>
                        </td>
                    </table>
                </div>
            </div>
            @endforeach

        </div>
    </div>
</div>
@endsection

@push('tambahStyle')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css"
    integrity="sha512-+4zCK9k+qNFUR5X+cKL9EIR+ZOhtIloNl9GIKS57V1MyNsYpYcUrUeQc9vNfzsWfV28IaLL3i96P9sdNyeRssA=="
    crossorigin="anonymous" />
<link rel="stylesheet" href="{{ url('adminarea/plugins/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ url('adminarea/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
<style>
    table#tabelBeborder td {
        border: 1px solid black;
    }

    table#tabelBeborder input {
        border-width: 0px;
        text-align: center;
        width: 100%;
    }

    td {
        padding-left: .3rem;
    }
</style>
@endpush

@push('tambahScript')
<script src="{{ url('adminarea/plugins/select2/js/select2.full.min.js') }}"></script>
<script>
    $('#selectNIM, #selectPembimbing1, #selectPembimbing2').select2({
        placeholder: "Select a state",
        theme: 'bootstrap4',
    // tags: true,
    // tokenSeparators: [',', ' ']
  });

    function hitung(a, b, c){
      a = parseFloat(a);
      var inputHasil = document.getElementById(c);
      var bobotxnilai = a * b;
      inputHasil.value = bobotxnilai.toFixed(2).replace('.', ',').replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
      // --
      var totalNilaiInput = document.getElementById('total_nilai');
      var hasil1a1 = parseFloat( document.getElementById('hasil1a1').value.replace(',', '.'));
      var hasil1b2 = parseFloat( document.getElementById('hasil1b2').value.replace(',', '.'));
      var hasil1b3 = parseFloat( document.getElementById('hasil1b3').value.replace(',', '.'));
      var hasil1b4 = parseFloat( document.getElementById('hasil1b4').value.replace(',', '.'));
      var hasil2c = parseFloat( document.getElementById('hasil2c').value.replace(',', '.'));
      var hasil2d = parseFloat( document.getElementById('hasil2d').value.replace(',', '.'));
      var hasil2e = parseFloat( document.getElementById('hasil2e').value.replace(',', '.'));
      var hasil3 = parseFloat( document.getElementById('hasil3').value.replace(',', '.'));
      var hasil4 = parseFloat( document.getElementById('hasil4').value.replace(',', '.'));

      totalNilaix = hasil1a1 + hasil1b2 + hasil1b3 + hasil1b4 + hasil2c + hasil2d + hasil2e + hasil3 + hasil4;
      if (!isNaN(totalNilaix)) {
        totalNilaiInput.value = totalNilaix.toFixed(2).replace('.', ',').replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
      }
    }
</script>
@endpush
