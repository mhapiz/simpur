@extends('layouts.backend')

@section('title')
SIMPUR | Penilaian Penyanggah Seminar Hasil Skripsi
@endsection

@section('header')
{{--  --}}
@endsection

@section('content')
<div class="row" style="margin-top: -10rem">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <table style="width: 100%;">
                    <tr>
                        <td class="font-weight-bold text-center text-uppercase h4 underline pb-3" colspan="3">
                            <u>Lembar Penilaian Penyanggah Seminar Usulan Penelitian</u></td>
                    </tr>
                </table>

                <dl class="row">
                    <dt class="mt-3 mt-md-2 mt-lg-0 col-4  d-flex justify-content-between">
                        Nama / NIM Seminaris / Tanggal<span class="ml-1">:</span>
                    </dt>
                    <dd class="mt-3 mt-md-2 mt-lg-0 col-8 ">
                        {{ $penilaian->mahasiswa->nama_mahasiswa }} /
                        {{ $penilaian->nim_seminaris }} /
                        {{ \Carbon\Carbon::parse($penilaian->sup->tgl_seminar)->isoFormat('D MMMM Y') }}
                    </dd>

                    <dt class="mt-3 mt-md-2 mt-lg-0 col-4  d-flex justify-content-between">
                        Program Studi / Minat<span class="ml-1">:</span>
                    </dt>
                    <dd class="mt-3 mt-md-2 mt-lg-0 col-8 ">
                        Kehutanan / {{ $penilaian->mahasiswa->minat }}
                    </dd>

                    <dt class="mt-3 mt-md-2 mt-lg-0 col-4  d-flex justify-content-between">
                        Nama Penyanggah / NIM<span class="ml-1">:</span>
                    </dt>
                    <dd class="mt-3 mt-md-2 mt-lg-0 col-8 ">
                        <div class="form-group">
                            <select id="selectMahasiswa" name="nim_penyanggah"
                                class="@error('nim_penyanggah') is-invalid @enderror" style="width: 100%;" disabled>
                                <option>
                                    {{ App\Models\Mahasiswa::where('nim','=',$penilaian->nim_penyanggah)->first()->nama_mahasiswa }}
                                    /
                                    {{ $penilaian->nim_penyanggah }}</option>
                            </select>
                        </div>
                    </dd>

                    <dt class="mt-3 mt-md-2 mt-lg-0 col-4  d-flex justify-content-between">
                        Judul Seminar<span class="ml-1">:</span>
                    </dt>
                    <dd class="mt-3 mt-md-2 mt-lg-0 col-8 ">
                        {{ $penilaian->sup->judul }}
                    </dd>
                </dl>


                <div class="my-3">
                    @foreach ($errors->all() as $error)
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <strong>Error</strong> --- {{ $error }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endforeach
                </div>

                <table class="mt-5" style="width: 100%;" id="tabelBeborder">
                    <thead style="text-align: center;">
                        <tr style="width: 100%;">
                            <td style="width: 5%;" class="font-weight-bold">NO</td>
                            <td style="width: auto;" class="font-weight-bold">ASPEK YANG DINILAI</td>
                            <td style="width: 8%;" class="font-weight-bold">NILAI <br> (>70-100)</td>
                            <td style="width: 7%;" class="font-weight-bold">BOBOT</td>
                            <td style="width: 8%;" class="font-weight-bold">BOBOT X <br> NILAI</td>
                        </tr>
                    </thead>
                    <tbody>
                        <form id="storeNilaiPenyanggahSUP"
                            action="{{ route('dosen.penyanggah-seminar-usulan.penilaian.storeNilai', $penilaian->id_penilaian_penyanggah_seminar_usulan_penelitian) }}"
                            method="POST">
                            @method('PUT')
                            @csrf
                            <input type="hidden" name="nim_penyanggah" value="{{ $penilaian->nim_penyanggah }}">
                            <tr>
                                <td class="text-center">1.</td>
                                <td>Kemampuan melakukan kritikan/saran/pendapat dalam bentuk komunikasi secara lisan
                                </td>
                                <td>
                                    <input type="text" autocomplete="off" placeholder="..." id="nilai1" autofocus
                                        name="nilai1"
                                        onkeyup="hitung(this.value.replace(',', '.').replace(',', '.'), 0.2, 'hasil1')"
                                        value="{{ $penilaian->nilai1 }}">

                                </td>
                                <td class="text-center">0,2</td>
                                <td>
                                    <input type="text" autocomplete="off" placeholder="..." id="hasil1" name="hasil1"
                                        value="{{ $penilaian->hasil1 }}" readonly>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center">2.</td>
                                <td>Saran, pendapat dan kritikan yang bersifat perbaikan terhadap naskah Seminar
                                    Usulan
                                </td>
                                <td>
                                    <input type="text" autocomplete="off" placeholder="..." id="nilai2" autofocus
                                        name="nilai2" onkeyup="hitung(this.value.replace(',', '.'), 0.2, 'hasil2')"
                                        value="{{ $penilaian->nilai2 }}">
                                </td>
                                <td class="text-center">0,2</td>
                                <td>
                                    <input type="text" autocomplete="off" placeholder="..." id="hasil2" name="hasil2"
                                        value="{{ $penilaian->hasil2 }}" readonly>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center">3.</td>
                                <td>Kedalaman saran, pendapat dan kritikan yang disampaikan kepada seminaris</td>
                                <td>
                                    <input type="text" autocomplete="off" placeholder="..." id="nilai3" autofocus
                                        name="nilai3" onkeyup="hitung(this.value.replace(',', '.'), 0.2, 'hasil3')"
                                        value="{{ $penilaian->nilai3 }}">
                                </td>
                                <td class="text-center">0,2</td>
                                <td>
                                    <input type="text" autocomplete="off" placeholder="..." id="hasil3" name="hasil3"
                                        value="{{ $penilaian->hasil3 }}" readonly>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center">4.</td>
                                <td>Saran, perbaikan dan pendapat tidak keluar dari tema usulan penelitian</td>
                                <td>
                                    <input type="text" autocomplete="off" placeholder="..." id="nilai4" autofocus
                                        name="nilai4" onkeyup="hitung(this.value.replace(',', '.'), 0.2, 'hasil4')"
                                        value="{{ $penilaian->nilai4 }}">
                                </td>
                                <td class="text-center">0,2</td>
                                <td>
                                    <input type="text" autocomplete="off" placeholder="..." id="hasil4" name="hasil4"
                                        value="{{ $penilaian->hasil4 }}" readonly>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center">5.</td>
                                <td>Kemampuan menyampaikan gagasan, ide yang bersifat kreatif dan inovatif terhadap
                                    naskah seminar</td>
                                <td>
                                    <input type="text" autocomplete="off" placeholder="..." id="nilai5" autofocus
                                        name="nilai5" onkeyup="hitung(this.value.replace(',', '.'), 0.2, 'hasil5')"
                                        value="{{ $penilaian->nilai5 }}">
                                </td>
                                <td class="text-center">0,2</td>
                                <td>
                                    <input type="text" autocomplete="off" placeholder="..." id="hasil5" name="hasil5"
                                        value="{{ $penilaian->hasil5 }}" readonly>
                                </td>
                            </tr>
                            <!-- -- -->
                            <tr class="">
                                <td colspan="4" class="text-center font-weight-bold py-4">Total Nilai</td>
                                <td class="text-center">
                                    <input type="text" autocomplete="off" placeholder="..." id="total_nilai"
                                        name="total_nilai" value="{{ $penilaian->total_nilai }}" readonly>
                                </td>
                            </tr>
                        </form>
                    </tbody>
                </table>

                <table style="width: 100%;">
                    <td>
                        <tr>
                            <td style="width: 60%;"></td>
                            <td>Banjarbaru,
                                @if ($penilaian->sup->tgl_seminar == null)
                                <span class="text-danger">-</span>
                                @else
                                {{ \Carbon\Carbon::parse($penilaian->sup->tgl_seminar)->isoFormat(' D MMMM Y') }}
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 60%;"></td>
                            <td>
                                @if ($penilaian->sup->pembimbing1 == $penilaian->dosen->nip)
                                Dosen Pembimbing I,
                                @elseif($penilaian->sup->pembimbing2 == $penilaian->dosen->nip)
                                Dosen Pembimbing II,
                                @endif
                            </td>
                        </tr>
                        <tr height="5rem">
                            <td style="width: 60%;"></td>
                            <td> <br> <br> <br> <br> </td>
                        </tr>
                        <tr>
                            <td style="width: 60%;"></td>
                            <td>{{ $penilaian->dosen->nama_dosen }}</td>
                        </tr>
                        <tr>
                            <td style="width: 60%;"></td>
                            <td>NIP. {{ $penilaian->dosen->nip }}</td>
                        </tr>
                    </td>
                </table>

                <div class="row mt-5">
                    <div class="col-12 text-right">
                        <button type="submit" form="storeNilaiPenyanggahSUP" class="btn btn-success">Simpan
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('tambahStyle')
<link rel="stylesheet" href="{{ url('adminarea/plugins/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ url('adminarea/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
<style>
    table#tabelBeborder td {
        border: 1px solid black;
    }

    table#tabelBeborder input {
        border-width: 0px;
        text-align: center;
        width: 100%;
    }

    td {
        padding-left: .3rem;
    }
</style>
@endpush

@push('tambahScript')
<script src="{{ url('adminarea/plugins/select2/js/select2.full.min.js') }}"></script>
<script>
    $('#selectMahasiswa').select2({
        placeholder: "Pilih Mahasiswa",
        theme: 'bootstrap4',
    // tags: true,
    // tokenSeparators: [',', ' ']
  });

    function hitung(a, b, c){
      a = parseFloat(a);
      var inputHasil = document.getElementById(c);
      var bobotxnilai = a * b;
      if (!isNaN(bobotxnilai)) {
        hasil = bobotxnilai.toFixed(2).replace('.', ',').replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
        inputHasil.value = hasil;
      }
      // --
      var totalNilaiInput = document.getElementById('total_nilai');
      var hasil1 = parseFloat( document.getElementById('hasil1').value.replace(',', '.'));
      var hasil2 = parseFloat( document.getElementById('hasil2').value.replace(',', '.'));
      var hasil3 = parseFloat( document.getElementById('hasil3').value.replace(',', '.'));
      var hasil4 = parseFloat( document.getElementById('hasil4').value.replace(',', '.'));
      var hasil5 = parseFloat( document.getElementById('hasil5').value.replace(',', '.'));

      totalNilai = hasil1 + hasil2 + hasil3 + hasil4 + hasil5;
      if (!isNaN(totalNilai)) {
        totalNilaiInput.value = totalNilai.toFixed(2).replace('.', ',').replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
      }
    }
</script>
@endpush
