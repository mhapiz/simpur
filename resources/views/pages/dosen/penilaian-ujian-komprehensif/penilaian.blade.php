@extends('layouts.backend')

@section('title')
SIMPUR | Ujian Komprehensif
@endsection

@section('header')
{{--  --}}
@endsection

@section('content')
<div class="row" style="margin-top: -10rem">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <table style="width: 100%;">
                        <tr>
                            <td class="font-weight-bold text-center text-uppercase h4 underline pb-5" colspan="3">
                                <u>Penilaian Ujian Komprehensif</u></td>
                        </tr>
                    </table>

                    <dl class="row">
                        <dt class="mt-3 mt-md-2 mt-lg-0 col-4 col-md-2 col-lg-3 d-flex justify-content-between">
                            Nama / NIM Seminaris<span class="ml-1">:</span>
                        </dt>
                        <dd class="mt-3 mt-md-2 mt-lg-0 col-8 col-md-10 col-lg-9">
                            {{ $penilaian->mahasiswa->nama_mahasiswa }} /
                            {{ $penilaian->nim_skripsi }}
                        </dd>

                        <dt class="mt-3 mt-md-2 mt-lg-0 col-4 col-md-2 col-lg-3 d-flex justify-content-between">
                            Program Studi / Minat<span class="ml-1">:</span>
                        </dt>
                        <dd class="mt-3 mt-md-2 mt-lg-0 col-8 col-md-10 col-lg-9">
                            Kehutanan / {{ $penilaian->mahasiswa->minat }}
                        </dd>

                        <dt class="mt-3 mt-md-2 mt-lg-0 col-4 col-md-2 col-lg-3 d-flex justify-content-between">
                            Hari / Tgl Skripsi<span class="ml-1">:</span>
                        </dt>
                        <dd class="mt-3 mt-md-2 mt-lg-0 col-8 col-md-10 col-lg-9">

                            @if ($penilaian->uk->tgl_skripsi == null)
                            <span class="text-danger">Tanggal Belum Ditentukan</span>
                            @else
                            {{ \Carbon\Carbon::parse($penilaian->uk->tgl_skripsi)->isoFormat('dddd / D MMMM Y') }}
                            @endif

                        </dd>
                    </dl>

                    <div class="my-3">
                        @foreach ($errors->all() as $error)
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <strong>Error</strong> --- {{ $error }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endforeach
                    </div>


                    <table class="mt-5" style="width: 100%;" id="tabelBeborder">
                        <thead style="text-align: center;">
                            <tr style="width: 100%;">
                                <td style="width: 5%;" class="font-weight-bold">NO</td>
                                <td style="width: auto;" class="font-weight-bold">Unsur Penilaian</td>
                                <td style="width: auto;" class="font-weight-bold">Acuan Penilaian</td>
                                <td style="width: 8%;" class="font-weight-bold">NILAI <br> (>70-100)</td>
                                <td style="width: 7%;" class="font-weight-bold">BOBOT</td>
                                <td style="width: 8%;" class="font-weight-bold">BOBOT X <br> NILAI</td>
                            </tr>
                        </thead>
                        <tbody>

                            <form id="storeNilaiUjianKompre"
                                action="{{ route('dosen.ujian-kompre.penilaian.storeNilai', $penilaian->id_penilaian_ujian_komprehensif) }}"
                                method="POST">
                                @method('PUT')
                                @csrf

                                <tr>
                                    <td class="text-center" rowspan="3">1.</td>
                                    <td rowspan="3">Penilaian Naskah Skripsi</td>
                                <tr>
                                    <td>a. Judul berhubungan dengan masalah, tujuan, metoda, hasil dan kemampuan.</td>
                                    <td>
                                        <input type="text" placeholder="..." autofocus autocomplete="off" id="nilai1a"
                                            name="nilai1a"
                                            onkeyup="hitung(this.value.replace(',', '.'), 0.10, 'hasil1a')"
                                            value="{{ $penilaian->nilai1a }}">
                                    </td>
                                    <td class="text-center">0,10</td>
                                    <td>
                                        <input type="text" placeholder="..." id="hasil1a" name="hasil1a" class="noedit"
                                            value="{{ $penilaian->hasil1a }}" readonly>
                                    </td>
                                </tr>
                                <tr>
                                    <td>b. Penggunaan Bahasa dan tata kalimat yang sesuai dengan kaidah ilmiah.</td>
                                    <td>
                                        <input type="text" placeholder="..." autofocus autocomplete="off" id="nilai1b"
                                            name="nilai1b"
                                            onkeyup="hitung(this.value.replace(',', '.'), 0.10, 'hasil1b')"
                                            value="{{ $penilaian->nilai1b }}">
                                    </td>
                                    <td class="text-center">0,10</td>
                                    <td>
                                        <input type="text" placeholder="..." id="hasil1b" name="hasil1b" class="noedit"
                                            value="{{ $penilaian->hasil1b }}" readonly>
                                    </td>
                                </tr>
                                </tr>


                                <tr>
                                    <td class="text-center" rowspan="3">2.</td>
                                    <td rowspan="3">Penyampaian Materi</td>
                                <tr>
                                    <td>a. Penguasaan terhadap skripsi.</td>
                                    <td>
                                        <input type="text" placeholder="..." autofocus autocomplete="off" id="nilai2a"
                                            name="nilai2a"
                                            onkeyup="hitung(this.value.replace(',', '.'), 0.15, 'hasil2a')"
                                            value="{{ $penilaian->nilai2a }}">
                                    </td>
                                    <td class="text-center">0,15</td>
                                    <td>
                                        <input type="text" placeholder="..." id="hasil2a" name="hasil2a" class="noedit"
                                            value="{{ $penilaian->hasil2a }}" readonly>
                                    </td>
                                </tr>
                                <tr>
                                    <td>b. Kualitas penyajian power point dan cara mempresentasikan.</td>
                                    <td>
                                        <input type="text" placeholder="..." autofocus autocomplete="off" id="nilai2b"
                                            name="nilai2b"
                                            onkeyup="hitung(this.value.replace(',', '.'), 0.10, 'hasil2b')"
                                            value="{{ $penilaian->nilai2b }}">
                                    </td>
                                    <td class="text-center">0,10</td>
                                    <td>
                                        <input type="text" placeholder="..." id="hasil2b" name="hasil2b" class="noedit"
                                            value="{{ $penilaian->hasil2b }}" readonly>
                                    </td>
                                </tr>
                                </tr>

                                <tr>
                                    <td class="text-center" rowspan="3">3.</td>
                                    <td rowspan="3">Wawasan dibidang kehutanan</td>
                                <tr>
                                    <td>a. Pemahaman umum di bidang kehutanan.</td>
                                    <td>
                                        <input type="text" placeholder="..." autofocus autocomplete="off" id="nilai3a"
                                            name="nilai3a"
                                            onkeyup="hitung(this.value.replace(',', '.'), 0.15, 'hasil3a')"
                                            value="{{ $penilaian->nilai3a }}">
                                    </td>
                                    <td class="text-center">0,15</td>
                                    <td>
                                        <input type="text" placeholder="..." id="hasil3a" name="hasil3a" class="noedit"
                                            value="{{ $penilaian->hasil3a }}" readonly>
                                    </td>
                                </tr>
                                <tr>
                                    <td>b. Keterkaitan bidang penelitian dengan ilmu kehutanan.</td>
                                    <td>
                                        <input type="text" placeholder="..." autofocus autocomplete="off" id="nilai3b"
                                            name="nilai3b"
                                            onkeyup="hitung(this.value.replace(',', '.'), 0.20, 'hasil3b')"
                                            value="{{ $penilaian->nilai3b }}">
                                    </td>
                                    <td class="text-center">0,20</td>
                                    <td>
                                        <input type="text" placeholder="..." id="hasil3b" name="hasil3b" class="noedit"
                                            value="{{ $penilaian->hasil3b }}" readonly>
                                    </td>
                                </tr>
                                </tr>

                                <tr>
                                    <td class="text-center">4.</td>
                                    <td>Cara menjawab pertanyaan dan kebenaran jawaban</td>
                                    <td>Jumlah pertanyaan yang mampu di jawab oleh eksaminandus/da</td>
                                    <td>
                                        <input type="text" placeholder="..." autofocus autocomplete="off" id="nilai4"
                                            name="nilai4" onkeyup="hitung(this.value.replace(',', '.'), 0.10, 'hasil4')"
                                            value="{{ $penilaian->nilai4 }}">
                                    </td>
                                    <td class="text-center">0,10</td>
                                    <td>
                                        <input type="text" placeholder="..." id="hasil4" name="hasil4" class="noedit"
                                            value="{{ $penilaian->hasil4 }}" readonly>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="text-center">5.</td>
                                    <td>Etika</td>
                                    <td>a. Sopan santun saat menghadapi ujian komprehensif dan kerapian cara berpakaian
                                    </td>
                                    <td>
                                        <input type="text" placeholder="..." autofocus autocomplete="off" id="nilai5a"
                                            name="nilai5a"
                                            onkeyup="hitung(this.value.replace(',', '.'), 0.10, 'hasil5a')"
                                            value="{{ $penilaian->nilai5a }}">
                                    </td>
                                    <td class="text-center">0,10</td>
                                    <td>
                                        <input type="text" placeholder="..." id="hasil5a" name="hasil5a" class="noedit"
                                            value="{{ $penilaian->hasil5a }}" readonly>
                                    </td>
                                </tr>


                                <tr class="">
                                    <td colspan="5" class="text-center font-weight-bold py-4">Total Nilai</td>
                                    <td class="text-center">
                                        <input type="text" placeholder="..." id="total_nilai" name="total_nilai"
                                            class="noedit" value="{{ $penilaian->total_nilai }}" readonly>
                                    </td>
                                </tr>
                            </form>


                        </tbody>
                    </table>

                    <table style="width: 100%;">
                        <tr>
                            <td colspan="3" style="width: 10px;">Keterangan :</td>
                        </tr>
                        <tr>
                            <td style="width: 10px;"></td>
                            <td style="width: 10px;">a.</td>
                            <td style="width: auto;">Nilai Kelulusan > 70;</td>
                        </tr>
                        <tr>
                            <td style="width: 10px;"></td>
                            <td style="width: 10px;">b.</td>
                            <td style="width: auto;">Nilai akhir adalah nilai rata-rata dari semua dosen penguji.</td>
                        </tr>
                        <tr>
                            <td style="width: 10px;"></td>
                            <td style="width: 10px;">c.</td>
                            <td style="width: auto;">Nilai A : >= 80; 77 - < 80; B+ : 75 - < 77; B : 70 - < 75 </td>
                                    </tr> </table> <table style="width: 100%;">
                            <td>
                        <tr>
                            <td style="width: 70%;"></td>
                            <td>Banjarbaru,
                                @if ($penilaian->uk->tgl_skripsi == null)
                                <span class="text-danger">Tanggal Belum Ditentukan</span>
                                @else
                                {{ \Carbon\Carbon::parse($penilaian->uk->tgl_skripsi)->isoFormat('D MMMM Y') }}
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 70%;"></td>
                            <td>


                                @if ($penilaian->uk->penguji1 == $penilaian->dosen->nip)
                                Penguji,
                                {{-- {{ App\Models\Dosen::where('nip', '=', $penilaian->uk->penguji1)->first()->nama_dosen }}
                                --}}

                                @elseif($penilaian->uk->penguji2 == $penilaian->dosen->nip)
                                Penguji,
                                {{-- {{ App\Models\Dosen::where('nip', '=', $penilaian->uk->penguji2)->first()->nama_dosen }}
                                --}}

                                @elseif($penilaian->uk->penguji3 == $penilaian->dosen->nip)
                                Penguji,
                                {{-- {{ App\Models\Dosen::where('nip', '=', $penilaian->uk->penguji3)->first()->nama_dosen }}
                                --}}

                                @elseif($penilaian->uk->penguji4 == $penilaian->dosen->nip)
                                Penguji,
                                {{-- {{ App\Models\Dosen::where('nip', '=', $penilaian->uk->penguji4)->first()->nama_dosen }}
                                --}}

                                @endif

                            </td>
                        </tr>
                        <tr height="5rem">
                            <td style="width: 70%;"></td>
                            <td> <br> <br> <br> <br> </td>
                        </tr>
                        <tr>
                            <td style="width: 70%;"></td>
                            <td>{{ $penilaian->dosen->nama_dosen }}</td>
                        </tr>
                        <tr>
                            <td style="width: 70%;"></td>
                            <td>NIP. {{ $penilaian->dosen->nip }}</td>
                        </tr>
                        </td>
                    </table>

                    <div class="row mt-5">
                        <div class="col-12 text-right">
                            <button type="submit" form="storeNilaiUjianKompre" class="btn btn-success">Simpan </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('tambahStyle')
<link rel="stylesheet" href="{{ url('adminarea/plugins/gijgo/css/gijgo.min.css') }}">
<style>
    .noedit {
        pointer-events: none;
    }

    table#tabelBeborder td {
        border: 1px solid black;
    }

    table#tabelBeborder input {
        border-width: 0px;
        text-align: center;
        width: 100%;
    }

    td {
        padding-left: .3rem;
    }
</style>
@endpush

@push('tambahScript')
<script src="{{ url('adminarea/plugins/gijgo/js/gijgo.min.js') }}"></script>
<script src="{{ url('adminarea/plugins/gijgo/js/messages/messages.id-id.js') }}"></script>
<script>
    $('#tgl_skripsi').datepicker({
      uiLibrary: 'materialdesign',
      format: "yyyy-mm-dd",
      locale: 'id-id',
    });

    function hitung(a, b, c){
      a = parseFloat(a);
      var inputHasil = document.getElementById(c);
      var bobotxnilai = a * b;
      inputHasil.value = bobotxnilai.toFixed(2).replace('.', ',').replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
      // --
      var totalNilaiInput = document.getElementById('total_nilai');
      var hasil1a = parseFloat( document.getElementById('hasil1a').value.replace(',', '.'));
      var hasil1b = parseFloat( document.getElementById('hasil1b').value.replace(',', '.'));

      var hasil2a = parseFloat( document.getElementById('hasil2a').value.replace(',', '.'));
      var hasil2b = parseFloat( document.getElementById('hasil2b').value.replace(',', '.'));

      var hasil3a = parseFloat( document.getElementById('hasil3a').value.replace(',', '.'));
      var hasil3b = parseFloat( document.getElementById('hasil3b').value.replace(',', '.'));

      var hasil4 = parseFloat( document.getElementById('hasil4').value.replace(',', '.'));
      var hasil5a = parseFloat( document.getElementById('hasil5a').value.replace(',', '.'));

      totalNilaix = hasil1a + hasil1b + hasil2a + hasil2b + hasil3a + hasil3b + hasil4 + hasil5a ;
      if (!isNaN(totalNilaix)) {
        totalNilaiInput.value = totalNilaix.toFixed(2).replace('.', ',').replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
      }
    }
</script>
@endpush
