@extends('layouts.backend')

@section('title')
SIMPUR | Ujian Skripsi
@endsection

@section('header')
{{--  --}}
@endsection

@section('content')
<div class="row" style="margin-top: -10rem">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <table style="width: 100%;">
                        <tr>
                            <td class="font-weight-bold text-center text-uppercase h4 underline pb-5" colspan="3">
                                <u>Nilai ujian Skripsi</u></td>
                        </tr>
                    </table>

                    <dl class="row">
                        <dt class="mt-3 mt-md-2 mt-lg-0 col-4 col-md-2 col-lg-3 d-flex justify-content-between">
                            Nama / NIM <span class="ml-1">:</span>
                        </dt>
                        <dd class="mt-3 mt-md-2 mt-lg-0 col-8 col-md-10 col-lg-9">
                            {{ $penilaian->mahasiswa->nama_mahasiswa }} /
                            {{ $penilaian->nim_skripsi }}
                        </dd>

                        <dt class="mt-3 mt-md-2 mt-lg-0 col-4 col-md-2 col-lg-3 d-flex justify-content-between">
                            Program Studi / Minat<span class="ml-1">:</span>
                        </dt>
                        <dd class="mt-3 mt-md-2 mt-lg-0 col-8 col-md-10 col-lg-9">
                            Kehutanan / {{ $penilaian->mahasiswa->minat }}
                        </dd>

                        <dt class="mt-3 mt-md-2 mt-lg-0 col-4 col-md-2 col-lg-3 d-flex justify-content-between">
                            Hari / Tgl Skripsi<span class="ml-1">:</span>
                        </dt>
                        <dd class="mt-3 mt-md-2 mt-lg-0 col-8 col-md-10 col-lg-9">
                            @if ($penilaian->us->tgl_skripsi == null)
                            <span class="text-danger">Tanggal Belum Ditentukan</span>
                            @else
                            {{ \Carbon\Carbon::parse($penilaian->us->tgl_skripsi)->isoFormat('dddd / D MMMM Y') }}
                            @endif
                        </dd>

                        <dt class="mt-3 mt-md-2 mt-lg-0 col-4 col-md-2 col-lg-3 d-flex justify-content-between">
                            Judul Seminar<span class="ml-1">:</span>
                        </dt>
                        <dd class="mt-3 mt-md-2 mt-lg-0 col-8 col-md-10 col-lg-9">
                            {{ $penilaian->us->judul }}
                        </dd>
                    </dl>


                    <div class="my-3">
                        @foreach ($errors->all() as $error)
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <strong>Error</strong> --- {{ $error }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endforeach
                    </div>


                    <table class="" style="width: 100%;" id="tabelBeborder">
                        <thead style="text-align: center;">
                            <tr style="width: 100%;">
                                <td style="width: 5%;" class="font-weight-bold">NO</td>
                                <td style="width: auto;" class="font-weight-bold">Unsur Penilaian</td>
                                <td style="width: auto;" class="font-weight-bold">Acuan Penilaian</td>
                                <td style="width: 8%;" class="font-weight-bold">NILAI <br> (>70-100)</td>
                                <td style="width: 7%;" class="font-weight-bold">BOBOT</td>
                                <td style="width: 8%;" class="font-weight-bold">BOBOT X <br> NILAI</td>
                            </tr>
                        </thead>


                        <tbody>
                            <form id="storeNilai"
                                action="{{ route('dosen.ujian-skripsi.penilaian.storeNilai', $penilaian->id_penilaian_ujian_skripsi) }}"
                                method="POST">
                                @method('PUT')
                                @csrf
                                <tr>
                                    <td class="text-center">1. </td>
                                    <td>Wawasan di bidang keilmuan</td>
                                    <td>Pemahaman umum dibidang kehutanan dan keterkaitan bidang penelitian dengan
                                        kehutanan</td>
                                    <td>
                                        <input type="text" placeholder="..." autofocus id="nilai1" name="nilai1"
                                            onkeyup="hitung(this.value.replace(',', '.'), 0.20, 'hasil1')"
                                            value="{{ $penilaian->nilai1 }}">
                                    </td>
                                    <td class="text-center">0,20</td>
                                    <td>
                                        <input type="text" placeholder="..." class="noedit" id="hasil1" name="hasil1"
                                            value="{{ $penilaian->hasil1 }}" readonly>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center" rowspan="4">2. </td>
                                    <td colspan="5">Isi dan penguasan terhadap skripsi</td>
                                <tr>
                                    <td>a. Kajian Teoritis</td>
                                    <td>Adanya relevansi dengan topik yang diteliti dan kemutakhiran dengan pustaka yang
                                        diacu</td>
                                    <td>
                                        <input type="text" placeholder="..." autofocus id="nilai2a" name="nilai2a"
                                            onkeyup="hitung(this.value.replace(',', '.'), 0.20, 'hasil2a')"
                                            value="{{ $penilaian->nilai2a }}">
                                    </td>
                                    <td class="text-center">0,20</td>
                                    <td>
                                        <input type="text" placeholder="..." class="noedit" id="hasil2a" name="hasil2a"
                                            value="{{ $penilaian->hasil2a }}" readonly>
                                    </td>
                                </tr>
                                <tr>
                                    <td>b. Metode Penelitian</td>
                                    <td>Kesesuaian dengan masalah; Ketepatan rancangan; Ketepatan instrument; Ketepatan
                                        dan ketepatan analisis</td>
                                    <td>
                                        <input type="text" placeholder="..." autofocus id="nilai2b" name="nilai2b"
                                            onkeyup="hitung(this.value.replace(',', '.'), 0.20, 'hasil2b')"
                                            value="{{ $penilaian->nilai2b }}">
                                    </td>
                                    <td class="text-center">0,20</td>
                                    <td>
                                        <input type="text" placeholder="..." class="noedit" id="hasil2b" name="hasil2b"
                                            value="{{ $penilaian->hasil2b }}" readonly>
                                    </td>
                                </tr>
                                <tr>
                                    <td>c. Hasil Penelitian</td>
                                    <td>Manfaat dan kontribusi bagi pengembangan ilmu; Kesesuaian dengan tujuan
                                        penelitian dan kedalaman pembahasan; dan Keaslian tulisan</td>
                                    <td>
                                        <input type="text" placeholder="..." autofocus id="nilai2c" name="nilai2c"
                                            onkeyup="hitung(this.value.replace(',', '.'), 0.30, 'hasil2c')"
                                            value="{{ $penilaian->nilai2c }}">
                                    </td>
                                    <td class="text-center">0,30</td>
                                    <td>
                                        <input type="text" placeholder="..." class="noedit" id="hasil2c" name="hasil2c"
                                            value="{{ $penilaian->hasil2c }}" readonly>
                                    </td>
                                </tr>
                                </tr>
                                <tr>
                                    <td class="text-center">3. </td>
                                    <td>Lain - lain</td>
                                    <td>Kesesuaian pemakaian EYD; Kesesuaian format dengan pedoman penulisan;
                                        Ringkasan/abstrak dan penampilan</td>
                                    <td>
                                        <input type="text" placeholder="..." autofocus id="nilai3" name="nilai3"
                                            onkeyup="hitung(this.value.replace(',', '.'), 0.10, 'hasil3')"
                                            value="{{ $penilaian->nilai3 }}">
                                    </td>
                                    <td class="text-center">0,10</td>
                                    <td>
                                        <input type="text" placeholder="..." class="noedit" id="hasil3" name="hasil3"
                                            value="{{ $penilaian->hasil3 }}" readonly>
                                    </td>
                                </tr>
                                <tr class="">
                                    <td colspan="5" class="text-center font-weight-bold py-4">Total Nilai</td>
                                    <td class="text-center">
                                        <input type="text" placeholder="..." class="noedit" id="total_nilai"
                                            name="total_nilai" value="{{ $penilaian->total_nilai }}" readonly>
                                    </td>
                                </tr>
                            </form>

                        </tbody>
                    </table>

                    <table style="width: 100%;">
                        <td>
                            <tr>
                                <td style="width: 70%;"></td>
                                <td>Banjarbaru,
                                    @if ($penilaian->us->tgl_skripsi == null)
                                    <span class="text-danger">Tanggal Belum Ditentukan</span>
                                    @else
                                    {{ \Carbon\Carbon::parse($penilaian->us->tgl_skripsi)->isoFormat('D MMMM Y') }}
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;"></td>
                                <td>
                                    @if ($penilaian->us->pembimbing1 == $penilaian->dosen->nip)
                                    Dosen Pembimbing I,
                                    @elseif($penilaian->us->pembimbing2 == $penilaian->dosen->nip)
                                    Dosen Pembimbing II,
                                    @endif

                                </td>
                            </tr>
                            <tr height="5rem">
                                <td style="width: 70%;"></td>
                                <td> <br> <br> <br> <br> </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;"></td>
                                <td>{{ $penilaian->dosen->nama_dosen }}</td>
                            </tr>
                            <tr>
                                <td style="width: 70%;"></td>
                                <td>NIP. {{ $penilaian->dosen->nip }}</td>
                            </tr>
                        </td>
                    </table>

                    <div class="row mt-5">
                        <div class="col-12 text-right">
                            <button type="submit" form="storeNilai" class="btn btn-success">Simpan </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('tambahStyle')
<link rel="stylesheet" href="{{ url('adminarea/plugins/gijgo/css/gijgo.min.css') }}">
<style>
    .noedit {
        pointer-events: none
    }

    table#tabelBeborder td {
        border: 1px solid black;
    }

    table#tabelBeborder input {
        border-width: 0px;
        text-align: center;
        width: 100%;
    }

    td {
        padding-left: .3rem;
    }
</style>
@endpush

@push('tambahScript')
<script src="{{ url('adminarea/plugins/gijgo/js/gijgo.min.js') }}"></script>
<script src="{{ url('adminarea/plugins/gijgo/js/messages/messages.id-id.js') }}"></script>
<script>
    $('#tgl_skripsi').datepicker({
      uiLibrary: 'materialdesign',
      format: "yyyy-mm-dd",
      locale: 'id-id',
    });

    function hitung(a, b, c){
      a = parseFloat(a);
      var inputHasil = document.getElementById(c);
      var bobotxnilai = a * b;
      inputHasil.value = bobotxnilai.toFixed(2).replace('.', ',').replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
      // --
      var totalNilaiInput = document.getElementById('total_nilai');
      var hasil1 = parseFloat( document.getElementById('hasil1').value.replace(',', '.'));
      var hasil2a = parseFloat( document.getElementById('hasil2a').value.replace(',', '.'));
      var hasil2b = parseFloat( document.getElementById('hasil2b').value.replace(',', '.'));
      var hasil2c = parseFloat( document.getElementById('hasil2c').value.replace(',', '.'));
      var hasil3 = parseFloat( document.getElementById('hasil3').value.replace(',', '.'));

      totalNilaix = hasil1 + hasil2a + hasil2b + hasil2c + hasil3 ;
      if (!isNaN(totalNilaix)) {
        totalNilaiInput.value = totalNilaix.toFixed(2).replace('.', ',').replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
      }
    }
</script>
@endpush
