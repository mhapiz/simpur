@extends('layouts.backend')

@section('title')
SIMPUR | Nilai Ujian Skripsi
@endsection

@section('header')
{{--  --}}
@endsection

@section('content')
<div class="row" style="margin-top: -10rem">
    <div class="row">
        <div class="col-12">

            <div class="card">
                <div class="card-body">
                    <table style="width: 100%;">
                        <tr>
                            <td class="font-weight-bold text-center text-uppercase h4 underline pb-5" colspan="3">
                                <u>Nilai Ujian Skripsi</u></td>
                        </tr>
                    </table>

                    <dl class="row">
                        <dt class="mt-3 mt-md-2 mt-lg-0 col-4 col-md-2 col-lg-3 d-flex justify-content-between">
                            Nama / NIM Seminaris<span class="ml-1">:</span>
                        </dt>
                        <dd class="mt-3 mt-md-2 mt-lg-0 col-8 col-md-10 col-lg-9">
                            {{ $us->mahasiswa->nama_mahasiswa }} /
                            {{ $us->nim_skripsi }}
                        </dd>

                        <dt class="mt-3 mt-md-2 mt-lg-0 col-4 col-md-2 col-lg-3 d-flex justify-content-between">
                            Program Studi / Minat<span class="ml-1">:</span>
                        </dt>
                        <dd class="mt-3 mt-md-2 mt-lg-0 col-8 col-md-10 col-lg-9">
                            Kehutanan / {{ $us->mahasiswa->minat }}
                        </dd>

                        <dt class="mt-3 mt-md-2 mt-lg-0 col-4 col-md-2 col-lg-3 d-flex justify-content-between">
                            Hari / Tgl Skripsi<span class="ml-1">:</span>
                        </dt>
                        <dd class="mt-3 mt-md-2 mt-lg-0 col-8 col-md-10 col-lg-9">
                            @if ($us->us->tgl_skripsi == null)
                            <span class="text-danger">Tanggal Belum Ditentukan</span>
                            @else
                            {{ \Carbon\Carbon::parse($us->us->tgl_skripsi)->isoFormat('dddd / D MMMM Y') }}
                            @endif
                        </dd>

                        <dt class="mt-3 mt-md-2 mt-lg-0 col-4 col-md-2 col-lg-3 d-flex justify-content-between">
                            Judul Seminar<span class="ml-1">:</span>
                        </dt>
                        <dd class="mt-3 mt-md-2 mt-lg-0 col-8 col-md-10 col-lg-9">
                            {{ $us->us->judul }}
                        </dd>
                    </dl>

                    <table class="" style="width: 100%;" id="tabelBeborder">
                        <thead style="text-align: center;">
                            <tr style="width: 100%;">
                                <td style="width: 5%;" class="font-weight-bold">NO</td>
                                <td style="width: auto;" class="font-weight-bold">Unsur Penilaian</td>
                                <td style="width: auto;" class="font-weight-bold">Acuan Penilaian</td>
                                <td style="width: 8%;" class="font-weight-bold">NILAI <br> (>70-100)</td>
                                <td style="width: 7%;" class="font-weight-bold">BOBOT</td>
                                <td style="width: 8%;" class="font-weight-bold">BOBOT X <br> NILAI</td>
                            </tr>
                        </thead>

                        <tbody>
                            <tr>
                                <td class="text-center">1. </td>
                                <td>Wawasan di bidang keilmuan</td>
                                <td>Pemahaman umum dibidang kehutanan dan keterkaitan bidang penelitian dengan kehutanan
                                </td>
                                <td>
                                    <input disabled type="text" placeholder="..." id="nilai1" name="nilai1"
                                        onkeyup="hitung(this.value.replace(',', '.'), 0.20, 'hasil1')"
                                        value="{{ $us->nilai1 }}">
                                </td>
                                <td class="text-center">0,20</td>
                                <td>
                                    <input disabled type="text" placeholder="..." id="hasil1" name="hasil1"
                                        value="{{ $us->hasil1 }}">
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center" rowspan="4">2. </td>
                                <td colspan="5">Isi dan penguasan terhadap skripsi</td>
                            <tr>
                                <td>a. Kajian Teoritis</td>
                                <td>Adanya relevansi dengan topik yang diteliti dan kemutakhiran dengan pustaka yang
                                    diacu</td>
                                <td>
                                    <input disabled type="text" placeholder="..." id="nilai2a" name="nilai2a"
                                        onkeyup="hitung(this.value.replace(',', '.'), 0.20, 'hasil2a')"
                                        value="{{ $us->nilai2a }}">
                                </td>
                                <td class="text-center">0,20</td>
                                <td>
                                    <input disabled type="text" placeholder="..." id="hasil2a" name="hasil2a"
                                        value="{{ $us->hasil2a }}">
                                </td>
                            </tr>
                            <tr>
                                <td>b. Metode Penelitian</td>
                                <td>Kesesuaian dengan masalah; Ketepatan rancangan; Ketepatan instrument; Ketepatan dan
                                    ketepatan analisis</td>
                                <td>
                                    <input disabled type="text" placeholder="..." id="nilai2b" name="nilai2b"
                                        onkeyup="hitung(this.value.replace(',', '.'), 0.20, 'hasil2b')"
                                        value="{{ $us->nilai2b }}">
                                </td>
                                <td class="text-center">0,20</td>
                                <td>
                                    <input disabled type="text" placeholder="..." id="hasil2b" name="hasil2b"
                                        value="{{ $us->hasil2b }}">
                                </td>
                            </tr>
                            <tr>
                                <td>c. Hasil Penelitian</td>
                                <td>Manfaat dan kontribusi bagi pengembangan ilmu; Kesesuaian dengan tujuan penelitian
                                    dan kedalaman pembahasan; dan Keaslian tulisan</td>
                                <td>
                                    <input disabled type="text" placeholder="..." id="nilai2c" name="nilai2c"
                                        onkeyup="hitung(this.value.replace(',', '.'), 0.30, 'hasil2c')"
                                        value="{{ $us->nilai2c }}">
                                </td>
                                <td class="text-center">0,30</td>
                                <td>
                                    <input disabled type="text" placeholder="..." id="hasil2c" name="hasil2c"
                                        value="{{ $us->hasil2c }}">
                                </td>
                            </tr>
                            </tr>
                            <tr>
                                <td class="text-center">3. </td>
                                <td>Lain - lain</td>
                                <td>Kesesuaian pemakaian EYD; Kesesuaian format dengan pedoman penulisan;
                                    Ringkasan/abstrak dan penampilan</td>
                                <td>
                                    <input disabled type="text" placeholder="..." id="nilai3" name="nilai3"
                                        onkeyup="hitung(this.value.replace(',', '.'), 0.10, 'hasil3')"
                                        value="{{ $us->nilai3 }}">
                                </td>
                                <td class="text-center">0,10</td>
                                <td>
                                    <input disabled type="text" placeholder="..." id="hasil3" name="hasil3" value="
                                        {{ $us->hasil3 }}">
                                </td>
                            </tr>
                            <tr class="">
                                <td colspan="5" class="text-center font-weight-bold py-4">Total Nilai</td>
                                <td class="text-center">
                                    <input disabled type="text" placeholder="..." id="total_nilai" name="total_nilai"
                                        value="{{ $us->total_nilai }}">
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <table style="width: 100%;">
                        <td>
                            <tr>
                                <td style="width: 70%;"></td>
                                <td>Banjarbaru,
                                    @if ($us->us->tgl_skripsi == null)
                                    <span class="text-danger">Tanggal Belum Ditentukan</span>
                                    @else
                                    {{ \Carbon\Carbon::parse($us->us->tgl_skripsi)->isoFormat('D MMMM Y') }}
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;"></td>
                                <td>
                                    @if ($us->us->pembimbing1 == $us->dosen->nip)
                                    Dosen Pembimbing I,
                                    @elseif($us->us->pembimbing2 == $us->dosen->nip)
                                    Dosen Pembimbing II,
                                    @endif

                                </td>
                            </tr>
                            <tr height="5rem">
                                <td style="width: 70%;"></td>
                                <td> <br> <br> <br> <br> </td>
                            </tr>
                            <tr>
                                <td style="width: 70%;"></td>
                                <td>{{ $us->dosen->nama_dosen }}</td>
                            </tr>
                            <tr>
                                <td style="width: 70%;"></td>
                                <td>NIP. {{ $us->dosen->nip }}</td>
                            </tr>
                        </td>
                    </table>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection

@push('tambahStyle')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css"
    integrity="sha512-+4zCK9k+qNFUR5X+cKL9EIR+ZOhtIloNl9GIKS57V1MyNsYpYcUrUeQc9vNfzsWfV28IaLL3i96P9sdNyeRssA=="
    crossorigin="anonymous" />
<link rel="stylesheet" href="{{ url('adminarea/plugins/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ url('adminarea/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
<style>
    table#tabelBeborder td {
        border: 1px solid black;
    }

    table#tabelBeborder input {
        border-width: 0px;
        text-align: center;
        width: 100%;
    }

    td {
        padding-left: .3rem;
    }
</style>
@endpush

@push('tambahScript')
<script src="{{ url('adminarea/plugins/select2/js/select2.full.min.js') }}"></script>
<script>
    $('#selectNIM, #selectPembimbing1, #selectPembimbing2').select2({
        placeholder: "Select a state",
        theme: 'bootstrap4',
    // tags: true,
    // tokenSeparators: [',', ' ']
  });

    function hitung(a, b, c){
      a = parseFloat(a);
      var inputHasil = document.getElementById(c);
      var bobotxnilai = a * b;
      inputHasil.value = bobotxnilai.toFixed(2).replace('.', ',').replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
      // --
      var totalNilaiInput = document.getElementById('total_nilai');
      var hasil1a1 = parseFloat( document.getElementById('hasil1a1').value.replace(',', '.'));
      var hasil1b2 = parseFloat( document.getElementById('hasil1b2').value.replace(',', '.'));
      var hasil1b3 = parseFloat( document.getElementById('hasil1b3').value.replace(',', '.'));
      var hasil1b4 = parseFloat( document.getElementById('hasil1b4').value.replace(',', '.'));
      var hasil2c = parseFloat( document.getElementById('hasil2c').value.replace(',', '.'));
      var hasil2d = parseFloat( document.getElementById('hasil2d').value.replace(',', '.'));
      var hasil2e = parseFloat( document.getElementById('hasil2e').value.replace(',', '.'));
      var hasil3 = parseFloat( document.getElementById('hasil3').value.replace(',', '.'));
      var hasil4 = parseFloat( document.getElementById('hasil4').value.replace(',', '.'));

      totalNilaix = hasil1a1 + hasil1b2 + hasil1b3 + hasil1b4 + hasil2c + hasil2d + hasil2e + hasil3 + hasil4;
      if (!isNaN(totalNilaix)) {
        totalNilaiInput.value = totalNilaix.toFixed(2).replace('.', ',').replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
      }
    }
</script>
@endpush
