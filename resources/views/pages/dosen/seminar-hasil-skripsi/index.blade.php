@extends('layouts.backend')

@section('title')
SIMPUR | Seminar Hasil Penelitian
@endsection

@section('header')
<div class="header text-center">
    <h2 class="title">Seminar Hasil Penelitian</h2>
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12 col-md-6">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb bg-white rounded-pill shadow">
                        <li class="breadcrumb-item"><a href="{{ route('dosen.index') }}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Seminar Hasil Penelitian</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
@endsection

@php
$dosenlog =
\App\Models\Dosen::where('id_dosen','=',Auth::user()->dosen_id)->first();

@endphp

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card rounded-pill">
            <div
                class="card-body px-0 pl-md-4 pr-md-4 pb-3 d-flex flex-column flex-lg-row align-items-center justify-content-between">

                <div class="d-flex">

                    <a href="{{ route('dosen.seminar-hasil.index') }}"
                        class="btn {{ (request()->is('dosen/seminar/hasil-penelitian')) ? 'btn-success' : 'btn-outline-success' }} btn-sm rounded-pill d-flex align-items-center">
                        Semua
                    </a>

                    <a href="{{ route('dosen.seminar-hasil.indexBelumDinilai') }}"
                        class="btn {{ (request()->is('dosen/seminar/hasil-penelitian/belum-dinilai')) ? 'btn-success' : 'btn-outline-success' }} btn-sm rounded-pill d-flex align-items-center">
                        Belum Dinilai
                    </a>
                </div>

                <div class="d-flex">
                    <form class="d-flex align-items-center" action="" method="GET">
                        <input class="form-control" type="search" placeholder="Cari Nama " aria-label="Search" name="q"
                            style="height: 2rem">
                        <button
                            class="btn btn-primary rounded-pill my-2 my-sm-0 d-flex justify-content-between align-items-center"
                            type="submit" style="height: 2rem">
                            <i class="fas fa-search"></i>
                            <span class="ml-2 d-none d-md-block">Cari</span>
                        </button>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
<div class="row">



    @foreach ($dataSHP as $shp)
    <div class="col-12 col-lg-6">
        <div class="card rounded-lg">
            <div class="card-header d-flex justify-content-between align-items-center">
                <h5 class="card-category">Seminar Hasil Skripsi</h5>

                <div class="d-flex">

                    <a href="{{ route('dosen.seminar-hasil.penilaian.show', $shp->id_seminar_hasil_penelitian) }}"
                        class="btn btn-round btn-outline-default btn-simple btn-icon no-caret d-flex justify-content-center align-items-center mx-2"
                        data-toggle="tooltip" data-placement="right" title="Lihat Nilai">
                        <span class="material-icons">
                            visibility
                        </span>
                    </a>

                    <div class="dropdown">
                        <button type="button"
                            class="btn btn-round btn-outline-default dropdown-toggle btn-simple btn-icon no-caret d-flex justify-content-center align-items-center"
                            data-toggle="dropdown">
                            <span class="material-icons">
                                list
                            </span>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right">

                            <a class="dropdown-item"
                                href="{{ route('dosen.seminar-hasil.penilaian.create', $shp->id_seminar_hasil_penelitian) }}">
                                Tambah
                                Nilai</a>
                            <a class="dropdown-item"
                                href="{{ route('dosen.penyanggah-1-seminar-hasil.penilaian.create', $shp->id_seminar_hasil_penelitian) }}">Tambah
                                Nilai Penyanggah 1</a>
                            <a class="dropdown-item"
                                href="{{ route('dosen.penyanggah-2-seminar-hasil.penilaian.create', $shp->id_seminar_hasil_penelitian) }}">Tambah
                                Nilai Penyanggah 2</a>
                        </div>
                    </div>
                </div>

            </div>
            <div class="card-body">

                <dl class="row">
                    <dt class="mt-3 mt-md-2 mt-lg-0 col-4 col-md-5 d-flex justify-content-between">
                        Nama / NIM Seminaris<span class="ml-1">:</span>
                    </dt>
                    <dd class="mt-3 mt-md-2 mt-lg-0 col-8 col-md-7">
                        {{ $shp->mahasiswa->nama_mahasiswa }} / {{ $shp->nim_seminaris }}
                    </dd>

                    <dt class="mt-3 mt-md-2 mt-lg-0 col-4 col-md-5 d-flex justify-content-between">
                        Program Studi / Minat<span class="ml-1">:</span>
                    </dt>
                    <dd class="mt-3 mt-md-2 mt-lg-0 col-8 col-md-7">
                        Kehutanan / {{ $shp->mahasiswa->minat }}
                    </dd>

                    <dt class="mt-3 mt-md-2 mt-lg-0 col-4 col-md-5 d-flex justify-content-between">
                        Hari / Tgl Seminar<span class="ml-1">:</span>
                    </dt>
                    <dd class="mt-3 mt-md-2 mt-lg-0 col-8 col-md-7">
                        @if ($shp->tgl_seminar == null)
                        <span class="text-danger">Tanggal Belum Ditentukan</span>
                        @else
                        {{ \Carbon\Carbon::parse($shp->tgl_seminar)->isoFormat('dddd / D MMMM Y') }}
                        @endif
                    </dd>

                    <dt class="mt-3 mt-md-2 mt-lg-0 col-4 col-md-5 d-flex justify-content-between">
                        Judul Seminar<span class="ml-1">:</span>
                    </dt>
                    <dd class="mt-3 mt-md-2 mt-lg-0 col-8 col-md-7">
                        {{ \Illuminate\Support\Str::limit($shp->judul, 350, $end='...') }}
                    </dd>
                </dl>

            </div>
            <div class="card-footer">
                <hr>
                <div class="row">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-12 text-center">
                                <h6>Pembimbing</h6>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="d-flex justify-content-between align-items-center" style="">
                                    <div class="text-truncate" style="">
                                        <small
                                            class="text-muted text-sm">{{ App\Models\Dosen::where('nip','=' ,$shp->pembimbing1)->first()->nama_dosen }}</small>
                                    </div>

                                    <div class="d-flex align-items-center justify-content-center">
                                        Nilai :
                                        @if ($shp->nilai_pembimbing1 == null)
                                        <span class="ml-2 material-icons text-danger">
                                            highlight_off
                                        </span>
                                        @else
                                        <span class="ml-2 material-icons text-success">
                                            check_circle_outline
                                        </span>
                                        @endif
                                    </div>

                                </div>

                                <div class="d-flex justify-content-between align-items-center">
                                    <div class="text-truncate" style="">
                                        <small
                                            class="text-muted text-sm">{{ App\Models\Dosen::where('nip','=' ,$shp->pembimbing2)->first()->nama_dosen }}</small>
                                    </div>
                                    <div class="d-flex align-items-center justify-content-center">
                                        Nilai :
                                        @if ($shp->nilai_pembimbing2 == null)
                                        <span class="ml-2 material-icons text-danger">
                                            highlight_off
                                        </span>
                                        @else
                                        <span class="ml-2 material-icons text-success">
                                            check_circle_outline
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="row">
                            <div class="col-12 text-center">
                                <h6>Penyanggah</h6>
                            </div>
                            <div class="col-12">
                                <div class="d-flex justify-content-between align-items-center" style="width: 100%">
                                    <div>
                                        @if ($shp->penyanggah1 == null)
                                        <small class="text-muted text-sm text-danger">
                                            Belum Ditentukan
                                        </small>
                                        @else
                                        <small class="text-muted text-sm">
                                            {{ App\Models\Mahasiswa::where('nim','=' ,$shp->penyanggah1)->first()->nama_mahasiswa }}
                                        </small>
                                        @endif
                                    </div>
                                    <div class="d-flex align-items-center">
                                        Nilai :

                                        @if ($shp->penyanggah1 != null)
                                        @php

                                        $ppshp =
                                        \App\Models\PenilaianPenyanggahSeminarHasilPenelitian::where([['seminar_hasil_penelitian_id','=',$shp->id_seminar_hasil_penelitian],['penilai','=',$dosenlog->nip],['nim_penyanggah','=',$shp->penyanggah1]])->first();

                                        @endphp

                                        @if($ppshp->total_nilai != null)
                                        <span class="ml-2 material-icons text-success">
                                            check_circle_outline
                                        </span>
                                        @else
                                        <span class="ml-2 material-icons text-danger">
                                            highlight_off
                                        </span>
                                        @endif

                                        @else

                                        <span class="ml-2 material-icons text-danger">
                                            highlight_off
                                        </span>

                                        @endif

                                    </div>
                                </div>

                                <div class="d-flex justify-content-between align-items-center">
                                    <div>
                                        @if ($shp->penyanggah2 == null)
                                        <small class="text-muted text-sm text-danger">
                                            Belum Ditentukan
                                        </small>
                                        @else
                                        <small class="text-muted text-sm">
                                            {{ App\Models\Mahasiswa::where('nim','=' ,$shp->penyanggah2)->first()->nama_mahasiswa }}
                                        </small>
                                        @endif

                                    </div>
                                    <div class="d-flex align-items-center">
                                        Nilai :

                                        @if ($shp->penyanggah2 != null)
                                        @php

                                        $ppshp =
                                        \App\Models\PenilaianPenyanggahSeminarHasilPenelitian::where([['seminar_hasil_penelitian_id','=',$shp->id_seminar_hasil_penelitian],['penilai','=',$dosenlog->nip],['nim_penyanggah','=',$shp->penyanggah2]])->first();

                                        @endphp

                                        @if($ppshp->total_nilai != null)
                                        <span class="ml-2 material-icons text-success">
                                            check_circle_outline
                                        </span>
                                        @else
                                        <span class="ml-2 material-icons text-danger">
                                            highlight_off
                                        </span>
                                        @endif

                                        @else

                                        <span class="ml-2 material-icons text-danger">
                                            highlight_off
                                        </span>

                                        @endif

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    @endforeach

</div>
@endsection

@push('tambahStyle')
{{-- @livewireStyles --}}
@endpush
@push('tambahScript')
{{-- @livewireScripts --}}
@endpush
