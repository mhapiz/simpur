@extends('layouts.backend')

@section('title')
SIMPUR | Ujian Komprehensif
@endsection

@section('header')
<div class="header text-center">
    <h2 class="title">Ujian Komprehensif</h2>
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12 col-md-6">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb bg-white rounded-pill shadow">
                        <li class="breadcrumb-item"><a href="{{ route('dosen.index') }}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Ujian Komprehensif</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card rounded-pill">
            <div
                class="card-body px-0 pl-md-4 pr-md-4 pb-3 d-flex flex-column flex-lg-row align-items-center justify-content-between">

                <div class="d-flex">

                    <a href="{{ route('dosen.ujian-kompre.index') }}"
                        class="btn {{ (request()->is('dosen/ujian/ujian-komprehensif')) ? 'btn-success' : 'btn-outline-success' }} btn-sm rounded-pill d-flex align-items-center">
                        Semua
                    </a>
                    <a href="{{ route('dosen.ujian-kompre.indexBelumDinilai') }}"
                        class="btn {{ (request()->is('dosen/ujian/ujian-komprehensif/belum-dinilai')) ? 'btn-success' : 'btn-outline-success' }} btn-sm rounded-pill d-flex align-items-center">
                        Belum Dinilai
                    </a>

                </div>

                <div class="d-flex">
                    <form class="d-flex align-items-center" action="" method="GET">
                        <input class="form-control" type="search" placeholder="Cari Nama " aria-label="Search" name="q"
                            style="height: 2rem">
                        <button
                            class="btn btn-primary rounded-pill my-2 my-sm-0 d-flex justify-content-between align-items-center"
                            type="submit" style="height: 2rem">
                            <i class="fas fa-search"></i>
                            <span class="ml-2 d-none d-md-block">Cari</span>
                        </button>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
<div class="row">



    @foreach ($dataUjianKompre as $uk)
    <div class="col-12 col-lg-6">
        <div class="card rounded-lg">
            <div class="card-header d-flex justify-content-between align-items-center">
                <h5 class="card-category">Ujian Komprehensif</h5>

                <div class="d-flex">
                    <a href="{{ route('dosen.ujian-kompre.penilaian.show', $uk->id_ujian_komprehensif) }}"
                        class="btn btn-round btn-outline-default btn-simple btn-icon no-caret d-flex justify-content-center align-items-center"
                        data-toggle="tooltip" data-placement="right" title="Lihat Nilai">
                        <span class="material-icons">
                            visibility
                        </span>
                    </a>

                    <a href="{{ route('dosen.ujian-kompre.penilaian.create', $uk->id_ujian_komprehensif) }}"
                        class="btn btn-round btn-outline-default btn-simple btn-icon no-caret d-flex justify-content-center align-items-center mx-2">
                        <span class="material-icons">
                            rate_review
                        </span>
                    </a>

                    <div class="dropdown">
                        <button type="button"
                            class="btn btn-round btn-outline-default dropdown-toggle btn-simple btn-icon no-caret d-flex justify-content-center align-items-center"
                            data-toggle="dropdown">
                            <span class="material-icons">
                                print
                            </span>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right">

                            <button type="button" class="dropdown-item" data-toggle="modal" data-target="#modalSmstr"
                                data-target-action="{{ route('dosen.ujian-kompre.printBeritaAcara', $uk->id_ujian_komprehensif) }}"
                                data-toggle="tooltip" data-placement="right"
                                title="Cetak Berita Acara Ujian Komprehensif">
                                Cetak Berita Acara
                            </button>

                        </div>
                    </div>

                </div>

            </div>
            <div class="card-body">

                <dl class="row">
                    <dt class="mt-3 mt-md-2 mt-lg-0 col-4 col-md-5 d-flex justify-content-between">
                        Nama / NIM <span class="ml-1">:</span>
                    </dt>
                    <dd class="mt-3 mt-md-2 mt-lg-0 col-8 col-md-7">
                        {{ $uk->mahasiswa->nama_mahasiswa }} / {{ $uk->nim_skripsi }}
                    </dd>

                    <dt class="mt-3 mt-md-2 mt-lg-0 col-4 col-md-5 d-flex justify-content-between">
                        Program Studi / Minat<span class="ml-1">:</span>
                    </dt>
                    <dd class="mt-3 mt-md-2 mt-lg-0 col-8 col-md-7">
                        Kehutanan / {{ $uk->mahasiswa->minat }}
                    </dd>

                    <dt class="mt-3 mt-md-2 mt-lg-0 col-4 col-md-5 d-flex justify-content-between">
                        Hari / Tgl Skripsi<span class="ml-1">:</span>
                    </dt>
                    <dd class="mt-3 mt-md-2 mt-lg-0 col-8 col-md-7 d-flex">
                        @if ($uk->tgl_skripsi == null)
                        <span class="text-danger">Tanggal Belum Ditentukan</span>
                        @else
                        {{ \Carbon\Carbon::parse($uk->tgl_skripsi)->isoFormat('dddd / D MMMM Y') }}
                        Jam
                        {{ date('h:i', strtotime($uk->tgl_skripsi)) }}
                        @endif
                    </dd>

                </dl>

            </div>

            <div class="card-footer">
                <hr>
                <div class="row">
                    <div class="col-12 text-center mb-2">
                        <h6>Penguji</h6>
                    </div>

                    <div class="col-12">
                        <div class="d-flex justify-content-between align-items-center" style="">
                            <div class="text-truncate" style="">
                                <small class="text-muted text-sm">
                                    1.
                                    {{ App\Models\Dosen::where('nip','=' ,$uk->penguji1)->first()->nama_dosen }}
                                </small>
                            </div>
                            <div class="d-flex align-items-center">
                                Nilai :
                                @if ($uk->nilai_penguji1 == null)
                                <span class="ml-2 material-icons text-danger">
                                    highlight_off
                                </span>
                                @else
                                <span class="ml-2 material-icons text-success">
                                    check_circle_outline
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="d-flex justify-content-between align-items-center">
                            <div class="text-truncate" style="">
                                <small class="text-muted text-sm">
                                    2.
                                    {{ App\Models\Dosen::where('nip','=' ,$uk->penguji2)->first()->nama_dosen }}
                                </small>
                            </div>
                            <div class="d-flex align-items-center">
                                Nilai :
                                @if ($uk->nilai_penguji2 == null)
                                <span class="ml-2 material-icons text-danger">
                                    highlight_off
                                </span>
                                @else
                                <span class="ml-2 material-icons text-success">
                                    check_circle_outline
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="d-flex justify-content-between align-items-center" style="">
                            <div class="text-truncate" style="">
                                @if ($uk->penguji3 != null)
                                <small class="text-muted text-sm">
                                    3. {{ App\Models\Dosen::where('nip','=' ,$uk->penguji3)->first()->nama_dosen }}
                                </small>
                                @else
                                <small class="text-danger text-sm">
                                    3. Belum Ditentukan
                                </small>
                                @endif
                            </div>
                            <div class="d-flex align-items-center">
                                Nilai :
                                @if ($uk->nilai_penguji3 == null)
                                <span class="ml-2 material-icons text-danger">
                                    highlight_off
                                </span>
                                @else
                                <span class="ml-2 material-icons text-success">
                                    check_circle_outline
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="d-flex justify-content-between align-items-center">
                            <div class="text-truncate" style="">
                                @if ($uk->penguji3 != null)
                                <small class="text-muted text-sm">
                                    4. {{ App\Models\Dosen::where('nip','=' ,$uk->penguji4)->first()->nama_dosen }}
                                </small>
                                @else
                                <small class="text-danger text-sm">
                                    4. Belum Ditentukan
                                </small>
                                @endif
                            </div>
                            <div class="d-flex align-items-center">
                                Nilai :
                                @if ($uk->nilai_penguji4 == null)
                                <span class="ml-2 material-icons text-danger">
                                    highlight_off
                                </span>
                                @else
                                <span class="ml-2 material-icons text-success">
                                    check_circle_outline
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    @endforeach

</div>

<div class="modal fade" id="modalSmstr" tabindex="-1" aria-labelledby="modalSmstrLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalSmstrLabel">Tentukan Semester</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="formCetak" method="POST">
                    @csrf
                    <label class="">Semester</label>

                    <div class="form-group">
                        <input type="text" name="semester" class="form-control" placeholder="Ganjil 1999/2000">
                        <small class="form-text text-alert ml-3">*Contoh : Ganjil 1999/2000</small>
                        @error('semester')
                        <span class="invalid-feedback ml-1" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <button type="submit" form="formCetak" class="btn btn-primary">Cetak</button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('tambahStyle')
{{-- @livewireStyles --}}
@endpush
@push('tambahScript')

<script>
    $(document).ready(function(){
        $("#modalSmstr").on("show.bs.modal", function(e) {
            var action = $(e.relatedTarget).data('target-action');
            $('#formCetak').attr('action', action);
            console.log(action);
        });
    });
</script>
@endpush
