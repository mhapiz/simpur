@extends('layouts.backend')

@section('title')
SIMPUR | Seminar Usulan Skripsi
@endsection

@section('header')
<div class="header text-center d-none">
    <h2 class=" title">Seminar Usulan Skripsi</h2>
    <p class="category">Handcrafted by our friend <a target="_blank" href="https://github.com/mouse0270">Robert
            McIntosh</a>. Please checkout the <a href="http://bootstrap-notify.remabledesigns.com/" target="_blank">full
            documentation.</a></p>
</div>
@endsection

@section('content')
<div class="row" style="margin-top: -10rem">
    <div class="col-12">
        <div class="card card-body">
            <form class="form-horizontal" action="{{ route('dosen.seminar-usulan.store') }}" method="POST">
                @csrf
                <label class="">Mahasiswa</label>

                <div class="">
                    <div class="form-group">
                        <select id="selectNIM" name="nim_seminaris" class="@error('nim_seminaris') is-invalid @enderror"
                            style="width: 100%;">
                            <option>Cari Mahasiswa</option>
                            @foreach ($mhs as $m)
                            <option value="{{ $m->nim }}">{{ $m->nim }} -- {{ $m->nama_mahasiswa }}</option>
                            @endforeach
                        </select>
                        @error('nim_seminaris')
                        <span class="invalid-feedback ml-1" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-md-6">
                        <label class="">Pembimbing 1</label>

                        <div class="form-group">
                            <select id="selectPembimbing1" name="pembimbing1"
                                class="@error('pembimbing1') is-invalid @enderror" style="width: 100%;">
                                <option>Pilih Pembimbing</option>
                                @foreach ($dosen as $d)
                                <option value="{{ $d->nip }}"
                                    {{ $d->nip == App\Models\Dosen::find(Auth::user()->dosen_id)->nip ? 'selected' : '' }}>
                                    {{ $d->nip }}
                                    -- {{ $d->nama_dosen }}</option>
                                @endforeach
                            </select>
                            @error('nim_seminaris')
                            <span class="invalid-feedback ml-1" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <label class="">Pembimbing 2</label>

                        <div class="form-group">
                            <select id="selectPembimbing2" name="pembimbing2"
                                class="@error('pembimbing2') is-invalid @enderror" style="width: 100%;">
                                <option>Pilih Pembimbing</option>
                                @foreach ($dosen as $d)
                                <option value="{{ $d->nip }}">{{ $d->nip }} -- {{ $d->nama_dosen }}</option>
                                @endforeach
                            </select>
                            @error('nim_seminaris')
                            <span class="invalid-feedback ml-1" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                </div>

                <label class="">Tanggal Seminar</label>

                <div class="">
                    <div class="form-group">
                        <input type="text" name="tgl_seminar" class="@error('tgl_seminar') is-invalid @enderror"
                            id="tgl_seminar" autocomplete="off">
                        @error('tgl_seminar')
                        <span class="invalid-feedback ml-1" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

                <label class="">Judul</label>

                <div class="">
                    <div class="form-group">
                        {{-- <input type="text" name="judul"
                            class="form-control @error('judul') is-invalid @enderror rounded-lg"> --}}
                        <textarea name="judul" style="width: 100%" rows="5"></textarea>
                        @error('judul')
                        <span class="invalid-feedback ml-1" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 text-right">
                        <button type="submit" class="btn btn-success">Tambah</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
@endsection

@push('tambahStyle')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css"
    integrity="sha512-+4zCK9k+qNFUR5X+cKL9EIR+ZOhtIloNl9GIKS57V1MyNsYpYcUrUeQc9vNfzsWfV28IaLL3i96P9sdNyeRssA=="
    crossorigin="anonymous" />
<link rel="stylesheet" href="{{ url('adminarea/plugins/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ url('adminarea/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ url('adminarea/plugins/gijgo/css/gijgo.min.css') }}">

@endpush

@push('tambahScript')
<script src="{{ url('adminarea/plugins/select2/js/select2.full.min.js') }}"></script>
<script src="{{ url('adminarea/plugins/gijgo/js/gijgo.min.js') }}"></script>
<script src="{{ url('adminarea/plugins/gijgo/js/messages/messages.id-id.js') }}"></script>
<script>
    $('#selectNIM, #selectPembimbing1, #selectPembimbing2').select2({
        placeholder: "",
        theme: 'bootstrap4',
    // tags: true,
    // tokenSeparators: [',', ' ']
  });

    $('#tgl_seminar').datepicker({
      uiLibrary: 'materialdesign',
      format: "yyyy-mm-dd",
      locale: 'id-id',
    });
</script>
@endpush
