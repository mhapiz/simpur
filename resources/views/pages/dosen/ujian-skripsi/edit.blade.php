@extends('layouts.backend')

@section('title')
SIMPUR | Ujian Skripsi
@endsection

@section('header')
<div class="header text-center d-none">
    <h2 class=" title">Edit Ujian Skripsi</h2>
    <p class="category">Handcrafted by our friend <a target="_blank" href="https://github.com/mouse0270">Robert
            McIntosh</a>. Please checkout the <a href="http://bootstrap-notify.remabledesigns.com/" target="_blank">full
            documentation.</a></p>
</div>
@endsection

@section('content')
<div class="row" style="margin-top: -10rem">
    <div class="col-12">
        <div class="card card-body">
            <table style="width: 100%;">
                <tr>
                    <td class="font-weight-bold text-center text-uppercase h4 underline pb-5" colspan="3">
                        <u>Nilai Ujian Skripsi</u></td>
                </tr>
            </table>
            <div class="row">
                <div class="col-sm-4 d-flex justify-content-start justify-content-md-between">
                    Nama / NIM Seminaris <span class="ml-2">:</span>
                </div>
                <div class="col">
                    {{ $us->mahasiswa->nama_mahasiswa }} /
                    {{ $us->nim_skripsi }}
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4 d-flex justify-content-start justify-content-md-between">
                    Program Studi / Minat<span class="ml-2">:</span>
                </div>
                <div class="col">
                    Kehutanan /
                    {{ $us->mahasiswa->minat }}
                </div>
            </div>

            <div class="row  py-2">
                <div class="col-sm-4 d-flex justify-content-start justify-content-md-between">
                    Hari / Tgl Seminar<span class="ml-2">:</span>
                </div>
                <div class="col">
                    @if ($us->tgl_skripsi == null)
                    <form action="{{ route('dosen.ujian-skripsi.update', $us->id_ujian_skripsi) }}" method="POST"
                        id="addTglSkripsi">
                        @method('PUT')
                        @csrf
                        <div class="form-group" style="width: 20%">
                            <input type="text" name="tgl_skripsi" class="@error('tgl_skripsi') is-invalid @enderror"
                                id="tgl_skripsi" autocomplete="off">
                            @error('tgl_skripsi')
                            <span class="invalid-feedback ml-1" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </form>
                    @else
                    {{ \Carbon\Carbon::parse($us->tgl_skripsi)->isoFormat('dddd / D MMMM Y') }}
                    @endif
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4 d-flex justify-content-start justify-content-md-between">
                    Judul Seminar<span class="ml-2">:</span>
                </div>
                <div class="col">
                    {{ $us->judul }}
                </div>
            </div>

            <div class="row">
                <div class="col-12 text-right">
                    <button type="submit" form="addTglSkripsi" class="btn btn-success">Simpan </button>

                </div>
            </div>

        </div>
    </div>
</div>
</div>
@endsection

@push('tambahStyle')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css"
    integrity="sha512-+4zCK9k+qNFUR5X+cKL9EIR+ZOhtIloNl9GIKS57V1MyNsYpYcUrUeQc9vNfzsWfV28IaLL3i96P9sdNyeRssA=="
    crossorigin="anonymous" />
<link rel="stylesheet" href="{{ url('adminarea/plugins/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ url('adminarea/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ url('adminarea/plugins/gijgo/css/gijgo.min.css') }}">

@endpush

@push('tambahScript')
<script src="{{ url('adminarea/plugins/select2/js/select2.full.min.js') }}"></script>
<script src="{{ url('adminarea/plugins/gijgo/js/gijgo.min.js') }}"></script>
<script src="{{ url('adminarea/plugins/gijgo/js/messages/messages.id-id.js') }}"></script>
<script>
    $('#selectNIM, #selectPembimbing1, #selectPembimbing2').select2({
        placeholder: "Select a state",
        theme: 'bootstrap4',
    // tags: true,
    // tokenSeparators: [',', ' ']
  });

    $('#tgl_skripsi').datepicker({
      uiLibrary: 'materialdesign',
      format: "yyyy-mm-dd",
      locale: 'id-id',
    });
</script>
@endpush
