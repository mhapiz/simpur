@extends('layouts.backend')

@section('title')
SIMPUR | Profil
@endsection

@section('header')
<div class="header text-center">
    <h2 class="title">Profil</h2>
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12 col-md-6">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb bg-white rounded-pill shadow">
                        <li class="breadcrumb-item"><a href="{{ route('redirect') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ url()->previous() }}">Profil</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Edit Profil</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">
                <h5 class="title">Edit Profil</h5>
            </div>
            <div class="card-body">

                @if (Auth::user()->role == 'ADMIN' || Auth::user()->role == 'SUPERADMIN')
                <form action="{{ route('update.admin', Auth::user()->id_user) }}" method="POST">
                    @method('PUT')
                    @csrf
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Username</label>
                                <input type="text" name="username"
                                    class="form-control @error('username') is-invalid @enderror" placeholder="Username"
                                    value="{{ $user->identity }}">
                            </div>
                        </div>
                    </div>
                    @elseif (Auth::user()->role == 'DOSEN')
                    <form action="{{ route('update.dosen', Auth::user()->id_user) }}" method="POST">
                        @method('PUT')
                        @csrf
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>NIP</label>
                                    <input type="text" name="nip"
                                        class="form-control @error('nip') is-invalid @enderror"
                                        value="{{ $user->Dosen->nip }}" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Nama</label>
                                    <input type="text" name="nama"
                                        class="form-control @error('nama') is-invalid @enderror" placeholder="Nama"
                                        value="{{ $user->Dosen->nama_dosen }}">
                                    @error('nama')
                                    <div class="invalid-feedback ml-3">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Jabatan</label>
                                    <input type="text" name="jabatan"
                                        class="form-control @error('jabatan') is-invalid @enderror"
                                        placeholder="Jabatan" value="{{ $user->Dosen->jabatan }}">
                                    @error('jabatan')
                                    <div class="invalid-feedback ml-3">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        @endif
                        <p>
                            <a class="btn btn-sm btn-warning" data-toggle="collapse" href="#gantiPass" role="button"
                                aria-expanded="false" aria-controls="gantiPass">
                                Ganti Password
                            </a>
                        </p>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="collapse" id="gantiPass">
                                    <div class="form-group" id="newPass">
                                        <label for="password">Password Baru</label>
                                        <input type="password" name="password"
                                            class="form-control @error('password') is-invalid @enderror" id="password"
                                            placeholder="Masukkan Password">
                                        @error('jabatan')
                                        <div class="invalid-feedback ml-3">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                        {{--  --}}
                                        <div class="mt-2 ml-2">
                                            <input id="checkBox" type="checkbox" onclick="lihatPassword()">
                                            <label onclick="lihatPassword()" style="cursor: pointer">
                                                Lihat Password
                                            </label>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 text-right">
                                <button type="submit" class="btn btn-success">Simpan</button>
                            </div>
                        </div>
                    </form>
            </div>
        </div>
    </div>

</div>
@endsection


@push('tambahScript')
<script>
    function lihatPassword() {
        var x = document.getElementById("password");
        var c = document.getElementById("checkBox");
        if (x.type === "password") {
            c.checked = true;
            x.type = "text";
        } else {
            x.type = "password";
            c.checked = false;
        }
    }
</script>

@endpush
