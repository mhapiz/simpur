@extends('layouts.backend')

@section('title')
SIMPUR | Profil
@endsection

@section('header')
<div class="header text-center">
    <h2 class="title">Profil</h2>
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12 col-md-6">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb bg-white rounded-pill shadow">
                        <li class="breadcrumb-item"><a href="{{ route('redirect') }}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Profil</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header d-flex justify-content-between align-items-center">
                <div>
                    <h5 class="title">Edit Profil</h5>
                </div>

                <div>
                    <a href="{{ route('myProfile.edit') }}" class="btn btn-danger btn-sm d-flex align-items-center"
                        style="margin-top: -5px">
                        <span class="material-icons  mr-3">
                            edit
                        </span>
                        <span class="font-weight-bold">Edit Profile</span>
                    </a>
                </div>
            </div>
            <div class="card-body">

                @if (Auth::user()->role == 'ADMIN' || Auth::user()->role == 'SUPERADMIN')
                <form>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Username</label>
                                <input type="text" name="username"
                                    class="form-control @error('username') is-invalid @enderror" placeholder="Username"
                                    value="{{ $user->identity }}">
                            </div>
                        </div>
                    </div>
                    @elseif (Auth::user()->role == 'DOSEN')
                    <form>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>NIP</label>
                                    <input type="text" name="nip"
                                        class="form-control @error('nip') is-invalid @enderror"
                                        value="{{ $user->Dosen->nip }}" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Nama</label>
                                    <input type="text" name="nama"
                                        class="form-control @error('nama') is-invalid @enderror" placeholder="Nama"
                                        value="{{ $user->Dosen->nama_dosen }}">
                                    @error('nama')
                                    <div class="invalid-feedback ml-3">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Jabatan</label>
                                    <input type="text" name="jabatan"
                                        class="form-control @error('jabatan') is-invalid @enderror"
                                        placeholder="Jabatan" value="{{ $user->Dosen->jabatan }}">
                                    @error('jabatan')
                                    <div class="invalid-feedback ml-3">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        @endif
                    </form>
            </div>
        </div>
    </div>

</div>
@endsection

@push('tambahStyle')
<style>
    input {
        pointer-events: none
    }
</style>
@endpush


@push('tambahScript')
<script>
    function lihatPassword() {
        var x = document.getElementById("password");
        var c = document.getElementById("checkBox");
        if (x.type === "password") {
            c.checked = true;
            x.type = "text";
        } else {
            x.type = "password";
            c.checked = false;
        }
    }
</script>

@endpush
