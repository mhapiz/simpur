@extends('layouts.backend')

@section('title')
SIMPUR | 403
@endsection

@section('header')
{{--  --}}
@endsection

@section('content')
<div class="row" style="margin-top: -10rem">
    <div class="col-12">
        <div class="card rounded-lg">
            <div class="card-body">
                <div class="d-flex justify-content-center">
                    <img src="{{ url('adminarea/assets/img/forbidden.png') }}" class="text-center"
                        style=" height: 12rem;">
                </div>
                <p class="text-center font-weight-bold display-4">403</p>
                <p class="text-center display-4">Akses Ditolak</p>
            </div>
        </div>
    </div>
</div>
@endsection

@push('tambahStyle')

@endpush

@push('tambahScript')
@endpush
