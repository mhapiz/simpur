@extends('layouts.backend')

@section('title')
    SIMPUR | Log Aktivitas
@endsection

@section('header')
    <div class="header text-center">
        <h2 class="title">Log Aktivitas</h2>
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-12 col-md-6">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb bg-white rounded-pill shadow">
                            <li class="breadcrumb-item"><a href="{{ route('redirect') }}">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Log Aktivitas</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="row justify-content-center">

        <div class="col-12 col-md-6">
            <div class="card">
                <div class="card-header text-center font-weight-bold">
                    <h5 class="card-category">Data User</h5>
                </div>
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead class="thead-dark text-center">
                            <tr>
                                <th scope="col" style="width:10px">No.</th>
                                <th scope="col" style="width: auto">Aktivitas</th>
                                <th scope=" col" style="width: auto">Waktu</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($logUser as $luser)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $luser->updated_by != null ? $luser->updated_by : 'Anon' }} Memperbarui Data User
                                        {{ $luser->identity }}
                                    </td>
                                    <td>{{ $luser->updated_at->diffForHumans() != null ? $luser->updated_at->diffForHumans() : '' }}
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="3" class="text-center">Belum Ada Aktivitas</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-12 col-md-6">
            <div class="card">
                <div class="card-header text-center font-weight-bold">
                    <h5 class="card-category">Data Dosen</h5>
                </div>
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead class="thead-dark text-center">
                            <tr>
                                <th scope="col" style="width:10px">No.</th>
                                <th scope="col" style="width: auto">Aktivitas</th>
                                <th scope=" col" style="width: auto">Waktu</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($logDosen as $ldosen)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $ldosen->updated_by }} Memperbarui Data Dosen {{ $ldosen->nama_dosen }}
                                    </td>
                                    <td>{{ $ldosen->updated_at->diffForHumans() }}</td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="3" class="text-center">Belum Ada Aktivitas</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-12 col-md-6">
            <div class="card">
                <div class="card-header text-center font-weight-bold">
                    <h5 class="card-category">Data Seminar Usulan Penelitian</h5>
                </div>
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead class="thead-dark text-center">
                            <tr>
                                <th scope="col" style="width:10px">No.</th>
                                <th scope="col" style="width: auto">Aktivitas</th>
                                <th scope=" col" style="width: auto">Waktu</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($logsup as $lup)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $lup->updated_by }} Memperbarui Data SUP Dari
                                        {{ $lup->mahasiswa->nama_mahasiswa }}
                                    </td>
                                    <td>{{ $lup->updated_at->diffForHumans() }}</td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="3" class="text-center">Belum Ada Aktivitas</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
                <div class="card-footer">
                    <small>*SUP = Seminar Usulan Penelitian</small>
                </div>
            </div>
        </div>

        <div class=" col-12 col-md-6">
            <div class="card">
                <div class="card-header text-center font-weight-bold">
                    <h5 class="card-category">Data Seminar Hasil Penelitian</h5>
                </div>
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead class="thead-dark text-center">
                            <tr>
                                <th scope="col" style="width:10px">No.</th>
                                <th scope="col" style="width: auto">Aktivitas</th>
                                <th scope=" col" style="width: auto">Waktu</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($logshp as $lhp)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $lhp->updated_by }} Memperbarui Data SHP Dari
                                        {{ $lhp->mahasiswa->nama_mahasiswa }}
                                    </td>
                                    <td>{{ $lhp->updated_at->diffForHumans() }}</td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="3" class="text-center">Belum Ada Aktivitas</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
                <div class="card-footer">
                    <small>*SHP = Seminar Hasil Penelitian</small>
                </div>
            </div>
        </div>

        <div class="col-12 col-md-6">
            <div class="card">
                <div class="card-header text-center font-weight-bold">
                    <h5 class="card-category">Data Ujian Skripsi</h5>
                </div>
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead class="thead-dark text-center">
                            <tr>
                                <th scope="col" style="width:10px">No.</th>
                                <th scope="col" style="width: auto">Aktivitas</th>
                                <th scope=" col" style="width: auto">Waktu</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($logus as $lus)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $lus->updated_by }} Memperbarui Data Ujian Skripsi Dari
                                        {{ $lus->mahasiswa->nama_mahasiswa }}
                                    </td>
                                    <td>{{ $lus->updated_at->diffForHumans() }}</td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="3" class="text-center">Belum Ada Aktivitas</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-12 col-md-6">
            <div class="card">
                <div class="card-header text-center font-weight-bold">
                    <h5 class="card-category">Data Ujian Komprehensif</h5>
                </div>
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead class="thead-dark text-center">
                            <tr>
                                <th scope="col" style="width:10px">No.</th>
                                <th scope="col" style="width: auto">Aktivitas</th>
                                <th scope=" col" style="width: auto">Waktu</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($loguk as $luk)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $luk->updated_by }} Memperbarui Data Ujian Komprehensif Dari
                                        {{ $luk->mahasiswa->nama_mahasiswa }}
                                    </td>
                                    <td>{{ $luk->updated_at->diffForHumans() }}</td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="3" class="text-center">Belum Ada Aktivitas</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
@endsection

@push('tambahStyle')

@endpush


@push('tambahScript')

@endpush
