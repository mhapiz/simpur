@extends('layouts.backend')

@section('title')
SIMPUR | Tambah User
@endsection

@section('content')
<div class="row justify-content-center">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">
                <h5 class="title">Tambah Akun Dosen</h5>
            </div>
            <div class="card-body">
                <form action="{{ route('admin.user-manage.dosen.storeMany') }}" method="POST"
                    enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Pilih File Excel</label>
                                <div class="custom-file">
                                    <input type="file"
                                        class="custom-file-input @error('file_input') is-invalid @enderror"
                                        id="file-input" name="file_input">
                                    <label class="custom-file-label" for="file-input">Pilih File</label>
                                </div>
                                @error('file_input')
                                <div class="text-danger text-sm">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-end">
                        <div class="col-6 text-right">
                            <button type="submit" class="btn btn-success">Tambah</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@push('tambahScript')
<script>
    $('#file-input').on('change',function(){
        var fileName = $(this).val();
        $(this).next('.custom-file-label').html(fileName);
    })
</script>
@endpush
