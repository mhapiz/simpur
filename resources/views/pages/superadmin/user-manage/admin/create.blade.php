@extends('layouts.backend')

@section('title')
SIMPUR | Tambah Admin
@endsection

@section('header')
<div class="header text-center">
    <h2 class=" title">Tambah Admin </h2>
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12 col-md-6">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb bg-white rounded-pill shadow">
                        <li class="breadcrumb-item"><a href="{{ route('super-admin.index') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('super-admin.user-manage.admin.index') }}">Data
                                Admin</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Tambah Admin</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">
                <h5 class="title">Tambah Akun Admin</h5>
            </div>
            <div class="card-body">
                <form action="{{ route('super-admin.user-manage.admin.store') }}" method="POST">
                    @csrf
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Username</label>
                                <input type="text" name="username"
                                    class="form-control @error('username') is-invalid @enderror"
                                    placeholder="Masukkan Username" value="{{ old('username') }}">
                                @error('username')
                                <div class="invalid-feedback text-capitalize ml-3">
                                    *{{ $message }}
                                </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="password">Kata Sandi</label>
                                <input type="password" name="password"
                                    class="form-control @error('password') is-invalid @enderror" id="password1"
                                    placeholder="Masukkan Kata Sandi">
                                @error('password')
                                <div class="invalid-feedback text-capitalize ml-3">
                                    *{{ $message }}
                                </div>
                                @enderror
                                <div style="margin-left: 1rem; margin-top: 10px;">
                                    <input style="cursor: pointer" id="checkBox1" type="checkbox"
                                        onclick="lihatPassword1()">
                                    <label style="cursor: pointer" for="defaultCheck1" onclick="lihatPassword1()">
                                        Lihat Password
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="password">Konfirmasi Kata Sandi</label>
                                <input type="password" name="password2"
                                    class="form-control @error('password2') is-invalid @enderror" id="password2"
                                    placeholder="Konfirmasi Kata Sandi">
                                @error('password2')
                                <div class="invalid-feedback text-capitalize ml-3">
                                    *{{ $message }}
                                </div>
                                @enderror
                                <div style="margin-left: 1rem; margin-top: 10px;">
                                    <input style="cursor: pointer" id="checkBox2" type="checkbox"
                                        onclick="lihatPassword2()">
                                    <label style="cursor: pointer" for="defaultCheck1" onclick="lihatPassword2()">
                                        Lihat Password
                                    </label>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="row justify-content-end">
                        <div class="col-6 text-right">
                            <button type="submit" class="btn btn-success">Tambah</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@push('tambahScript')
<script>
    function lihatPassword1() {
        var x = document.getElementById("password1");
        var c = document.getElementById("checkBox1");
        if (x.type === "password") {
            c.checked = true;
            x.type = "text";
        } else {
            x.type = "password";
            c.checked = false;
        }
    }
    function lihatPassword2() {
        var x = document.getElementById("password2");
        var c = document.getElementById("checkBox2");
        if (x.type === "password") {
            c.checked = true;
            x.type = "text";
        } else {
            x.type = "password";
            c.checked = false;
        }
    }
</script>
@endpush
