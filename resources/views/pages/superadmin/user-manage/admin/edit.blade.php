@extends('layouts.backend')

@section('title')
SIMPUR | Perbarui Data Admin
@endsection

@section('header')
<div class="header text-center">
    <h2 class=" title">Perbarui Data Admin Fakultas Kehutanan</h2>
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12 col-md-6">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb bg-white rounded-pill shadow">
                        <li class="breadcrumb-item">
                            <a href="{{ route('super-admin.index') }}">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="{{ route('super-admin.user-manage.admin.index') }}">Data
                                Admin</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Perbarui Data Dosen</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">
                <h5 class="title">Perbarui Akun Admin</h5>
            </div>
            <div class="card-body">
                <form action="{{ route('super-admin.user-manage.admin.update', $user->id_user) }}" method="POST">
                    @method('PUT')
                    @csrf
                    <div class="row">
                        <div class="col-12 col-md-8">
                            <div class="form-group">
                                <label>Username</label>
                                <input type="text" name="identity"
                                    class="form-control @error('identity') is-invalid @enderror"
                                    placeholder="Masukkan Username" value="{{ $user->identity }}">
                                @error('identity')
                                <div class="invalid-feedback text-capitalize ml-3">
                                    *{{ $message }}
                                </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="form-group">
                                <label>Role</label>
                                <input type="text" name="identity"
                                    class="form-control @error('identity') is-invalid @enderror"
                                    placeholder="Masukkan Username" value="{{ $user->role }}" disabled>

                            </div>
                        </div>
                    </div>

                    <p>
                        <a class="btn btn-sm btn-warning" data-toggle="collapse" href="#gantiPass" role="button"
                            aria-expanded="false" aria-controls="gantiPass">
                            Ganti Password
                        </a>
                    </p>
                    <div class="collapse" id="gantiPass">
                        <div class="form-group" id="newPass">
                            <label for="password">Password Baru</label>
                            <input type="password" name="password"
                                class="form-control @error('password') is-invalid @enderror" id="password"
                                placeholder="Masukkan Password">
                            {{--  --}}
                            <div style="margin-left: 1rem; margin-top: 10px;">
                                <input style="cursor: pointer" id="checkBox" type="checkbox" onclick="lihatPassword()">
                                <label style="cursor: pointer" for="defaultCheck1" onclick="lihatPassword()">
                                    Lihat Password
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="row justify-content-end">
                        <div class="col-6 text-right">
                            <button type="submit" class="btn btn-success">Perbarui</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection


@push('tambahScript')
<script>
    function lihatPassword() {
        var x = document.getElementById("password");
        var c = document.getElementById("checkBox");
        if (x.type === "password") {
            c.checked = true;
            x.type = "text";
        } else {
            x.type = "password";
            c.checked = false;
        }
    }
</script>
@endpush
