@extends('layouts.backend')

@section('title')
SIMPUR | User Manage
@endsection

@section('header')
<div class="header text-center">
    <h2 class=" title">Data Dosen Fakultas Kehutanan</h2>
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12 col-md-6">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb bg-white rounded-pill shadow">
                        <li class="breadcrumb-item"><a href="{{ route('super-admin.index') }}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Data Dosen</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <div class="row">
                    <div class="col-12 d-flex justify-content-between">
                        <div></div>
                        <div class="d-flex ">
                            <a href="{{ route('super-admin.user-manage.dosen.create') }}"
                                class="btn btn-danger btn-sm d-flex align-items-center">
                                <span class="material-icons  mr-3">
                                    person_add
                                </span>
                                <span class="font-weight-bold">Tambah Data Dosen</span>
                            </a>
                            <a href="{{ route('super-admin.user-manage.dosen.addMany') }}"
                                class="btn btn-danger btn-sm d-flex align-items-center">
                                <span class="material-icons  mr-3">
                                    group_add
                                </span>
                                <span class="font-weight-bold">Tambah Banyak Data Dosen</span>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered text-center" id="table-dosen">
                                <thead class="text-Success thead-dark">
                                    <tr>
                                        <th class="font-weight-bold" width="10px">No.</th>
                                        <th class="font-weight-bold">NIP</th>
                                        <th class="font-weight-bold">Nama</th>
                                        <th class="font-weight-bold">Jabatan</th>
                                        <th class="font-weight-bold" width="10px">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection

@push('tambahStyle')
<link rel="stylesheet" href="{{ url('adminarea/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ url('adminarea/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
@endpush

@push('tambahScript')
<script src="{{ url('adminarea/plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ url('adminarea/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ url('adminarea/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ url('adminarea/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>

<script>
    function htmlDecode(data){
    var txt=document.createElement('textarea');
    txt.innerHTML=data;
    return txt.value
    }

    $(document).ready(function() {
    $('#table-dosen').DataTable({
      language:{
            "url":"//cdn.datatables.net/plug-ins/1.10.21/i18n/Indonesian.json",
            "sEmptyTable":"Tidak Ada Data"
      },
      processing:true,
      serverside:true,
      ajax:"{{ route('super-admin.user-manage.dosen.getDosen') }}",
      columns:[
        {data:'DT_RowIndex', name:'DT_RowIndex',orderable: false, searchable: false},
        {data:'nip', name:'nip'},
        {data:'nama_dosen', name:'nama_dosen'},
        {data:'jabatan', name:'jabatan'},
        {data: "aksi",
              render: function(data){
                return htmlDecode(data);
              },orderable: false, searchable: false
            }
      ]
    });

  } );
</script>
@endpush
