@extends('layouts.backend')

@section('title')
SIMPUR | Tambah Data Mahasiswa
@endsection

@section('header')
<div class="header text-center">
    <h2 class=" title">Tambah Data Mahasiswa Fakultas Kehutanan</h2>
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12 col-md-6">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb bg-white rounded-pill shadow">
                        <li class="breadcrumb-item"><a href="{{ route('super-admin.index') }}">Home</a></li>
                        <li class="breadcrumb-item"><a
                                href="{{ route('super-admin.user-manage.mahasiswa.index') }}">Data
                                Mahasiswa</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Tambah Data Mahasiswa</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">
                <h5 class="title">Tambah Akun Mahasiswa</h5>
            </div>
            <div class="card-body">
                <form action="{{ route('super-admin.user-manage.mahasiswa.store') }}" method="POST">
                    @csrf
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>NIM</label>
                                <input type="text" name="nim" class="form-control @error('nim') is-invalid @enderror"
                                    placeholder="Masukkan NIM" value="{{ old('nim') }}">
                                @error('nim')
                                <div class="invalid-feedback ml-3">
                                    *{{ $message }}
                                </div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Nama Mahasiswa</label>
                                <input type="text" name="nama_mahasiswa"
                                    class="form-control @error('nama_mahasiswa') is-invalid @enderror"
                                    placeholder="Nama Mahasiswa" value="{{ old('nama_mahasiswa') }}">
                                @error('nama_mahasiswa')
                                <div class="invalid-feedback ml-3">
                                    *{{ $message }}
                                </div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Minat</label>
                                <select name="minat"
                                    class="custom-select form-control @error('minat') is-invalid @enderror">
                                    <option></option>
                                    <option value="Manajemen Hutan">Manajemen Hutan</option>
                                    <option value="Silvikultur">Silvikultur</option>
                                    <option value="Teknologi Hasil Hutan">Teknologi Hasil Hutan</option>
                                </select>
                                @error('minat')
                                <div class="invalid-feedback ml-3">
                                    *{{ $message }}
                                </div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Tahun Angkatan</label>
                                <input type="text" name="tahun_angkatan"
                                    class="form-control @error('tahun_angkatan') is-invalid @enderror"
                                    placeholder="tahun Angkatan" value="{{ old('tahun_angkatan') }}">
                                @error('tahun_angkatan')
                                <div class="invalid-feedback ml-3">
                                    *{{ $message }}
                                </div>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-end">
                        <div class="col-6 text-right">
                            <button type="submit" class="btn btn-success">Tambah</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
