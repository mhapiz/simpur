<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Nilai Mutu {{ $nm->mahasiswa->nama_mahasiswa }}</title>

    <style>
        * {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }

        table {
            border-collapse: collapse;
        }

        /* table,
        tr,
        td {
            border: 1px solid black;
        } */

        table#bordered td {
            border: 1px solid black;

        }

        .tnr {
            font-family: 'Times New Roman', Times, serif;
        }

        .verdana {
            font-family: Verdana, Geneva, Tahoma, sans-serif;
        }

        .mt-0 {
            margin-top: 100px;
        }

        .cap {
            text-transform: capitalize;
        }

        .w-100 {
            width: 100%;
        }

        table#bordered thead td {
            padding: 2px 5px;
        }

        .nilai td {
            padding: 1.2rem 0;
        }
    </style>
</head>

<body style="padding: 10px 2rem">
    <table class="w-100">
        <tr>
            <td style="width: 25%;">
                <div style="text-align: center">
                    <img src="{{ url('adminarea/assets/img/ulm-new.png') }}" alt="" width="175px">
                </div>
            </td>

            <td class="tnr" style="text-align: center;">
                <div style="font-size: 16px;">
                    <P style="font-size: 1.2rem"><span style="font-size: 1rem">KEMENTERIAN PENDIDIKAN DAN
                            KEBUDAYAAN <br> RISET, DAN TEKNOLOGI </span> <br>
                        UNIVERSITAS LAMBUNG MANGKURAT <br>
                        FAKULTAS KEHUTANAN</P>
                    <h4 style="font-size: 1.4rem; font-weight: 700;">PROGRAM STUDI KEHUTANAN</h4>
                    <P style="line-height: 1.1rem; font-size: .9rem">Jalan A. Yani KM 36 Kampus Unlam Banjarbaru
                        Kalimantan Selatan <br>
                        Telepon/Fax : (0511) 4772290 <br>
                        surel : prodi.kehutanan@ulm.ac.id</P>
                </div>
            </td>

            <td style="width: 25%;">

            </td>

        </tr>
    </table>
    <hr style="margin-top: .2rem">

    <table class="verdana w-100">
        <tr>
            <td style="text-align: center; padding: .7rem 0;">
                <p style="font-weight: 700; font-size: 20px;"><u>PERHITUNGAN NILAI MUTU</u></p>
            </td>
        </tr>
    </table>

    <table class="verdana w-100">
        <tr class="">
            <td style="width: 25%"></td>
            <td style="width: 15%">NAMA</td>
            <td style="width: 1%; text-align: center;">:</td>
            <td style="widht: auto"> {{ $nm->mahasiswa->nama_mahasiswa }}</td>
        </tr>
        <tr class="">
            <td style="width: 25%"></td>
            <td style="width: 15%">NIM</td>
            <td style="width: 1%; text-align: center;">:</td>
            <td style="widht: auto"> {{ $nm->mahasiswa->nim }}</td>
        </tr>
        <tr class="">
            <td style="width: 25%"></td>
            <td style="width: 15%">JURUSAN</td>
            <td style="width: 1%; text-align: center;">:</td>
            <td style="widht: auto"> Kehutanan</td>
        </tr>
        <tr class="">
            <td style="width: 25%"></td>
            <td style="width: 15%">MINAT STUDI</td>
            <td style="width: 1%; text-align: center;">:</td>
            <td style="widht: auto"> {{ $nm->mahasiswa->minat }}</td>
        </tr>
    </table>

    <table id="bordered" class="verdana w-100" style="margin-top: 2rem">
        <thead style="text-align: center">
            <tr>
                <td colspan="3">Nilai Proposal</td>
                <td colspan="3">Nilai Skripsi</td>
                <td rowspan="2">Nilai Seminar Rerata</td>
                <td colspan="3">Nilai Ujian Skripsi</td>
                <td rowspan="2">Nilai Komprehensif</td>
                <td rowspan="2">Nilai Skripsi</td>
                <td rowspan="2">Nilai Mutu</td>
            </tr>

            <tr>
                <td>Penyanggah</td>
                <td>Seminar</td>
                <td>Rerata</td>

                <td>Penyanggah </td>
                <td>Seminar </td>
                <td>Rerata</td>

                <td>Pemb. <br> I</td>
                <td>Pemb. <br> II</td>
                <td>Rerata</td>
            </tr>
            <tr>
                <td>A</td>
                <td>B</td>
                <td>C</td>

                <td>D</td>
                <td>E</td>
                <td>F</td>

                <td>G</td>
                <td>H</td>
                <td>I</td>
                <td>J</td>
                <td>K</td>
                <td>L</td>
                <td>M</td>
            </tr>
        </thead>
        <tbody>
            <tr style="text-align: center" class="nilai">
                <td>{{ $nm->rerata_nilai_penyanggah_sup }}</td>
                <td>{{ $nm->rerata_nilai_sup }}</td>
                <td>{{ $nm->rerata_nilai_usulan_penelitian }}</td>

                <td>{{ $nm->rerata_nilai_penyanggah_shp }}</td>
                <td>{{ $nm->rerata_nilai_shp }}</td>
                <td>{{ $nm->rerata_nilai_hasil_penelitian }}</td>
                <td>{{ $nm->rerata_nilai_seminar }}</td>

                <td>{{ $nm->nilai_p1_ujian_skripsi }}</td>
                <td>{{ $nm->nilai_p2_ujian_skripsi }}</td>
                <td>{{ $nm->rerata_nilai_ujian_skripsi }}</td>

                <td>{{ $nm->nilai_komprehensif }}</td>

                <td>{{ $nm->nilai_skripsi }}</td>
                <td>{{ $nm->nilai_mutu }}</td>
            </tr>

        </tbody>
    </table>


    <table class="verdana w-100" style="margin-top: 1.3rem">
        <tr>
            <td style="width: 75%"></td>
            <td>Banjarbaru,...................</td>
        </tr>

        <tr>
            <td style="width: 75%"></td>
            <td>Ketua,</td>
        </tr>

        <tr>
            <td style="width: 75%"></td>
            <td><br><br><br><br><br></td>
        </tr>

        <tr>
            <td style="width: 75%"></td>
            <td style="border-bottom: 1px solid black"></td>
        </tr>
    </table>

    <table class="verdana w-100" style="margin-top: 1.3rem">
        <tr>
            <td>Catatan : </td>
        </tr>

        <tr>
            <td style="padding-left: 20px; width: 20%">1. C = (A + B) / 2</td>
            <td>2. F = (D + E) / 2</td>
        </tr>
        <tr>
            <td style="padding-left: 20px; width: 20%;padding-top: 5px">3. J = (H + I) / 2</td>
            <td>4. L = (J x 4) + K + G / 6</td>
        </tr>

    </table>


</body>

</html>
