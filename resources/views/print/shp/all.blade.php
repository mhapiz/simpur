<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        .page-break {
            page-break-after: always;
        }

        * {
            margin: 0;
            padding: 0;
        }

        table {
            border-collapse: collapse;
        }

        /* table,
        tr,
        td {
            border: 1px solid black;

        } */

        .tnr {
            font-family: 'Times New Roman', Times, serif;
        }

        .verdana {
            font-family: Verdana, Geneva, Tahoma, sans-serif;
        }

        .w-100 {
            width: 100%
        }

        .text-center {
            text-align: center;
        }

        .pl-3 {
            padding-left: .8rem
        }

        table#tabelBeborder td {
            border: 1px solid black;
            vertical-align: center;
            padding-top: .3rem;
            padding-bottom: .3rem;
        }

        table#tabelBeborder input {
            border-width: 0px;
            text-align: center;
            width: 100%;
        }

        table#no-border td {
            border: border: 1px solid rgba(255, 255, 255, 0) !important;
            padding: 0;
        }

        td {
            padding-left: .3rem;
        }

        .font-weight-bold {
            font-weight: 700;
        }
    </style>
</head>

<body style="padding: 10px 2rem">




    @foreach ($nilaishp as $nshp)

    <table style="width: 100%;">
        <tr>
            <td style="width: 15%;">
                <div style="display: flex; justify-content: flex-end;">
                    <img src="{{ url('adminarea/assets/img/ulm-new.png') }}" alt="" width="130px">
                </div>
            </td>

            <td class="tnr" style="text-align: center;">
                <div style="font-size: 16px;">
                    <P style="font-size: 1.2rem"><span style="font-size: 1rem">KEMENTERIAN PENDIDIKAN DAN
                            KEBUDAYAAN <br> RISET, DAN TEKNOLOGI </span> <br>
                        UNIVERSITAS LAMBUNG MANGKURAT <br>
                        FAKULTAS KEHUTANAN</P>
                    <h4 style="font-size: 1.4rem; font-weight: 700;">PROGRAM STUDI KEHUTANAN</h4>
                    <P style="line-height: 1.1rem; font-size: .9rem">Jalan A. Yani KM 36 Kampus Unlam Banjarbaru
                        Kalimantan Selatan <br>
                        Telepon/Fax : (0511) 4772290 <br>
                        surel : prodi.kehutanan@ulm.ac.id</P>
                </div>
            </td>

            <td style="width: 15%;">

            </td>

        </tr>
    </table>
    <hr style="margin-top: .2rem">

    <table class="verdana w-100">
        <tr>
            <td style="text-align: center; padding-top: 2rem; padding-bottom: 1rem ">
                <p style="font-weight: 700; font-size: 20px; text-transform: uppercase"><u>Nilai Seminar Hasil
                        Penelitian</u></p>
            </td>
        </tr>
    </table>

    <table class="verdana w-100">
        <tr>
            <td style="width: 29%; vertical-align: top;" class="">
                <span style="">Nama / NIM Seminaris</span></td>
            <td style="width:  1%; vertical-align: top;" class=""><span style="">:</span></td>
            <td style="width: auto; vertical-align: top; padding-left: .4rem" class="">
                {{ $nshp->mahasiswa->nama_mahasiswa }} / {{ $nshp->nim_seminaris }}

            </td>
        </tr>
        <tr>
            <td style="width: 29%; vertical-align: top;" class="">
                <span style="">Program Studi / Minat</span>
            </td>
            <td style="width: 1%; vertical-align: top;" class=""><span style="">:</span></td>
            <td style="width: auto; vertical-align: top; padding-left: .4rem" class="">Kehutanan /
                {{ $nshp->mahasiswa->minat }}</td>
        </tr>
        <tr>
            <td style="width: 29%; vertical-align: top;" class="">
                <span style="">Hari / Tgl Seminar</span>
            </td>
            <td style="width: 1%; vertical-align: top;" class=" "><span style="">:</span></td>
            <td style="width: auto; vertical-align: top; padding-left: .4rem" class="">
                {{ \Carbon\Carbon::parse($nshp->shp->tgl_seminar)->isoFormat('dddd / D MMMM Y') }}
            </td>
        </tr>
        <tr>
            <td style="width: 29%; vertical-align: top;">
                <span style="">Judul Seminar</span>
            </td>
            <td style="width: 1%; vertical-align: top;"><span style="">:</span>
            </td>
            <td style="width: auto; vertical-align: top; padding-left: .4rem" style="word-wrap: break-word">
                {{ $nshp->shp->judul }}
            </td>
        </tr>
    </table>

    <table class="verdana w-100" style="width: 100%; margin-top: 1rem" id="tabelBeborder">
        <thead style="text-align: center;">
            <tr style="width: 100%;">
                <td style="width: auto;" class="font-weight-bold">NO</td>
                <td style="width: auto;" class="font-weight-bold">ASPEK YANG DINILAI</td>
                <td style="width: auto;" class="font-weight-bold">NILAI <br> (>70-100)</td>
                <td style="width: auto;" class="font-weight-bold">BOBOT</td>
                <td style="width: auto;" class="font-weight-bold">BOBOT X <br> NILAI</td>
            </tr>
        </thead>

        <tbody>
            <tr>
                <td rowspan="6" class="text-center">1</td>
                <td colspan="4">a. Penulisan</td>
            <tr>
                <td class="pl-3">
                    <table class="verdana w-100 " id="no-border">
                        <td>1)</td>
                        <td>Kesesuaian naskah usulan penelitian dengan pedoman penulisan
                            ilmian
                            yang berlaku</td>
                    </table>
                </td>
                <td class="text-center">{{ $nshp->nilai1a1 }} </td>
                <td class="text-center">0,10</td>
                <td class="text-center">{{ $nshp->hasil1a1 }} </td>
            </tr>
            <tr>
                <td colspan="4">b. Substansi isi naskah usulan penelitian</td>
            </tr>
            <tr>
                <td class="pl-3">

                    <table class="verdana w-100 " id="no-border">
                        <td>2)</td>
                        <td>Judul ringkas, selaras dengan batang tubuh naskah usulan
                            penelitian</td>
                    </table>

                </td>
                <td class="text-center">{{ $nshp->nilai1b2 }} </td>
                <td class="text-center">0,10</td>
                <td class="text-center">{{ $nshp->hasil1b2 }} </td>
            </tr>
            <tr>
                <td class="pl-3">

                    <table class="verdana w-100 " id="no-border">
                        <td>3)</td>
                        <td>Metode penelitian sesuai dengan tujuan penelitian</td>
                    </table>

                </td>
                <td class="text-center">{{ $nshp->nilai1b3 }} </td>
                <td class="text-center">0,10</td>
                <td class="text-center">{{ $nshp->hasil1b3 }} </td>
            </tr>
            <tr>
                <td class="pl-3">

                    <table class="verdana w-100 " id="no-border">
                        <td>4)</td>
                        <td>Pustaka yang diacu minimal 15 sumber pustaka dan 80% berasal
                            dari
                            pustaka</td>
                    </table>

                </td>
                <td class="text-center">{{ $nshp->nilai1b4 }} </td>
                <td class="text-center">0,10</td>
                <td class="text-center">{{ $nshp->hasil1b4 }} </td>
            </tr>
            </tr>
            <!-- -- -->
            <tr>
                <td rowspan="4" class="text-center">2</td>
                <td colspan="4">Penugasan materi presentasi</td>
            <tr>
                <td class="pl-3">c. Kemampuan pemaparan</td>
                <td class="text-center">{{ $nshp->nilai2c }} </td>
                <td class="text-center">0,15</td>
                <td class="text-center">{{ $nshp->hasil2c }} </td>
            </tr>
            <tr>
                <td class="pl-3">d. Kualitas penyajian</td>
                <td class="text-center">{{ $nshp->nilai2d }} </td>
                <td class="text-center">0,10</td>
                <td class="text-center">{{ $nshp->hasil2d }} </td>
            </tr>
            <tr>
                <td class="pl-3">e. Ketepatan waktu</td>
                <td class="text-center">{{ $nshp->nilai2e }} </td>
                <td class="text-center">0,10</td>
                <td class="text-center">{{ $nshp->hasil2e }} </td>
            </tr>
            </tr>

            <!-- -- -->
            <tr>
                <td class="text-center">3</td>
                <td>Kemampuan penalaran dan pemikiran ilmiah (logis, kritis, sistematis dan kreatif)
                </td>
                <td class="text-center">{{ $nshp->nilai3 }} </td>
                <td class="text-center">0,15</td>
                <td class="text-center">{{ $nshp->hasil3 }} </td>
            </tr>
            <!-- -- -->
            <tr>
                <td class="text-center">4</td>
                <td>Sikap dan etika seminaris</td>
                <td class="text-center">{{ $nshp->nilai4 }} </td>
                <td class="text-center">0,10</td>
                <td class="text-center">{{ $nshp->hasil4 }} </td>
            </tr>
            <!-- -- -->
            <tr class="">
                <td colspan="4" class="text-center font-weight-bold py-4">Total Nilai</td>
                <td class="text-center">{{ $nshp->total_nilai }}
                </td>
            </tr>
        </tbody>
    </table>

    <table class="verdana w-100" style="margin-top: 1.5rem">
        <tr>
            <td style="width: 50%;"></td>
            <td>Banjarbaru,
                {{ \Carbon\Carbon::parse($nshp->shp->tgl_seminar)->isoFormat('D MMMM Y') }}
            </td>
        </tr>
        <tr>
            <td style="width: 50%;"></td>
            <td>
                @if ($nshp->shp->pembimbing1 == $nshp->dosen->nip)
                Dosen Pembimbing I,
                @elseif($nshp->shp->pembimbing2 == $nshp->dosen->nip)
                Dosen Pembimbing II,
                @endif

            </td>
        </tr>

        <tr height="5rem">
            <td style="width: 50%;"></td>
            <td> <br> <br> <br> <br> </td>
        </tr>
        <tr>
            <td style="width: 50%;"></td>
            <td>{{ $nshp->dosen->nama_dosen }}</td>
        </tr>
        <tr>
            <td style="width: 50%;"></td>
            <td>NIP. {{ $nshp->dosen->nip }}</td>
        </tr>
    </table>


    <div class="page-break"></div>
    @endforeach



    {{-- PENYANGGAH  --}}


    @foreach ($nilaiPenyanggahshp as $npshp)

    <table style="width: 100%;">
        <tr>
            <td style="width: 15%;">
                <div style="display: flex; justify-content: flex-end;">
                    <img src="{{ url('adminarea/assets/img/ulm-new.png') }}" alt="" width="130px">
                </div>
            </td>

            <td class="tnr" style="text-align: center;">
                <div style="font-size: 16px;">
                    <P style="font-size: 1.2rem"><span style="font-size: 1rem">KEMENTERIAN PENDIDIKAN DAN
                            KEBUDAYAAN <br> RISET, DAN TEKNOLOGI </span> <br>
                        UNIVERSITAS LAMBUNG MANGKURAT <br>
                        FAKULTAS KEHUTANAN</P>
                    <h4 style="font-size: 1.4rem; font-weight: 700;">PROGRAM STUDI KEHUTANAN</h4>
                    <P style="line-height: 1.1rem; font-size: .9rem">Jalan A. Yani KM 36 Kampus Unlam Banjarbaru
                        Kalimantan Selatan <br>
                        Telepon/Fax : (0511) 4772290 <br>
                        surel : prodi.kehutanan@ulm.ac.id</P>
                </div>
            </td>

            <td style="width: 15%;">

            </td>

        </tr>
    </table>
    <hr style="margin-top: 1rem">

    <table class="verdana w-100">
        <tr>
            <td style="text-align: center; padding-top: 3rem; padding-bottom: 1rem ">
                <p style="font-weight: 700; font-size: 20px; text-transform: uppercase"><u>Lembar Penilaian Penyanggah
                        Seminar Hasil Penelitian</u></p>
            </td>
        </tr>
    </table>

    <table class="verdana w-100" style="padding: 1rem 0;">
        <tr>
            <td style="width: 29%; vertical-align: top;" class="">
                <span style="">Nama / NIM Seminaris / Tgl</span></td>
            <td style="width:  1%; vertical-align: top;" class=""><span style="">:</span></td>
            <td style="width: auto; vertical-align: top; padding-left: .4rem" class="">
                {{ $npshp->mahasiswa->nama_mahasiswa }} / {{ $npshp->nim_seminaris }} /
                {{ \Carbon\Carbon::parse($npshp->shp->tgl_seminar)->isoFormat('D MMMM Y') }}

            </td>
        </tr>
        <tr>
            <td style="width: 29%; vertical-align: top;" class="">
                <span style="">Program Studi / Minat</span>
            </td>
            <td style="width: 1%; vertical-align: top;" class=""><span style="">:</span></td>
            <td style="width: auto; vertical-align: top; padding-left: .4rem" class="">Kehutanan /
                {{ $npshp->mahasiswa->minat }}</td>
        </tr>
        <tr>
            <td style="width: 29%; vertical-align: top;" class="">
                <span style="">Nama Penyanggah / NIM</span>
            </td>
            <td style="width: 1%; vertical-align: top;" class=" "><span style="">:</span></td>
            <td style="width: auto; vertical-align: top; padding-left: .4rem" class="">
                {{ App\Models\Mahasiswa::where('nim','=',$npshp->nim_penyanggah)->first()->nama_mahasiswa }}
                /
                {{ $npshp->nim_penyanggah }}
            </td>
        </tr>
        <tr>
            <td style="width: 29%; vertical-align: top;" class="">
                <span style="">Minat</span>
            </td>
            <td style="width: 1%; vertical-align: top;" class=" "><span style="">:</span></td>
            <td style="width: auto; vertical-align: top; padding-left: .4rem" class="">
                {{ App\Models\Mahasiswa::where('nim','=',$npshp->nim_penyanggah)->first()->minat }}

            </td>
        </tr>
        <tr>
            <td style="width: 29%; vertical-align: top;">
                <span style="">Judul Skripsi</span>
            </td>
            <td style="width: 1%; vertical-align: top;"><span style="">:</span>
            </td>
            <td style="width: auto; vertical-align: top; padding-left: .4rem" style="word-wrap: break-word">
                {{ $npshp->shp->judul }}
            </td>
        </tr>
    </table>

    <table class="verdana w-100" id="tabelBeborder" style="margin-top: 1rem">
        <thead style="text-align: center;">
            <tr style="width: 100%;">
                <td style="width: 5%;" class="font-weight-bold">NO</td>
                <td style="width: 55%;" class="font-weight-bold">UNSUR PENILAIAN</td>
                <td style="width: auto;" class="font-weight-bold">NILAI <br> (>70-100)</td>
                <td style="width: auto;" class="font-weight-bold">BOBOT</td>
                <td style="width: auto;" class="font-weight-bold">BOBOT X <br> NILAI</td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="text-center">1.</td>
                <td>Kemampuan melakukan kritikan/saran/pendapat dalam bentuk komunikasi secara lisan
                </td>
                <td class="text-center"> {{ $npshp->nilai1 }} </td>
                <td class="text-center">0,2</td>
                <td class="text-center"> {{ $npshp->hasil1 }} </td>
            </tr>
            <tr>
                <td class="text-center">2.</td>
                <td>Saran, pendapat dan kritikan yang bersifat perbaikan terhadap naskah Seminar
                    Usulan
                </td>
                <td class="text-center"> {{ $npshp->nilai2 }} </td>
                <td class="text-center">0,2</td>
                <td class="text-center"> {{ $npshp->hasil2 }} </td>
            </tr>
            <tr>
                <td class="text-center">3.</td>
                <td>Kedalaman saran, pendapat dan kritikan yang disampaikan kepada seminaris</td>
                <td class="text-center"> {{ $npshp->nilai3 }} </td>
                <td class="text-center">0,2</td>
                <td class="text-center"> {{ $npshp->hasil3 }} </td>
            </tr>
            <tr>
                <td class="text-center">4.</td>
                <td>Saran, perbaikan dan pendapat tidak keluar dari tema usulan penelitian</td>
                <td class="text-center"> {{ $npshp->nilai4 }} </td>
                <td class="text-center">0,2</td>
                <td class="text-center"> {{ $npshp->hasil4 }} </td>
            </tr>
            <tr>
                <td class="text-center">5.</td>
                <td>Kemampuan menyampaikan gagasan, ide yang bersifat kreatif dan inovatif terhadap
                    naskah seminar</td>
                <td class="text-center"> {{ $npshp->nilai5 }} </td>
                <td class="text-center">0,2</td>
                <td class="text-center"> {{ $npshp->hasil5 }} </td>
            </tr>
            <!-- -- -->
            <tr class="">
                <td colspan="4" class="text-center font-weight-bold py-4">Total Nilai</td>
                <td class="text-center">
                    {{ $npshp->total_nilai }}
                </td>
            </tr>
        </tbody>
    </table>


    <table class="verdana w-100" style="margin-top: 1.5rem">
        <tr>
            <td style="width: 50%;"></td>
            <td>Banjarbaru,
                {{ \Carbon\Carbon::parse($npshp->shp->tgl_seminar)->isoFormat('D MMMM Y') }}
            </td>
        </tr>
        <tr>
            <td style="width: 50%;"></td>
            <td>
                @if ($npshp->shp->pembimbing1 == $npshp->dosen->nip)
                Dosen Pembimbing I,
                @elseif($npshp->shp->pembimbing2 == $npshp->dosen->nip)
                Dosen Pembimbing II,
                @endif

            </td>
        </tr>

        <tr height="5rem">
            <td style="width: 50%;"></td>
            <td> <br> <br> <br> <br> </td>
        </tr>
        <tr>
            <td style="width: 50%;"></td>
            <td>{{ $npshp->dosen->nama_dosen }}</td>
        </tr>
        <tr>
            <td style="width: 50%;"></td>
            <td>NIP. {{ $npshp->dosen->nip }}</td>
        </tr>
    </table>


    <div class="page-break"></div>
    @endforeach


</body>

</html>
