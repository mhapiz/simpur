<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        .page-break {
            page-break-after: always;
        }

        * {
            margin: 0;
            padding: 0;
        }

        table {
            border-collapse: collapse;
        }

        /* table,
        tr,
        td {
            border: 1px solid black;

        } */

        .tnr {
            font-family: 'Times New Roman', Times, serif;
        }

        .verdana {
            font-family: Verdana, Geneva, Tahoma, sans-serif;
        }

        .w-100 {
            width: 100%
        }

        .text-center {
            text-align: center;
        }

        .pl-3 {
            padding-left: .8rem
        }

        table#tabelBeborder td {
            border: 1px solid black;
            vertical-align: center;
            padding-top: .3rem;
            padding-bottom: .3rem;
        }

        table#tabelBeborder input {
            border-width: 0px;
            text-align: center;
            width: 100%;
        }

        table#no-border td {
            border: border: 1px solid rgba(255, 255, 255, 0) !important;
            padding: 0;
        }

        td {
            padding-left: .3rem;
        }

        .font-weight-bold {
            font-weight: 700;
        }
    </style>
</head>

<body style="padding: 10px 2rem">




    @foreach ($nilaisup as $nsup)

    <table style="width: 100%;">
        <tr>
            <td style="width: 15%;">
                <div style="display: flex; justify-content: flex-end;">
                    <img src="{{ url('adminarea/assets/img/ulm-new.png') }}" alt="" width="130px">
                </div>
            </td>

            <td class="tnr" style="text-align: center;">
                <div style="font-size: 16px;">
                    <P style="font-size: 1.2rem"><span style="font-size: 1rem">KEMENTERIAN PENDIDIKAN DAN
                            KEBUDAYAAN <br> RISET, DAN TEKNOLOGI </span> <br>
                        UNIVERSITAS LAMBUNG MANGKURAT <br>
                        FAKULTAS KEHUTANAN</P>
                    <h4 style="font-size: 1.4rem; font-weight: 700;">PROGRAM STUDI KEHUTANAN</h4>
                    <P style="line-height: 1.1rem; font-size: .9rem">Jalan A. Yani KM 36 Kampus Unlam Banjarbaru
                        Kalimantan Selatan <br>
                        Telepon/Fax : (0511) 4772290 <br>
                        surel : prodi.kehutanan@ulm.ac.id</P>
                </div>
            </td>

            <td style="width: 15%;">

            </td>

        </tr>
    </table>
    <hr style="margin-top: .2rem">

    <table class="verdana w-100">
        <tr>
            <td style="text-align: center; padding-top: 2rem; padding-bottom: 1rem ">
                <p style="font-weight: 700; font-size: 20px; text-transform: uppercase"><u>Nilai Seminar Usulan
                        Penelitian</u></p>
            </td>
        </tr>
    </table>

    <table class="verdana w-100">
        <tr>
            <td style="width: 29%; vertical-align: top;" class="">
                <span style="">Nama / NIM Seminaris</span></td>
            <td style="width:  1%; vertical-align: top;" class=""><span style="">:</span></td>
            <td style="width: auto; vertical-align: top; padding-left: .4rem" class="">
                {{ $nsup->mahasiswa->nama_mahasiswa }} / {{ $nsup->nim_seminaris }}

            </td>
        </tr>
        <tr>
            <td style="width: 29%; vertical-align: top;" class="">
                <span style="">Program Studi / Minat</span>
            </td>
            <td style="width: 1%; vertical-align: top;" class=""><span style="">:</span></td>
            <td style="width: auto; vertical-align: top; padding-left: .4rem" class="">Kehutanan /
                {{ $nsup->mahasiswa->minat }}</td>
        </tr>
        <tr>
            <td style="width: 29%; vertical-align: top;" class="">
                <span style="">Hari / Tgl Seminar</span>
            </td>
            <td style="width: 1%; vertical-align: top;" class=" "><span style="">:</span></td>
            <td style="width: auto; vertical-align: top; padding-left: .4rem" class="">
                {{ \Carbon\Carbon::parse($nsup->sup->tgl_seminar)->isoFormat('dddd / D MMMM Y') }}
            </td>
        </tr>
        <tr>
            <td style="width: 29%; vertical-align: top;">
                <span style="">Judul Seminar</span>
            </td>
            <td style="width: 1%; vertical-align: top;"><span style="">:</span>
            </td>
            <td style="width: auto; vertical-align: top; padding-left: .4rem" style="word-wrap: break-word">
                {{ $nsup->sup->judul }}
            </td>
        </tr>
    </table>

    <table class="verdana w-100" style="width: 100%; margin-top: 1rem" id="tabelBeborder">
        <thead style="text-align: center;">
            <tr style="width: 100%;">
                <td style="width: auto;" class="font-weight-bold">NO</td>
                <td style="width: auto;" class="font-weight-bold">ASPEK YANG DINILAI</td>
                <td style="width: auto;" class="font-weight-bold">NILAI <br> (>70-100)</td>
                <td style="width: auto;" class="font-weight-bold">BOBOT</td>
                <td style="width: auto;" class="font-weight-bold">BOBOT X <br> NILAI</td>
            </tr>
        </thead>

        <tbody>
            <tr>
                <td rowspan="6" class="text-center">1</td>
                <td colspan="4">a. Penulisan</td>
            <tr>
                <td class="pl-3">
                    <table class="verdana w-100 " id="no-border">
                        <td>1)</td>
                        <td>Kesesuaian naskah usulan penelitian dengan pedoman penulisan
                            ilmian
                            yang berlaku</td>
                    </table>
                </td>
                <td class="text-center">{{ $nsup->nilai1a1 }} </td>
                <td class="text-center">0,10</td>
                <td class="text-center">{{ $nsup->hasil1a1 }} </td>
            </tr>
            <tr>
                <td colspan="4">b. Substansi isi naskah usulan penelitian</td>
            </tr>
            <tr>
                <td class="pl-3">

                    <table class="verdana w-100 " id="no-border">
                        <td>2)</td>
                        <td>Judul ringkas, selaras dengan batang tubuh naskah usulan
                            penelitian</td>
                    </table>

                </td>
                <td class="text-center">{{ $nsup->nilai1b2 }} </td>
                <td class="text-center">0,10</td>
                <td class="text-center">{{ $nsup->hasil1b2 }} </td>
            </tr>
            <tr>
                <td class="pl-3">

                    <table class="verdana w-100 " id="no-border">
                        <td>3)</td>
                        <td>Metode penelitian sesuai dengan tujuan penelitian</td>
                    </table>

                </td>
                <td class="text-center">{{ $nsup->nilai1b3 }} </td>
                <td class="text-center">0,10</td>
                <td class="text-center">{{ $nsup->hasil1b3 }} </td>
            </tr>
            <tr>
                <td class="pl-3">

                    <table class="verdana w-100 " id="no-border">
                        <td>4)</td>
                        <td>Pustaka yang diacu minimal 15 sumber pustaka dan 80% berasal
                            dari
                            pustaka</td>
                    </table>

                </td>
                <td class="text-center">{{ $nsup->nilai1b4 }} </td>
                <td class="text-center">0,10</td>
                <td class="text-center">{{ $nsup->hasil1b4 }} </td>
            </tr>
            </tr>
            <!-- -- -->
            <tr>
                <td rowspan="4" class="text-center">2</td>
                <td colspan="4">Penugasan materi presentasi</td>
            <tr>
                <td class="pl-3">c. Kemampuan pemaparan</td>
                <td class="text-center">{{ $nsup->nilai2c }} </td>
                <td class="text-center">0,15</td>
                <td class="text-center">{{ $nsup->hasil2c }} </td>
            </tr>
            <tr>
                <td class="pl-3">d. Kualitas penyajian</td>
                <td class="text-center">{{ $nsup->nilai2d }} </td>
                <td class="text-center">0,10</td>
                <td class="text-center">{{ $nsup->hasil2d }} </td>
            </tr>
            <tr>
                <td class="pl-3">e. Ketepatan waktu</td>
                <td class="text-center">{{ $nsup->nilai2e }} </td>
                <td class="text-center">0,10</td>
                <td class="text-center">{{ $nsup->hasil2e }} </td>
            </tr>
            </tr>

            <!-- -- -->
            <tr>
                <td class="text-center">3</td>
                <td>Kemampuan penalaran dan pemikiran ilmiah (logis, kritis, sistematis dan kreatif)
                </td>
                <td class="text-center">{{ $nsup->nilai3 }} </td>
                <td class="text-center">0,15</td>
                <td class="text-center">{{ $nsup->hasil3 }} </td>
            </tr>
            <!-- -- -->
            <tr>
                <td class="text-center">4</td>
                <td>Sikap dan etika seminaris</td>
                <td class="text-center">{{ $nsup->nilai4 }} </td>
                <td class="text-center">0,10</td>
                <td class="text-center">{{ $nsup->hasil4 }} </td>
            </tr>
            <!-- -- -->
            <tr class="">
                <td colspan="4" class="text-center font-weight-bold py-4">Total Nilai</td>
                <td class="text-center">{{ $nsup->total_nilai }}
                </td>
            </tr>
        </tbody>
    </table>

    <table class="verdana w-100" style="margin-top: 1.5rem">
        <tr>
            <td style="width: 50%;"></td>
            <td>Banjarbaru,
                {{ \Carbon\Carbon::parse($nsup->sup->tgl_seminar)->isoFormat('D MMMM Y') }}
            </td>
        </tr>
        <tr>
            <td style="width: 50%;"></td>
            <td>
                @if ($nsup->sup->pembimbing1 == $nsup->dosen->nip)
                Dosen Pembimbing I,
                @elseif($nsup->sup->pembimbing2 == $nsup->dosen->nip)
                Dosen Pembimbing II,
                @endif

            </td>
        </tr>

        <tr height="5rem">
            <td style="width: 50%;"></td>
            <td> <br> <br> <br> <br> </td>
        </tr>
        <tr>
            <td style="width: 50%;"></td>
            <td>{{ $nsup->dosen->nama_dosen }}</td>
        </tr>
        <tr>
            <td style="width: 50%;"></td>
            <td>NIP. {{ $nsup->dosen->nip }}</td>
        </tr>
    </table>


    <div class="page-break"></div>
    @endforeach


</body>

</html>
