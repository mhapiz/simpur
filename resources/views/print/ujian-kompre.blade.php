<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ujian Komprehensif {{ $uk->mahasiswa->nama_mahasiswa }}</title>

    <style>
        * {
            margin: 0;
            padding: 0;
        }

        /*
        table,
        tr,
        td {
            border: 1px solid black;
            border-collapse: collapse;
        } */

        .tnr {
            font-family: 'Times New Roman', Times, serif;
        }

        .verdana {
            font-family: Verdana, Geneva, Tahoma, sans-serif;
        }

        .mt-0 {
            margin-top: 100px;
        }

        .cap {
            text-transform: capitalize;
        }

        .w-100 {
            width: 100%
        }
    </style>
</head>

<body style="padding: 10px 2rem">
    <table style="width: 100%;">
        <tr>
            <td style="width: 15%;">
                <div style="display: flex; justify-content: flex-end;">
                    <img src="{{ url('adminarea/assets/img/ulm-new.png') }}" alt="" width="130px">
                </div>
            </td>

            <td class="tnr" style="text-align: center;">
                <div style="font-size: 16px;">
                    <P style="font-size: 1.2rem"><span style="font-size: 1rem">KEMENTERIAN PENDIDIKAN DAN
                            KEBUDAYAAN <br> RISET, DAN TEKNOLOGI </span> <br>
                        UNIVERSITAS LAMBUNG MANGKURAT <br>
                        FAKULTAS KEHUTANAN</P>
                    <h4 style="font-size: 1.4rem; font-weight: 700;">PROGRAM STUDI KEHUTANAN</h4>
                    <P style="line-height: 1.1rem; font-size: .9rem">Jalan A. Yani KM 36 Kampus Unlam Banjarbaru
                        Kalimantan Selatan <br>
                        Telepon/Fax : (0511) 4772290 <br>
                        surel : prodi.kehutanan@ulm.ac.id</P>
                </div>
            </td>

            <td style="width: 15%;">

            </td>

        </tr>
    </table>
    <hr style="margin-top: .2rem">

    <table class="verdana w-100">
        <tr>
            <td style="text-align: center; padding: 2rem 0;">
                <p style="font-weight: 700; font-size: 20px;"><u>BERITA ACARA UJIAN KOMPREHENSIF</u></p>
            </td>
        </tr>
    </table>

    <table class="verdana w-100">

        <tr>
            <td>
                Pada hari ini <span class="cap">{{ $uk->hariSkripsi }}</span> Tanggal
                <span class="cap">{{ $uk->tglSkripsi }}</span> <span class="cap">{{ $uk->bulanSkripsi }}</span>
                Tahun
                <span class="cap">{{ $uk->tahunSkripsi }}</span> Program Studi Kehutanan Fakultas Kehutanan Universitas
                Lambung Mangkurat di Banjarbaru mengadakan Ujian Komprehensif atas nama
                mahasiswa :
            </td>

        </tr>

    </table>




    <table class="verdana w-100">

        <tr style="">
            <table class="verdana" style="margin-left: 1.5rem; margin-top: .6rem;">
                <tr class="mt-0">
                    <td style=" padding-top: .2rem;">Nama
                    </td>
                    <td style="padding: 0px 10px">:</td>
                    <td style="padding-top: .2rem;"> {{ $uk->mahasiswa->nama_mahasiswa }}</td>
                </tr>
                <tr class="mt-0">
                    <td style=" padding-top: .2rem;">NIM
                    </td>
                    <td style="padding: 0px 10px">:</td>
                    <td style="padding-top: .2rem;"> {{ $uk->mahasiswa->nim }}</td>
                </tr>
                <tr class="mt-0">
                    <td style=" padding-top: .2rem;">
                        Semester </td>
                    <td style="padding: 0px 10px">:</td>
                    <td style="padding-top: .2rem;"> {{ $uk->semester }}</td>
                </tr>
                <tr class="mt-0">
                    <td style=" padding-top: .2rem;">Minat
                        Studi </td>
                    <td style="padding: 0px 10px">:</td>
                    <td style="padding-top: .2rem;"> {{ $uk->mahasiswa->minat }}</td>
                </tr>
            </table>
        </tr>

    </table>

    <table class="verdana" style="width: 100%; padding-top : .8rem">
        <tr>
            <td style="width: 100%;">
                Bertempat di Ruang Sidang Fakultas Kehutanan mulai Jam {{ $uk->jam }} Wita sampai dengan selesai Tim
                Dosen
                Penguji pada Ujian Komprehensif ini terdiri dari :
            </td>
        </tr>

        <tr style="">
            <table class="verdana" style="width: 100%; margin-top: 1rem; margin-left: 1.5rem;">
                <tr class="mt-0">
                    <td style="width: 50%%;padding: .2rem 0;"> 1.
                        {{ \App\Models\Dosen::where('nip','=', $uk->penguji1)->first()->nama_dosen }} </td>
                    <td style="width: auto;"> (Pembimbing Utama/Ketua Sidang)</td>
                </tr>
                <tr class="mt-0">
                    <td style="width: 50%%;padding: .2rem 0;"> 2.
                        {{ \App\Models\Dosen::where('nip','=', $uk->penguji2)->first()->nama_dosen }} </td>
                    <td style="width: auto;"> (Pembimbing Kedua/Anggota)</td>
                </tr>
                <tr class="mt-0">
                    <td style="width: 50%%;padding: .2rem 0;"> 3.
                        {{ \App\Models\Dosen::where('nip','=', $uk->penguji3)->first()->nama_dosen }} </td>
                    <td style="width: auto;"> (Anggota Penguji)</td>
                </tr>
                <tr class="mt-0">
                    <td style="width: 50%%;padding: .2rem 0;"> 4.
                        {{ \App\Models\Dosen::where('nip','=', $uk->penguji4)->first()->nama_dosen }} </td>
                    <td style="width: auto;"> (Anggota Penguji)</td>
                </tr>
            </table>
        </tr>

    </table>

    <table class="verdana" style="width: 100%; padding-top: 1rem">
        <tr>
            <td style="width: 100%;">
                Dalam Ujian Komprehensif tersebut Examinandus/a dinyatakan
                <u>{{ $uk->nilaimutu->nilai_skripsi >= 70 ? 'Lulus' : 'Tidak Lulus' }}</u> dengan nilai
                <u>{{ round(str_replace(',','.',$uk->nilaimutu->nilai_komprehensif)) }}</u>
                ( <u><span class="cap">{{ $uk->nilai_terbilang }}</span></u> )
            </td>
        </tr>
    </table>

    <table class="verdana" style="width: 100%; margin-top: .1rem">
        <tr>
            <td style="width: 50%;">
                <table class="verdana" style="width: 100%; text-align: center;">
                    <tr>
                        <td>Examinandus/Examinanda,</td>
                    </tr>

                    <tr>
                        <td style="height: 5.5rem;"></td>
                    </tr>

                    <tr>
                        <td>{{ $uk->mahasiswa->nama_mahasiswa }}</td>
                    </tr>
                </table>
            </td>

            <td style="width: 50%;">
                <table class="verdana" style="width: 100%; text-align: center;">
                    <tr>
                        <td>Koordinator Program Studi Kehutanan,</td>
                    </tr>

                    <tr>
                        <td style="height: 5.5rem;"></td>
                    </tr>

                    <tr>
                        <td>Dr. Badaruddin, S.Hut, M.P</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <table class="verdana" style="width: 100%; margin-top: 1rem">
        <tr>
            <td style="width: 100%; text-align: center; text-transform: uppercase;">Dosen Penguji</td>
        </tr>
    </table>
    <table class="verdana" style="width: 100%;">
        <tr>
            <td style="width: 50%;">
                <table class="verdana" style="width: 100%; text-align: center;">


                    <tr>
                        <td style="height: 5.5rem;"></td>
                    </tr>

                    <tr>
                        <td>1. {{ \App\Models\Dosen::where('nip','=', $uk->penguji1)->first()->nama_dosen }}</td>
                    </tr>
                </table>
            </td>

            <td style="width: 50%;">
                <table class="verdana" style="width: 100%; text-align: center;">


                    <tr>
                        <td style="height: 5.5rem;"></td>
                    </tr>

                    <tr>
                        <td>2. {{ \App\Models\Dosen::where('nip','=', $uk->penguji2)->first()->nama_dosen }}</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <table class="verdana" style="width: 100%;">
        <tr>
            <td style="width: 50%;">
                <table class="verdana" style="width: 100%; text-align: center;">


                    <tr>
                        <td style="height: 5.5rem;"></td>
                    </tr>

                    <tr>
                        <td>3. {{ \App\Models\Dosen::where('nip','=', $uk->penguji3)->first()->nama_dosen }}</td>
                    </tr>
                </table>
            </td>

            <td style="width: 50%;">
                <table class="verdana" style="width: 100%; text-align: center;">


                    <tr>
                        <td style="height: 5.5rem;"></td>
                    </tr>

                    <tr>
                        <td>4. {{ \App\Models\Dosen::where('nip','=', $uk->penguji4)->first()->nama_dosen }}</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>




</body>

</html>
