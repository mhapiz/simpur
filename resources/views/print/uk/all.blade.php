<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        .page-break {
            page-break-after: always;
        }

        * {
            margin: 0;
            padding: 0;
        }

        table {
            border-collapse: collapse;
        }

        /* table,
        tr,
        td {
            border: 1px solid black;

        } */

        .tnr {
            font-family: 'Times New Roman', Times, serif;
        }

        .verdana {
            font-family: Verdana, Geneva, Tahoma, sans-serif;
        }

        .w-100 {
            width: 100%
        }

        .text-center {
            text-align: center;
        }

        .pl-3 {
            padding-left: .8rem
        }

        table#tabelBeborder td {
            border: 1px solid black;
            vertical-align: center;
            padding-top: .3rem;
            padding-bottom: .3rem;
        }

        table#tabelBeborder input {
            border-width: 0px;
            text-align: center;
            width: 100%;
        }

        table#no-border td {
            border: border: 1px solid rgba(255, 255, 255, 0) !important;
            padding: 0;
        }

        td {
            padding-left: .3rem;
        }

        .font-weight-bold {
            font-weight: 700;
        }
    </style>
</head>

<body style="padding: 10px 2rem">




    @foreach ($nuk as $n)

    <table style="width: 100%;">
        <tr>
            <td style="width: 15%;">
                <div style="display: flex; justify-content: flex-end;">
                    <img src="{{ url('adminarea/assets/img/ulm-new.png') }}" alt="" width="130px">
                </div>
            </td>

            <td class="tnr" style="text-align: center;">
                <div style="font-size: 16px;">
                    <P style="font-size: 1.2rem"><span style="font-size: 1rem">KEMENTERIAN PENDIDIKAN DAN
                            KEBUDAYAAN <br> RISET, DAN TEKNOLOGI </span> <br>
                        UNIVERSITAS LAMBUNG MANGKURAT <br>
                        FAKULTAS KEHUTANAN</P>
                    <h4 style="font-size: 1.4rem; font-weight: 700;">PROGRAM STUDI KEHUTANAN</h4>
                    <P style="line-height: 1.1rem; font-size: .9rem">Jalan A. Yani KM 36 Kampus Unlam Banjarbaru
                        Kalimantan Selatan <br>
                        Telepon/Fax : (0511) 4772290 <br>
                        surel : prodi.kehutanan@ulm.ac.id</P>
                </div>
            </td>

            <td style="width: 15%;">

            </td>

        </tr>
    </table>
    <hr style="margin-top: .2rem">

    <table class="verdana w-100">
        <tr>
            <td style="text-align: center; padding-top: 2rem; padding-bottom: 1rem ">
                <p style="font-weight: 700; font-size: 20px; text-transform: uppercase"><u>Nilai Ujian Komprehensif</u>
                </p>
            </td>
        </tr>
    </table>

    <table class="verdana w-100">
        <tr>
            <td style="width: 39%; vertical-align: top;" class="">
                <span style="">Nama Examinandus/Examinanda </span></td>
            <td style="width:  1%; vertical-align: top;" class=""><span style="">:</span></td>
            <td style="width: auto; vertical-align: top; padding-left: .7rem" class="">
                {{ $n->mahasiswa->nama_mahasiswa }}

            </td>
        </tr>
        <tr>
            <td style="width: 39%; vertical-align: top;" class="">
                <span style="">NIM </span>
            </td>
            <td style="width: 1%; vertical-align: top;" class=""><span style="">:</span></td>
            <td style="width: auto; vertical-align: top; padding-left: .7rem" class="">
                {{ $n->mahasiswa->nim }}
            </td>
        </tr>

    </table>
    {{--  --}}

    <table class="verdana w-100" style="margin-top: 1rem" id="tabelBeborder">
        <thead style="text-align: center;">
            <tr style="width: 100%;">
                <td style="width: 5%" class="font-weight-bold">NO</td>
                <td style="width: 20%;" class="font-weight-bold">Unsur Penilaian</td>
                <td style="width: 40%;" class="font-weight-bold">Acuan Penilaian</td>
                <td style="width: auto;" class="font-weight-bold">NILAI <br> (>70-100)</td>
                <td style="width: auto;" class="font-weight-bold">BOBOT</td>
                <td style="width: auto;" class="font-weight-bold">BOBOT X <br> NILAI</td>
            </tr>
        </thead>
        <tbody>



            <tr>
                <td class="text-center" rowspan="2">1.</td>
                <td rowspan="2">Penilaian Naskah Skripsi</td>
                <td>
                    <table class="verdana w-100 " id="no-border">
                        <td>a.</td>
                        <td>Judul berhubungan dengan masalah, tujuan, metoda, hasil dan kemampuan.</td>
                    </table>
                </td>
                <td class="text-center">{{ $n->nilai1a }}</td>
                <td class="text-center">0,10</td>
                <td class="text-center">{{ $n->hasil1a }}</td>
            </tr>

            <tr>
                <td>
                    <table class="verdana w-100 " id="no-border">
                        <td>b.</td>
                        <td>Penggunaan Bahasa dan tata kalimat yang sesuai dengan kaidah ilmiah.</td>
                    </table>
                </td>
                <td class="text-center">{{ $n->nilai1b }}</td>
                <td class="text-center">0,10</td>
                <td class="text-center">{{ $n->hasil1b }}</td>
            </tr>

            <tr>
                <td class="text-center" rowspan="2">2.</td>
                <td rowspan="2">Penyampaian Materi</td>
                <td>
                    <table class="verdana w-100 " id="no-border">
                        <td>a.</td>
                        <td>Penguasaan terhadap skripsi.</td>
                    </table>
                </td>
                <td class="text-center">{{ $n->nilai2a }}</td>
                <td class="text-center">0,15</td>
                <td class="text-center">{{ $n->hasil2a }}</td>
            </tr>

            <tr>
                <td>
                    <table class="verdana w-100 " id="no-border">
                        <td>b.</td>
                        <td>Kualitas penyajian power point dan cara mempresentasikan.</td>
                    </table>
                </td>
                <td class="text-center">{{ $n->nilai2b }}</td>
                <td class="text-center">0,10</td>
                <td class="text-center">{{ $n->hasil2b }}</td>
            </tr>

            <tr>
                <td class="text-center" rowspan="2">3.</td>
                <td rowspan="2">Wawasan dibidang kehutanan</td>
                <td>
                    <table class="verdana w-100 " id="no-border">
                        <td>a.</td>
                        <td>Pemahaman umum di bidang kehutanan.</td>
                    </table>
                </td>
                <td class="text-center">{{ $n->nilai3a }}</td>
                <td class="text-center">0,15</td>
                <td class="text-center">{{ $n->hasil3a }}</td>
            </tr>

            <tr>
                <td>
                    <table class="verdana w-100 " id="no-border">
                        <td>b.</td>
                        <td>Keterkaitan bidang penelitian dengan ilmu kehutanan.</td>
                    </table>
                </td>
                <td class="text-center">{{ $n->nilai3b }}</td>
                <td class="text-center">0,20</td>
                <td class="text-center">{{ $n->hasil3b }}</td>
            </tr>

            <tr>
                <td class="text-center">4.</td>
                <td>Cara menjawab pertanyaan dan kebenaran jawaban</td>
                <td> Jumlah pertanyaan yang mampu di jawab oleh eksaminandus/da </td>
                <td class="text-center">{{ $n->nilai4 }}</td>
                <td class="text-center">0,10</td>
                <td class="text-center">{{ $n->hasil4 }}</td>
            </tr>

            <tr>
                <td class="text-center">4.</td>
                <td>Etika</td>
                <td>
                    <table class="verdana w-100 " id="no-border">
                        <td>a.</td>
                        <td>Sopan santun saat menghadapi ujian komprehensif dan kerapian cara berpakaian</td>
                    </table>
                </td>
                <td class="text-center">{{ $n->nilai5a }}</td>
                <td class="text-center">0,10</td>
                <td class="text-center">{{ $n->hasil5a }}</td>
            </tr>

            <tr class="">
                <td colspan="5" class="text-center font-weight-bold py-4">Total Nilai</td>
                <td class="text-center">
                    {{ $n->total_nilai }}
                </td>
            </tr>

        </tbody>
    </table>

    <table class="verdana w-100">
        <tr>
            <td style="padding-left: .3rem" style="width: 50%;">Keterangan :</td>
        </tr>
        <tr>
            <td style="padding-left: .7rem">a. Nilai Kelulusan > 70;</td>
        </tr>
        <tr>
            <td style="padding-left: .7rem">b. Nilai akhir adalah nilai rata-rata dari semua dosen penguji.</td>
        </tr>
        <tr>
            <td style="padding-left: .7rem">{{ $n->nilainilai }}
            </td>
        </tr>
    </table>
    <table class="verdana w-100" style="margin-top: 1.5rem">
        <tr>
            <td style="width: 50%;"></td>
            <td>Banjarbaru,
                {{ \Carbon\Carbon::parse($n->uk->tgl_skripsi)->isoFormat('D MMMM Y') }}
            </td>
        </tr>
        <tr>
            <td style="width: 50%;"></td>
            <td>
                Penguji,

            </td>
        </tr>

        <tr height="5rem">
            <td style="width: 50%;"></td>
            <td> <br> <br> <br> <br> </td>
        </tr>
        <tr>
            <td style="width: 50%;"></td>
            <td>{{ $n->dosen->nama_dosen }}</td>
        </tr>
        <tr>
            <td style="width: 50%;"></td>
            <td>NIP. {{ $n->dosen->nip }}</td>
        </tr>
    </table>
    <div class="page-break">
    </div>


    @endforeach


</body>

</html>
