<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        .page-break {
            page-break-after: always;
        }

        * {
            margin: 0;
            padding: 0;
        }

        table {
            border-collapse: collapse;
        }

        /* table,
        tr,
        td {
            border: 1px solid black;

        } */

        .tnr {
            font-family: 'Times New Roman', Times, serif;
        }

        .verdana {
            font-family: Verdana, Geneva, Tahoma, sans-serif;
        }

        .w-100 {
            width: 100%
        }

        .text-center {
            text-align: center;
        }

        .pl-3 {
            padding-left: .8rem
        }

        table#tabelBeborder td {
            border: 1px solid black;
            vertical-align: center;
            padding-top: .3rem;
            padding-bottom: .3rem;
        }

        table#tabelBeborder input {
            border-width: 0px;
            text-align: center;
            width: 100%;
        }

        table#no-border td {
            border: border: 1px solid rgba(255, 255, 255, 0) !important;
            padding: 0;
        }

        td {
            padding-left: .3rem;
        }

        .font-weight-bold {
            font-weight: 700;
        }
    </style>
</head>

<body style="padding: 10px 2rem">




    @foreach ($nus as $n)

    <table style="width: 100%;">
        <tr>
            <td style="width: 15%;">
                <div style="display: flex; justify-content: flex-end;">
                    <img src="{{ url('adminarea/assets/img/ulm-new.png') }}" alt="" width="130px">
                </div>
            </td>

            <td class="tnr" style="text-align: center;">
                <div style="font-size: 16px;">
                    <P style="font-size: 1.2rem"><span style="font-size: 1rem">KEMENTERIAN PENDIDIKAN DAN
                            KEBUDAYAAN <br> RISET, DAN TEKNOLOGI </span> <br>
                        UNIVERSITAS LAMBUNG MANGKURAT <br>
                        FAKULTAS KEHUTANAN</P>
                    <h4 style="font-size: 1.4rem; font-weight: 700;">PROGRAM STUDI KEHUTANAN</h4>
                    <P style="line-height: 1.1rem; font-size: .9rem">Jalan A. Yani KM 36 Kampus Unlam Banjarbaru
                        Kalimantan Selatan <br>
                        Telepon/Fax : (0511) 4772290 <br>
                        surel : prodi.kehutanan@ulm.ac.id</P>
                </div>
            </td>

            <td style="width: 15%;">

            </td>

        </tr>
    </table>
    <hr style="margin-top: .2rem">

    <table class="verdana w-100">
        <tr>
            <td style="text-align: center; padding-top: 2rem; padding-bottom: 1rem ">
                <p style="font-weight: 700; font-size: 20px; text-transform: uppercase"><u>Nilai Ujian Skripsi</u></p>
            </td>
        </tr>
    </table>

    <table class="verdana w-100">
        <tr>
            <td style="width: 24%; vertical-align: top;" class="">
                <span style="">Nama / NIM </span></td>
            <td style="width:  1%; vertical-align: top;" class=""><span style="">:</span></td>
            <td style="width: auto; vertical-align: top; padding-left: .7rem" class="">
                {{ $n->mahasiswa->nama_mahasiswa }} / {{ $n->nim_skripsi }}

            </td>
        </tr>
        <tr>
            <td style="width: 24%; vertical-align: top;" class="">
                <span style="">Program Studi / Minat</span>
            </td>
            <td style="width: 1%; vertical-align: top;" class=""><span style="">:</span></td>
            <td style="width: auto; vertical-align: top; padding-left: .7rem" class="">Kehutanan /
                {{ $n->mahasiswa->minat }}</td>
        </tr>
        <tr>
            <td style="width: 24%; vertical-align: top;" class="">
                <span style="">Hari / Tgl Ujian</span>
            </td>
            <td style="width: 1%; vertical-align: top;" class=" "><span style="">:</span></td>
            <td style="width: auto; vertical-align: top; padding-left: .7rem" class="">
                {{ \Carbon\Carbon::parse($n->us->tgl_skripsi)->isoFormat('dddd / D MMMM Y') }}
            </td>
        </tr>
        <tr>
            <td style="width: 24%; vertical-align: top;">
                <span style="">Judul Skripsi</span>
            </td>
            <td style="width: 1%; vertical-align: top;"><span style="">:</span>
            </td>
            <td style="width: auto; vertical-align: top; padding-left: .7rem" style="word-wrap: break-word">
                {{ $n->us->judul }}
            </td>
        </tr>
    </table>
    {{--  --}}

    <table class="verdana w-100" style="margin-top: 1rem" id="tabelBeborder">
        <thead style="text-align: center;">
            <tr style="width: 100%;">
                <td style="width: 5%;" class="font-weight-bold text-center">NO</td>
                <td style="width: 20%;" class="font-weight-bold text-center">Unsur Penilaian</td>
                <td style="width: 40%;" class="font-weight-bold text-center">Acuan Penilaian</td>
                <td style="width: auto;" class="font-weight-bold text-center">NILAI <br> (>70-100)</td>
                <td style="width: auto;" class="font-weight-bold text-center">BOBOT</td>
                <td style="width: auto;" class="font-weight-bold text-center">BOBOT X <br> NILAI</td>
            </tr>
        </thead>


        <tbody>
            <tr>
                <td class="text-center">1. </td>
                <td>Wawasan di bidang keilmuan</td>
                <td>Pemahaman umum dibidang kehutanan dan keterkaitan bidang penelitian dengan bidang
                    kehutanan</td>
                <td class="text-center">{{ $n->nilai1 }}</td>
                <td class="text-center">0,20</td>
                <td class="text-center">{{ $n->hasil1 }}</td>

            </tr>
            <tr>
                <td class="text-center" rowspan="4">2. </td>
                <td colspan="5">Isi dan penguasan terhadap skripsi</td>
            <tr>
                <td>
                    <table class="verdana w-100 " id="no-border">
                        <td>a.</td>
                        <td>Kajian Teoritis</td>
                    </table>
                </td>
                <td>Adanya relevansi dengan topik yang diteliti dan kemutakhiran dengan pustaka yang
                    diacu</td>
                <td class="text-center">{{ $n->nilai2a }}</td>
                <td class="text-center">0,20</td>
                <td class="text-center">{{ $n->hasil2a }}</td>
            </tr>
            <tr>
                <td>
                    <table class="verdana w-100 " id="no-border">
                        <td>b.</td>
                        <td>Metode Penelitian</td>
                    </table>
                </td>
                <td>Kesesuaian dengan masalah; Ketepatan rancangan; Ketepatan instrument; Ketepatan
                    dan ketajaman analisis</td>
                <td class="text-center">{{ $n->nilai2b }}</td>
                <td class="text-center">0,20</td>
                <td class="text-center">{{ $n->hasil2b }}</td>
            </tr>
            <tr>
                <td>
                    <table class="verdana w-100 " id="no-border">
                        <td>c.</td>
                        <td>Hasil Penelitian</td>
                    </table>
                </td>
                <td>Manfaat dan kontribusi bagi pengembangan ilmu; Kesesuaian dengan tujuan
                    penelitian dan kedalaman pembahasan; dan Keaslian tulisan</td>
                <td class="text-center">{{ $n->nilai2c }}</td>
                <td class="text-center">0,30</td>
                <td class="text-center">{{ $n->hasil2c }}</td>
            </tr>
            </tr>
            <tr>
                <td class="text-center">3. </td>
                <td>Lain - lain</td>
                <td>Kesesuaian pemakaian EYD; Kesesuaian format dengan pedoman penulisan;
                    Ringkasan/abstrak dan penampilan</td>
                <td class="text-center">{{ $n->nilai3 }}</td>
                <td class="text-center">0,20</td>
                <td class="text-center">{{ $n->hasil3 }}</td>
            </tr>
            <tr class="">
                <td colspan="5" class="text-center font-weight-bold py-4">Total Nilai</td>
                <td class="text-center">
                    {{ $n->total_nilai }}
                </td>
            </tr>
        </tbody>
    </table>

    <table class="verdana w-100" style="margin-top: 1.5rem">
        <tr>
            <td style="width: 50%;"></td>
            <td>Banjarbaru,
                {{ \Carbon\Carbon::parse($n->us->tgl_skripsi)->isoFormat('D MMMM Y') }}
            </td>
        </tr>
        <tr>
            <td style="width: 50%;"></td>
            <td>
                @if ($n->us->pembimbing1 == $n->dosen->nip)
                Dosen Pembimbing I,
                @elseif($n->us->pembimbing2 == $n->dosen->nip)
                Dosen Pembimbing II,
                @endif

            </td>
        </tr>

        <tr height="5rem">
            <td style="width: 50%;"></td>
            <td> <br> <br> <br> <br> </td>
        </tr>
        <tr>
            <td style="width: 50%;"></td>
            <td>{{ $n->dosen->nama_dosen }}</td>
        </tr>
        <tr>
            <td style="width: 50%;"></td>
            <td>NIP. {{ $n->dosen->nip }}</td>
        </tr>
    </table>


    <div class="page-break"></div>
    @endforeach


</body>

</html>
