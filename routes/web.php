<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Profil\ProfilController;
//---
use App\Http\Controllers\RedirectController;
//---ADMIN
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\DosenManageController;
use App\Http\Controllers\Admin\HasilPenelitian\HasilPenelitianController;
use App\Http\Controllers\Admin\MahasiswaManageController;
use App\Http\Controllers\Admin\NilaiMutu\AdminNilaiMutuController;
use App\Http\Controllers\Admin\UjianKomprehensif\AdminUjianKomprehensifController;
use App\Http\Controllers\Admin\UjianSkripsi\AdminUjianSkripsiController;
use App\Http\Controllers\Admin\UserManageController;
use App\Http\Controllers\Admin\Usulan\UsulanController;
//---DOSEN
use App\Http\Controllers\Dosen\DosenController;
use App\Http\Controllers\Dosen\NilaiMutu\NilaiMutuController;
use App\Http\Controllers\Dosen\SeminarUsulan\SeminarUsulanPenelitianController;
use App\Http\Controllers\Dosen\SeminarUsulan\PenilaianSeminarUsulanPenelitianController;
use App\Http\Controllers\Dosen\SeminarUsulan\PenilaianPenyanggahSeminarUsulanPenelitianController;
//
use App\Http\Controllers\Dosen\SeminarHasil\SeminarHasilPenelitianController;
use App\Http\Controllers\Dosen\SeminarHasil\PenilaianSeminarHasilPenelitianController;
use App\Http\Controllers\Dosen\SeminarHasil\PenilaianPenyanggahSeminarHasilPenelitianController;
use App\Http\Controllers\Dosen\Ujian\PenilaianUjianSkripsiController;
//
use App\Http\Controllers\Dosen\Ujian\UjianSkripsiController;
use App\Http\Controllers\Dosen\UjianKomprehensif\PenilaianUjianKomprehensifController;
use App\Http\Controllers\Dosen\UjianKomprehensif\UjianKomprehensifController;
use App\Http\Controllers\SuperAdmin\LogAktivitasController;
use App\Http\Controllers\SuperAdmin\SuperAdminAdminController;
use App\Http\Controllers\SuperAdmin\SuperAdminController;
use App\Http\Controllers\SuperAdmin\SuperAdminDosenController;
use App\Http\Controllers\SuperAdmin\SuperAdminMahasiswaController;

Route::get('/', function () {
    return redirect()->route('redirect');
})->middleware('auth');

Route::get('403-forbidden', [RedirectController::class, 'forbidden'])->name('forbidden');

Route::get('redirect', [RedirectController::class, 'redirect'])->middleware('auth')->name('redirect');

Route::middleware('auth')
    ->group(function () {
        Route::get('/profilku', [ProfilController::class, 'index'])->name('myProfile.index');
        // --
        Route::get('/profilku/edit', [ProfilController::class, 'editProfile'])->name('myProfile.edit');
        // --
        Route::put('/profilku/update-admin/{id}', [ProfilController::class, 'updateProfilAdmin'])->name('update.admin');
        // --
        Route::put('/profilku/update-dosen/{id}', [ProfilController::class, 'updateProfileDosen'])->name('update.dosen');
        // --
    });


Route::prefix('super-admin')
    ->namespace('SuperAdmin')
    ->middleware('auth', 'isSuperAdmin')
    ->group(function () {
        Route::get('/', [SuperAdminController::class, 'index'])->name('super-admin.index');

        Route::get('log-aktivitas', [LogAktivitasController::class, 'index'])->name('super-admin.log.index');

        Route::prefix('user-manage')->group(function () {

            Route::prefix('admin')->group(function () {
                Route::get('/', [SuperAdminAdminController::class, 'index'])->name('super-admin.user-manage.admin.index');

                Route::get('super-admin/get-mahasiswa', [SuperAdminAdminController::class, 'ajaxGetAdmin'])->name('super-admin.user-manage.admin.getAdmin');
                Route::get('tambah', [SuperAdminAdminController::class, 'createAdmin'])->name('super-admin.user-manage.admin.create');
                Route::post('tambah/store', [SuperAdminAdminController::class, 'storeAdmin'])->name('super-admin.user-manage.admin.store');
                Route::get('edit/{id}', [SuperAdminAdminController::class, 'editAdmin'])->name('super-admin.user-manage.admin.edit');
                Route::put('edit/update/{id}', [SuperAdminAdminController::class, 'updateAdmin'])->name('super-admin.user-manage.admin.update');
                Route::delete('destroy/{id}', [SuperAdminAdminController::class, 'destroyAdmin'])->name('super-admin.user-manage.admin.destroy');
            });
        });
    });


Route::prefix('admin')
    ->namespace('Admin')
    ->middleware('auth', 'isAdmin')
    ->group(function () {
        Route::get('/', [AdminController::class, 'index'])->name('admin.index');

        Route::prefix('user-manage')->group(function () {
            Route::get('/', [UserManageController::class, 'index'])->name('admin.user-manage.index');
            // ---
            Route::get('dosen', [DosenManageController::class, 'index'])->name('admin.user-manage.dosen.index');
            Route::get('get-dosen', [DosenManageController::class, 'ajaxGetDosen'])->name('ajax.getDosen');
            Route::get('dosen/tambah', [DosenManageController::class, 'createDosen'])->name('admin.user-manage.dosen.create');
            Route::post('dosen/tambah/store', [DosenManageController::class, 'storeDosen'])->name('admin.user-manage.dosen.store');
            Route::get('dosen/upload-data-dosen', [DosenManageController::class, 'addManyDosen'])->name('admin.user-manage.dosen.addMany');
            Route::post('dosen/upload-data-dosen/store', [DosenManageController::class, 'storeManyDosen'])->name('admin.user-manage.dosen.storeMany');
            Route::get('dosen/edit/{id}', [DosenManageController::class, 'editDosen'])->name('admin.user-manage.dosen.edit');
            Route::put('dosen/edit/update/{id}', [DosenManageController::class, 'updateDosen'])->name('admin.user-manage.dosen.update');
            Route::delete('dosen/destroy/{id}', [DosenManageController::class, 'destroyDosen'])->name('admin.user-manage.dosen.destroy');

            //---
            Route::get('mahasiswa', [MahasiswaManageController::class, 'index'])->name('admin.user-manage.mahasiswa.index');
            Route::get('get-mahasiswa', [MahasiswaManageController::class, 'ajaxGetMahasiswa'])->name('ajax.getMahasiswa');
            Route::get('mahasiswa/tambah', [MahasiswaManageController::class, 'createMahasiswa'])->name('admin.user-manage.mahasiswa.create');
            Route::post('mahasiswa/tambah/store', [MahasiswaManageController::class, 'storeMahasiswa'])->name('admin.user-manage.mahasiswa.store');
            Route::get('mahasiswa/upload-data-mahasiswa', [MahasiswaManageController::class, 'addManyMahasiswa'])->name('admin.user-manage.mahasiswa.addMany');
            Route::post('mahasiswa/upload-data-mahasiswa/store', [MahasiswaManageController::class, 'storeManyMahasiswa'])->name('admin.user-manage.mahasiswa.storeMany');
            Route::get('mahasiswa/edit/{id}', [MahasiswaManageController::class, 'editMahasiswa'])->name('admin.user-manage.mahasiswa.edit');
            Route::put('mahasiswa/edit/update/{id}', [MahasiswaManageController::class, 'updateMahasiswa'])->name('admin.user-manage.mahasiswa.update');
            Route::get('mahasiswa/destroy/{id}', [MahasiswaManageController::class, 'destroyMahasiswa'])->name('admin.user-manage.mahasiswa.destroy');
        });

        Route::prefix('usulan-penelitian')->group(function () {
            Route::get('/', [UsulanController::class, 'index'])->name('admin.usulan.index');
            Route::get('sudah-dinilai', [UsulanController::class, 'indexSudahDinilai'])->name('admin.usulan.indexSudahDinilai');

            Route::get('lihat-nilai/{id}', [UsulanController::class, 'show'])->name('admin.usulan.show');

            Route::get('tambah-usulan', [UsulanController::class, 'create'])->name('admin.usulan.create');
            Route::post('get-nilai/fetch', [UsulanController::class, 'fetchNilai'])->name('admin.usulan.fetchNilai');
            Route::post('tambah-usulan/store', [UsulanController::class, 'store'])->name('admin.usulan.store');

            Route::get('cetak/{id}', [UsulanController::class, 'print'])->name('admin.usulan.cetak');
            Route::get('cetak-nilai-usulan/{id}', [UsulanController::class, 'printUsulan'])->name('admin.usulan.cetakNilaiUsulan');
            Route::get('cetak-nilai-penyanggah-usulan/{id}', [UsulanController::class, 'printPenyanggahUsulan'])->name('admin.usulan.cetakNilaiPenyanggahUsulan');

            Route::get('edit-usulan/{id}', [UsulanController::class, 'edit'])->name('admin.usulan.edit');
            Route::put('edit-usulan/update/{id}', [UsulanController::class, 'update'])->name('admin.usulan.update');

            Route::get('tambah-penyanggah/{id}', [UsulanController::class, 'tambahPenyanggah'])->name('admin.usulan.tambahPenyanggah');
            Route::post('tambah-penyanggah/store/{id}', [UsulanController::class, 'storePenyanggah'])->name('admin.usulan.storePenyanggah');
        });

        Route::prefix('hasil-penelitian')->group(function () {
            Route::get('/', [HasilPenelitianController::class, 'index'])->name('admin.hasil.index');
            Route::get('sudah-dinilai', [HasilPenelitianController::class, 'indexSudahDinilai'])->name('admin.hasil.indexSudahDinilai');

            Route::get('lihat-nilai/{id}', [HasilPenelitianController::class, 'show'])->name('admin.hasil.show');

            Route::put('addTanggal/{id}', [HasilPenelitianController::class, 'addTanggal'])->name('admin.hasil.addTanggal');

            Route::get('cetak/{id}', [HasilPenelitianController::class, 'print'])->name('admin.hasil.cetak');
            Route::get('cetak-nilai-hasil/{id}', [HasilPenelitianController::class, 'printHasil'])->name('admin.hasil.cetakNilaiHasil');
            Route::get('cetak-nilai-penyanggah-hasil/{id}', [HasilPenelitianController::class, 'printPenyanggahHasil'])->name('admin.hasil.cetakNilaiPenyanggahHasil');

            Route::get('edit/{id}', [HasilPenelitianController::class, 'edit'])->name('admin.hasil.edit');
            Route::put('edit/update/{id}', [HasilPenelitianController::class, 'update'])->name('admin.hasil.update');

            Route::get('tambah-penyanggah/{id}', [HasilPenelitianController::class, 'tambahPenyanggah'])->name('admin.hasil.tambahPenyanggah');
            Route::post('tambah-penyanggah/store/{id}', [HasilPenelitianController::class, 'storePenyanggah'])->name('admin.hasil.storePenyanggah');
        });

        Route::prefix('ujian-skripsi')->group(function () {
            Route::get('/', [AdminUjianSkripsiController::class, 'index'])->name('admin.ujian-skripsi.index');
            Route::get('sudah-dinilai', [AdminUjianSkripsiController::class, 'indexSudahDinilai'])->name('admin.ujian-skripsi.indexSudahDinilai');

            Route::put('addTanggal/{id}', [AdminUjianSkripsiController::class, 'addTanggal'])->name('admin.ujian-skripsi.addTanggal');

            Route::get('edit-tanggal/{id}', [AdminUjianSkripsiController::class, 'editTanggal'])->name('admin.ujian-skripsi.editTanggal');
            Route::put('edit-tanggal/update/{id}', [AdminUjianSkripsiController::class, 'updateTanggal'])->name('admin.ujian-skripsi.updateTanggal');

            Route::get('lihat-nilai/{id}', [AdminUjianSkripsiController::class, 'show'])->name('admin.ujian-skripsi.show');

            Route::get('cetak/{id}', [AdminUjianSkripsiController::class, 'print'])->name('admin.ujian-skripsi.cetak');
        });

        Route::prefix('ujian-komprehensif')->group(function () {
            Route::get('/', [AdminUjianKomprehensifController::class, 'index'])->name('admin.ujian-kompre.index');
            Route::get('sudah-dinilai', [AdminUjianKomprehensifController::class, 'indexSudahDinilai'])->name('admin.ujian-kompre.indexSudahDinilai');


            Route::put('addTanggal/{id}', [AdminUjianKomprehensifController::class, 'addTanggal'])->name('admin.ujian-kompre.addTanggal');

            Route::get('lihat-nilai/{id}', [AdminUjianKomprehensifController::class, 'show'])->name('admin.ujian-kompre.show');

            Route::post('cetak-berita-acara/{id}', [AdminUjianKomprehensifController::class, 'printBeritaAcara'])->name('admin.ujian-kompre.printBeritaAcara');

            Route::get('cetak-nilai/{id}', [AdminUjianKomprehensifController::class, 'printNilai'])->name('admin.ujian-kompre.printNilai');

            Route::get('tentukan-penguji/{id}', [AdminUjianKomprehensifController::class, 'addPenguji'])->name('admin.ujian-kompre.addPenguji');
            Route::put('tentukan-penguji/store/{id}', [AdminUjianKomprehensifController::class, 'storePenguji'])->name('admin.ujian-kompre.storePenguji');
        });

        Route::prefix('nilai-mutu')->group(function () {
            Route::get('/', [AdminNilaiMutuController::class, 'index'])->name('admin.nilai-mutu.index');
            Route::get('get-nilai', [AdminNilaiMutuController::class, 'ajaxAdminGetNilaiMutu'])->name('ajax.adminGetNilaiMutu');

            Route::post('export', [AdminNilaiMutuController::class, 'export'])->name('admin.nilai-mutu.export');
            //
            Route::get('cetak/nilai-mutu/{id}', [AdminNilaiMutuController::class, 'print'])->name('admin.nilai-mutu.print');
            // ---
            Route::get('edit-nilai-mutu/{id}', [AdminNilaiMutuController::class, 'edit'])->name('admin.nilai-mutu.edit');
            Route::put('edit-nilai-mutu/update/{id}', [AdminNilaiMutuController::class, 'update'])->name('admin.nilai-mutu.update');
            Route::delete('destroy-nilai-mutu/{id}', [AdminNilaiMutuController::class, 'destroy'])->name('admin.nilai-mutu.destroy');
        });
    });


Route::prefix('dosen')
    ->namespace('Dosen')
    ->middleware('auth', 'isDosen')
    ->group(function () {
        Route::get('/', [DosenController::class, 'index'])->name('dosen.index');

        Route::prefix('seminar')->group(function () {
            // ---
            Route::get('usulan-penelitian', [SeminarUsulanPenelitianController::class, 'index'])->name('dosen.seminar-usulan.index');
            Route::get('usulan-penelitian/belum-dinilai', [SeminarUsulanPenelitianController::class, 'indexBelumDinilai'])->name('dosen.seminar-usulan.indexBelumDinilai');

            //---
            Route::get('usulan-penelitian/penilaian/{id}', [PenilaianSeminarUsulanPenelitianController::class, 'show'])->name('dosen.seminar-usulan.penilaian.show');
            Route::get('usulan-penelitian/penilaian/beri-nilai/{id}', [PenilaianSeminarUsulanPenelitianController::class, 'penilaian'])->name('dosen.seminar-usulan.penilaian.create');
            Route::put('usulan-penelitian/penilaian/beri-nilai/update/{id}', [PenilaianSeminarUsulanPenelitianController::class, 'storeNilai'])->name('dosen.seminar-usulan.penilaian.storeNilai');
            //---
            // Route::get('penyanggah-usulan-penelitian/penilaian/{id}', [PenilaianSeminarUsulanPenelitianController::class, 'show'])->name('dosen.seminar-usulan.penilaian.show');
            Route::get('penyanggah-1-usulan-penelitian/penilaian/beri-nilai/{id}', [PenilaianPenyanggahSeminarUsulanPenelitianController::class, 'penilaianPenyanggah1'])->name('dosen.penyanggah-1-seminar-usulan.penilaian.create');
            Route::get('penyanggah-2-usulan-penelitian/penilaian/beri-nilai/{id}', [PenilaianPenyanggahSeminarUsulanPenelitianController::class, 'penilaianPenyanggah2'])->name('dosen.penyanggah-2-seminar-usulan.penilaian.create');
            Route::put('penyanggah-usulan-penelitian/penilaian/beri-nilai/update/{id}', [PenilaianPenyanggahSeminarUsulanPenelitianController::class, 'storeNilai'])->name('dosen.penyanggah-seminar-usulan.penilaian.storeNilai');
            // -----------------------------------------------------------------------------------------------------------------------------
            Route::get('hasil-penelitian', [SeminarHasilPenelitianController::class, 'index'])->name('dosen.seminar-hasil.index');
            Route::get('hasil-penelitian/belum-dinilai', [SeminarHasilPenelitianController::class, 'indexBelumDinilai'])->name('dosen.seminar-hasil.indexBelumDinilai');


            Route::get('hasil-penelitian/edit/{id}', [SeminarHasilPenelitianController::class, 'edit'])->name('dosen.seminar-hasil.edit');
            Route::put('hasil-penelitian/update/{id}', [SeminarHasilPenelitianController::class, 'update'])->name('dosen.seminar-hasil.update');
            //---
            Route::get('hasil-penelitian/penilaian/{id}', [PenilaianSeminarHasilPenelitianController::class, 'show'])->name('dosen.seminar-hasil.penilaian.show');
            Route::get('hasil-penelitian/penilaian/beri-nilai/{id}', [PenilaianSeminarHasilPenelitianController::class, 'penilaian'])->name('dosen.seminar-hasil.penilaian.create');
            Route::put('hasil-penelitian/penilaian/beri-nilai/update/{id}', [PenilaianSeminarHasilPenelitianController::class, 'storeNilai'])->name('dosen.seminar-hasil.penilaian.storeNilai');
            // //---
            // Route::get('penyanggah-hasil-penelitian/penilaian/{id}', [PenilaianSeminarHasilPenelitianController::class, 'show'])->name('dosen.seminar-usulan.penilaian.show');
            Route::get('penyanggah-1-hasil-penelitian/penilaian/beri-nilai/{id}', [PenilaianPenyanggahSeminarHasilPenelitianController::class, 'penilaianPenyanggah1'])->name('dosen.penyanggah-1-seminar-hasil.penilaian.create');
            Route::get('penyanggah-2-hasil-penelitian/penilaian/beri-nilai/{id}', [PenilaianPenyanggahSeminarHasilPenelitianController::class, 'penilaianPenyanggah2'])->name('dosen.penyanggah-2-seminar-hasil.penilaian.create');
            Route::put('penyanggah-hasil-penelitian/penilaian/beri-nilai/update/{id}', [PenilaianPenyanggahSeminarHasilPenelitianController::class, 'storeNilai'])->name('dosen.penyanggah-seminar-hasil.penilaian.storeNilai');
            // -----------------------------------------------------------------------------------------------------------------------------
        });

        Route::prefix('ujian')->group(function () {
            Route::get('ujian-skripsi', [UjianSkripsiController::class, 'index'])->name('dosen.ujian-skripsi.index');
            Route::get('ujian-skripsi/belum-dinilai', [UjianSkripsiController::class, 'indexBelumDinilai'])->name('dosen.ujian-skripsi.indexBelumDinilai');


            Route::get('ujian-skripsi/tambah', [UjianSkripsiController::class, 'create'])->name('dosen.ujian-skripsi.create');
            Route::post('ujian-skripsi/tambah/store', [UjianSkripsiController::class, 'store'])->name('dosen.ujian-skripsi.store');
            Route::get('ujian-skripsi/edit/{id}', [UjianSkripsiController::class, 'edit'])->name('dosen.ujian-skripsi.edit');
            Route::put('ujian-skripsi/update/{id}', [UjianSkripsiController::class, 'update'])->name('dosen.ujian-skripsi.update');
            //---
            Route::get('ujian-skripsi/penilaian/{id}', [PenilaianUjianSkripsiController::class, 'show'])->name('dosen.ujian-skripsi.penilaian.show');
            Route::get('ujian-skripsi/penilaian/beri-nilai/{id}', [PenilaianUjianSkripsiController::class, 'penilaian'])->name('dosen.ujian-skripsi.penilaian.create');
            Route::put('ujian-skripsi/penilaian/beri-nilai/update/{id}', [PenilaianUjianSkripsiController::class, 'storeNilai'])->name('dosen.ujian-skripsi.penilaian.storeNilai');
            // -----------------------------------------------------------------------------------
            Route::get('ujian-komprehensif', [UjianKomprehensifController::class, 'index'])->name('dosen.ujian-kompre.index');
            Route::get('ujian-komprehensif/belum-dinilai', [UjianKomprehensifController::class, 'indexBelumDinilai'])->name('dosen.ujian-kompre.indexBelumDinilai');

            Route::post('cetak-berita-acara/{id}', [UjianKomprehensifController::class, 'printBeritaAcara'])->name('dosen.ujian-kompre.printBeritaAcara');

            //---
            Route::get('ujian-komprehensif/penilaian/{id}', [PenilaianUjianKomprehensifController::class, 'show'])->name('dosen.ujian-kompre.penilaian.show');
            Route::get('ujian-komprehensif/penilaian/beri-nilai/{id}', [PenilaianUjianKomprehensifController::class, 'penilaian'])->name('dosen.ujian-kompre.penilaian.create');
            Route::put('ujian-komprehensif/penilaian/beri-nilai/update/{id}', [PenilaianUjianKomprehensifController::class, 'storeNilai'])->name('dosen.ujian-kompre.penilaian.storeNilai');
        });
    });
